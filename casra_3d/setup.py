"""

Module for compiling the CASRA 3D tool

"""

import sys
import re

try:
    import numpy
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python numpy module. Please make sure it is installed.")

from distutils.core import setup
from distutils.extension import Extension
from distutils.errors import CCompilerError, DistutilsExecError, DistutilsPlatformError
from Cython.Build import cythonize

ext_errors = (CCompilerError, DistutilsExecError, DistutilsPlatformError, IOError, SystemExit)

sys.path.append("")

base_cargs = ['-std=c++14', '-fopenmp', '-O3', '-march=native']
base_largs = ['-std=c++14', '-fopenmp', '-O3', '-march=native']
ext_modules = [
    Extension(
        "electrostatic_solver.cbem.BEM_matrix_construction",
        ["electrostatic_solver/cbem/BEM_matrix_construction.pyx"],
        language="c++",
        extra_compile_args=base_cargs,
        extra_link_args=base_largs,
    )
]

setup(
    name="BEM_matrix_construction",
    ext_modules=cythonize(ext_modules),
    include_dirs=[numpy.get_include()]
)

ext_modules = [
    Extension(
        "ls_operators.extension_velocity.extension_velocity",
        ["ls_operators/extension_velocity/extension_velocity.pyx"],
        extra_compile_args=base_cargs,
        extra_link_args=base_largs,
    )
]

setup(
    name="extension_velocity",
    ext_modules=cythonize(ext_modules),
    include_dirs=[numpy.get_include()]
)

ext_modules = [
    Extension(
        "ls_operators.sdf",
        ["ls_operators/sdf.pyx"],
        language='c++',
        extra_compile_args=base_cargs,
        extra_link_args=base_largs,
    )
]

setup(
    name="sdf",
    ext_modules=cythonize(ext_modules),
    include_dirs=[numpy.get_include()]
)

ext_modules = [
    Extension(
        "core.calculate_hitmap",
        ["core/calculate_hitmap.pyx"],
        language='c++',
        extra_compile_args=base_cargs,
        extra_link_args=base_largs,
    )
]

setup(
    name="calculate_hitmap",
    ext_modules=cythonize(ext_modules),
    include_dirs=[numpy.get_include()]
)

ext_modules = [
    Extension(
        "mesh.non_cgal.mesh_library",
        ["mesh/non_cgal/mesh_library.pyx"],
        language='c++',
        extra_compile_args=base_cargs,
        extra_link_args=base_largs,
    )
]

setup(
    name="mesh_library",
    ext_modules=cythonize(ext_modules),
    include_dirs=[numpy.get_include()]
)

ext_modules = [
    Extension(
        "image_processing.interp",
        ["image_processing/interp.pyx"],
        language='c++',
        extra_compile_args=base_cargs,
        extra_link_args=base_largs,
    )
]

setup(
    name="interp",
    ext_modules=cythonize(ext_modules),
    include_dirs=[numpy.get_include()]
)

ext_modules = [
    Extension(
        "electrostatic_solver.cbem.coctree",
        ["electrostatic_solver/cbem/coctree.pyx"],
        language='c++',
        extra_compile_args=base_cargs,
        extra_link_args=base_largs,
    )
]

setup(
    name="coctree",
    ext_modules=cythonize(ext_modules),
    include_dirs=[numpy.get_include()]
)

#try compilation with cgal flag (required for CGAL<4.0)
haveCGALV5 = False
try:
    f = open('/usr/include/CGAL/version.h')
    cre = re.compile('^#define CGAL_VERSION_NR.*')
    for lines in f:
        if cre.match(lines):
            data = lines.split(" ")
            if len(data) != 3:
                break
            haveCGALV5 = (data[2] >= '1050021000')
            break
except RuntimeError:
    print('Exception loading cgal header')

cgal_compile_args = base_cargs
cgal_link_args = base_largs
cgal_link_args.extend(["-lpthread", "-lmpfr", "-lgmp", "-ltbb", "-ltbbmalloc"])
cgal_libraries = ["gmp"]
if not haveCGALV5:
    cgal_link_args.append('-lCGAL')
    cgal_compile_args.append('-lCGAL')
    cgal_libraries.append("CGAL")

try:
    ext_modules = [
        Extension(
            "mesh.cgal.cgal_linker",
            ["mesh/cgal/cgal_linker.pyx", 'mesh/cgal/cgal_remesh.cpp'],
            language='c++',
            libraries=cgal_libraries,
            extra_compile_args=cgal_compile_args,
            extra_link_args=cgal_link_args,
        )
    ]

    setup(
        name="cgal_linker",
        ext_modules=cythonize(ext_modules),
        include_dirs=[numpy.get_include()]
    )
except ext_errors:
    print('CGAL compile/Link failed')
