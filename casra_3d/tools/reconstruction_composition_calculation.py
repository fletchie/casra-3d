"""

Module for calculating reconstruction composition and density

Author: Charles Fletcher, Daniel Haley
Atom Probe Tomography Group, Department of Materials, University of Oxford
August 2020

"""

import sys
import copy
import os
import sqlite3
import csv
import json

try:
    import numpy as np
except ModuleNotFoundError:
    raise ModuleNotFoundError("cannot find numpy package. " +
                              "Install via apt or pip.")

try:
    import scipy
except ModuleNotFoundError:
    raise ModuleNotFoundError("cannot find scipy package. " +
                              "Install via apt or pip.")

try:
    import sklearn
    import sklearn.neighbors
except ModuleNotFoundError:
    raise ModuleNotFoundError("cannot find scikit-learn package. " +
                              "Install via apt or pip.")

from casra_3d.io import model_loaders as model_loaders
from casra_3d.io import model_writers as model_writers

def calculate_compositions(input_file, output_file, materials_db, bw=1.5e-9,
                           bc=100, dd_output="", detection_efficiency=0.39,
                           simulated=True, phases={}):

    """

    Function for calculating reconstruction compositions and density

    :param1 input_file (str): The input reconstruction (in .vtk format)
    :param2 output_file (str): The output .vtk reconstruction filename
    :param3 materials_db (str): The materials database
    :param4 bw (float): The Kernel Density Estimation (KDE) bandwidth -
                        uses the epanechnikov Kernel
                        (see https://scikit-learn.org/stable/modules/density.html)
    :param5 bc (int): The number of grid cells along a dimension
                      (plan to change to cell width in future version)
    :param6 dd_output (str): The filename to output the density data
    :param7 detection_efficiency (float): The detector efficiency
                                          (for calculating reconstruction density)
    :param8 simulated (bool): Whether the data is simulated (True) or
                              experimental (False)
    :param9 phases (dict): Dictionary with element_id keys and species label
                           values. Overides any species labels found in materials_db.
                           e.g. {"11": "W"}

    :returns:
        None

    :raises:
        FileNotFoundError: No Materials database has been found

    """

    EMPTY = ""

    #check materials db exists
    if os.path.exists(materials_db) == False:
        raise FileNotFoundError("No database can be found at directory: " +
                                str(materials_db) +
                                ". /n Please ensure the passed directory is correct.")

    ###creating output directories
    if len(output_file.split("/")) > 1:
        out_dir = "/".join(output_file.split("/")[:-1])
        if os.path.exists(out_dir) == False:
            print("creating output directory: " + out_dir)
            os.makedirs(out_dir)

    if dd_output is not EMPTY:
        if len(dd_output.split("/")) > 1:
            dd_output_dir = "/".join(dd_output.split("/")[:-1])
            if os.path.exists(dd_output_dir) == False:
                print("creating output density data directory: " + dd_output_dir)
                os.makedirs(dd_output_dir)

    #connect to database to match up element ids to element labels
    conn = sqlite3.connect(materials_db)
    c = conn.cursor()
    el_ids = c.execute('SELECT element_id, element FROM element_ids').fetchall()
    ids = np.array([a[0] for a in el_ids])
    els = np.array([a[1] for a in el_ids], dtype = 'object')
    els = els[np.sort(ids)]
    ids = ids[np.sort(ids)]

    mesh = model_loaders.read_vtk(input_file)

    atoms = np.array(mesh.points)
    atom_properties = mesh.point_data
    uchem = np.unique(mesh.point_data["chemistry"][0])

    x = np.linspace(np.min(atoms[:, 0]),
                    np.max(atoms[:, 0]), bc) * 1e9
    y = np.linspace(np.min(atoms[:, 1]),
                    np.max(atoms[:, 1]), bc) * 1e9
    z = np.linspace(np.min(atoms[:, 2]),
                    np.max(atoms[:, 2]), bc) * 1e9
    XX, XY, XZ = np.meshgrid(x, y, z)

    chem = np.zeros((len(uchem), len(x), len(y), len(z)))
    local_density = []
    
    print("estimate reconstruction compositions")
    for uc in range(len(uchem)):
        catoms = atoms[mesh.point_data["chemistry"][0] == uchem[uc]]

        kde = sklearn.neighbors.KernelDensity(kernel="epanechnikov",
                                              bandwidth=(bw * 1e9)).fit(catoms * 1e9)
        print("score atoms: chem - " + str(uc) + "/" + str(len(uchem) -1))
        ndv = kde.score_samples(np.vstack((XY.flatten(), XX.flatten(), XZ.flatten())).T)
        ndv = ndv.reshape((len(x), len(y), len(z)))
        ndv = np.exp(ndv) * len(catoms)
        chem[uc, :, :, :] = ndv
        function = scipy.interpolate.RegularGridInterpolator((x, y, z), ndv, method='linear')
        atom_scores = function(atoms * 1e9)
        local_density.append(atom_scores)

    local_density = np.array(local_density).T
    norm = np.sum(local_density, axis = 1)

    grid_density = np.sum(chem, axis = 0)

    #append small tolerance to prevent local normalisation by zero
    compositions = local_density/(norm.reshape((len(norm), 1)) + 1e-10)

    #add new KDE composition properties - matching element ids to labels via database
    for uc in range(len(uchem)):
        if simulated == False:
            try:
                plabel = phases[str(int(uchem[uc]))]
                atom_properties[plabel] = compositions[:, uc]
            except:
                try:
                    plabel = phases[str(uchem[uc])]
                    atom_properties[plabel] = compositions[:, uc]
                except:
                    atom_properties[els[int(uchem[uc])]] = compositions[:, uc]
        else:
            try:
                plabel = phases[str(int(uchem[uc]))]
                atom_properties[plabel] = compositions[:, uc]
            except:
                try:
                    plabel = phases[str(uchem[uc])]
                    atom_properties[plabel] = compositions[:, uc]
                except:
                    atom_properties["phase_" + str(int(uchem[uc]))] = compositions[:, uc]

    #save density to properties - rescale densities by dimensions and detector efficiency
    atom_properties["density"] = (norm/((1e-9)**3.0) * 1.0)/detection_efficiency
    grid_density = (grid_density/((1e-9)**3.0) * 1.0)/detection_efficiency

    for p in atom_properties:
        atom_properties[p] = np.array(atom_properties[p]).flatten()

    #output reconstruction
    point_ind = np.linspace(0, len(atoms), len(atoms), endpoint=False).astype(int)
    cells = point_ind.reshape(-1, 1)
    model_writers.write_vtk(output_file, atoms, cells, point_data=atom_properties)

    print("Successfully calculated compositions and density and outputed " +
          "reconstruction to: " + output_file)

    #output the reconstruction filtered boundary density values (for histogram plot)
    if dd_output is not EMPTY:

        #perform simple voxel histogram for calculation of edge voxels
        info = np.histogramdd(np.vstack((atoms[:, 0], atoms[:, 1], atoms[:, 2])).T, bins=bc)
        bin_vals = info[0]
        binx = info[1][0]
        biny = info[1][1]
        binz = info[1][2]

        #filter out boundary density values
        fdensity_value = []
        for a in range(1, len(binx) - 2):
            for b in range(1, len(biny) - 2):
                for c in range(1, len(binz) - 2):
                    if (bin_vals[a, b, c] * bin_vals[a + 1, b, c] * bin_vals[a - 1, b, c]  *
                        bin_vals[a, b + 1, c] * bin_vals[a + 1, b + 1, c] * bin_vals[a - 1, b + 1, c] *
                        bin_vals[a, b - 1, c] * bin_vals[a + 1, b - 1, c] * bin_vals[a - 1, b - 1, c]) > 0:
                        if (bin_vals[a, b, c + 1] * bin_vals[a + 1, b, c + 1] * bin_vals[a - 1, b, c + 1]  *
                            bin_vals[a, b + 1, c + 1] * bin_vals[a + 1, b + 1, c + 1] *
                            bin_vals[a - 1, b + 1, c + 1] * bin_vals[a, b - 1, c + 1] *
                            bin_vals[a + 1, b - 1, c + 1] * bin_vals[a - 1, b - 1, c + 1]) > 0:
                            if (bin_vals[a, b, c - 1] * bin_vals[a + 1, b, c - 1] *
                                bin_vals[a - 1, b, c - 1]  * bin_vals[a, b + 1, c - 1] *
                                bin_vals[a + 1, b + 1, c - 1] * bin_vals[a - 1, b + 1, c - 1] *
                                bin_vals[a, b - 1, c - 1] * bin_vals[a + 1, b - 1, c - 1] *
                                bin_vals[a - 1, b - 1, c - 1]) > 0:
                                fdensity_value.append(grid_density[b, a, c])

        fdensity_value = np.array(fdensity_value)

        #save density values to .csv
        with open(dd_output, mode='w+') as density_data_file:
            density_data_writer = csv.writer(density_data_file, delimiter='\t')
            for a in range(0, len(fdensity_value)):
                density_data_writer.writerow([str(fdensity_value[a])])

#run if script executed via the command line
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--input', metavar='The input reconstruction', type=str,
                        default="", help='The .vtk reconstruction file')
    parser.add_argument('--out', metavar='output reconstruction', type=str, default="",
                        help='The output filename to save the .vtk point cloud')
    parser.add_argument('--dd_output', metavar='The output density data filename', type=str, default="",
                        help='The output density data filename (for plotting the density histogram). ' +
                             'Density values at the reconstruction edges have been filtered out. ' +
                             'Leave blank to not output (optional argument).')
    parser.add_argument('--bandwidth', metavar='The Kernel bandwidth', type=float, default=1.5e-9,
                        help='The bandwidth of the kernel to use for density estimation')
    parser.add_argument('--grid_width', metavar='The grid width for sampling', type=int, default=100,
                        help='The number of grid cells along a dimension. ' +
                             'Plan to change to cell width in a future version.' +
                             'If returning empty file, try a smaller value for grid_width.')
    parser.add_argument('--phases', metavar='Phases', type=str, default='{}',
                        help='Dictionary of phase labels for particular element ids.' +
                             'Overides any labels in material_db.' +
                             'e.g. {"11": "W"}')
    parser.add_argument('--material_db', metavar='The materials database', type=str,
                        default="c3d_input/materials", help='The materials database to connect to')
    parser.add_argument('--detection_efficiency', metavar='The detection efficiency', type=float,
                        default=0.4, help='The detection efficiency')
    parser.add_argument('--simulated', metavar='simulated', type=int,
                        default=1, help='Whether simulated (1) or ' +
                        'real experimental data (0)')

    args = parser.parse_args()
    input_file = args.input
    output_file = args.out
    bw = args.bandwidth
    bc = args.grid_width
    phases = args.phases
    materials_db = args.material_db
    detection_efficiency = args.detection_efficiency
    dd_output = args.dd_output
    simulated = args.simulated

    phases = json.loads(phases)

    EMPTY = ""

    if input_file is EMPTY:
        raise ValueError("input reconstruction .vtk file must be parsed to --input argument")
    if output_file is EMPTY:
        raise ValueError("output reconstruction .vtk filename must be parsed to --out argument")

    calculate_compositions(input_file, output_file, materials_db, bw=bw, bc=bc,
                           dd_output=dd_output, detection_efficiency=detection_efficiency,
                           simulated=simulated, phases=phases)
