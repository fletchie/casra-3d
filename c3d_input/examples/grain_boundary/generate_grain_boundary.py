#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os,sys,inspect
sys.path.insert(0,'')

from casra_3d.geometries import generate_model_geometry as generate_model_geometry

import numpy as np

#simulation parameters - for conversion
#scale = 2.2e-8

###example commmands###
filename = "c3d_input/examples/grain_boundary/grain_boundary.mstate"
directory = "c3d_input/examples/grain_boundary"

#tip geom + grain 1
generate_model_geometry.generate_tip(2.5, 2.0, 13.0, filename, directory, name = "grain1", offset = 0.2)

#grain 2
generate_model_geometry.generate_cuboid(-10.0, 10.0, 0.0, 10.0, -10.0, 20.0, 0.0, 0.0, 0.0, filename, directory, "grain2")
