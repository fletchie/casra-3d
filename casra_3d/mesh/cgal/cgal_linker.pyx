
"""

Wrapper module for performing linking to the CGAL c++ library

Links to isotropic remeshing and alpha hull construction CGAL functions

Author: Charles Fletcher
Atom Probe Tomography Group, Department of Materials, University of Oxford
May 2020

"""

import cython
import numpy as np
cimport numpy as np
from cython.parallel import prange
from libcpp.vector cimport vector

cdef extern from "cgal_remesh.h":
    cdef int test_return(int d)

    cdef struct mesh_container:
      vector[vector[double]] npoints
      vector[vector[long]] nfaces
      vector[long] findices
      vector[long] vindices

    cdef struct tetra_container:
      vector[long] findices
      vector[vector[double]] npoints

    cdef mesh_container iso_remesh(double *v, long *f, int fc, int vf, int pts, int vd, double edge_coll)
    cdef mesh_container sample_remeshing(double *v, long *f, int fc, int vf, int pts, int vd, vector[double] z_regions, vector[double] edge_coll)
    cdef tetra_container alpha_shape(double *points, int pts, int dim, double alpha)

    cdef int build_mesh(double *v, long *f, int fc, int vf, int pts, int vd, double edge_coll)

def test(int d):
    return test_return(d)

def generate_alpha_shape(double[:, ::1] verts, int dim, double alpha):

    """

    Wrapper function for calling the CGAL alpha shape function

    :param1 verts: The mesh vertices
    :param2 dim: The space dimension
    :param3 alpha: The alpha shape alpha value
    :returns:
        :vertices: The alpha shape vertices
        :simplices: The alpha shape tetrahedral simplices
    """

    v = alpha_shape(&verts[0, 0], len(verts), dim, alpha)
    simplices = np.asarray(v.findices).reshape(-1, 4)
    vertices = np.asarray(v.npoints)

    return vertices, simplices

def adaptive_remeshing(double[:, ::1] verts, long[:, ::1] faces, double[::1] z_regions, double[::1] edge_coll, bint void):

    """

    Wrapper function for calling the CGAL isotropic remeshing function

    :param1 verts: The mesh vertices
    :param2 faces: The mesh connectivity matrix
    :param3 z_regions: 1D array of z values between which to remesh with the corresponding value at the same index in edge_coll
    :param4 edge_coll: 1D array of average panel lengths for the remeshing algorithm to aim for
    :param void: Whether a void is present or not (bool)

    :returns:
        :vertices: The alpha shape vertices
        :simplices: The alpha shape tetrahedral simplices
    """

    cdef vector[double] vz_regions
    cdef vector[double] vedge_coll

    for b in range(0, len(z_regions)):
        vz_regions.push_back(z_regions[b])
        vedge_coll.push_back(edge_coll[b])

    # Call isotropic remeshing function
    v = sample_remeshing(&verts[0, 0], &faces[0, 0], len(faces), 3, len(verts), 3, vz_regions, vedge_coll)

    rverts = np.asarray(v.npoints)
    vindices = np.asarray(v.vindices)
    rfaces = np.asarray(v.findices)

    cdef int a

    for a in range(0, len(vindices)):
        rfaces[rfaces == vindices[a]] = a

    rfaces = rfaces.reshape(-1, 3)

    return rverts, rfaces

def isotropic_remesh(double[:, ::1] verts, long[:, ::1] faces, double edge_coll):

    v = iso_remesh(&verts[0, 0], &faces[0, 0], len(faces), 3, len(verts), 3,edge_coll)

    rverts = np.asarray(v.npoints)
    vindices = np.asarray(v.vindices)
    rfaces = np.asarray(v.findices)

    cdef int a

    for a in range(0, len(vindices)):
        rfaces[rfaces == vindices[a]] = a

    rfaces = rfaces.reshape(-1, 3)

    return rverts, rfaces
