
"""

Submodule contains methods for generating piecewise tip geometries
for the level set simulation

Author: Charles Fletcher
Atom Probe Tomography Group, Department of Materials, University of Oxford
May 2020

"""

import os
import sys

try:
    import numpy as np
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python package numpy. " +
                              "Please make sure package is installed.")

try:
    import scipy.spatial
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python package scipy. " +
                              "Please make sure package is installed.")

from casra_3d.io import model_writers as model_writers

def generate_tip(R0, R1, length, filename, out_dir,
                 name="surface", offset=0.0):

    """

    function for generating the tip geometry

    :param1 R0 (double): the base radius
    :param2 R1 (double): the apex radius
    :param3 length (double): the shank length
    :param4 filename (str): The output .mstate filename
    :param5 out_dir (str): The output directory to save the .vtk files
    :param6 name (str): A string defining the phase name
    :param7 offset (double): The spherical apex cap offset (+ve shifts down)

    :returns:
        returncode (int): 0 if successful write to .mstate file

    :raises:
        ValueError: offset to cap must be equal or less than R1

    """

    R = np.linspace(R0, R1, 100)
    Z = np.linspace(R1, R0, 100) * length/abs(R0 - R1)


    if abs(offset) > R1:
        raise ValueError("offset to cap must be equal or less than R1 " +
                         "(maximum cap radius for continuity)")

    points = []

    #construct cone
    for r in range(0, len(R)):
        for ang in np.linspace(0.0, 2.0 * np.pi, endpoint=False):
            x = R[r] * np.cos(ang)
            y = R[r] * np.sin(ang)
            z = Z[r]
            points.append([x, y, z])

    dz = (np.max(Z) - np.min(Z)) * (np.max(R)/np.min(R) - 1)**(-1.0)
    shank_angle = np.arctan2(np.min(R), dz) * 180/np.pi
    print("shank angle: " + str(shank_angle))

    #construct cap
    print("cap join angle: " + str(90.0 - 180.0/np.pi * np.arctan2(R1, offset)))

    for theta in np.linspace(0.0, 2.0 * np.pi, endpoint=False):
        for phi in np.linspace(0.0, np.pi, endpoint=False):

            x = (R1**2.0 + offset**2.0)**0.5 * np.cos(theta) * np.sin(phi)
            y = (R1**2.0 + offset**2.0)**0.5 * np.sin(theta) * np.sin(phi)
            z = (R1**2.0 + offset**2.0)**0.5 * np.cos(phi) + Z[-1] - offset
            if z > Z[-1]:
                points.append([x, y, z])

    verts = np.array(points)
    verts[:, 2] -= np.min(verts[:, 2])

    sim = scipy.spatial.ConvexHull(points, qhull_options='Qbb Qc')
    faces = sim.simplices

    normals = np.cross(verts[faces[:, 2], :] - verts[faces[:, 1], :],
                       verts[faces[:, 1], :] - verts[faces[:, 0], :])
    normals = normals/np.linalg.norm(normals, axis=1).reshape(-1, 1)

    ###reorder simplices to obey outwards orientation - assumes convex hull
    cp = np.mean(verts[faces], axis=1)
    centroid = np.mean(cp, axis=0)

    orientation = np.sum((cp - centroid) * normals, axis=1) < 0

    for a in range(0, len(orientation)):
        if orientation[a] == True:
            tf = faces[a, 2]
            faces[a, 2] = faces[a, 1]
            faces[a, 1] = tf

    normals = np.cross(verts[faces[:, 2], :] - verts[faces[:, 1], :],
                       verts[faces[:, 1], :] - verts[faces[:, 0], :])
    normals = normals/np.linalg.norm(normals, axis=1).reshape(-1, 1)

    if not os.path.exists(out_dir + "/initial_geometry"):
        os.makedirs(out_dir + "/initial_geometry")

    #write generated sample surface
    cells = faces
    cell_data={"normals": normals}
    model_writers.write_vtk(out_dir + "/initial_geometry/sample_surface.vtk",
                            verts, cells, cell_data=cell_data)

    model_writers.write_mesh_to_mstate(name, verts, faces, filename, 1)
    return 0

def list_faces(t):
  t.sort(axis=1)
  n_t, m_t= t.shape 
  f = np.empty((4*n_t, 3) , dtype=int)
  i = 0
  for j in range(4):
    f[i:i+n_t,0:j] = t[:,0:j]
    f[i:i+n_t,j:3] = t[:,j+1:4]
    i=i+n_t
  return f

def extract_unique_triangles(t):
  _, indxs, count  = np.unique(t, axis=0, return_index=True, return_counts=True)
  return t[indxs[count==1]]

def extract_surface(t):
  f=list_faces(t)
  f=extract_unique_triangles(f)
  return f

def generate_shell(c0, R0, R1, filename, phasename, out_dir, res=20, surf=0, alpha = 1.0):
    
    """

    Function for generating a shell shaped phase

    :param1 c0 (np.array, (3), double): the shell centre
    :param2 R (double): the shell radius
    :param3 filename (str): The output .mstate filename
    :param4 out_dir (str): The output directory to save the .vtk files
    :param5 phasename (str): A string defining the phase name
    :param6 res (int): The precipitate resolution
                       (e.g. number of panels defining surface)
    :param7 surf (int): Whether sample surface/front (1) or not (0).
                        Setting to 1 allows for running collapse test
    :returns:
        returncode (int): 0 if successful write to .mstate file

    :raises:
        None

    """
    
    points = []

    for theta in np.linspace(0.0, 2.0 * np.pi, res, endpoint=False):
        for phi in np.linspace(0.0, np.pi/2.0, int(res/2) + 1, endpoint=True)[1:-1]:

            x = R0 * np.cos(theta) * np.sin(phi) + c0[0]
            y = R0 * np.sin(theta) * np.sin(phi) + c0[1]
            z = R0 * np.cos(phi) + c0[2]
            points.append([x, y, z])
        
            x = R1 * np.cos(theta) * np.sin(phi) + c0[0]
            y = R1 * np.sin(theta) * np.sin(phi) + c0[1]
            z = R1 * np.cos(phi) + c0[2]
            points.append([x, y, z])
    
    from casra_3d.mesh.cgal import cgal_linker as cgal_linker
    verts, simplices = cgal_linker.generate_alpha_shape(np.array(points), 3, alpha**2.0)
    
    faces = extract_surface(simplices)
    
    #write precipitate phase mesh to .vtk file
    print(out_dir + "/" + phasename + ".vtk")
    cells = faces
    model_writers.write_vtk(out_dir + "/initial_geometry/" + phasename + ".vtk",
                            verts, cells)

    model_writers.write_mesh_to_mstate(phasename, verts, faces, filename, surf)

def generate_precipitate(c0, R, filename, phasename, out_dir, res=20, surf=0):

    """

    Function for generating a precipitate (sphere) shaped phase

    :param1 c0 (np.array, (3), double): the precipitate centre
    :param2 R (double): the precipitate radius
    :param3 filename (str): The output .mstate filename
    :param4 out_dir (str): The output directory to save the .vtk files
    :param5 phasename (str): A string defining the phase name
    :param6 res (int): The precipitate resolution
                       (e.g. number of panels defining surface)
    :param7 surf (int): Whether sample surface/front (1) or not (0).
                        Setting to 1 allows for running collapse test
    :returns:
        returncode (int): 0 if successful write to .mstate file

    :raises:
        None

    """

    points = []

    for theta in np.linspace(0.0, 2.0 * np.pi, res, endpoint=False):
        for phi in np.linspace(0.0, np.pi, int(res/2) + 1, endpoint=True)[1:-1]:

            x = R * np.cos(theta) * np.sin(phi) + c0[0]
            y = R * np.sin(theta) * np.sin(phi) + c0[1]
            z = R * np.cos(phi) + c0[2]
            points.append([x, y, z])

    verts = np.array(points)
    delaunay = scipy.spatial.Delaunay(points)
    faces = delaunay.convex_hull

    if not os.path.exists(out_dir + "/initial_geometry"):
        os.makedirs(out_dir + "/initial_geometry")

    #write precipitate phase mesh to .vtk file
    print(out_dir + "/" + phasename + ".vtk")
    cells = faces
    model_writers.write_vtk(out_dir + "/initial_geometry/" + phasename + ".vtk",
                            verts, cells)

    model_writers.write_mesh_to_mstate(phasename, verts, faces, filename, surf)

    return 0

def generate_cuboid(xmin, xmax, ymin, ymax, zmin, zmax, alpha,
                    beta, gamma, filename, out_dir, phasename, surf=0):

    """

    function for generating a cuboid shaped phase

    :param1 xmin (double): the minimum x coord of the cuboid
    :param2 xmax (double): the maximum x coord of the cuboid
    :param3 ymin (double): the minimum y coord of the cuboid
    :param5 ymax (double): the maximum y coord of the cuboid
    :param6 zmax (double): the minimum z coord of the cuboid
    :param7 zmax (double): the maximum z coord of the cuboid
    :param8 alpha (double): the yaw rotation angle of the cuboid
    :param9 beta (double): the pitch rotation angle of the cuboid
    :param10 gamma (double): the roll rotation angle of the cuboid
    :param11 filename (str): The output .mstate filename
    :param12 out_dir (str): The output directory to save the .vtk files
    :param13 phasename (str): A string defining the phase name
    :param14 surf (int): Whether sample surface/front (1) or not (0).
                         Setting to 1 allows for running collapse test
    :returns:
        returncode (int): 0 if successful write to .mstate file

    :raises:
        None

    """


    points = np.array([[xmin, ymin, zmin], [xmax, ymin, zmin], [xmin, ymax, zmin],
                       [xmin, ymin, zmax], [xmax, ymax, zmin], [xmax, ymin,zmax],
                       [xmin, ymax, zmax], [xmax, ymax, zmax]])

    verts = np.array(points)
    delaunay = scipy.spatial.Delaunay(points)
    faces = delaunay.convex_hull

    normals = np.cross(verts[faces[:, 2], :] - verts[faces[:, 1], :],
                       verts[faces[:, 1], :] - verts[faces[:, 0], :])
    normals = normals/np.linalg.norm(normals, axis=1).reshape(-1, 1)

    ###reorder simplices to obey outwards orientation - assumes convex hull
    cp = np.mean(verts[faces], axis=1)
    centroid = np.mean(cp, axis=0)

    orientation = np.sum((cp - centroid) * normals, axis =1) < 0

    for a in range(0, len(orientation)):
        if orientation[a] == True:
            tf = faces[a, 2]
            faces[a, 2] = faces[a, 1]
            faces[a, 1] = tf

    ###rotate around centre
    centre = (np.min(cp, axis=0) + np.max(cp, axis=0))/2.0

    verts -= centre

    gamma = gamma * np.pi/180.0
    beta = beta *  np.pi/180.0
    alpha = alpha * np.pi/180.0

    R_roll = np.array([[1, 0, 0], [0.0, np.cos(gamma), -np.sin(gamma)],
                       [0.0, np.sin(gamma), np.cos(gamma)]])
    R_pitch = np.array([[np.cos(beta), 0.0, np.sin(beta)], [0.0, 1.0, 0.0],
                        [-np.sin(beta), 0.0, np.cos(beta)]])
    R_yaw = np.array([[np.cos(alpha), -np.sin(alpha), 0.0],
                      [np.sin(alpha), np.cos(alpha), 0.0],
                      [0.0, 0.0, 1.0]])

    m = np.array([verts[:, 0], verts[:, 1], verts[:, 2]]).T

    mr = np.matmul(R_roll, m.T).T
    mp = np.matmul(R_pitch, mr.T).T
    my = np.matmul(R_yaw, mp.T).T

    verts[:, 0] = my[:, 0]
    verts[:, 1] = my[:, 1]
    verts[:, 2] = my[:, 2]

    verts += centre

    if not os.path.exists(out_dir + "/initial_geometry"):
        os.makedirs(out_dir + "/initial_geometry")

    #write cuboid phase mesh to .vtk
    cells = faces
    model_writers.write_vtk(out_dir + "/initial_geometry/" + phasename + ".vtk",
                            verts, cells)

    model_writers.write_mesh_to_mstate(phasename, verts, faces, filename, surf)

    return 0


def generate_trapezoid(p0, p1, p2, p3, p4, p5, p6, p7, alpha, beta, gamma,
                       filename, out_dir, phasename, surf=0):

    """

    function for generating a trapezoid shaped phase

    :param1 p0 (np.array, (3), double): A 3d np array defining a trapesoid coordinate
    :param2 p1 (np.array, (3), double): A 3d np array defining a trapesoid coordinate
    :param3 p2 (np.array, (3), double): A 3d np array defining a trapesoid coordinate
    :param4 p3 (np.array, (3), double): A 3d np array defining a trapesoid coordinate
    :param5 p4 (np.array, (3), double): A 3d np array defining a trapesoid coordinate
    :param6 p5 (np.array, (3), double): A 3d np array defining a trapesoid coordinate
    :param7 p6 (np.array, (3), double): A 3d np array defining a trapesoid coordinate
    :param8 p7 (np.array, (3), double): A 3d np array defining a trapesoid coordinate
    :param9 p7 (np.array, (3), double): A 3d np array defining a trapesoid coordinate
    :param10 alpha (double): the yaw rotation angle of the trapesoid
    :param11 beta (double): the pitch rotation angle of the trapesoid
    :param12 gamma (double): the roll rotation angle of the trapesoid
    :param13 filename (str): The output .mstate filename
    :param14 out_dir (str): The output directory to save the .vtk files
    :param15 phasename (str): A string defining the phase name
    :param16 surf (int): Whether sample surface/front (1) or not (0).
                         Setting to 1 allows for running collapse test
    :returns:
        returncode (int): 0 if successful write to .mstate file

    :raises:
        None
    """

    points = np.array([p0, p1, p2, p3, p4, p5, p6, p7])

    verts = np.array(points)
    delaunay = scipy.spatial.Delaunay(points)
    faces = delaunay.convex_hull

    normals = np.cross(verts[faces[:, 2], :] - verts[faces[:, 1], :],
                       verts[faces[:, 1], :] - verts[faces[:, 0], :])
    normals = normals/np.linalg.norm(normals, axis=1).reshape(-1, 1)

    ###reorder simplices to obey outwards orientation - assumes convex hull
    cp = np.mean(verts[faces], axis=1)
    centroid = np.mean(cp, axis=0)

    orientation = np.sum((cp - centroid) * normals, axis=1) < 0

    for a in range(0, len(orientation)):
        if orientation[a] == True:
            tf = faces[a, 2]
            faces[a, 2] = faces[a, 1]
            faces[a, 1] = tf

    ###rotate around centre
    centre = (np.min(cp, axis=0) + np.max(cp, axis=0))/2.0

    verts -= centre

    gamma = gamma * np.pi/180.0
    beta = beta *  np.pi/180.0
    alpha = alpha * np.pi/180.0

    R_roll = np.array([[1, 0, 0],
                       [0.0, np.cos(gamma), -np.sin(gamma)],
                       [0.0, np.sin(gamma), np.cos(gamma)]])
    R_pitch = np.array([[np.cos(beta), 0.0, np.sin(beta)],
                        [0.0, 1.0, 0.0],
                        [-np.sin(beta), 0.0, np.cos(beta)]])
    R_yaw = np.array([[np.cos(alpha), -np.sin(alpha), 0.0],
                      [np.sin(alpha), np.cos(alpha), 0.0],
                      [0.0, 0.0, 1.0]])

    m = np.array([verts[:, 0], verts[:, 1], verts[:, 2]]).T

    mr = np.matmul(R_roll, m.T).T
    mp = np.matmul(R_pitch, mr.T).T
    my = np.matmul(R_yaw, mp.T).T

    verts[:, 0] = my[:, 0]
    verts[:, 1] = my[:, 1]
    verts[:, 2] = my[:, 2]

    verts += centre

    if not os.path.exists(out_dir + "/initial_geometry"):
        os.makedirs(out_dir + "/initial_geometry")

    #write trapezoid phase mesh to .vtk
    cells = faces
    model_writers.write_vtk(out_dir + "/initial_geometry/" + phasename + ".vtk",
                            verts, cells)

    model_writers.write_mesh_to_mstate(phasename, verts, faces, filename, surf)

    return 0
