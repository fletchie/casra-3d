#!/bin/bash

start=$(date +'%s')
echo "Run void model pipeline"

if [ ! -d casra_3d ] ; then
	echo " please run script from top level folder of casra repository: could not find casra_3d in current dir."
	exit 1
fi

#generate the void geometry/model
python3 c3d_input/examples/void/generate_void.py 
#simulate the void model evaporaion
python3 -m casra_3d.tip_simulation --mat_db c3d_input/materials --tip_geom c3d_input/examples/void/void.mstate --mref c3d_input/examples/void/void.mref --parameters c3d_input/examples/void/parameters --it 1000 --out c3d_output/examples/void --termination_height 2.0e-7
finish=$(date +'%s')
duration=$((finish-start))
hrs=$(( duration/3600 )); mins=$(( (duration-hrs*3600)/60)); secs=$(( duration-hrs*3600-mins*60 ))
printf 'Time spent: %02d:%02d:%02d\n' $hrs $mins $secs

