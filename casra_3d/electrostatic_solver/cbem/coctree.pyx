
"""

The Cython module for constructing spatial octrees
accelerates domain point field calculations (e.g. ion projection)

Module must be compiled prior to using casra_3d using the following command when in the casra_3d directory:
`python3 setup.py build_ext --inplace`

Author: Charles Fletcher
Atom Probe Tomography Group, Department of Materials, University of Oxford
May 2020

"""

import cython
import numpy as np
cimport numpy as np

from cython.parallel import prange
from libcpp.vector cimport vector

from casra_3d.electrostatic_solver.cbem.BEM_matrix_construction cimport cfield_calculation as field_calculation

#external function
cdef extern from "math.h" nogil:
    double sqrt(double m)

cdef extern from "math.h" nogil:
    double atan2(double a, double b)

cdef extern from "math.h" nogil:
    double fabs(double v)

cdef extern from "math.h" nogil:
    double log(double v)

#initialise integration variables
cdef double t = 1.0/3.0
cdef double s = (1.0 - sqrt(15.0))/7.0
cdef double r = (1.0 + sqrt(15.0))/7.0
cdef double w1 = 9.0/40.0
cdef double w2 = (155.0 + sqrt(15.0))/1200.0
cdef double w3 = (155.0 - sqrt(15.0))/1200.0
cdef double int1 = t + 2.0 * t * s
cdef double int2 = t - t * s
cdef double int3 = t + 2.0 * t * r
cdef double int4 = t - t * r

#class for defining an octree node object
cdef class octreenode:

    """Class defining octreenode object instance (i.e. the cells/panel clusters in the octree)"""

    cdef double min_coords_x
    cdef double min_coords_y
    cdef double min_coords_z

    cdef double max_coords_x
    cdef double max_coords_y
    cdef double max_coords_z

    cdef double centre_x
    cdef double centre_y
    cdef double centre_z

    cdef double radius
    cdef octreenode parent

    cdef bint top

    cdef bint leaf
    cdef bint empty

    cdef int level
    cdef list children
    cdef int panels

    cdef vector[long] faces

    #Currently only two moments of the multipole expansion have been implemented
    cdef double zeroth_moment_K
    cdef double first_moment_Kx
    cdef double first_moment_Ky
    cdef double first_moment_Kz

    #initialise the octree node
    def __init__(self, double[::1] min_coords, double[::1] max_coords, double[:, ::1] verts, long[:, ::1] faces, double[:, ::1] cp, bint top, octreenode par):

        """

        Method for initialising a coctree node instance

        :param1 min_coords: 1D array of minimum x,y,z coordinates for an octree cell
        :param2 max_coords: 1D array of maximum x,y,z coordinates for an octree cell
        :param3 verts: 2D array of panel vertices
        :param4 faces: 2D array of panel connectivity matrix
        :param5 cp: 2D array of panel centre points
        :param6 top: Whether node is at the top of octree or not
        :param7 par: pointer to the parent node (if applicable)

        :returns: 0

        """

        self.min_coords_x = min_coords[0]
        self.min_coords_y = min_coords[1]
        self.min_coords_z = min_coords[2]

        self.max_coords_x = max_coords[0]
        self.max_coords_y = max_coords[1]
        self.max_coords_z = max_coords[2]

        self.children = []

        #these are related to the panel clustering algorithm (And therefore depend on panel edge vertices, not just collocation points)
        self.top = top

        if top == 0:
            self.parent = par

        self.inherit(verts, faces, cp)

        self.level = 0

        if self.panels > 0:
            self.cluster_parameters(verts, faces, cp)

        if self.panels > 3:
            self.split(verts, faces, cp)
        else:
            self.leaf = True

        if self.panels == 0:
            self.empty = True

    cpdef inherit(self, double[:, ::1] verts, long[:, ::1]  faces, double[:, ::1] cp):

        """

        Method for calculating panel ownership from parent node

        :param1 verts: 2D array of panel vertices
        :param2 faces: 2D array of panel connectivity matrix
        :param3 cp: 2D array of panel centre points
        :returns: 0

        """

        cdef int i
        cdef int panel_count = 0

        if self.top == 1:
            pcp = np.linspace(0, len(faces), len(faces), dtype = int, endpoint = False)
            self.panels = len(pcp)
            self.faces = np.array(pcp)

        else:
            for i in range(0, self.parent.panels):
                if (cp[self.parent.faces[i], 0] > self.min_coords_x) and (cp[self.parent.faces[i], 0] < self.max_coords_x) and (cp[self.parent.faces[i], 1] > self.min_coords_y) and (cp[self.parent.faces[i], 1] < self.max_coords_y) and (cp[self.parent.faces[i], 2] > self.min_coords_z) and (cp[self.parent.faces[i], 2] < self.max_coords_z):
                    self.faces.push_back(self.parent.faces[i])
                    panel_count += 1
            self.panels = panel_count

            self.level = self.parent.level + 1

    cpdef cluster_parameters(self, double[:, ::1] verts, long[:, ::1] faces, double[:, ::1] cp):

        """

        Method for calculating cluster/node centre of mass and radius

        :param1 verts: 2D array of panel vertices
        :param2 faces: 2D array of panel connectivity matrix
        :param3 cp: 2D array of panel centre points
        :returns: 0

        """

        tfaces_ind = np.array(self.faces)
        afaces = np.asarray(faces)
        averts = np.asarray(verts)
        tverts = averts[np.unique(afaces[tfaces_ind])]
        centre = np.mean(tverts, axis = 0)
        self.radius = max(np.linalg.norm(tverts - centre, axis = 1))
        self.centre_x, self.centre_y, self.centre_z = centre

    cpdef calculate_moments(self, double[:, ::1] verts, long[:, ::1] faces, double[:, ::1] cp, double[::1] areas, double[:, ::1] norms, double[::1] BP, double[::1] BF):

        """

        Method called recursively (via a downward pass of the tree) for calculating the expansion moments

        :param1 verts: 2D array of panel vertices
        :param2 faces: 2D array of panel connectivity matrix
        :param3 cp: 2D array of panel centre points
        :param3 areas: 1D array of panel areas
        :param3 norms: 2D array of panel normals
        :param3 BP: 1D array of panel potentials
        :param3 BF: 1D array of panel normal fluxes

        :returns: 0

        """

        self.zeroth_moment_K = K_zeroth_moment_calculate(verts, self.faces, BP, BF, areas, self.centre_x, self.centre_y, self.centre_z, norms, self.radius)
        self.first_moment_Kx, self.first_moment_Ky, self.first_moment_Kz = K_first_moment_calculate(verts, faces, self.faces, BP, BF, areas, self.centre_x, self.centre_y, self.centre_z, norms, self.radius)

        for child in self.children:
            (<octreenode> child).calculate_moments(verts, faces, cp, areas, norms, BP, BF)

    cpdef split(self, double[:, ::1] verts, long[:, ::1] faces, double[:, ::1] cp):

        """

        Method for spliting an octree node into children nodes

        :param1 verts: 2D array of panel vertices
        :param2 faces: 2D array of panel connectivity matrix
        :param3 cp: 2D array of panel centre points

        :returns: 0

        """

        #split along x axis
        cdef double npx = (self.max_coords_x + self.min_coords_x)/2.0
        cdef double npy = (self.max_coords_y + self.min_coords_y)/2.0
        cdef double npz = (self.max_coords_z + self.min_coords_z)/2.0

        c0_min_coords = np.array([self.min_coords_x, self.min_coords_y, self.min_coords_z])
        c0_max_coords = np.array([npx, npy, npz])

        c1_min_coords = np.array([npx, self.min_coords_y, self.min_coords_z])
        c1_max_coords = np.array([self.max_coords_x, npy, npz])

        c2_min_coords = np.array([self.min_coords_x, npy, self.min_coords_z])
        c2_max_coords = np.array([npx, self.max_coords_y, npz])

        c3_min_coords = np.array([self.min_coords_x, self.min_coords_y, npz])
        c3_max_coords = np.array([npx, npy, self.max_coords_z])

        c4_min_coords = np.array([npx, npy, self.min_coords_z])
        c4_max_coords = np.array([self.max_coords_x, self.max_coords_y, npz])

        c5_min_coords = np.array([npx, self.min_coords_y, npz])
        c5_max_coords = np.array([self.max_coords_x, npy, self.max_coords_z])

        c6_min_coords = np.array([self.min_coords_x, npy, npz])
        c6_max_coords = np.array([npx, self.max_coords_y, self.max_coords_z])

        c7_min_coords = np.array([npx, npy, npz])
        c7_max_coords = np.array([self.max_coords_x, self.max_coords_y, self.max_coords_z])

        c0_child = octreenode(c0_min_coords, c0_max_coords, verts, faces, cp, 0, self)
        c1_child = octreenode(c1_min_coords, c1_max_coords, verts, faces, cp, 0, self)
        c2_child = octreenode(c2_min_coords, c2_max_coords, verts, faces, cp, 0, self)
        c3_child = octreenode(c3_min_coords, c3_max_coords, verts, faces, cp, 0, self)
        c4_child = octreenode(c4_min_coords, c4_max_coords, verts, faces, cp, 0, self)
        c5_child = octreenode(c5_min_coords, c5_max_coords, verts, faces, cp, 0, self)
        c6_child = octreenode(c6_min_coords, c6_max_coords, verts, faces, cp, 0, self)
        c7_child = octreenode(c7_min_coords, c7_max_coords, verts, faces, cp, 0, self)

        if c0_child.empty == False:
            self.children.append(c0_child)
        if c1_child.empty == False:
            self.children.append(c1_child)
        if c2_child.empty == False:
            self.children.append(c2_child)
        if c3_child.empty == False:
            self.children.append(c3_child)
        if c4_child.empty == False:
            self.children.append(c4_child)
        if c5_child.empty == False:
            self.children.append(c5_child)
        if c6_child.empty == False:
            self.children.append(c6_child)
        if c7_child.empty == False:
            self.children.append(c7_child)

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.nonecheck(False)
    @cython.cdivision(True)
    cpdef (double, double, double) calculate_field(self, double[::1] x, double[:, ::1] verts, long[:, ::1] faces, double[:, ::1] cp, double[::1] areas, double[:, ::1] norms, double[::1] BP, double[::1] BF, double eps, double[::1] ls2, int order):

        """

        Method called on node at top of tree to calculate the electric field at domain point x

        H moments need not be evaluated for conductor approximation of tip

        :param1 x: The domain point at which the field is to be calculated
        :param2 verts: 2D array of panel vertices
        :param3 faces: 2D array of panel connectivity matrix
        :param4 cp: 2D array of panel centre points
        :param5 areas: 1D array of panel areas
        :param6 norms: 2D array of panel normals
        :param7 BP: 1D array of panel potentials
        :param8 BF: 1D array of panel normal fluxes
        :param9 eps: The adaptive integration tolerance for the near field integration
        :param10 ls2: 1D array of maximum length of the edges of triangle elements/panels
        :param11 order: The number of expansion terms to include (either 1 term (order = 0) or two terms (order = 1))

        :returns:
            :Ex: The electric field x component at point x
            :Ey: The electric field y component at point x
            :Ez: The electric field z component at point x

        """

        cdef double pot = 1.0/sqrt((x[0] - self.centre_x)*(x[0] - self.centre_x) + (x[1] - self.centre_y)*(x[1] - self.centre_y) + (x[2] - self.centre_z)*(x[2] - self.centre_z))
        cdef double pot3 = pot * pot * pot
        cdef double tol = 1.0e-3
        cdef double pot5 = pot3 * pot * pot

        cdef double field[3]
        field[0] = 0.0
        field[1] = 0.0
        field[2] = 0.0

        cdef (double, double, double) tfield
        cdef double prod_val

        #if admissible
        if pot * self.radius < eps:

            #deduct K
            field[0] -= self.zeroth_moment_K * pot3 * (x[0] - self.centre_x)
            field[1] -= self.zeroth_moment_K * pot3 * (x[1] - self.centre_y)
            field[2] -= self.zeroth_moment_K * pot3 * (x[2] - self.centre_z)
            if order > 0:
                field[0] += self.first_moment_Kx * pot3
                field[1] += self.first_moment_Ky * pot3
                field[2] += self.first_moment_Kz * pot3

                prod_val = self.first_moment_Kx * (x[0] - self.centre_x) + self.first_moment_Ky * (x[1] - self.centre_y) + self.first_moment_Kz * (x[2] - self.centre_z)

                field[0] -= 3.0 * pot5 * prod_val * (x[0] - self.centre_x)
                field[1] -= 3.0 * pot5 * prod_val * (x[1] - self.centre_y)
                field[2] -= 3.0 * pot5 * prod_val * (x[2] - self.centre_z)


        elif self.faces.size() > 10: #if not admissible and less than 10 elements
            for child in self.children:
                tfield = (<octreenode>child).calculate_field(x, verts, faces, cp, areas, norms, BP, BF, eps, ls2, order)
                field[0] += tfield[0]
                field[1] += tfield[1]
                field[2] += tfield[2]

        else: #otherwise pass to children
            tfield = field_calculation(x, verts, faces, self.faces, cp, areas, norms, BP, BF, ls2, self.faces.size(), tol)
            field[0] += tfield[0]
            field[1] += tfield[1]
            field[2] += tfield[2]

        return field[0], field[1], field[2]

def construct_octree(verts, faces, cp):

    """

    Function called from Python for constructing the octree

    :param1 verts: 2D array of panel vertices
    :param2 faces: 2D array of panel connectivity matrix
    :param3 cp: 2D array of panel centre points

    :returns:
        :root: The octree root node (top of tree)

    """

    min_coords = np.min(verts, axis = 0)
    max_coords = np.max(verts, axis = 0)
    root = octreenode(min_coords, max_coords, verts, faces, cp, 1, None)
    return root

def return_zeroth_moment(octreenode node):

    """

    Function for returning zeroth K moment for particular octreenode node

    :param1 node: octreenode

    :returns:
        :zeroth_moment_K: zeroth moment of octreenode node

    """

    return node.zeroth_moment_K

def return_first_moment(octreenode node):

    """

    Function for returning first-order K moment vector for particular octreenode node

    :param1 node: octreenode

    :returns:
        :first_moment_Kx: first moment x component of octreenode node
        :first_moment_Ky: first moment y component of octreenode node
        :first_moment_Kz: first moment z component of octreenode node

    """

    return node.first_moment_Kx, node.first_moment_Ky, node.first_moment_Kz

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cpdef K_zeroth_moment_calculate(double[:, ::1] verts, vector[long] faces, double[::1] BP, double[::1] BF, double[::1] areas, double centre_x, double centre_y, double centre_z, double[:, ::1] norms, double radius):

    """

    Function for calculating the zeroth K moment

    :param1 verts: 2D array of panel vertices
    :param1 faces: vector for indices of panels within the cluster
    :param1 BP: 1D array of panel potentials
    :param1 BF: 1D array of panel normal fluxes
    :param1 areas: 1D array of panel areas
    :param1 centre_x: Centre x component of cluster
    :param1 centre_y: Centre y component of cluster
    :param1 centre_z: Centre z component of cluster
    :param1 norms: 2D array of panel normals
    :param1 radius: radius of cluster

    :returns:
        :zeroth_moment_K: zeroth moment of cluster

    """

    cdef Py_ssize_t i
    cdef double zeroth_moment_K = 0.0

    for i in range(faces.size()):
        zeroth_moment_K -= areas[faces[i]] * BF[faces[i]]

    return zeroth_moment_K/(4.0 * 3.14159265359)

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cpdef K_first_moment_calculate(double[:, ::1] verts, long[:, ::1] faces, vector[long] cluster_faces, double[::1] u, double[::1] qc, double[::1] areas, double centre_x, double centre_y, double centre_z, double[:, ::1] norms, double radius):

    """

    Function for calculating the first-order K moment vector

    :param1 verts: 2D array of panel vertices
    :param1 faces: vector for indices of panels within the cluster
    :param1 BP: 1D array of panel potentials
    :param1 BF: 1D array of panel normal fluxes
    :param1 areas: 1D array of panel areas
    :param1 centre_x: Centre x component of cluster
    :param1 centre_y: Centre y component of cluster
    :param1 centre_z: Centre z component of cluster
    :param1 norms: 2D array of panel normals
    :param1 radius: radius of cluster

    :returns:
        :first_order_moment_Kx: first-order x component K moment of cluster
        :first_order_moment_Ky: first-order y component K moment of cluster
        :first_order_moment_Kz: first-order z component K moment of cluster

    """

    cdef Py_ssize_t i
    cdef double first_order_moment_Kx = 0.0
    cdef double first_order_moment_Ky = 0.0
    cdef double first_order_moment_Kz = 0.0
    cdef (double, double, double) int_val
    cdef long face

    for i in range(cluster_faces.size()):
        face = cluster_faces[i]
        int_val = first_k(centre_x, centre_y, centre_z, verts[faces[face, 0], 0], verts[faces[face, 0], 1], verts[faces[face, 0], 2], verts[faces[face, 1], 0], verts[faces[face, 1], 1], verts[faces[face, 1], 2], verts[faces[face, 2], 0], verts[faces[face, 2], 1], verts[faces[face, 2], 2])
        first_order_moment_Kx += areas[face] * qc[face] * int_val[0]
        first_order_moment_Ky += areas[face] * qc[face] * int_val[1]
        first_order_moment_Kz += areas[face] * qc[face] * int_val[2]

    return first_order_moment_Kx/(4.0 * 3.14159265359),first_order_moment_Ky/(4.0 * 3.14159265359), first_order_moment_Kz/(4.0 * 3.14159265359)


@cython.cdivision(True)
cdef (double, double, double) first_k(double ycl0, double ycl1, double ycl2, double ya0, double ya1, double ya2, double yb0, double yb1, double yb2, double yc0, double yc1, double yc2) nogil:

    """
    degree 5 gaussian Cubature
    source: https://arxiv.org/pdf/1606.03743.pdf

    Function for calculating the numerical solution to the first-order K matrix components

    :param 1 ycl0: x coordinate of cluster centre
    :param 2 ycl1: y coordinate of cluster centre
    :param 3 ycl2: z coordinate of cluster centre
    :param 4 ya0: x coordinate of triangle element corner 1
    :param 5 ya1: y coordinate of triangle element corner 1
    :param 6 ya2: z coordinate of triangle element corner 1
    :param 7 yb0: x coordinate of triangle element corner 2
    :param 8 yb1: y coordinate of triangle element corner 2
    :param 9 yb2: z coordinate of triangle element corner 2
    :param 10 yc0: x coordinate of triangle element corner 3
    :param 11 yc1: y coordinate of triangle element corner 3
    :param 12 yc2: z coordinate of triangle element corner 3

    :returns:
        :sol: 3 tuple of first-order K matrix components

    """

    cdef double k1x, k1y, k1z
    cdef (double, double, double) sol

    #a = (ya + yb + yc) * t
    a0 = (ya0 + yb0 + yc0) * t
    a1 = (ya1 + yb1 + yc1) * t
    a2 = (ya2 + yb2 + yc2) * t

    #b = ya * (t + 2.0 * t * s) + (yb + yc) * (t - t * s)
    b0 = ya0 * int1 + (yb0 + yc0) * int2
    b1 = ya1 * int1 + (yb1 + yc1) * int2
    b2 = ya2 * int1 + (yb2 + yc2) * int2

    #c = yb * (t + 2.0 * t * s) + (ya + yb) * (t - t * s)
    c0 = yb0 * int1 + (ya0 + yc0) * int2
    c1 = yb1 * int1 + (ya1 + yc1) * int2
    c2 = yb2 * int1 + (ya2 + yc2) * int2

    #d = yc * (t + 2.0 * t * s) + (yb + ya) * (t - t * s)
    d0 = yc0 * int1 + (yb0 + ya0) * int2
    d1 = yc1 * int1 + (yb1 + ya1) * int2
    d2 = yc2 * int1 + (yb2 + ya2) * int2

    #e = ya * (t + 2.0 * t * r) + (yb + yc) * (t - t * r)
    e0 = ya0 * int3 + (yb0 + yc0) * int4
    e1 = ya1 * int3 + (yb1 + yc1) * int4
    e2 = ya2 * int3 + (yb2 + yc2) * int4

    #f = yb * (t + 2.0 * t * r) + (ya + yc) * (t - t * r)
    f0 = yb0 * int3 + (ya0 + yc0) * int4
    f1 = yb1 * int3 + (ya1 + yc1) * int4
    f2 = yb2 * int3 + (ya2 + yc2) * int4

    #g = yc * (t + 2.0 * t * r) + (yb + ya) * (t - t * r)
    g0 = yc0 * int3 + (yb0 + ya0) * int4
    g1 = yc1 * int3 + (yb1 + ya1) * int4
    g2 = yc2 * int3 + (yb2 + ya2) * int4

    k1x  = (a0 - ycl0) * w1
    k1x += (b0 - ycl0) * w2
    k1x += (c0 - ycl0) * w2
    k1x += (d0 - ycl0) * w2
    k1x += (e0 - ycl0) * w3
    k1x += (f0 - ycl0) * w3
    k1x += (g0 - ycl0) * w3

    k1y  = (a1 - ycl1) * w1
    k1y += (b1 - ycl1) * w2
    k1y += (c1 - ycl1) * w2
    k1y += (d1 - ycl1) * w2
    k1y += (e1 - ycl1) * w3
    k1y += (f1 - ycl1) * w3
    k1y += (g1 - ycl1) * w3

    k1z  = (a2 - ycl2) * w1
    k1z += (b2 - ycl2) * w2
    k1z += (c2 - ycl2) * w2
    k1z += (d2 - ycl2) * w2
    k1z += (e2 - ycl2) * w3
    k1z += (f2 - ycl2) * w3
    k1z += (g2 - ycl2) * w3

    sol = (k1x, k1y, k1z)

    return sol
