
"""

Module for running level set field iterators (e.g. model evolution under field evaporation)
and ion projection methods

Author: Charles Fletcher
Atom Probe Tomography Group, Department of Materials, University of Oxford
May 2020
"""

import copy

from casra_3d.core import main as main
from casra_3d.core import ion_projection as ion_projection

try:
    import numpy as np
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python package numpy." +
                              " Please make sure it has been installed.")

def run_model(geom, its, a, surface,
              qc=None, adaptive=True,
              voltage_ramping=True,
              terminate_height=-10.0):

    """

    Default function for iterating the level set model.

    :param1 geom (dict): The simulation dictionary
    :param2 its (int): The maximum number of model iterations to execute
    :param3 a (int): The initial iteration number - if starting from non-zero iteration.
    :param4 surface (dict): The simulation output
                            dictionary {"verts": [],
                                        "faces": [],
                                        "cp": [],
                                        "phase": [],
                                        "areas": [],
                                        "normals": [],
                                        "pot": [],
                                        "flux": [],
                                        "smflux": [],
                                        "time": [],
                                        "sample_surface": [],
                                        "smoothed_velocity": [],
                                        "volume": [],
                                        "voltage": []}
    :param5 adaptive (bool): Whether to perform adaptive remeshing of the sample
                             surface mesh using CGAL
    :param6 voltage_ramping (bool): Whether to perform voltage rescaling or apply a
                                    constant tip voltage
    :param7 terminate_height (float): The maximum apex z height after which to terminate
                                      the model evolution

    :returns:
        :field (np.array, (L1,L2,L3), dtype=float): The final level set field
        :it (int): The final iteration number at termination

    :raises:
        None

    """

    #copy level set field from simulation geometry object
    field = np.copy(geom["field"])
    time = 0.0
    init_gap = 20
    qc = np.array([0.0])
    for it in range(a, its):
        (field, dt, u, qc, cp, areasc, normsc,
         verts, faces, phase, smflux, sample_surface,
         smoothed_velocity, volume, voltage, max_z, temperature,
         iteration_stability) = main.field_iteration_cgal(field, it, geom, qc,
                                                          adaptive=adaptive,
                                                          voltage_ramping=voltage_ramping)

        surface["verts"].append(verts)
        surface["faces"].append(faces)
        surface["cp"].append(cp)
        surface["areas"].append(areasc)
        surface["normals"].append(normsc)
        surface["pot"].append(u)
        surface["flux"].append(qc)
        surface["phase"].append(phase)
        surface["smflux"].append(smflux)
        surface["sample_surface"].append(sample_surface)
        surface["smoothed_velocity"].append(smoothed_velocity)
        surface["volume"].append(volume)
        surface["time"].append(time)
        surface["voltage"].append(voltage)
        surface["temperature"].append(temperature)
        surface["iteration_stability"].append(iteration_stability)
        time += dt

        if it == a:
            init_gap = field.shape[2] - max_z

        #reduce array size of field if possible - can only reduce along z axis for
        #fixed shank geometry due to no base evaporation
        if geom["simulation_grid"]["inactive"] != 0:
            if (field.shape[2] - max_z) > (init_gap + 5):
                nf = np.zeros((field.shape[0], field.shape[1], field.shape[2] - 5))
                nf[:, :, :] = field[:, :, :-5]
                field = copy.deepcopy(nf)
                print("shrink level set field")

        #terminate simulation if sample volume only 5% of original volume or
        #termination height condition reached
        mh = np.max(cp[:, 2])
        if volume/surface["volume"][0] < 0.05 or mh < terminate_height:
            break

    return field, it

def run_projection(geom, detector_coordinate, rel_sim_tol, abs_sim_tol,
                   projection, stability_info, surface, a, its, mapping_frac,
                   BEM_adist=10.0**2.0, para=True, real_detector=False):

    """

    Function for performing ion projection (fixed ion number per element).
    Called from the casra_3d.tip_simulation submodule.

    :param1 geom (dict): The simulation dictionary
    :param2 detector_coordinate (float): The z coordinate at which to position the 2D detector
    :param3 rel_sim_tol (float): The relative tolerance for the RK5 trajectory integrator
    :param4 abs_sim_tol (float): The absolute tolerance for the RK5 trajectory integrator
    :param5 projection (dict): The ion projection dictionary {"num": [],
                                                              "sample_space": [],
                                                              "detector_space": [],
                                                              "chemistry": [],
                                                              "trajectories": [],
                                                              "panel_index": []}
    :param6 surface (dict): The simulation output
                           dictionary {"verts": [],
                                       "faces": [],
                                       "cp": [],
                                       "phase": [],
                                       "areas": [],
                                       "normals": [],
                                       "pot": [],
                                       "flux": [],
                                       "smflux": [],
                                       "time": [],
                                       "sample_surface": [],
                                       "smoothed_velocity": [],
                                       "volume": [],
                                       "voltage": []}
    :param7 a (int): The initial iteration number - if starting from non-zero iteration.
    :param8 its (int): The maximum number of model iterations to execute
    :param9 mapping_frac (int): The fraction of model iterations between ion projection
    :param10 BEM_adist (float): The approximation distance (admissability condition)
                                for switching from 7-point to single-point panel integration
    :param11 para (bool): Whether to parallelise the surface projection of ions via the
                          python3 multiprocessing module

    :returns:
        None

    :raises:
        None

    """


    ###ion projection step
    BEM_adist = BEM_adist * geom["simulation_grid"]["cell_width"]**2.0

    for it in np.arange(a, its, mapping_frac, dtype = int):

        verts = surface["verts"][it].astype(np.float64)
        faces = surface["faces"][it].astype(np.int64)
        areasc = surface["areas"][it].astype(np.float64)
        normsc = surface["normals"][it].astype(np.float64)
        u = surface["pot"][it].astype(np.float64)
        qc = surface["flux"][it].astype(np.float64)
        sample_surface = surface["sample_surface"][it]
        #smoothed_velocity = surface["smoothed_velocity"][it]

        if it - mapping_frac >= 0:
            verts2 = surface["verts"][it - mapping_frac].astype(np.float64)
            faces2 = surface["faces"][it - mapping_frac].astype(np.int64)
            sample_surface2 = surface["sample_surface"][it - mapping_frac]
        else:
            verts2 = np.array([])
            faces2 = np.array([])
            sample_surface2 = np.array([])


        if para == True:
            (sample_space,
             detector_space,
             vel,
             chemistry,
             ion_phase,
             trajectories,
             panel_index,
             stability) = ion_projection.derive_mapping(geom, detector_coordinate,
                                                        rel_sim_tol, abs_sim_tol,
                                                        areasc, normsc, u, qc,
                                                        verts, faces,
                                                        sample_surface,
                                                        verts2, faces2,
                                                        sample_surface2,
                                                        BEM_adist, real_detector)
        else:
            (sample_space,
             detector_space,
             chemistry,
             ion_phase,
             trajectories,
             panel_index) = ion_projection.non_para_derive_mapping(geom, detector_coordinate,
                                                                   rel_sim_tol, abs_sim_tol,
                                                                   areasc, normsc, u, qc,
                                                                   verts, faces,
                                                                   sample_surface, BEM_adist, real_detector)

        #append ion projection data to dictionary for output
        projection["num"].append(it)
        projection["sample_space"].append(sample_space)
        projection["detector_space"].append(detector_space)
        projection["vel"].append(vel)
        projection["chemistry"].append(chemistry)
        projection["ion_phase"].append(ion_phase)
        projection["trajectories"].append(trajectories)
        projection["panel_index"].append(panel_index)

        try:
            stability_info["num"].append(it)
            stability_info["sv0"].append(stability["sv0"])
            stability_info["sv1"].append(stability["sv1"])
            stability_info["sv2"].append(stability["sv2"])
            stability_info["dv0"].append(stability["dv0"])
            stability_info["dv1"].append(stability["dv1"])
            stability_info["dv2"].append(stability["dv2"])
            stability_info["vel0"].append(stability["vel0"])
            stability_info["vel1"].append(stability["vel1"])
            stability_info["vel2"].append(stability["vel2"])
            stability_info["jacobian"].append(stability["jacobian"])
            stability_info["conformality"].append(stability["conformality"])
            stability_info["J00"].append(stability["J00"])
            stability_info["J10"].append(stability["J10"])
            stability_info["J01"].append(stability["J01"])
            stability_info["J11"].append(stability["J11"])
            stability_info["evap_rate"].append(stability["evap_rate"])
            stability_info["panel_index"].append(stability["panel_index"])
        except:
            pass

def run_rate_weighted_projection(geom, detector_coordinate, rel_sim_tol,
                                 abs_sim_tol, projection, surface, a, its,
                                 atomic_volume, mapping_frac,
                                 BEM_adist=10.0**2.0,
                                 total_ions=5000, real_projection=True, real_detector=True):


    """

    Function for performing evaporation rate dependent ion projection
    (where local flux depends on the evaporation rate).
    Called from the casra_3d.tip_simulation submodule.

    :param1 geom (dict): The simulation dictionary
    :param2 detector_coordinate (float): The z coordinate at which to position the 2D detector
    :param3 rel_sim_tol (float): The relative tolerance for the RK5 trajectory integrator
    :param4 abs_sim_tol (float): The absolute tolerance for the RK5 trajectory integrator
    :param5 projection (dict): The ion projection dictionary {"num": [],
                                                              "sample_space": [],
                                                              "detector_space": [],
                                                              "chemistry": [],
                                                              "trajectories": [],
                                                              "panel_index": []}
    :param6 surface (dict): The simulation output
                           dictionary {"verts": [],
                                       "faces": [],
                                       "cp": [],
                                       "phase": [],
                                       "areas": [],
                                       "normals": [],
                                       "pot": [],
                                       "flux": [],
                                       "smflux": [],
                                       "time": [],
                                       "sample_surface": [],
                                       "smoothed_velocity": [],
                                       "volume": [],
                                       "voltage": []}
    :param7 a (int): The initial iteration number - if starting from non-zero iteration.
    :param8 its (int): The maximum number of model iterations to execute
    :param9 mapping_frac (int): The fraction of model iterations between ion projection
    :param10 BEM_adist (float): The approximation distance (admissability condition)
                                for switching from 7-point to single-point panel integration

    :param11 total_ions (int): The total ions to project. Only important if
                               real_projection=False
    :param12 real_projection (bool): Whether to launch ions proportional to the surface
                                     local evaporation rate (True) or not (False)

    :returns:
        None

    :raises:
        None

    """
    ###ion projection step
    BEM_adist = BEM_adist * geom["simulation_grid"]["cell_width"]**2.0

    for it in np.arange(a, its - mapping_frac, mapping_frac, dtype = int):
        proj = False
        while proj == False:
            verts = surface["verts"][it].astype(np.float64)
            verts2 = surface["verts"][it + int(mapping_frac)].astype(np.float64)
            faces = surface["faces"][it].astype(np.int64)
            faces2 = surface["faces"][it + int(mapping_frac)].astype(np.int64)
            areasc = surface["areas"][it].astype(np.float64)
            normsc = surface["normals"][it].astype(np.float64)
            u = surface["pot"][it].astype(np.float64)
            qc = surface["smflux"][it].astype(np.float64)
            sample_surface = surface["sample_surface"][it]
            sample_surface2 = surface["sample_surface"][it + int(mapping_frac)]
            smoothed_velocity = surface["smoothed_velocity"][it]
            volume_diff = surface["volume"][it + int(mapping_frac)] - surface["volume"][it]
            print(surface["volume"][it])

            iteration_stability = surface["iteration_stability"][it]
            #if iteration unstable, try next iteration for ion projection
            if iteration_stability == False:
                it += 1
                continue

            proj = True
            (sample_space, detector_space,
             chemistry, ion_phase,
             trajectories, panel_index) = ion_projection.project_ions(geom, detector_coordinate,
                                                                      rel_sim_tol, abs_sim_tol,
                                                                      areasc, normsc, u, qc, verts,
                                                                      verts2, faces, faces2,
                                                                      sample_surface, sample_surface2,
                                                                      BEM_adist, smoothed_velocity,
                                                                      volume_diff, surface["volume"][it],
                                                                      surface["volume"][it + int(mapping_frac)],
                                                                      total_ions, atomic_volume,
                                                                      real_projection, real_detector)
            projection["num"].append(it)
            projection["sample_space"].append(sample_space)
            projection["detector_space"].append(detector_space)
            projection["vel"].append(len(detector_space) * [[0.0, 0.0, 0.0]])
            projection["chemistry"].append(chemistry)
            projection["ion_phase"].append(ion_phase)
            projection["trajectories"].append(trajectories)
            projection["panel_index"].append(panel_index)
