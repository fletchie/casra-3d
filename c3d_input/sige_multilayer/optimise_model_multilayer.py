"""

script for sweeping over and optimising a nominated model evaporation field

Reruns model various times, performing regular grid search over the nominated
evaporation fields

Author: Charles Fletcher
Atom Probe Tomography Group, Department of Materials, University of Oxford
March 2021

"""

import os
import sys
import time
from multiprocessing import Pool
import multiprocessing
from functools import partial
import time as timer

sys.path.append("")

try:
    import numpy as np
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Package numpy. Please make sure it is installed.")

from casra_3d import tip_simulation as tip_simulation
from casra_3d import perform_projection as perform_projection
from casra_3d.reconstruction import landmark_fitting as landmark_fitting
from casra_3d.reconstruction import model_reconstruction as model_reconstruction
from casra_3d.reconstruction import evaluate_similarity as evaluate_similarity
from casra_3d.reconstruction import tune_image_transfer as tune_image_transfer

def overlap_function(regular_grid_cell, nominated_phases):

    #####inputs/outputs#####
    pool_id = 0

    #experiment input parameters
    apt_data = "c3d_input/sige_multilayer/sige_multilayer.ato"
    rangefile = "c3d_input/sige_multilayer/sige_multilayer.rrng"

    det_dist = 0.102
    det_rad  = 0.038

    ##evaporate model parameters
    model_filename = "c3d_input/sige_multilayer/sige_multilayer.mstate"
    out_filename = "c3d_output/sige_multilayer/sweep" + str(pool_id)
    global_parameters_filename = "c3d_input/sige_multilayer/parameters"
    mref_filename = "c3d_input/sige_multilayer/sige_multilayer.mref"
    materials_db = "c3d_input/materials"
    its = 2000
    reset_field = False
    terminate_height = 0.3e-7

    if not os.path.exists("c3d_output/sige_multilayer/sweep" + str(pool_id)):
        os.makedirs("c3d_output/sige_multilayer/sweep" + str(pool_id))

    ##project model parameters
    filename = "c3d_input/sige_multilayer/sige_multilayer.mstate"
    out_filename = "c3d_output/sige_multilayer/sweep" + str(pool_id)
    dirname = "c3d_output/sige_multilayer/sweep" + str(pool_id)

    ##image transfer parameters
    projection_data = "c3d_output/sige_multilayer/sweep" + str(pool_id) + "/projection_data/projection_data.tsv"
    stability_data = "c3d_output/sige_multilayer/sweep" + str(pool_id) + "/projection_data/stability_data.tsv"
    model_landmarks_roi ="B-layer5:enter,C-layer4:enter,E-layer3:enter,D-layer2:enter".split(",")

    exp_landmarks_roi = "500000,820000,1190000,1730000".split(",")
    exp_landmarks_roi = [int(b) for b in exp_landmarks_roi]

    ##landmark placement parameters
    mapping_data = "c3d_output/sige_multilayer/sweep" + str(pool_id) + "/projection_data/projection_data.tsv"
    model_landmarks = "B-layer5:enter,C-layer4:enter,E-layer3:enter,D-layer2:enter".split(",")
    exp_landmarks = "500000,820000,1190000,1730000".split(",")
    exp_landmarks = [int(b) for b in exp_landmarks]

    landmark_calibration_file = "c3d_output/sige_multilayer/sweep" + str(pool_id) + "/recon/landmark_calibration.rec"
    projection_calibration_file = "c3d_output/sige_multilayer/sweep" + str(pool_id) + "/recon/projection_calibration.rec"

    ###model driven reconstruction parameters
    out_recon_file = "c3d_output/sige_multilayer/sweep" + str(pool_id) + "/recon/recon.vtk"

    #evaluate MI parameters
    filename = "c3d_input/sige_multilayer/imec.mstate"
    out_dir = "c3d_output/sige_multilayer/sweep" + str(pool_id) + "/recon"
    micro_elements = "bulk:0.66-O;layer1:1-Si;layer2:0.5-Ge;layer3:1-Ge;layer4:1-Si;layer5:1-Si;layer6:1-Si"
    bc = 80
    bw = 1
    detector_efficiency = 0.5
    out_recon_file = "c3d_output/sige_multilayer/sweep" + str(pool_id) + "/recon/recon.vtk"
    model_scale = 0.3e-9

    #set model free parameters
    free_params = {}
    for a in range(0, len(nominated_phases)):
        #handle multiple free phases having the same field
        nom_phase_spl = nominated_phases[a].split(";")
        for b in range(0, len(nom_phase_spl)):
            free_params[nom_phase_spl[b]] = regular_grid_cell[2 + a]

    ###perform specimen evaporation simulation
    its = tip_simulation.evaporate_model(model_filename, out_filename, global_parameters_filename,
                                         mref_filename, materials_db, its, reset_field,
                                         free_params, termination_height = terminate_height)

    ###perform ion projection from model
    perform_projection.project(filename, out_filename, global_parameters_filename,
                               mref_filename, materials_db, its, dirname,
                               para = True, real_projection = False,
                               weighted_projection = False, real_detector = False)

    #perform specimen alignment and ICF calibration
    optimised_parameters = tune_image_transfer.optimise_image_transfer(projection_data, stability_data,
                                                                       det_dist, det_rad, apt_data,
                                                                       rangefile, model_landmarks_roi,
                                                                       exp_landmarks_roi, nbins = 100)

    #define image transfer list of optimum parameters
    image_transfer = [det_dist, optimised_parameters["tdx"], optimised_parameters["tdy"],
                      optimised_parameters["theta"], optimised_parameters["ICF"]]

    print("calculating calibration landmarks")
    landmark_fitting.landmark(mapping_data, det_rad, dirname, model_landmarks,
                              exp_landmarks, landmark_calibration_file,
                              projection_calibration_file, image_transfer)

    model_reconstruction.recon(mapping_data, dirname, landmark_calibration_file,
                               projection_calibration_file, apt_data, rangefile,
                               out_recon_file, materials_db, image_transfer)
    print("reconstructed")

    #####evaluate reconstruction MI
    overlap_value_diff, overlap_value_MI, spread, normalised_spread, normalised_spread_bins, return_code = evaluate_similarity.similarity(filename, out_recon_file,
                                                                                                                                         out_dir, materials_db, bw,
                                                                                                                                         bc, micro_elements,
                                                                                                                                         detector_efficiency, model_scale)
    #overlap_value_MI = 0.0

    #print(overlap_value_MI)
    #print(overlap_value_diff)
    #print(spread)
    ###write optimization outputs###
    f = open(output_dir + "/optimisation_results.tsv", "a+")
    for a in range(len(nominated_phases)):
        f.write(nominated_phases[a] + "\t" + str(regular_grid_cell[2 + a]) + "\t" + str(image_transfer[1]) +
                "\t" + str(image_transfer[2]) + "\t" + str(image_transfer[3]) + "\t" + str(image_transfer[4]) +
                "\t" + str(overlap_value_MI) + "\t" + str(optimised_parameters["MI"]) + "\t" + str(spread) + "\t" + str(normalised_spread) + "\t" + str(normalised_spread_bins) + "\n")
    f.close()

###default parameters

init_this_time = timer.time()

#fguess = [29864444444.4444, 29500000000]
fguess = [30666666666.666668, 29500000000.0]

nominated_phases = ["bulk", "layer1;layer6"]
grid_sampling_x = 12
grid_sampling_y = 1

pools = int(os.cpu_count()/2)

output_dir = "c3d_output/sige_multilayer/optimisation"

#create directory for outputting optimisation results
if not os.path.exists(output_dir):
    os.makedirs(output_dir, exist_ok=True)

#create/clear previous optimisation results
f = open(output_dir + "/optimisation_results.tsv", "w+")
f.close()

#construct regular grid
if grid_sampling_x > 1:
    F00 = np.linspace(18000000000,
                      45000000000,
                      num = grid_sampling_x, endpoint = True)
else:
    F00 = np.array([fguess[0]])

if grid_sampling_y > 1:
    F01 = np.linspace(29000000000,
                      34000000000,
                      num = grid_sampling_y, endpoint = True)
else:
    F01 = np.array([fguess[1]])

ids = np.tile(np.linspace(0, pools - 1, pools), int(len(F00)/pools + 1))
F00, F01 = np.meshgrid(F00,F01)

ids = np.tile(ids, len(F01.flatten()))[:len(F01.flatten())]
unid = np.linspace(1, len(ids), len(ids))

regular_grid = np.vstack((ids, unid, F00.flatten(), F01.flatten())).T

#non-parallelised version
for a in range(len(regular_grid)):
    overlap_function(regular_grid[a], nominated_phases)

print("parameter sweeping complete")
print(timer.time() - init_this_time)
