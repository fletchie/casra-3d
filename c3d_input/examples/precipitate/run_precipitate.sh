#!/bin/bash

flightpath=0.12

echo "Run precipitate model driven reconstruction pipeline"

if [ ! -d casra_3d ] ; then
	echo " please run script from top level folder of casra repository: could not find casra_3d in current dir."
	exit 1
fi

###Generate synthetic precipitate dataset
#generate model geometry
python3 c3d_input/examples/precipitate/generate_precipitate.py
 
#simulate model evaporation
python3 -m casra_3d.tip_simulation --tip_geom c3d_input/examples/precipitate/precipitate.mstate --parameters c3d_input/examples/precipitate/parameters --mat_db c3d_input/materials --it 1000 --out c3d_output/examples/precipitate --mref c3d_input/examples/precipitate/precipitate.mref --termination_height 1.95e-7

#generate synthetic APT data (launch ions proportional to local evaporation rate)
python3 -m casra_3d.perform_projection --tip_geom c3d_input/examples/precipitate/precipitate.mstate --out c3d_output/examples/precipitate --parameters c3d_input/examples/precipitate/parameters --it 1000 --dirname c3d_output/examples/precipitate --parallelisation True --weighted_projection True --real_projection True --mref c3d_input/examples/precipitate/precipitate.mref

#generate species distribution histogram (for determining exp_landmarks in landmark_fitting below)
python3 -m casra_3d.tools.species_distribution --input_data c3d_output/examples/precipitate/projection_data/projection_data.tsv --det_rad 0.038 --output c3d_output/examples/precipitate --bins 200

#generate isolines for distortion visualisation
python3 -m casra_3d.tools.reconstruction_mapping --mapping_data c3d_output/examples/precipitate/projection_data/projection_data.tsv --det_radius 0.038 --detector_lines 10 --dirname c3d_output/examples/precipitate/recon

###derive reconstruction mapping data
python3 -m casra_3d.perform_projection --tip_geom c3d_input/examples/precipitate/precipitate.mstate --out c3d_output/examples/precipitate_mapping --parameters c3d_input/examples/precipitate/parameters --it 5000 --dirname c3d_output/examples/precipitate --parallelisation True --weighted_projection False --real_projection False --mref c3d_input/examples/precipitate/precipitate.mref

#fit model landmarks for reconstruction calibration
python3 -m casra_3d.reconstruction.landmark_fitting --mapping_data c3d_output/examples/precipitate_mapping/projection_data/projection_data.tsv --det_radius 0.038 --model_landmarks A-0,B-phase1:enter,C-phase1:exit,D-140 --exp_landmarks 0,6529,23000,33834 --dirname c3d_output/examples/precipitate --landmark_calib_file c3d_output/examples/precipitate/recon/landmark_calib.txt --projection_calib_file c3d_output/examples/precipitate/recon/projection_calib.txt --image_transfer "$flightpath,0,0,0,1"
 
#perform the model reconstruction using the generated landmark files (generated via landmark_fitting)
python3 -m casra_3d.reconstruction.model_reconstruction --mapping_data c3d_output/examples/precipitate_mapping/projection_data/projection_data.tsv --rangefile "" --dirname c3d_output/precipitate --apt_data c3d_output/examples/precipitate/projection_data/projection_data.tsv --material_db c3d_input/materials --out_recon c3d_output/examples/precipitate/recon/recon.vtk --landmark_calib_file c3d_output/examples/precipitate/recon/landmark_calib.txt --projection_calib_file c3d_output/examples/precipitate/recon/projection_calib.txt --image_transfer "$flightpath,0,0,0,1"

#calculate local compositions and densities within model reconstruction
python3 -m casra_3d.tools.reconstruction_composition_calculation --input c3d_output/examples/precipitate/recon/recon.vtk --bandwidth 2e-9 --grid_width 100 --material_db c3d_input/materials --out c3d_output/examples/precipitate/recon/crecon.vtk

#perform a comparative point projection reconstruction
python3 -m casra_3d.point_projection_reconstruction.method --input_data c3d_output/examples/precipitate/projection_data/projection_data.tsv --output c3d_output/examples/precipitate/pprecon --parameters c3d_input/examples/precipitate/pprecon_parameters.tsv 

#calculate local compositions and densities within point projection reconstruction
python3 -m casra_3d.tools.reconstruction_composition_calculation --input c3d_output/examples/precipitate/pprecon/recon.vtk --bandwidth 2e-9 --grid_width 100 --material_db c3d_input/materials --out c3d_output/examples/precipitate/pprecon/crecon.vtk
