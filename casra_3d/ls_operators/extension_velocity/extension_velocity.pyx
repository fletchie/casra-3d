
"""

Module for performing extension velocity construction through Cython

Module must be compiled prior to using casra_3d using the following
command when in the casra_3d directory:
`python3 setup.py build_ext --inplace`

Author: Charles Fletcher, Daniel Haley
Atom Probe Tomography Group, Department of Materials, University of Oxford
May 2020

"""

import cython
import numpy as np
cimport numpy as np
from cython.parallel import prange
from libc.math cimport round
from cpython.exc cimport PyErr_CheckSignals

#external sqrt function from math
cdef extern from "math.h":
    double sqrt(double m)

#removes error checking code from compiled cython for matrix index behaviour
@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def ext_vel(double[::1] bv, long[:, ::1] connectivity_matrix,
            double[:, ::1] vertices, long[::1] sample_surface,
            int x, int y, int z, int x0, int y0, int z0, int shank):

    """

    Python callable function for calculating the extension velocity via a
    closest-point method (see below)
    [1] R. Malladi, J. A. Sethian, and B. C. Vemuri,
    “Shape modeling with front propagation: a level set approach,”
    vol. 17, no. 2, pp. 158–175, 1995.

    :param1 bv (np.array, (N), double): 1D array of panel velocities
                                        (calculated from the normal fluxes)
    :param2 connectivity_matrix (np.array, (N, 3), double): 2D array defining
                                                            the mesh connectivity matrix
    :param3 vertices (np.array, (M, 3), double): 2D array of mesh vertices
    :param4 sample_surface (np.array, (N), int): 1D integer array recording whether
                                                 panel defines the sample surface
                                                 (1) or an internal contour (0)
    :param5 x (int): x dimension of the level set grid
    :param6 y (int): y dimension of the level set grid
    :param7 z (int): z dimension of the level set grid
    :param8 shank (int): z grid index to start extension
                         velocity construction
                         (for `frozen` shank evaporation)

    :returns:
      :ext_vel (np.array, (x, y, z), double): 3D array of extension velocity
                                              over level set grid
      :surf_nonsurf (np.array, (x, y, z), double): 3D array of whether cell is
                                                   closest to a sample surface panel (1)
                                                   or an internal contour (0)

    :raises:
      None

    """

    cdef Py_ssize_t bmax = connectivity_matrix.shape[0]
    cdef Py_ssize_t vmax = bv.shape[0]

    assert bmax, vmax

    ext_vel = np.zeros((x, y, z), dtype=np.double)
    cdef double[:, :, ::1] ext_vel_view=ext_vel

    surf_nonsurf = np.zeros((x, y, z), dtype=np.int32) + 1
    cdef int[:, :, ::1] surf_nonsurf_view=surf_nonsurf

    cdef int rv = 0

    cdef double dist1 = x*x*x*x
    cdef double dist2 =  x*x*x*x
    cdef double tdist = 0.0

    cdef double c1x = 0.0
    cdef double c1y = 0.0

    cdef double c2x = 0.0
    cdef double c2y = 0.0

    cdef Py_ssize_t i, j, k, v
    cdef Py_ssize_t xmax = x
    cdef Py_ssize_t ymax = y
    cdef Py_ssize_t zmax = z

    cdef double sb = 0.0
    cdef double sa = 0.0
    cdef double l = 0.0
    cdef double AB = 0.0

    cdef double ix
    cdef double iy
    cdef double iz
    cdef double iix
    cdef double iiy
    cdef double iiz

    ##default compile mode is for parallelisation
    for i in prange(xmax, nogil=True):
    #for i in range(xmax):
        #FIXME: This should only be called from main thread; however calling
        #from other threads should not have an effect
        ix = i - x0
        for j in range(ymax):
            iy = j - y0
            for k in range(shank, zmax):
                iz = k - z0
                dist1 = 4.0 * x * x * 16.0

                for v in range(bmax):
                    iix = ix - vertices[v, 0]
                    iiy = iy - vertices[v, 1]
                    iiz = iz - vertices[v, 2]
                    tdist = iix * iix + iiy * iiy + iiz * iiz

                    if tdist < dist1:
                        dist1 = tdist
                        rv = v

                ext_vel_view[i, j, k] = bv[rv]
                surf_nonsurf_view[i, j, k] = sample_surface[rv]
    PyErr_CheckSignals()

    return ext_vel, surf_nonsurf

#removes error checking code from compiled cython for matrix index behaviour
@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def ext_vel_adaptive(double[::1] bv, double[:, :, ::1] field,
                     long[:, ::1] connectivity_matrix, double[:, ::1] vertices,
                     long[::1] sample_surface, int x, int y, int z, int xc,
                     int yc, int zc, int shank):

  """

  Python callable function for calculating the extension velocity via
  a closest-point method (see below)

  Method reduces extension velocity sampling outside of a narrowband region
  (sampling one in every other grid point and then performing trilinear interpolation)

  [1] R. Malladi, J. A. Sethian, and B. C. Vemuri,
  “Shape modeling with front propagation: a level set approach,”
  vol. 17, no. 2, pp. 158–175, 1995.

  :param1 bv (np.array, (N), double): 1D array of panel velocities
                                       (calculated from the normal fluxes)
  :param2 field (np.array, (x, y, z), double): The 3D level set field
  :param3 connectivity_matrix (np.array, (N, 3), double): 2D array defining
                                                          the mesh connectivity
                                                          matrix
  :param4 vertices (np.array, (M, 3), double): 2D array of mesh vertices
  :param5 sample_surface (np.array, (N), int): 1D integer array recording
                                               whether panel defines the
                                               sample surface (1) or
                                               an internal contour (0)
  :param6 x (int): x dimension of the level set grid
  :param7 y (int): y dimension of the level set grid
  :param8 z (int): z dimension of the level set grid
  :param9 shank (int): z grid index to start extension velocity construction
                       (for `frozen` shank evaporation)

  :returns:
    :ext_vel (np.array, (x, y, z), double): 3D array of extension velocity
                                            over level set grid
    :surf_nonsurf (np.array, (x, y, z), double): 3D array of whether cell is
                                                 closest to a sample surface panel
                                                 (1) or an internal contour (0)

  :raises:
    None

  """

  ###approximation parameters - default works well
  #larger value -> faster but lower resolution
  cdef int sampling = 2

  #size of narrowband (full evaluation region surrounding interface)
  cdef double narrowband = 6.0

  cdef Py_ssize_t bmax = connectivity_matrix.shape[0]
  cdef Py_ssize_t vmax = bv.shape[0]

  assert bmax, vmax

  ext_vel = np.zeros((x, y, z), dtype=np.double)
  cdef double[:, :, ::1] ext_vel_view=ext_vel

  surf_nonsurf = np.zeros((x, y, z), dtype=np.int32) + 1
  cdef int[:, :, ::1] surf_nonsurf_view=surf_nonsurf

  cdef int rv = 0

  cdef double dist1 = x * x * x * x
  cdef double dist2 = x * x * x * x
  cdef double tdist = 0.0

  cdef double c1x = 0.0
  cdef double c1y = 0.0

  cdef double c2x = 0.0
  cdef double c2y = 0.0

  cdef Py_ssize_t i, j, k, v
  cdef Py_ssize_t xmax = x
  cdef Py_ssize_t ymax = y
  cdef Py_ssize_t zmax = z

  cdef double sb = 0.0
  cdef double sa = 0.0
  cdef double l = 0.0
  cdef double AB = 0.0

  cdef double ix
  cdef double iy
  cdef double iz
  cdef double iix
  cdef double iiy
  cdef double iiz

  cdef int dx, dy, dz, x0, y0, z0, x1, y1, z1
  cdef double c00, c01, c10, c11, c0, c1, xd, yd, zd

  ##default compile mode is for parallelisation
  for i in prange(xmax, nogil=True):
  #for i in range(xmax):
    #FIXME: This should only be called from main thread; however calling
    #from other threads should not have an effect
    ix = i - xc
    for j in range(ymax):
      iy = j - yc
      for k in range(shank, zmax):
        if (i == 0 or j == 0 or k == shank or i == (x - 1) or j == (y - 1) or
            k == (z - 1) or (field[i, j, k] < narrowband and
            field[i, j, k] > -narrowband) or
            (i % sampling == 0 and j % sampling == 0 and k % sampling == 0)):

          iz = k - zc
          dist1 = 4.0 * x * x * 16.0
          for v in range(bmax):
            iix = ix - vertices[v, 0]
            iiy = iy - vertices[v, 1]
            iiz = iz - vertices[v, 2]
            tdist = iix * iix + iiy * iiy + iiz * iiz

            if tdist < dist1:
                dist1 = tdist
                rv = v

          ext_vel_view[i, j, k] = bv[rv]
          surf_nonsurf_view[i, j, k] = sample_surface[rv]
  PyErr_CheckSignals()

  #perform trilinear grid interpolation from sampled values to
  #construct full extension velocity
  for i in prange(xmax, nogil=True):
  #for i in range(xmax):
    #FIXME: This should only be called from main thread; however calling
    #from other threads should not have an effect
    for j in range(ymax):
      for k in range(shank, zmax):
        if field[i, j, k] > narrowband or field[i, j, k] < -narrowband:
          #find bounding coordinates
          dx = i % sampling
          dy = j % sampling
          dz = k % sampling

          if dx != 0 or dy != 0 or dz != 0:
            x0 = i - dx
            if x0 < 0:
              x0 = 0
            y0 = j - dy
            if y0 < 0:
              y0 = 0
            z0 = k - dz
            if z0 < shank:
              z0 = shank

            x1 = i + sampling - dx
            if x1 > (xmax - 1):
              x1 = xmax -1
            y1 = j + sampling - dy
            if y1 > (ymax - 1):
              y1 = ymax - 1
            z1 = k + sampling - dz
            if z1 > (zmax - 1):
              z1 = zmax - 1

            #performing trilinear interpolation
            xd = 0
            if (x1 - x0) > 0:
              xd = (i - x0) * 1.0/(x1 - x0)
            yd = 0
            if (y1 - y0) > 0:
              yd = (j - y0) * 1.0/(y1 - y0)
            zd = 0
            if (z1 - z0) > 0:
              zd = (k - z0) * 1.0/(z1 - z0)

            c00 = ext_vel_view[x0, y0, z0] * (1.0 - xd) + ext_vel_view[x1, y0, z0] * xd
            c01 = ext_vel_view[x0, y0, z1] * (1.0 - xd) + ext_vel_view[x1, y0, z1] * xd
            c10 = ext_vel_view[x0, y1, z0] * (1.0 - xd) + ext_vel_view[x1, y1, z0] * xd
            c11 = ext_vel_view[x0, y1, z1] * (1.0 - xd) + ext_vel_view[x1, y1, z1] * xd

            c0 = c00 * (1 - yd) + c10 * yd
            c1 = c01 * (1 - yd) + c11 * yd
            ext_vel_view[i, j, k] = c0 * (1 - zd) + c1 * zd

            c00 = (surf_nonsurf_view[x0, y0, z0] * (1.0 - xd) +
                   surf_nonsurf_view[x1, y0, z0] * xd)
            c01 = (surf_nonsurf_view[x0, y0, z1] * (1.0 - xd) +
                   surf_nonsurf_view[x1, y0, z1] * xd)
            c10 = (surf_nonsurf_view[x0, y1, z0] * (1.0 - xd) +
                   surf_nonsurf_view[x1, y1, z0] * xd)
            c11 = (surf_nonsurf_view[x0, y1, z1] * (1.0 - xd) +
                   surf_nonsurf_view[x1, y1, z1] * xd)

            c0 = c00 * (1 - yd) + c10 * yd
            c1 = c01 * (1 - yd) + c11 * yd
            surf_nonsurf_view[i, j, k] = int(round(c0 * (1 - zd) + c1 * zd))

  return ext_vel, surf_nonsurf
