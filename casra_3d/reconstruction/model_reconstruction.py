
"""

Submodule containing functions for performing model driven reconstruction

Takes data from model evaporation simulation and ion projection, as well as user guided landmarks to
reduce numerical drift (generated using module `landmark_fitting`)

Author: Charles Fletcher
Atom Probe Tomography Group, Department of Materials, University of Oxford
May 2020

"""

import copy
import os
import csv
import sys
import sqlite3

try:
    import numpy as np
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Package numpy. Please make sure it is installed.")

try:
    import scipy as sp
    import scipy.interpolate as spinterpolate
    import scipy.spatial
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Package scipy. Please make sure it is installed.")

from casra_3d.point_projection_reconstruction import loader as loader
from casra_3d.point_projection_reconstruction import point_projection_mapping as ppm
from casra_3d.io import model_loaders as model_loaders
from casra_3d.io import model_writers as model_writers
from casra_3d.core import ion_projection as ion_projection

def read_calibration_from_file(calib_filename):

    """

    Function for reading in the reconstruction calibration file from text file (tab separated).

    :param1 calib_filename: String giving reconstruction calibration file location
    :returns:
        :read_dict: Dictionary with keys and values

    """

    read_dict = {}
    col_names = []
    with open(calib_filename) as f:
        reader = csv.reader(f, delimiter='\t')
        h = 0
        for row in reader:
            if len(row) > 1:
                if row[0][0] != "#":
                    if h == 0:
                        col_names = row
                        for a in range(0, len(row)):
                            read_dict[col_names[a]] = []
                        h+=1
                        continue
                    for a in range(0, len(row)):
                        read_dict[col_names[a]].append(float(row[a]))
    return read_dict


def recon(mapping_data, dirname, landmark_calibration_file, projection_calibration_file, apt_data,
          rangefile, out_recon_file, material_db, image_transfer = [0.102, 0.0, 0.0, 0.0, 0.75],
          mapping_resolution = 600, sampling = 1, filt_rad = np.inf):

    """

    Function for performing the model driven APT reconstruction

    :param1 mapping_data: The model trajectory data (.tsv)
    :param2 dirname: The model simulated evaporation data
    :param3 landmark_calibration_file: The landmark calibration filename (generated via the module landmark_fitting)
    :param4 projection_calibration_file: The projection calibration filename (generated via the module landmark_fitting)
    :param5 apt_data: The APT data (.epos, .ato or .tsv)
    :param6 rangefile: The accompanying range filename
    :param7 out_recon_file: The output filename for the generated reconstruction .vtk
    :param8 material_db: The materials sqlite database
    :param9: image_transfer (list): List of image transfer parameters.
                                    This variable should have the following structure (list)
                                    [flight path, dx-transform, dy-transform, theta, ICF]
    :param10 mapping_resolution: The grid dimensions (x and y) for interpolating the reconstruction mapping
    :param11 sample (float): The ion sampling fraction

    :returns: 0

    """

    #check that the materials database can be found at the inputed file location
    if os.path.exists(material_db) == False:
        raise FileNotFoundError("No database can be found at location: " + str(material_db) + ". /n Please ensure the passed directory is correct.")

    #create the output directory if it doesn't exist
    directory = "/".join(out_recon_file.split("/")[:-1])
    if not os.path.exists(directory):
        os.makedirs(directory)

    #read in the landmark configuration file
    lcf = read_calibration_from_file(landmark_calibration_file)

    #read in the projection configuration file
    pcf = read_calibration_from_file(projection_calibration_file)
    det_radius = lcf["detector_radius"][0]

    det_rad_2 = det_radius**2.0
    dx, dy, sx, sy, sz, chem, ions, ic, VP, da, da_p, experimental = model_loaders.load_apt_data(apt_data, rangefile, sampling)

    experimental = False

    if len(ions) > 0:
        experimental = True

    #filter ions outside of the detector radius
    rad2 = dx**2.0 + dy**2.0
    dx = dx[rad2 < det_rad_2]
    dy = dy[rad2 < det_rad_2]
    sx = sx[rad2 < det_rad_2]
    sy = sy[rad2 < det_rad_2]
    sz = sz[rad2 < det_rad_2]
    chem = chem[rad2 < det_rad_2]
    da = da[rad2 < det_rad_2]

    #filter out non-ranged ions
    if len(ions) > 0:
        dx = dx[chem != 0]
        dy = dy[chem != 0]
        da = da[chem != 0]
        chem = chem[chem != 0] - 1

    #load simulated model projection data
    projection = {"num": [], "sample_space": [], "detector_space": [], "chemistry": [], "ion_phase": [], "trajectories": [], "vel": [], "panel_index": []}
    model_loaders.load_tsv_ion_trajectories(projection, mapping_data)

    det_dist = float(image_transfer[0])
    ts = np.array([float(image_transfer[1]), float(image_transfer[2])])
    theta = float(image_transfer[3]) * np.pi/180.0
    ICF = float(image_transfer[4])

    #perform detector transfer
    projection["detector_space"] = ion_projection.transfer_function(projection["detector_space"], projection["vel"], det_dist, kappa=ICF)
    coords = copy.deepcopy(projection["detector_space"])
    coordsn = copy.deepcopy(projection["detector_space"])

    #rotate trajectory mapping - inverse rigid body transformation

    P = np.array([[0.0, 1.0], [1.0, 0.0]])
    coordsn[:, :2] = np.matmul(P, coords[:, :2].T).T

    #fl = copy.deepcopy(coords[:, 0])
    #coords[:, 0] = coords[:, 1]
    #coords[:, 1] = fl

    Rinv = np.array([[np.cos(theta), -np.sin(theta)], [np.sin(theta), np.cos(theta)]]).T
    coordsn[:, :2] = np.matmul(Rinv, (coordsn[:, :2]).T).T
    inv_ts = np.matmul(Rinv, ts)
    coordsn[:, :2] = coordsn[:, :2] - inv_ts

    #fl = copy.deepcopy(coords[:, 0])
    #coords[:, 0] = coords[:, 1]
    #coords[:, 1] = fl
    coordsn[:, :2] = np.matmul(P, coordsn[:, :2].T).T
    projection["detector_space"][:, :2] = coordsn[:, :2]
    ###########

    sample_points = np.linspace(-det_radius * 1.1, det_radius * 1.1, mapping_resolution, endpoint = True)
    xx, xy = np.meshgrid(sample_points, sample_points)

    #if simulated data (and complex ion array is 0). Construct array.
    if len(ions) == 0:
        ic = np.array([1] * len(chem))

    #perform the APT model driven reconstruction
    rcx, rcy, rcz, rchem, reconed = reconstruction_core(projection, xx, xy, lcf, pcf, dx, dy, chem, ions, ic, experimental)
    print("reconstructed ions: " + str(len(rcx)))

    da = da[reconed]
    dx = dx[reconed]
    dy = dy[reconed]

    if len(reconed) == len(sx):
        sx = sx[reconed]
        sy = sy[reconed]
        sz = sz[reconed]

    #remove any ions with spatial positioning errors
    filt = np.isnan(rcx) + np.isnan(rcy) + np.isnan(rcz)
    rcx = np.array(rcx)
    rcy = np.array(rcy)
    rcz = np.array(rcz)
    rchem = np.array(rchem)

    rcx = rcx[filt == False]
    rcy = rcy[filt == False]
    rcz = rcz[filt == False]
    rchem = rchem[filt == False]
    da = da[filt == False]
    dx = dx[filt == False]
    dy = dy[filt == False]

    if len(sx) == len(filt):
        sx = sx[filt == False]
        sy = sy[filt == False]
        sz = sz[filt == False]

    ##these must be matched to the ids given in the materials db
    #connect to materials database
    conn = sqlite3.connect(material_db)
    c = conn.cursor()

    #obtain list of element ids within the materials database
    el_ids = c.execute('SELECT element_id, element FROM element_ids').fetchall()
    ids = np.array([a[0] for a in el_ids])
    els = np.array([a[1] for a in el_ids], dtype = 'object')
    els = els[np.sort(ids)]
    ids = ids[np.sort(ids)]

    #split ions to currently remove complex ion problem (all ions treated as simple ions)
    #the database needs updating to include complex ions
    single_ions = [a.split(":")[0] for a in ions]

    #determine id mapping from experimental data to database ion ids
    dbids = []
    for a in range(len(single_ions)):
        try:
            dbids.append(ids[els == single_ions[a]][0])
        except IndexError:
            raise IndexError("Cannot find species id " + single_ions[a] + " within the materials database `element_ids` table. Please make sure it has been added.")

    #perform mapping of element ids such that outputted data matches the ion ids defined in the database
    nchem = copy.deepcopy(rchem)
    for a in range(len(single_ions)):
        nchem[rchem == a] = dbids[a]

    prfilt = dx**2.0 + dy**2.0 <= filt_rad**2.0

    #output model driven reconstruction to .vtk
    print("saving reconstruction to location: " + out_recon_file)
    points = np.vstack((rcx[prfilt], rcy[prfilt], rcz[prfilt])).T
    point_ind = np.linspace(0, len(points), len(points), endpoint = False).astype(int)
    cells = point_ind.reshape(-1, 1)
    point_data = {"chemistry": np.array(nchem)[prfilt] * 1.0, "Da": np.array(da)[prfilt], "dx": np.array(dx)[prfilt], "dy": np.array(dy)[prfilt]}
    print(points.shape)
    print((np.array(nchem) * 1.0).shape)
    print(np.array(da).shape)
    print(np.array(dx).shape)
    print(np.array(dy).shape)
    model_writers.write_vtk(out_recon_file, points, cells, point_data = point_data)

    #output original coordinates to .vtk
    if len(sx) == len(rcx):
        original_coords_dir = "/".join(out_recon_file.split("/")[:-1])
        print("saving original coordinates to location: " + original_coords_dir + "/original_pos.vtk")
        points = np.vstack((sx, sy, sz)).T
        point_ind = np.linspace(0, len(points), len(points), endpoint = False).astype(int)
        cells = point_ind.reshape(-1, 1)
        point_data = {"chemistry": np.array(nchem) * 1.0, "Da": np.array(da)}
        model_writers.write_vtk(original_coords_dir + "/original_pos.vtk", points, cells, point_data = point_data)

def reconstruction_core(mapping, xx, xy, lcf, rcf, dx, dy, chem, ions, ic, experimental = True):

    """

    Function containing core operations for performing the model driven reconstruction

    :param1 mapping: A dictionary containing the loaded model simulated trajectory data
    :param2 xx: Grid of dx coordinate values for reconstruction mapping interpolation
    :param3 xy: Grid of dy coordinate values for reconstruction mapping interpolation
    :param4 lcf: Python dictionary containing the landmark configuration file data
    :param5 lcf: Python dictionary containing the projection configuration file data
    :param6 dx: The ion detector x coordinates
    :param7 dy: The ion detector y coordinates
    :param8 chem: The ion species ids (index corresponds to label in input parameter `ions`)
    :param9 ions: List of ion labels
    :param10 ic: List of number of nuclei for each ion within the input parameter `ions`
    :param11 experimental: Whether the data is real experimental data or not

    :returns:
        :rcx: The reconstructed x positions
        :rcy: The reconstructed y positions
        :rcz: The reconstructed z positions
        :rchem: The reconstructed ion species ids (index corresponds to label in input parameter `ions`)

    """

    #extract list of simulated landmarks
    slandmarks = np.array(lcf["simulation_landmarks"]).astype(int)
    #extract list of experimental landmarks
    elandmarks = np.array(lcf["experiment_landmarks"]).astype(int)

    #extract cumulative evaporated model volumes at landmark frames
    lvolumes = np.array(lcf["volume"])

    #extract list of projection frames
    proj_frames = np.array(rcf["projection_frame"]).astype(int)
    #extract cumulative evaporated model volumes at projection frames
    rvolumes = np.array(rcf["volume"])

    #obtain list of model frames for which ion projection was performed
    projection_frames_list = np.unique(mapping["num"])

    #initialisation of variables
    rvol = 0.0
    lint = 0
    a = elandmarks[0]
    rcx = []
    rcy = []
    rcz = []
    rchem = []
    sample_space = []
    det_space = []
    minxx = np.min(xx)
    minxy = np.min(xy)
    maxxx = np.max(xx)
    maxxy = np.max(xy)
    xshape = xx.shape
    yshape = xy.shape

    #filter evaporated volumes and projection_frames_list to only include those lying within the
    #model landmarks defined for the reconstruction
    rvolumes = rvolumes[projection_frames_list >= slandmarks[0]]
    projection_frames_list = projection_frames_list[projection_frames_list >= slandmarks[0]]
    rvolumes = rvolumes[projection_frames_list <= slandmarks[-1]]
    projection_frames_list = projection_frames_list[projection_frames_list <= slandmarks[-1]]

    reconed = np.array([False] * len(dx))

    #identify number of nuclei within each compound ion
    raw_ions = np.zeros((len(chem)))

    if experimental == True:
        for b in range(0, len(ions)):
            raw_ions[chem == b] = ic[b]
    else:
        ions = np.unique(chem)
        for b in range(0, len(ions)):
            raw_ions[chem == ions[b]] = ic[b]

    #define the first landmark (start of reconstruction) as corresponding to an evaporated volume of zero
    rvolumes = rvolumes - rvolumes[0]
    lvolumes = lvolumes - lvolumes[0]

    #obtain a cumulative sum of ion nuclei counts
    raw_ions = np.cumsum(raw_ions)

    #set intial temporal interpolation region
    vc_exp_landmark_front = raw_ions[elandmarks[1]]
    vc_exp_landmark_back = raw_ions[elandmarks[0]]

    print(elandmarks)
    print(a)

    for m_ind in range(len(projection_frames_list) - 1):
        sample_space1 = mapping["sample_space"][mapping["num"] == projection_frames_list[m_ind]]
        det_space1 = mapping["detector_space"][mapping["num"] == projection_frames_list[m_ind]]

        #check for and filter out ion trajectory numerical errors
        sample_space1 = sample_space1[(abs(det_space1[:, 0]) < 1e10)*(abs(det_space1[:, 1]) < 1e10)]
        det_space1 = det_space1[(abs(det_space1[:, 0]) < 1e10) * (abs(det_space1[:, 1]) < 1e10)][:, :2]

        #spatially interpolate the reconstruction position at projection frame 1 over the detector
        spx1 = spinterpolate.griddata(det_space1, sample_space1[:, 0], (xx, xy), method='linear').T
        spy1 = spinterpolate.griddata(det_space1, sample_space1[:, 1], (xx, xy), method='linear').T
        spz1 = spinterpolate.griddata(det_space1, sample_space1[:, 2], (xx, xy), method='linear').T

        sample_space2 = mapping["sample_space"][mapping["num"] == projection_frames_list[m_ind + 1]]
        det_space2 = mapping["detector_space"][mapping["num"] == projection_frames_list[m_ind + 1]]

        #check for and filter out ion trajectory numerical errors
        sample_space2 = sample_space2[(abs(det_space2[:, 0]) < 1e10)*(abs(det_space2[:, 1]) < 1e10)]
        det_space2 = det_space2[(abs(det_space2[:, 0]) < 1e10) * (abs(det_space2[:, 1]) < 1e10)][:, :2]

        #spatially interpolate the reconstruction position at projection frame 2 over the detector
        spx2 = spinterpolate.griddata(det_space2, sample_space2[:, 0], (xx, xy), method='linear').T
        spy2 = spinterpolate.griddata(det_space2, sample_space2[:, 1], (xx, xy), method='linear').T
        spz2 = spinterpolate.griddata(det_space2, sample_space2[:, 2], (xx, xy), method='linear').T

        #calculate the volume fraction from the model landmarks the ion resides between
        vol_frac = (lvolumes[lint + 1] - lvolumes[lint])/(vc_exp_landmark_front - vc_exp_landmark_back)

        if a > 1e10:
            at = 1020000
            det_space = mapping["detector_space"][mapping["num"] == projection_frames_list[41]]
            import matplotlib.pyplot as plt
            plt.scatter(det_space[:, 0], det_space[:, 1])
            plt.scatter(dx[(at-800):(at+800)], dy[(at-800):(at+800)])
            plt.xlim(-0.038, 0.038)
            plt.ylim(-0.038, 0.038)
            plt.savefig("saved.png")
            sys.exit()

        #while ion count lies between landmark index values, calculate the reconstruction position
        #via the projection 1 interpolation and projection 2 interpolation
        while rvol < rvolumes[m_ind + 1]:

            #ion has entered next adjacent landmark region
            if rvol > lvolumes[lint + 1]:
                lint += 1

                ###calculate total number of ions (splitting compound ions)
                vc_exp_landmark_front = raw_ions[elandmarks[lint + 1]]
                vc_exp_landmark_back = raw_ions[elandmarks[lint]]

                #update volume fraction to use based on new landmarks
                vol_frac = (lvolumes[lint + 1] - lvolumes[lint])/(vc_exp_landmark_front - vc_exp_landmark_back)
                print("landmark: " + str(m_ind))

            #temporally interpolate between projection 1 and projection 2
            intv2 = (rvol - rvolumes[m_ind])
            intv1 = (rvolumes[m_ind + 1] - rvol)
            intnorm = (rvolumes[m_ind + 1] - rvolumes[m_ind])

            #spatially interpolate from the grid
            indx = int((xshape[0]) * (dx[a] - minxx)/((maxxx - minxx + 1e-50)))
            indy = int((yshape[1]) * (dy[a] - minxy)/((maxxy - minxy + 1e-50)))

            #determine final ion reconstruction position
            cx = (spx2[indx, indy] * intv2 + spx1[indx, indy] * intv1)/intnorm
            cy = (spy2[indx, indy] * intv2 + spy1[indx, indy] * intv1)/intnorm
            cz = (spz2[indx, indy] * intv2 + spz1[indx, indy] * intv1)/intnorm

            cc = chem[a]
            reconed[a] = True
            #append ion to the model driven reconstruction
            rcx.append(cx)
            rcy.append(cy)
            rcz.append(cz)
            rchem.append(cc)

            #iterate ion counter
            a += 1
            if experimental == True:
                rvol += vol_frac * ic[cc]
            else:
                rvol += vol_frac * ic[a]

    return rcx, rcy, rcz, rchem, reconed

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--mapping_data', metavar='mapping_data', type = str, default = "",
                        help='.tsv containing model simulated mapping data')
    parser.add_argument('--image_transfer', metavar='detector transfer function', type = str,
                        default = "0.102, 0.0, 0.0, 0.0, 0.8",
                        help='List of image transfer parameters. This variable should have the ' +
                             'following structure (list) [flight path, dx-transform, dy-transform, theta, ICF]')
    parser.add_argument('--landmark_calib_file', metavar='The landmark configuration file', type = str, default = "",
                        help='The pre-generated landmark calibration file (generated via the landmark_fitting module)')
    parser.add_argument('--projection_calib_file', metavar='The projection configuration file', type = str, default = "",
                        help='The pre-generated projection calibration file (generated via the landmark_fitting module)')
    parser.add_argument('--dirname', metavar='directory name', type = str, default = "",
                        help='The directory containing the model simulated evaporation data')
    parser.add_argument('--apt_data', metavar='atom probe data', type = str, default = "",
                        help='atom probe data: either .epos, .ato, or .tsv')
    parser.add_argument('--rangefile', metavar='range file', type = str, default = "", help='The range file for labelling ions within the reconstruction')
    parser.add_argument('--out_recon_file', metavar='The output reconstruction filename', type = str, default = "", help='The output reconstruction filename')
    parser.add_argument('--material_db', metavar='materials database', type = str, default = "c3d_input/materials", help='The location of the sqlite materials database')
    args = parser.parse_args()

    #simulation parameter definition
    mapping_data = args.mapping_data
    dirname = args.dirname
    landmark_calibration_file = args.landmark_calib_file
    projection_calibration_file = args.projection_calib_file
    apt_data = args.apt_data
    rangefile = args.rangefile
    out_recon_file = args.out_recon_file
    material_db = args.material_db
    image_transfer = args.image_transfer.split(",")
    image_transfer = [float(a) for a in image_transfer]

    if len(mapping_data) == 0:
        raise ValueError("model mapping data tsv must be parsed to --mapping_data argument")
    if len(landmark_calibration_file) == 0:
        raise ValueError("landmark calibration file must be parsed to --landmark_calib_file argument")
    if len(projection_calibration_file) == 0:
        raise ValueError("projection calibration file must be parsed to --projection_calib_file argument")
    if len(dirname) == 0:
        raise ValueError("simulation directory must be parsed to --dirname argument")
    if len(apt_data) == 0:
        raise ValueError("apt data file must be parsed to --apt_data argument")
    if len(out_recon_file) == 0:
        raise ValueError("output reconstruction filename must be parsed to --out_recon_file argument")

    recon(mapping_data, dirname, landmark_calibration_file, projection_calibration_file, apt_data, rangefile, out_recon_file, material_db, image_transfer)
