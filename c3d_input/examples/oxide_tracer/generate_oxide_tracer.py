#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#add current working directory to system path
import os,sys,inspect
sys.path.insert(0,'')

from casra_3d.geometries import generate_model_geometry as generate_model_geometry
import numpy as np

###example commmands###
filename = "c3d_input/examples/oxide_tracer/oxide_tracer.mstate"
directory = "c3d_input/examples/oxide_tracer"

#simulation parameters - for conversion
#scale = 35nm

generate_model_geometry.generate_tip(5.0, 1.7, 12, filename, directory, name = "bulk", offset = 0.2)

c0 = np.array([0.0, 0.0, 11.8])
R0 = 1.45
R1 = 1.75

generate_model_geometry.generate_shell(c0, R0, R1, filename, "shell", directory, res=100, surf=0, alpha = 0.3)