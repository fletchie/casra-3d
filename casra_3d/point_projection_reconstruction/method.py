
"""

Module for generating point-projection reconstructions from input data

Author: Charles Fletcher
Atom Probe Tomography Group, Department of Materials, University of Oxford
May 2020

"""

import csv
import copy
import sys
import os

try:
    import numpy as np
except ModuleNotFoundError:
    raise ModuleNotFoundError("Could not import Python package numpy. Please make sure it is installed.")

from casra_3d.point_projection_reconstruction import point_projection_mapping as ppm
from casra_3d.point_projection_reconstruction import loader as loader
from casra_3d.io import model_loaders as model_loaders
from casra_3d.io import model_writers as model_writers

def data_reconstruction(filename, out_dir, param_filename, sampling = 1.0, rangefile = "", free_params = {}):

    """

    Function for performing the point-projection reconstruction of APT data.
    Data can be loaded in from a .tsv, .epos, .ato
    Outputs a .vtk file containing a point cloud of the reconstruction

    :param1 filename: The APT experiment input data
    :param2 out_dir: The output directory for the reconstruction
    :param3 param_filename: The filename for the .tsv containing reconstruction parameter.
    :param4 sampling: The sampling rate for the reconstruction (fraction of atoms reconstructed).
    :param5 rangefile: The range file for the inputed APT data (not required for .tsv)
    :param6 free_params: Dictionary containing parameters to overide those in param_filename

    :returns:
        :output1 ions: list of ion labels (index + 1 corresponds to chem)
        :output2 ic: Number of nuclei per ion

    """

    #creating output directory
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    ###initialise lists to read in APT data
    print("Loading dataset...")
    dx, dy, sx, sy, sz, chem, ions, ic, VP, da, da_p, experimental = model_loaders.load_apt_data(filename, rangefile, sampling)

    dx = dx[chem != 0]
    dy = dy[chem != 0]
    sx = sx[chem != 0]
    sy = sy[chem != 0]
    sz = sz[chem != 0]
    da = da[chem != 0]
    chem = chem[chem != 0]

    print("dataset ions: " + str(len(dx)))
    parameters = loader.read_reconstruction_parameters(param_filename, dx)

    #overide .tsv loaded parameters with free parameter values (passed by dictionary)
    for param in free_params:
        parameters[param] = free_params[param]

    ICF = parameters["ICF"]
    R0 = parameters["R0"]
    alpha = parameters["alpha"]
    scale =parameters["scale"]
    ionic_volume = parameters["ionic_volume"]
    Ix = parameters["Ix"]
    Iy = parameters["Iy"]
    flipxy = parameters["flipxy"]
    lower = parameters["lower"]
    upper = parameters["upper"]
    offx = parameters["offx"]
    offy = parameters["offy"]
    offz = parameters["offz"]
    detector_efficiency = parameters["detector_efficiency"]

    #flight path - this is the virtual flight path rather than actual (accounting for the reflection)
    flight_path = parameters["flight_path"]
    det_rad = parameters["det_rad"]

    #flip xy if true
    if flipxy == 1:
        dx1 = copy.deepcopy(dx)
        dx = copy.deepcopy(dy)
        dy = dx1

    #calculate the detector area
    det_area = np.pi * det_rad**2.0

    #filter out ions outside of the detector radius
    rad = np.sqrt(dx**2.0 + dy**2.0)
    ft = rad < det_rad

    if len(sx) == len(dx):
        sx = sx[ft]
        sy = sy[ft]
        sz = sz[ft]

    dx = dx[ft]
    dy = dy[ft]
    chem = chem[ft]

    sample_num = int(1.0/sampling)
    dv = ionic_volume * sample_num

    #perform point projection reconstruction
    result = ppm.reconstruction(dx, dy, chem, da, dv, R0, [lower, upper], 1, flight_path, det_rad, alpha, ICF, detector_efficiency)

    #assign reconstruction output and perform user defined translation and reflection
    tz = np.array(result[2::5])
    tz = offz + tz - np.max(tz)
    tx = np.array(result[::5]) * (1.0 - 2.0 * Ix) - offx
    ty = -np.array(result[1::5]) * (1.0 - 2.0 * Iy) - offy

    if len(sx) == len(dx):
        tsx = sx[lower:upper]
        tsy = sy[lower:upper]
        tsz = sz[lower:upper]

    #output reconstructed data to .vtk
    points = np.vstack((tx, ty, tz)).T
    point_ind = np.linspace(0, len(points), len(points), endpoint = False).astype(int)
    cells = point_ind.reshape(-1, 1)
    print("outputing reconstruction to: " + out_dir + "/recon.vtk")
    if da_p == True:
        point_data = {"chemistry": np.array(result[3::5], dtype = float), "Da": np.array(result[4::5])}
        model_writers.write_vtk(out_dir + "/recon.vtk", points, cells, point_data = point_data)
    else:
        point_data =  {"chemistry": np.array(result[3::5], dtype = float)}
        model_writers.write_vtk(out_dir + "/recon.vtk", points, cells, point_data = point_data)

    H, edges = np.histogramdd(np.vstack((tx, ty, tz)).T, bins = 30)
    bv = ((np.max(edges[0]) - np.min(edges[0])) * (np.max(edges[1]) - np.min(edges[1])) * (np.max(edges[2]) - np.min(edges[2])))/(30**3.0)
    print("true recon volume: " + str(np.sum(H > 0) * bv))
    print("reconstruction density: " + str(len(tx)/(np.sum(H > 0) * bv)))

    H, edges = np.histogramdd(np.vstack((tsx, tsy, tsz)).T, bins = 30)
    bv = ((np.max(edges[0]) - np.min(edges[0])) * (np.max(edges[1]) - np.min(edges[1])) * (np.max(edges[2]) - np.min(edges[2])))/(30**3.0)
    print("true density: " + str(len(tsx)/(np.sum(H > 0) * bv)))

    #if original coordinates detected, also output  to .vtk
    if len(sx) == len(dx):
        print("Original coordinates detected and outputed.")
        points = np.vstack((tsx, tsy, tsz)).T
        point_ind = np.linspace(0, len(points), len(points), endpoint = False).astype(int)
        cells = point_ind.reshape(-1, 1)
        print("outputing original coordinates to: " + out_dir + "/original_pos.vtk")
        point_data = {"chemistry": np.array(result[3::5], dtype = float)}
        model_writers.write_vtk(out_dir + "/original_pos.vtk", points, cells, point_data = point_data)

    return ions, ic, dx, dy

#if called from command line
if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument('--input_data', metavar='input data', type = str, default = "", help='')
    parser.add_argument('--output', metavar='output directory will save: original coordinates to original.vtk, reconstructed coordinates to recon.vtk', type = str, default = "", help='')
    parser.add_argument('--parameters', metavar='tsv file containing point-projection reconstruction parameters', type = str, default = "", help='')
    parser.add_argument('--sample_frac', metavar='The sample fraction to use within the reconstruction', type = float, default = 1.0, help='')
    parser.add_argument('--rangefile', metavar='The range file for labelling ions within the reconstruction', type = str, default = "", help='')

    args = parser.parse_args()

    #simulation parameter definition
    filename = args.input_data
    out_dir = args.output
    param_filename = args.parameters
    rangefile = args.rangefile
    sampling = args.sample_frac

    if len(filename) == 0:
        raise ValueError("input data file must be parsed to --input_data argument")
    if len(out_dir) == 0:
        raise ValueError("output directory must be parsed to --output argument")
    if len(param_filename) == 0:
        raise ValueError("reconstruction parameters file must be parsed to --parameters argument")

    ions, ic, dx, dy = data_reconstruction(filename, out_dir, param_filename, sampling, rangefile)
