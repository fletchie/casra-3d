import numpy as np
cimport numpy as np
from libc.math cimport floor
from cython cimport boundscheck, wraparound, nonecheck, cdivision

DTYPE = np.float
ctypedef np.float_t DTYPE_t

@boundscheck(False)
@wraparound(False)
@nonecheck(False)
def interp3D(DTYPE_t[:,:,::1] v, DTYPE_t[:,:,::1] xs, DTYPE_t[:,:,::1] ys, DTYPE_t[:,:,::1] zs):

    cdef int X, Y, Z
    X,Y,Z = v.shape[0], v.shape[1], v.shape[2]
    cdef np.ndarray[DTYPE_t, ndim=3] interpolated = np.zeros((X, Y, Z), dtype=DTYPE)

    _interp3D(&v[0,0,0], &xs[0,0,0], &ys[0,0,0], &zs[0,0,0], &interpolated[0,0,0], X, Y, Z)
    return interpolated

@boundscheck(False)
@wraparound(False)
@nonecheck(False)
def interp2D(DTYPE_t[:,::1] v, DTYPE_t[:,::1] xs, DTYPE_t[:,::1] ys):

    cdef int X, Y
    X,Y = v.shape[0], v.shape[1]
    cdef np.ndarray[DTYPE_t, ndim=2] interpolated = np.zeros((X, Y), dtype=DTYPE)

    _interp2D(&v[0,0], &xs[0,0], &ys[0,0], &interpolated[0,0], X, Y)
    return interpolated


@cdivision(True)
cdef inline void _interp3D(DTYPE_t *v, DTYPE_t *x_points, DTYPE_t *y_points, DTYPE_t *z_points,
               DTYPE_t *result, int X, int Y, int Z):

    cdef:
        int i, x0, x1, y0, y1, z0, z1, dim
        DTYPE_t x, y, z, xd, yd, zd, c00, c01, c10, c11, c0, c1, c

    dim = X*Y*Z

    for i in range(dim):
        x = x_points[i]
        y = y_points[i]
        z = z_points[i]

        x0 = <int>floor(x)
        x1 = x0 + 1
        y0 = <int>floor(y)
        y1 = y0 + 1
        z0 = <int>floor(z)
        z1 = z0 + 1

        xd = (x-x0)/(x1-x0)
        yd = (y-y0)/(y1-y0)
        zd = (z-z0)/(z1-z0)

        if x0 >= 0 and y0 >= 0 and z0 >= 0:
            c00 = v[Y*Z*x0+Z*y0+z0]*(1-xd) + v[Y*Z*x1+Z*y0+z0]*xd
            c01 = v[Y*Z*x0+Z*y0+z1]*(1-xd) + v[Y*Z*x1+Z*y0+z1]*xd
            c10 = v[Y*Z*x0+Z*y1+z0]*(1-xd) + v[Y*Z*x1+Z*y1+z0]*xd
            c11 = v[Y*Z*x0+Z*y1+z1]*(1-xd) + v[Y*Z*x1+Z*y1+z1]*xd

            c0 = c00*(1-yd) + c10*yd
            c1 = c01*(1-yd) + c11*yd

            c = c0*(1-zd) + c1*zd

        else:
            c = 0

        result[i] = c

@cdivision(True)
cdef inline void _interp2D(DTYPE_t *v, DTYPE_t *x_points, DTYPE_t *y_points,
               DTYPE_t *result, int X, int Y):

    cdef:
        int i, x0, x1, y0, y1, dim
        DTYPE_t x, y, xd, yd, c00, c01, c

    dim = X*Y

    for i in range(dim):
        x = x_points[i]
        y = y_points[i]

        x0 = <int>floor(x)
        x1 = x0 + 1
        y0 = <int>floor(y)
        y1 = y0 + 1

        xd = (x-x0)/(x1-x0)
        yd = (y-y0)/(y1-y0)

        if x0 >= 0 and y0 >= 0 and x0 < (X - 1) and y0 < (Y - 1):
            c00 = v[Y*x0+y0]*(1-xd) + v[Y*x1+y0]*xd
            c01 = v[Y*x0+y1]*(1-xd) + v[Y*x1+y1]*xd

            c = c00*(1-yd) + c01*yd
        else:
            c = 0

        result[i] = c

#filter out boundary density values
@boundscheck(False)
@wraparound(False)
@nonecheck(False)
@cdivision(True)
def erode_recon(double[:, :, ::1] kde_vals, long[:, :, ::1] bin_vals, long binx, long biny,  long binz, double fill):

    cdef int a, b, c
    cdef int lenx, leny, lenz

    lenx = binx
    leny = biny
    lenz = binz

    nkde_vals = np.zeros((lenx, leny, lenz), dtype = "double")
    nbin_vals = np.zeros((lenx, leny, lenz), dtype = "long")
    cdef double[:, :, ::1] nkde_vals_view = nkde_vals
    cdef long[:, :, ::1] nbin_vals_view = nbin_vals

    cdef float temp1
    cdef long temp2

    for a in range(1, binx - 2):
        for b in range(1, biny - 2):
            for c in range(1, binz - 2):
                temp1 = kde_vals[a, b, c]
                nkde_vals_view[a, b, c] = temp1
                temp2 = bin_vals[a, b, c]
                nbin_vals_view[a, b, c] = temp2
                if bin_vals[a, b, c] * bin_vals[a + 1, b, c] * bin_vals[a-1, b, c]  * bin_vals[a, b + 1, c] * bin_vals[a + 1, b + 1, c] * bin_vals[a-1, b + 1, c] * bin_vals[a, b - 1, c] * bin_vals[a + 1, b - 1, c] * bin_vals[a-1, b - 1, c] == 0:
                    if bin_vals[a, b, c + 1] * bin_vals[a + 1, b, c + 1] * bin_vals[a-1, b, c + 1]  * bin_vals[a, b + 1, c + 1] * bin_vals[a + 1, b + 1, c + 1] * bin_vals[a-1, b + 1, c + 1] * bin_vals[a, b - 1, c + 1] * bin_vals[a + 1, b - 1, c + 1] * bin_vals[a-1, b - 1, c + 1] == 0:
                        if bin_vals[a, b, c - 1] * bin_vals[a + 1, b, c - 1] * bin_vals[a-1, b, c - 1]  * bin_vals[a, b + 1, c - 1] * bin_vals[a + 1, b + 1, c - 1] * bin_vals[a-1, b + 1, c - 1] * bin_vals[a, b - 1, c - 1] * bin_vals[a + 1, b - 1, c - 1] * bin_vals[a-1, b - 1, c - 1] == 0:
                            nkde_vals_view[a, b, c] = fill
                            nbin_vals_view[a, b, c] = 0

    return nkde_vals, nbin_vals
