
"""

Module for performing ion projection (constant per element and evaporation rate dependent)

Author: Charles Fletcher
Atom Probe Tomography Group, Department of Materials, University of Oxford
May 2020

"""

import math
import random
from multiprocessing import Pool
import os
from functools import partial
import sys
import copy

try:
    import numpy as np
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find the required Python module numpy. Please make sure it is installed.")
try:
    import scipy
    import scipy.integrate
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find the required Python module scipy. Please make sure it is installed.")

from casra_3d.electrostatic_solver.cbem import BEM_matrix_construction as BEM_matrix_construction
from casra_3d.electrostatic_solver.cbem import coctree as coctree
from casra_3d.ls_operators import sdf as sdf
from casra_3d.mesh.non_cgal import mesh_library as mesh_library
from casra_3d.io import model_writers as model_writers

#append casra_3d directory to system path for pickling in multiprocessing to work correctly
sys.path.append("casra_3d")

class ion:
    def __init__(self, p, v, q = + 1.0, m = 1.0, detector_coordinate = 0.12, detector_voltage = 1.0, real_detector = True, proj_dist = 1000*1e-9):

        """

        Method for initialising ion instance

        :param1 self: The ion instance
        :param2 p: The initial ion position
        :param3 v: Initial ion velocity
        :param4 domain: The projection domain
        :param5 q: Ion charge
        :param6 m: Ion mass
        :param7 detector_coordinate: The detector coordinates
        :returns: 0

        """

        self.q = q
        self.p = np.array([p[0], p[1], p[2]]) #positions need redefining
        self.prev_pos = np.array([p[0], p[1], p[2]])
        self.v = v
        self.m = m

        self.detector_coordinate = detector_coordinate
        self.detector_voltage = detector_voltage

        self.active = True

        self.first_it_field = [False]

        #origin (boundary element)
        self.origin = p
        self.detector_impact = None

        self.prev_dist = 0.0
        self.current_dist = 0.0
        self.proj_dist = proj_dist
        self.real_detector = real_detector


    def iterate(self, croot, BP, BF, verts, faces, cp, areas, norms, rel_sim_tol, abs_sim_tol, ls2, eps):

        """

        Performs ion trajectory integration until termination condition is reached (see `solout`)

        :param1 self: The ion instance
        :param2 croot: The octree object
        :param3 BP: The tip surface boundary element potentials
        :param4 BF: The tip surface boundary element fluxes
        :param5 verts: The mesh vertices
        :param6 faces: The mesh connectivity matrix
        :param7 cp: The mesh panel centre points
        :param8 areas: The panel areas
        :param9 norms: The mesh panel normals
        :param10 rel_sim_tol: The relative RK5 integrator tolerance
        :param11 abs_sim_tol: The absolute RK5 integrator tolerance
        :param12 ls2: Array of longest panel edge magnitude
        :param13 eps: adaptive panel integration tolerance (admissability for switching between 7-point and 1-point quadrature)

        :returns: 0

        """

        #calculate initial distance from specimen centre
        self.c = np.mean(cp, axis=0)
        self.max_rad = np.max(np.linalg.norm(cp - self.c, axis=1))
        self.current_dist = 0.0
        self.prev_dist = 0.0
        self.proj_dist += self.max_rad

        self.prev_pos = self.p
        self.time_step = []
        self.trajectory = []
        y0 = [self.p[0], self.p[1], self.p[2], 0.0, 0.0, 0.0]
        self.trajectory.append(y0)
        integrator = scipy.integrate.ode(self.newton).set_integrator('dop853', rtol=rel_sim_tol, atol=abs_sim_tol)
        integrator.set_solout(self.solout)
        integrator.set_initial_value(y0, 0.05).set_f_params(self.m, self.q, croot, BP, BF, faces, verts, cp, areas, norms, ls2, len(faces), eps)
        integrator.integrate(math.inf)

    def newton(self, t, y, m, q, croot, BP, BF, faces, verts, cp, areas, norms, ls2, f_l, eps):

        """

        One step of the ion trajectory integrator

        :param1 self: The ion instance
        :param2 t: The current projection time
        :param3 y: The phase space vector
        :param4 m: The ion mass
        :param5 q: The ion charge
        :param2 croot: The octree object
        :param3 BP: The tip surface boundary element potentials
        :param4 BF: The tip surface boundary element fluxes
        :param5 verts: The mesh vertices
        :param6 faces: The mesh connectivity matrix
        :param7 cp: The mesh panel centre points
        :param8 areas: The panel areas
        :param9 norms: The mesh panel normals
        :param10 ls2: Array of longest panel edge magnitude
        :param11 f_l: Number of panel faces
        :param12 eps: adaptive panel integration tolerance (admissability for switching between 7-point and 1-point quadrature)
        :returns: The phase space gradient vector

        """
        px, py, pz, vx, vy, vz = y


        #det_voltage = 1.5
        #flight_path = 0.12

        ###calculate electric field via octree
        Ex, Ey, Ez = croot.calculate_field(y[:3], verts, faces, cp, areas, norms, BP, BF, 0.1, ls2, 1)
        #print(Ex, Ey, Ez)
        #add on z-field bias from chamber
        #Ez = Ez - self.detector_voltage/self.detector_coordinate

        ###non-octree field calculate
        #Ex, Ey, Ez = BEM_matrix_construction.field_calculation_adaptive(y[:3], verts, faces, cp, areas, norms, BP, BF, ls2, len(faces), eps)

        dydt = [vx, vy, vz, -q*Ex/m, -q*Ey/m, -q*Ez/m]
        return dydt

    #dynamic breaker for trajectory integrator
    def solout(self, t, y):
        """

        dynamic breaker for the trajectory integrator. Trajectory integration terminated
        on ion intersecting detector or exiting domain

        :param1 self: The ion instance
        :param2 t: The ion integration time
        :param3 y: The phase space vector
        :returns:
            -1 -- If termination condition reach
            0 -- If integration should continue

        """
        self.time_step.append(t)
        self.trajectory.append([*y])

        self.prev_dist = self.current_dist
        self.current_dist = ((y[0] - self.c[0])**2.0 + (y[1] - self.c[1])**2.0 + (y[2] - self.c[2])**2.0)**0.5

        #project up to real detector
        if self.real_detector == True:
            if y[2] > self.detector_coordinate:
                return -1
            else:
                return 0
        else: #project up to set distance from specimen
            if self.current_dist > self.proj_dist:
                return -1
            else:
                return 0

def derive_mapping(geom, detector_coordinate, rel_sim_tol, abs_sim_tol,
                   areasc, normsc, u, qc, verts, faces, sample_surface,
                   verts2, faces2, sample_surface2, BEM_adist, real_detector = False):

    """

    Project single ion per panel centre point
    Uses the python3 multiprocessing module to parallelise the ion projection

    :param1 geom: The simulation geometry object
    :param2 detector_coordinate: The z-detector coordinate
    :param3 rel_sim_tol: The relative trajectory integrator tolerance (RK5 adaptive)
    :param4 abs_sim_tol: The absolue trajectory integrator tolerance (RK5 adaptive)
    :param5 areasc: The surface element areas
    :param6 normsc: The surface element normal vectors
    :param7 u: The potential solution (per panel centre collocation point)
    :param8 qc: The normal flux solution (per panel centre collocation point)
    :param9 verts: The mesh vertices
    :param10 faces: The mesh elements (connectivity matrix)
    :param11 sample_surface: Whether elements are sample surface or not
    :param12 BEM_adist: The adaptive quadrature threshold (from 7-point to single-point quadrature panel integration)
    :returns:
        sample_space - ion surface launch positions
        det_space - the detector impact coordinates
        chemistry - The ion phase database material_id (determined by initial launch position)
        ion_phase - The ion phase name (determined by initial launch position)
        trajectories - the ion trajectories
    """

    print("calculating projection ...")

    #filter out non sample surface boundaries - ensures only sample surface is used during ion projection (both field solution and projection sites)
    lf = sample_surface == 1
    #faces = faces[sample_surface == 1]
    #qc = qc[sample_surface == 1]
    #areasc = areasc[sample_surface == 1]
    #normsc = normsc[sample_surface == 1]
    #u = u[sample_surface == 1]

    cp = np.mean(verts[faces], axis =  1)
    z_max = np.max(cp[:, 2])
    z_min = np.min(cp[:, 2])

    projection_cuttoff = cp[:, 2]> np.max(cp[:, 2]) - 8.0 * geom['simulation_grid']['cell_width']
    cp_proj = cp[lf * projection_cuttoff]
    projection_ind = np.array(np.linspace(0, len(faces), len(faces), endpoint = False, dtype = "int"))[lf * projection_cuttoff].reshape(-1, 1)

    sample_space = []
    det_space = []
    trajectories = []
    chemistry = []
    norms = []

    #calculate ls2 - longest element side
    ls2x = np.sum((verts[faces[:, 1]] -  verts[faces[:, 0]]) * (verts[faces[:, 1]] -  verts[faces[:, 0]]), axis = 1)**0.5
    ls2y = np.sum((verts[faces[:, 2]] -  verts[faces[:, 1]]) * (verts[faces[:, 2]] -  verts[faces[:, 1]]), axis = 1)**0.5
    ls2z = np.sum((verts[faces[:, 0]] -  verts[faces[:, 2]]) * (verts[faces[:, 0]] -  verts[faces[:, 2]]), axis = 1)**0.5
    ls2 = np.maximum(np.maximum(ls2x, ls2y), ls2z)**2.0

    #Calculate panel area via Heron's formula
    s = (ls2x + ls2y + ls2z)/2.0
    panel_area = (s * (s - ls2x) * (s - ls2y) * (s - ls2z))**0.5

    #calculates shortest (normal distance) between panel and future surface -
    #used to estimate local evaporated volume for calculating ion projection density
    if len(verts2) > 0:
        faces2 = faces2[sample_surface2 == 1]
        tevap_rate = mesh_library.shortest_surface_distance(verts2 * 1e9,
                                                           cp_proj * 1e9,
                                                           faces2)

        #scale by panel area
        tevap_rate = tevap_rate #* panel_area[lf * projection_cuttoff]

        evap_rate = np.array([0.0] * len(cp))
        evap_rate[lf * projection_cuttoff] = tevap_rate
    else:
        evap_rate = np.array([0.0] * len(cp))


    #calculate normals
    vert_norms = np.zeros((len(verts), 3))
    for j in range(0, len(cp)):

        y0 = verts[faces[j, 0]]
        y1 = verts[faces[j, 1]]
        y2 = verts[faces[j, 2]]

        n = np.cross(y1 - y0, y2 - y0)
        n = n/np.linalg.norm(n)
        norms.append(n)

        vert_norms[faces[j, 0]] += 1/3.0 * n
        vert_norms[faces[j, 1]] += 1/3.0 * n
        vert_norms[faces[j, 2]] += 1/3.0 * n

    norms = np.array(norms)
    norms_proj = norms[lf * projection_cuttoff]

    #######uncomment to add additional ion projection from panel corners#######
    launch_corners = True
    if (launch_corners == True):
        verts_projection_cuttoff = verts[:, 2] > np.max(cp[:, 2]) - 8.0 * geom['simulation_grid']['cell_width']
        verts_proj = verts[verts_projection_cuttoff]
        vert_norms_proj = vert_norms[verts_projection_cuttoff]
        cp_proj = verts_proj
        norms_proj = vert_norms_proj
        projection_ind = mesh_library.adjacency_list_vertex_faces(verts.astype("double"), faces.astype("long"))
        #print(verts_projection_cuttoff)
        projection_ind = [projection_ind[a] for a in range(len(projection_ind)) if verts_projection_cuttoff[a] == True]

    ############################
    projection_ind = [[str(col) for col in row] for row in projection_ind]

    croot = coctree.construct_octree(verts, faces, cp)
    croot.calculate_moments(verts, faces, cp, areasc, normsc, u, qc)

    ###This is the section that can be parallelised###
    pool = Pool(os.cpu_count())
    lc = np.linspace(0, len(cp_proj), len(cp_proj), dtype = int, endpoint = False)
    launch_coords = cp_proj + norms_proj[:, :3] * 0.5 * geom["simulation_grid"]["cell_width"]

    partial_ = partial(single_ion_projection, launch_coords,
                       cp_proj, cp, croot, geom, detector_coordinate,
                       rel_sim_tol, abs_sim_tol, areasc,
                       normsc, u, qc, verts, faces, sample_surface,
                       BEM_adist, ls2, real_detector)
    data_outputs = pool.map(partial_, lc)
    pool.close()
    pool.join()
    ###end of parallelisation###

    chemistry = []
    ion_phase = []
    trajectories = []
    sample_space = []
    det_space = []
    panel_index = []
    vel = []

    failed_ions = np.array([1] * len(cp_proj), dtype = int)

    for j in range(0, len(cp_proj)):
        #remove projection singularity calculations
        if np.sum(abs(data_outputs[j][4]) > 1e10) == 0:
            sample_space.append(data_outputs[j][0])
            det_space.append(data_outputs[j][1])
            chemistry.append(data_outputs[j][2])
            ion_phase.append(data_outputs[j][3])
            trajectories.append(data_outputs[j][4])
            panel_index.append(projection_ind[j])
            vel.append(data_outputs[j][5])
            failed_ions[j] = 0

    vel = np.array(vel)
    chemistry = np.array(chemistry)
    #normalise velocity
    vel = vel/(np.linalg.norm(vel, axis=1).reshape(-1, 1))

    ###calculate ion stability###
    launch_cp = [[] for a in range(len(cp))]
    nions = [0 for a in range(len(cp))]
    itc = 0
    for a in range(len(projection_ind)):
        if failed_ions[a] == 0:
            for b in range(len(projection_ind[a])):
                launch_cp[int(projection_ind[a][b])].append(itc)
                nions[int(projection_ind[a][b])] += 1
            itc += 1

    sample_space = np.array(sample_space)
    det_space = np.array(det_space)
    evap_rate = np.array(evap_rate)

    #create list of faces containing vertex indices for which ions were launched from
    launch_cp = [launch_cp[a] for a in range(len(launch_cp)) if nions[a] == 3]

    stability, lcp, dcp, conformal, J00, J10, J01, J11 = trajectory_stability(launch_cp, verts, sample_space, det_space)

    #from casra_3d.io import model_writers as model_writers
    #model_writers.write_vtk("stab/test.vtk", lcp, np.linspace(0, len(lcp), len(lcp), endpoint = False, dtype = int).reshape(-1, 1), point_data = {"stability": np.array(stability), "conformal": np.array(conformal)})
    #model_writers.write_vtk("stab/dctest.vtk", dcp, np.linspace(0, len(dcp), len(dcp), endpoint = False, dtype = int).reshape(-1, 1), point_data = {"stability": np.array(stability), "conformal": np.array(conformal)})
    launch_cp = np.array(launch_cp)
    dcz = np.array([detector_coordinate] * len(launch_cp)).reshape(-1, 1)

    #find panel index
    pind = []
    for a in range(len(launch_cp)):
        tcp = np.mean(sample_space[launch_cp[a, :], :], axis=0)
        ind = np.argmin(np.linalg.norm(tcp - cp, axis=1))
        pind.append(ind)

    stability = {
    "sv0": sample_space[launch_cp[:, 0], :],
    "sv1": sample_space[launch_cp[:, 1], :],
    "sv2": sample_space[launch_cp[:, 2], :],
    "dv0": det_space[launch_cp[:, 0], :],
    "dv1": det_space[launch_cp[:, 1], :],
    "dv2": det_space[launch_cp[:, 2], :],
    "vel0": vel[launch_cp[:, 0], :],
    "vel1": vel[launch_cp[:, 1], :],
    "vel2": vel[launch_cp[:, 2], :],
    "jacobian": np.array(stability),
    "J00": np.array(J00),
    "J10": np.array(J10),
    "J01": np.array(J01),
    "J11": np.array(J11),
    "conformality": np.array(conformal),
    "panel_index": pind,
    "evap_rate": evap_rate[pind]}

    return sample_space, det_space, vel, chemistry, ion_phase, trajectories, panel_index, stability

def launch_ions(launch_coords, original_coords, marker, geom,
                detector_coordinate, rel_sim_tol, abs_sim_tol,
                areasc, normsc, u, qc, verts, faces, sample_surface, BEM_adist,
                real_detector):

    """

    Project single ion per panel centre point
    Uses the python3 multiprocessing module to parallelise the ion projection

    :param1 geom: The simulation geometry object
    :param2 detector_coordinate: The z-detector coordinate
    :param3 rel_sim_tol: The relative trajectory integrator tolerance (RK5 adaptive)
    :param4 abs_sim_tol: The absolue trajectory integrator tolerance (RK5 adaptive)
    :param5 areasc: The surface element areas
    :param6 normsc: The surface element normal vectors
    :param7 u: The potential solution (per panel centre collocation point)
    :param8 qc: The normal flux solution (per panel centre collocation point)
    :param9 verts: The mesh vertices
    :param10 faces: The mesh elements (connectivity matrix)
    :param11 sample_surface: Whether elements are sample surface or not
    :param12 BEM_adist: The adaptive quadrature threshold (from 7-point to single-point quadrature panel integration)
    :returns:
        sample_space - ion surface launch positions
        det_space - the detector impact coordinates
        chemistry - The ion phase database material_id (determined by initial launch position)
        ion_phase - The ion phase name (determined by initial launch position)
        trajectories - the ion trajectories
    """

    print("calculating projection ...")

    #filter out non sample surface boundaries - ensures only sample surface is used during ion projection (both field solution and projection sites)
    cp = np.mean(verts[faces], axis =  1)

    sample_space = []
    det_space = []
    trajectories = []
    chemistry = []
    norms = []

    #calculate ls2 - longest element side squared
    ls2x = np.sum((verts[faces[:, 1]] -  verts[faces[:, 0]]) * (verts[faces[:, 1]] -  verts[faces[:, 0]]), axis = 1)
    ls2y = np.sum((verts[faces[:, 2]] -  verts[faces[:, 1]]) * (verts[faces[:, 2]] -  verts[faces[:, 1]]), axis = 1)
    ls2z = np.sum((verts[faces[:, 0]] -  verts[faces[:, 2]]) * (verts[faces[:, 0]] -  verts[faces[:, 2]]), axis = 1)
    ls2 = np.maximum(np.maximum(ls2x, ls2y), ls2z)


    croot = coctree.construct_octree(verts, faces, cp)
    croot.calculate_moments(verts, faces, cp, areasc, normsc, u, qc)

    ###This is the section that can be parallelised###
    pool = Pool(os.cpu_count())
    lc = np.linspace(0, len(launch_coords), len(launch_coords), dtype = int, endpoint = False)
    partial_ = partial(single_ion_projection, launch_coords, original_coords,
                       cp, croot, geom, detector_coordinate, rel_sim_tol,
                       abs_sim_tol, areasc, normsc, u, qc, verts,
                       faces, sample_surface, BEM_adist, ls2, real_detector)
    data_outputs = pool.map(partial_, lc)
    pool.close()
    pool.join()
    ###end of parallelisation###

    chemistry = []
    ion_phase = []
    trajectories = []
    sample_space = []
    det_space = []
    panel_index = []

    for j in range(0, len(launch_coords)):
        #remove projection singularity calculations
        if np.sum(abs(data_outputs[j][4]) > 1e10) == 0:
            sample_space.append(data_outputs[j][0])
            det_space.append(data_outputs[j][1])
            chemistry.append(data_outputs[j][2])
            ion_phase.append(data_outputs[j][3])
            trajectories.append(data_outputs[j][4])
            panel_index.append(marker[j])

    chemistry = np.array(chemistry)
    panel_index = np.array(panel_index)

    print("projection complete")
    return sample_space, det_space, chemistry, ion_phase, trajectories, panel_index

def single_ion_projection(launch_coords, phase_coords, cp, croot, geom,
                          detector_coordinate, rel_sim_tol, abs_sim_tol,
                          areasc, normsc, u, qc, verts, faces, sample_surface,
                          BEM_adist, ls2, real_detector, i):

        """

        Function for projecting single ion under parallelisation (via Python multiprocessing)
        governs the projection of a single ion

        :param1 cp_proj: Array of centre points of panels from which ions are projected from
        :param2 norms_proj: Array of normals of panels from which ions are projected from
        :param2 cp: Array of panel centre points
        :param4 croot: The octree object
        :param5 geom: The simulation dictionary
        :param6 detector_coordinate: The detector z coordinate
        :param7 rel_sim_tol: The relative RK5 simulation tolerance
        :param8 abs_sim_tol: The absolute RK5 simulation tolerance
        :param9 areasc: Array of panel areas
        :param10 normsc: Array of panel normals
        :param11 u: Array of panel pontentials
        :param12 qc: Array of panel normal fluxes
        :param13 verts: Array of mesh panel vertices
        :param14 faces: mesh connectivity matrix
        :param14 sample_surface: Boolean array of whether mesh panel is sample surface (1) or not (0)
        :param15 BEM_adist: adaptive BEM integration tolerance
        :param16 ls2: Array of longest edge for each panel
        :param17 i: projected ion iteration number

        return:
            :launch_pos: initial launch coordinates
            :detector_coordinates: Final detector coordinates
            :ion_chemistry: The ions chemistry
            :particle trajectory: The entire particle trajectory (to be saved to .vtk)

        """

        launch_pos = phase_coords[i]
        ionpos = launch_coords[i, :]

        #derive chemistry of ions via point-in-polygon test
        ion_chemistry = None
        ion_phase = None
        for o in range(0, len(geom["objts"])):
            if geom["objts"][o]["surface"] == 1:
                ion_chemistry = geom["objts"][o]["mat_id"]
                ion_phase = geom["objts"][o]["name"]

        for o in range(0, len(geom["objts"])):
            if geom["objts"][o]["surface"] != 1 and geom["objts"][o]["mat_id"] != 0:
                pverts = geom["objts"][o]["verts"]
                pfaces = geom["objts"][o]["faces"]
                lpos = launch_pos/geom["simulation_grid"]["scale"]
                if sdf.pyinside(*lpos, pverts, pfaces, len(pfaces)) == 0:
                    ion_chemistry = geom["objts"][o]["mat_id"]
                    ion_phase = geom["objts"][o]["name"]
                    break

        #distance up to which to terminate projection
        proj_dist = 500e-9

        particle = ion(ionpos, np.array([0.0, 0.0, 0.0]), q = + 1e-9, m = 1.0, detector_coordinate = detector_coordinate, real_detector = real_detector, proj_dist = proj_dist)

        particle.iterate(croot, u, qc, verts, faces, cp, areasc, normsc, rel_sim_tol, abs_sim_tol, ls2, 1e-3)

        particle.trajectory = np.array(particle.trajectory)

        if real_detector == True:

            mx = (particle.trajectory[-1, 0] - particle.trajectory[-2, 0])/(particle.trajectory[-1, 2] - particle.trajectory[-2, 2])
            my = (particle.trajectory[-1, 1] - particle.trajectory[-2, 1])/(particle.trajectory[-1, 2] - particle.trajectory[-2, 2])

            x = particle.trajectory[-2, 0] + mx * (detector_coordinate - particle.trajectory[-2, 2])
            y = particle.trajectory[-2, 1] + my * (detector_coordinate - particle.trajectory[-2, 2])

            particle.trajectory[-1, :3] = np.array([x, y, detector_coordinate])
            final_pos = np.array([x, y, detector_coordinate])
            int_vel = np.array([1.0, 0.0, 0.0])

        else:
            lp = len(particle.trajectory) - 1
            for ip in range(0, lp + 1):
                fpos = particle.trajectory[lp - ip, :3]
                #calculate shortest distance
                min_dist = np.min(np.linalg.norm(fpos - cp, axis=1))
                if min_dist < proj_dist and ip > 0:
                    ipos = particle.trajectory[lp - ip + 1, :3]
                    norm_dist = np.linalg.norm(fpos - ipos)
                    #find solution
                    res = scipy.optimize.minimize(min_dist_shell, 0.5, args=(cp, fpos, ipos, proj_dist, norm_dist), method='Nelder-Mead', options={'xtol':1e-5, 'ftol':1e-5})
                    int_pos =  res.x * (ipos - fpos) + fpos

                    ivel = particle.trajectory[lp - ip + 1, 3:]
                    fvel = particle.trajectory[lp - ip, 3:]
                    int_vel = res.x * (ivel - fvel) + fvel

                    particle.trajectory = particle.trajectory[:(lp - ip + 2)]
                    particle.trajectory[lp - ip + 1, :3] = int_pos
                    particle.trajectory[lp - ip + 1, 3:] = int_vel
                    final_pos = int_pos
                    break

        #return values for joining
        return  launch_pos, final_pos, ion_chemistry, ion_phase, particle.trajectory, int_vel

def min_dist_shell(lda, cp, fpos, ipos, proj_dist, norm_dist):
    x = lda[0] * (ipos - fpos) + fpos
    dist = abs(np.min(np.linalg.norm(x - cp, axis=1)) - proj_dist)
    return dist

def non_para_derive_mapping(geom, detector_coordinate, rel_sim_tol, abs_sim_tol, areasc, normsc, u, qc, verts, faces, sample_surface, BEM_adist):

    """
    (not called by default)

    Project single ion per panel centre point
    ion projection unparallelised

    :param1 geom: The simulation geometry object
    :param2 detector_coordinate: The z-detector coordinate
    :param3 rel_sim_tol: The relative trajectory integrator tolerance (RK5 adaptive)
    :param4 abs_sim_tol: The absolue trajectory integrator tolerance (RK5 adaptive)
    :param5 areasc: The surface element areas
    :param6 normsc: The surface element normal vectors
    :param7 u: The potential solution (per panel centre collocation point)
    :param8 qc: The normal flux solution (per panel centre collocation point)
    :param9 verts: The mesh vertices
    :param10 faces: The mesh elements (connectivity matrix)
    :param11 sample_surface: Whether elements are sample surface or not
    :param12 BEM_adist: The adaptive quadrature threshold (from 7-point to single-point quadrature panel integration)
    :returns:
        sample_space - ion surface launch positions
        det_space - the detector impact coordinates
        chemistry - The ion phase database material_id (determined by initial launch position)
        ion_phase - The ion phase name (determined by initial launch position)
        trajectories - the ion trajectories
    """

    print("calculating projection ...")

    #filter out non sample surface boundaries - ensures only sample surface is used during ion projection (both field solution and projection sites)
    faces = faces[sample_surface == 1]
    qc = qc[sample_surface == 1]
    areasc = areasc[sample_surface == 1]
    normsc = normsc[sample_surface == 1]
    u = u[sample_surface == 1]

    cp = np.mean(verts[faces], axis =  1)
    z_max = np.max(cp[:, 2])
    z_min = np.min(cp[:, 2])
    projection_filter = cp[:, 2] > (z_max * 0.9 + z_min * 0.1)
    cp_proj = cp[projection_filter]
    panel_index = np.linspace(0, len(faces), len(faces), endpoint = False, dtype = int)[projection_filter]

    sample_space = []
    det_space = []
    trajectories = []
    chemistry = []
    ion_phase = []
    norms = []
    panel_ind = []
    #calculate ls2 - longest element side squared
    ls2x = np.sum((verts[faces[:, 1]] -  verts[faces[:, 0]]) * (verts[faces[:, 1]] -  verts[faces[:, 0]]), axis = 1)
    ls2y = np.sum((verts[faces[:, 2]] -  verts[faces[:, 1]]) * (verts[faces[:, 2]] -  verts[faces[:, 1]]), axis = 1)
    ls2z = np.sum((verts[faces[:, 0]] -  verts[faces[:, 2]]) * (verts[faces[:, 0]] -  verts[faces[:, 2]]), axis = 1)
    ls2 = np.maximum(np.maximum(ls2x, ls2y), ls2z)

    #calculate normals
    for j in range(0, len(cp)):

        y0 = verts[faces[j, 0]]
        y1 = verts[faces[j, 1]]
        y2 = verts[faces[j, 2]]
        n = np.cross(y1 - y0, y2 - y0)
        n = n/np.linalg.norm(n)
        norms.append(n)

    norms = np.array(norms)
    norms_proj = norms[cp[:, 2] > (z_max * 0.9 + z_min * 0.1)]

    croot = coctree.construct_octree(verts, faces, cp)
    croot.calculate_moments(verts, faces, cp, areasc, normsc, u, qc)

    ###This is the section that can be parallelised###

    for i in range(0, len(cp_proj)):

        #derive chemistry of ions via point-in-polygon test
        for o in range(0, len(geom["objts"])):
            if geom["objts"][o]["surface"] == 1:
                chemistry.append(geom["objts"][o]["mat_id"])
                ion_phase.append(geom["objts"][o]["name"])


        for o in range(0, len(geom["objts"])):
            if geom["objts"][o]["surface"] != 1 and geom["objts"][o]["mat_id"] != 0:
                pverts = geom["objts"][o]["verts"]
                pfaces = geom["objts"][o]["faces"]
                lpos = cp_proj[i]/geom["simulation_grid"]["scale"]
                if sdf.pyinside(*lpos, pverts, pfaces, len(pfaces)) == False:
                    chemistry[-1] = geom["objts"][o]["mat_id"]
                    ion_phase[-1] = geom["objts"][o]["name"]
                    break

        ionpos = cp_proj[i] + norms_proj[i, :3] * 0.5 * geom["simulation_grid"]["cell_width"]

        particle = ion(ionpos, np.array([0.0, 0.0, 0.0]), q = + 1e-9, m = 1.0, detector_coordinate = detector_coordinate)

        particle.iterate(croot, u, qc, verts, faces, cp, areasc, normsc, rel_sim_tol, abs_sim_tol, ls2, 1e-3)
        launch_pos = cp_proj[i]

        particle.trajectory = np.array(particle.trajectory)

        #print(particle.trajectory)
        #calculate detector coordinates
        mx = (particle.trajectory[-1, 0] - particle.trajectory[-2, 0])/(particle.trajectory[-1, 2] - particle.trajectory[-2, 2])
        my = (particle.trajectory[-1, 1] - particle.trajectory[-2, 1])/(particle.trajectory[-1, 2] - particle.trajectory[-2, 2])

        x = particle.trajectory[-2, 0] + mx * (detector_coordinate - particle.trajectory[-2, 2])
        y = particle.trajectory[-2, 1] + my * (detector_coordinate - particle.trajectory[-2, 2])

        particle.trajectory[-1, :3] = np.array([x, y, detector_coordinate])

        #check if any ion
        if np.sum(abs(particle.trajectory) > 1e10) == 0:
            trajectories.append(particle.trajectory)
            panel_ind.append([panel_index[i]])
            sample_space.append(launch_pos)
            det_space.append(np.array([x, y]))
        else:
            chemistry.pop(-1)

    chemistry = np.array(chemistry)
    print("projection complete")
    return sample_space, det_space, chemistry, ion_phase, trajectories, panel_ind

def project_ions(geom, detector_coordinate, rel_sim_tol, abs_sim_tol,
                 areasc, normsc, u, qc, verts, verts2, faces, faces2,
                 sample_surface, sample_surface2, BEM_adist, smoothed_velocity,
                 diff_volume, vol0, vol1, total_ions=5000,
                 atomic_volume=0.01, real_projection=True, real_detector=False):

    """
    Project ions with a surface evaporation rate-dependent density

    :param1 geom: The simulation geometry object
    :param2 detector_coordinate: The z-detector coordinate
    :param3 rel_sim_tol: The relative trajectory integrator tolerance (RK5 adaptive)
    :param4 abs_sim_tol: The absolue trajectory integrator tolerance (RK5 adaptive)
    :param5 areasc: The surface element areas
    :param6 normsc: The surface element normal vectors
    :param7 u: The potential solution (per panel centre collocation point)
    :param8 qc: The normal flux solution (per panel centre collocation point)
    :param9 verts: The mesh vertices
    :param10 verts2: The previous iteration step mesh vertices
    :param11 faces: The mesh elements (connectivity matrix)
    :param12 faces2: The previous iteration step mesh connectivity matrix
    :param13 sample_surface: Whether elements are sample surface or not
    :param14 sample_surface: For previous iteration step mesh. Whether elements are sample surface or not
    :param15 BEM_adist: The adaptive quadrature threshold (from 7-point to single-point quadrature panel integration)
    :param16 smoothed_velocity: The smoothed evaporation rate values per panel
    :param17 total_ions: The total number of ions per projection
    :param18 atomic_volume: The atomic volume (scale^3) taken up by each ion
    :param19 real_projection: Whether to project ions proportional to the evaporated volume or a constant number per projection iteration

    :returns:
        sample_space - ion surface launch positions
        det_space - the detector impact coordinates
        chemistry - The ion phase database material_id (determined by initial launch position)
        ion_phase - The ion phase name (determined by initial launch position)
        trajectories - the ion trajectories
    """

    print("calculating projection ... - evaporation rate dependent")

    #filter out non sample surface boundaries - ensures only sample surface is used during ion projection (both field solution and projection sites)
    faces = faces[sample_surface == 1]
    qc = qc[sample_surface == 1]
    areasc = areasc[sample_surface == 1]
    normsc = normsc[sample_surface == 1]
    u = u[sample_surface == 1]

    faces2 = faces2[sample_surface2 == 1]
    cp = np.mean(verts[faces], axis =  1)
    z_max = np.max(cp[:, 2])
    z_min = np.min(cp[:, 2])

    #this height threshold could be removed for ion projection to allow for projection everywhere - however, evaporation rate at base is non-zero
    projection_cuttoff = cp[:, 2] > z_max - 10.0 * geom['simulation_grid']['cell_width']
    cp_proj = cp[projection_cuttoff]

    lfaces = np.linspace(0, len(faces), len(faces), endpoint = False, dtype = int)[projection_cuttoff]

    sample_space = []
    det_space = []
    trajectories = []
    chemistry = []

    norms = []

    lcp2 = np.mean(verts2[faces2], axis =  1)

    #calculates shortest (normal distance) between panel and future surface - used to estimate local evaporated volume for calculating ion projection density
    shortest_surf_dist = mesh_library.shortest_surface_distance(verts2 * 1e9, cp_proj * 1e9, faces2)

    area_weighted_evaporation_rate = shortest_surf_dist * areasc[projection_cuttoff]

    #corresponding face indices - to be binned
    cumulate_evap_rates = np.cumsum(area_weighted_evaporation_rate)
    norm_cum_evap_rates = (cumulate_evap_rates - np.min(cumulate_evap_rates))/(np.max(cumulate_evap_rates) - np.min(cumulate_evap_rates))

    for j in range(0, len(cp)):
        y0 = verts[faces[j, 0]]
        y1 = verts[faces[j, 1]]
        y2 = verts[faces[j, 2]]
        n = np.cross(y1 - y0, y2 - y0)
        n = n/np.linalg.norm(n)
        norms.append(n)

    norms = np.array(norms)
    norms_proj = norms[projection_cuttoff]

    #calculate ls2 - longest element side squared
    ls2x = np.sum((verts[faces[:, 1]] -  verts[faces[:, 0]]) * (verts[faces[:, 1]] -  verts[faces[:, 0]]), axis = 1)
    ls2y = np.sum((verts[faces[:, 2]] -  verts[faces[:, 1]]) * (verts[faces[:, 2]] -  verts[faces[:, 1]]), axis = 1)
    ls2z = np.sum((verts[faces[:, 0]] -  verts[faces[:, 2]]) * (verts[faces[:, 0]] -  verts[faces[:, 2]]), axis = 1)
    ls2 = np.maximum(np.maximum(ls2x, ls2y), ls2z)

    #construct panel octree and calculate moments
    croot = coctree.construct_octree(verts, faces, cp)
    croot.calculate_moments(verts, faces, cp, areasc, normsc, u, qc)

    avol = atomic_volume*1e-27
    if real_projection == True:
        #calculate number of projected ions
        ls_vol = abs(diff_volume/avol)
        total_ions = int(ls_vol)
        print("total ions: " + str(total_ions))

    ###Start parallelisation###
    pool = Pool(os.cpu_count())
    total_launches = np.linspace(0, total_ions, total_ions, dtype = int, endpoint = False)
    partial_ion_projection = partial(single_random_ion_projection, lfaces, norm_cum_evap_rates,
                                     cp_proj, normsc, cp, croot, geom, detector_coordinate,
                                     rel_sim_tol, abs_sim_tol, areasc, norms, u, qc,
                                     verts, faces, sample_surface, BEM_adist, ls2, real_detector)
    data_outputs = pool.map(partial_ion_projection, total_launches)
    pool.close()
    pool.join()
    ###end of parallelisation###

    chemistry = []
    ion_phase = []
    trajectories = []
    sample_space = []
    det_space = []
    panel_index = []

    for j in range(0, total_ions):
        #remove projection singularity calculations
        if np.sum(abs(data_outputs[j][4]) > 1e10) == 0:
            sample_space.append(data_outputs[j][0])
            det_space.append(data_outputs[j][1])
            chemistry.append(data_outputs[j][2])
            ion_phase.append(data_outputs[j][3])
            trajectories.append(data_outputs[j][4])
            panel_index.append([str(data_outputs[j][5])])

    chemistry = np.array(chemistry)
    panel_index = np.array(panel_index)
    print("projection complete")
    return sample_space, det_space, chemistry, ion_phase, trajectories, panel_index


def single_random_ion_projection(lfaces, norm_cum_evap_rates, cp_proj, norms_proj,
                                 cp, croot, geom, detector_coordinate,
                                 rel_sim_tol, abs_sim_tol, areasc, normsc, u, qc,
                                 verts, faces, sample_surface, BEM_adist, ls2, real_detector, evol):

    """
    Internal function called to allow for parallelisation (via Python multiprocessing)
    governs the projection of a single ion from a random point on a surface panel

        :param1 lfaces: Array of possible panel projection candidates
        :param2 norm_cum_evap_rates: Array of normalised panel evaporation rates
        :param3 cp_proj: Array of centre points of panels from which ions are projected from
        :param4 norms_proj: Array of normals of panels from which ions are projected from
        :param5 cp: Array of panel centre points
        :param6 croot: The octree object
        :param7 geom: The simulation dictionary
        :param8 detector_coordinate: The detector z coordinate
        :param9 rel_sim_tol: The relative RK5 simulation tolerance
        :param10 abs_sim_tol: The absolute RK5 simulation tolerance
        :param11 areasc: Array of panel areas
        :param12 normsc: Array of panel normals
        :param13 u: Array of panel pontentials
        :param14 qc: Array of panel normal fluxes
        :param15 verts: Array of mesh panel vertices
        :param16 faces: mesh connectivity matrix
        :param17 sample_surface: Boolean array of whether mesh panel is sample surface (1) or not (0)
        :param18 BEM_adist: adaptive BEM integration tolerance
        :param19 ls2: Array of longest edge for each panel
        :param19 evol: Projected ion launch number

        return:
            :launch_pos: initial launch coordinates
            :detector_coordinates: Final detector coordinates
            :ion_chemistry: The ions chemistry
            :particle trajectory: The entire particle trajectory (to be saved to .vtk)
            :i: The index of the projected panel


    """

    rv = random.random()
    i = lfaces[rv < norm_cum_evap_rates][0]
    lface = faces[i]

    v0 = verts[lface[0]]
    v1 = verts[lface[1]]
    v2 = verts[lface[2]]

    #generate random point on triangle - the launch site
    alpha = 1.0
    beta = 1.0

    while (alpha + beta) > 1.0:
        alpha = random.random()
        beta = random.random()

    launch_pos = v0 + (v1 - v0) * alpha + (v2 - v0) * beta

    #derive chemistry of ions via point-in-polygon test of launch position
    ion_chemistry = None
    ion_phase = None

    #set default ion phase and chemistry to bulk
    for o in range(0, len(geom["objts"])):
        if geom["objts"][o]["surface"] == 1:
            ion_chemistry = geom["objts"][o]["mat_id"]
            ion_phase = geom["objts"][o]["name"]

    for o in range(0, len(geom["objts"])):
        if geom["objts"][o]["surface"] != 1 and geom["objts"][o]["mat_id"] != 0:
            pverts = geom["objts"][o]["verts"]
            pfaces = geom["objts"][o]["faces"]
            pverts = pverts

            #transform to normalised coordinates to avoid point-in-polygon test numerical error
            #possibly epsilon value of ray test
            lpos = launch_pos/geom["simulation_grid"]["scale"]
            if sdf.pyinside(*lpos, pverts, pfaces, len(pfaces)) == 0:
                ion_chemistry = geom["objts"][o]["mat_id"]
                ion_phase = geom["objts"][o]["name"]
                break

    #initial launch position above surface
    ionpos = launch_pos + normsc[i, :3] * 0.5 * geom['simulation_grid']['cell_width']

    particle = ion(ionpos, np.array([0.0, 0.0, 0.0]), q = + 1e-9, m = 1.0, detector_coordinate = detector_coordinate, proj_dist = 1.0)

    particle.iterate(croot, u, qc, verts, faces, cp, areasc, normsc, rel_sim_tol, abs_sim_tol, ls2, 1e-3)
    particle.trajectory = np.array(particle.trajectory)

    if real_detector == True:

        mx = (particle.trajectory[-1, 0] - particle.trajectory[-2, 0])/(particle.trajectory[-1, 2] - particle.trajectory[-2, 2])
        my = (particle.trajectory[-1, 1] - particle.trajectory[-2, 1])/(particle.trajectory[-1, 2] - particle.trajectory[-2, 2])

        x = particle.trajectory[-2, 0] + mx * (detector_coordinate - particle.trajectory[-2, 2])
        y = particle.trajectory[-2, 1] + my * (detector_coordinate - particle.trajectory[-2, 2])

        particle.trajectory[-1, :3] = np.array([x, y, detector_coordinate])
        final_pos = np.array([x, y, detector_coordinate])

    else:
        lp = len(particle.trajectory) - 1
        for ip in range(0, lp + 1):
            fpos = particle.trajectory[lp - ip, :3]
            #calculate shortest distance
            min_dist = np.min(np.linalg.norm(fpos - cp, axis=1))
            if min_dist < proj_dist and ip > 0:
                ipos = particle.trajectory[lp - ip + 1, :3]
                norm_dist = np.linalg.norm(fpos - ipos)
                #find solution
                res = scipy.optimize.minimize(min_dist_shell, 0.5,
                                              args=(cp, fpos, ipos, proj_dist, norm_dist),
                                              method='Nelder-Mead',
                                              options={'xtol':1e-5, 'ftol':1e-5})

                int_pos =  res.x * (ipos - fpos) + fpos

                ivel = particle.trajectory[lp - ip + 1, 3:]
                fvel = particle.trajectory[lp - ip, 3:]
                int_vel = res.x * (ivel - fvel) + fvel

                particle.trajectory = particle.trajectory[:(lp - ip + 2)]
                particle.trajectory[lp - ip + 1, :3] = int_pos
                particle.trajectory[lp - ip + 1, 3:] = int_vel
                final_pos = int_pos
                break

    #determine trajectory coordinates
    mx = (particle.trajectory[-1, 0] - particle.trajectory[-2, 0])/(particle.trajectory[-1, 2] - particle.trajectory[-2, 2])
    my = (particle.trajectory[-1, 1] - particle.trajectory[-2, 1])/(particle.trajectory[-1, 2] - particle.trajectory[-2, 2])

    x = particle.trajectory[-2, 0] + mx * (detector_coordinate - particle.trajectory[-2, 2])
    y = particle.trajectory[-2, 1] + my * (detector_coordinate - particle.trajectory[-2, 2])

    particle.trajectory[-1, :3] = np.array([x, y, detector_coordinate])

    #return values for joining
    return  launch_pos, np.array([x, y, detector_coordinate]), ion_chemistry, ion_phase, particle.trajectory, i

def trajectory_stability(launch_cp, verts, sample_space, det_space):

    """
    Calculates the jacobian determinant of the forward mapping

    Method taken from Polygon Mesh Processing, Botsch
    https://math.stackexchange.com/questions/2930816/gradient-in-a-triangle-unclear-notation

    :param1 launch_cp: 2D array of indices of faces adjacent to launch vertices
    :param2 verts: 2D array of mesh vertices
    :param3 sample_space: 2D array of sampled sample surface coordinates
    :param4 det_space: 2D array of sampled detector coordinates
    :returns:
        :stability: 1D arry of calculated local Jacobian determinants
        :lcp: The surface panel centre points for which the Jacobian is defined
        :dcp: The detector element centre points for which the Jacobian is defined
        :conformal: The local conformality conditions
    """

    launch_cp = np.array(launch_cp)
    lcp = np.mean(sample_space[launch_cp], axis = 1)
    dcp = np.mean(det_space[launch_cp], axis = 1)
    stability = []
    conformal = []
    J00 = []
    J10 = []
    J01 = []
    J11 = []

    for a in range(len(launch_cp)):
        i0 = launch_cp[a, 0]
        i1 = launch_cp[a, 1]
        i2 = launch_cp[a, 2]

        sp0 = sample_space[i0]
        sp1 = sample_space[i1]
        sp2 = sample_space[i2]

        ds0 = det_space[i0]
        ds1 = det_space[i1]
        ds2 = det_space[i2]

        #calculate local triangle basis
        X = (sp1 - sp0)/np.linalg.norm(sp1 - sp0)
        n = np.cross(X, sp2 - sp0)
        n = n/np.linalg.norm(n)
        Y = np.cross(n, X)

        #X = np.array([1.0, 0.0, 0.0])
        #Y = np.array([0.0, 1.0, 0.0])

        #X = X - np.sum(X * n) * n
        #Y = Y - np.sum(Y * n) * n

        X = np.array([1.0, 0.0, 0.0])
        Y = np.array([0.0, 1.0, 0.0])

        X = X - np.sum(X * n) * n
        Y = Y - np.sum(Y * n) * n - np.sum(Y * X) * X

        X = X/np.linalg.norm(X)
        Y = Y/np.linalg.norm(Y)

        Xi = 0.0
        Yi = 0.0
        Xj = np.sum((sp1 - sp0) * X)/(np.sum(X * X))
        Yj = np.sum((sp1 - sp0) * Y)/(np.sum(Y * Y))
        Xk = np.sum((sp2 - sp0) * X)/(np.sum(X * X))
        Yk = np.sum((sp2 - sp0) * Y)/(np.sum(Y * Y))

        #Heron's formula
        da = np.linalg.norm(sp1 - sp0)
        db = np.linalg.norm(sp2 - sp1)
        dc = np.linalg.norm(sp0 - sp2)
        s = (da + db + dc)/2.0
        A = np.sqrt(s * (s - da) * (s - db) * (s - dc))

        M = np.array([[Yj - Yk, Yk - Yi, Yi - Yj], [Xk - Xj, Xi - Xk, Xj - Xi]])
        #M = np.array([[Y[1] - Y[2], Y[2] - Y[0]], [X[2] - X[1], X[0] - X[2]]])
        M = 1.0/(2.0 * A) * np.matmul(M, np.array([[ds0[0], ds1[0], ds2[0]], [ds0[1], ds1[1], ds2[1]]]).T).T
        #du = 1.0/(2.0 * A) * np.matmul(M, np.array([ds[0], ds[1]]))
        #calculate partial Jacobian
        J = M[0, 0] * M[1, 1] - M[1, 0] * M[0, 1]
        stability.append(J)

        C0 = M[0, 0] - M[1, 1]
        C1 = M[0, 1] + M[1, 0]
        conformal.append(abs(C0) + abs(C1))
        J00.append(M[0, 0])
        J10.append(M[1, 0])
        J01.append(M[0, 1])
        J11.append(M[1, 1])

    return stability, lcp, dcp, conformal, J00, J10, J01, J11

def transfer_function(initial_coords, launch_normal, det_pos, kappa=1.0):


    """
    Performs linear projection from trajectory termination point onto detector

    :param1 initial_coords: 2D array of ion trajectory termination points
    :param2 launch_normal: 2D array of ion trajectory direction vectors
    :param3 det_pos: Detector z-position
    :param4 kappa: The image compression parameter (launch angle rescaling)
    :returns:
        :ext_points: 2D array of projected detector impact positions
    """

    #define equation for detector

    p0 = np.array([0.0, 0.0, det_pos])
    l = launch_normal
    l = l/np.linalg.norm(l, axis=1).reshape(-1, 1)
    n = np.array([0.0, 0.0, 1.0])
    l0 = initial_coords

    #transform launch normal via linear law
    theta = np.arccos(np.sum(n * l, axis=1))
    phi = np.arctan2(l[:, 1], l[:, 0])
    mtheta = theta * kappa

    ln = copy.deepcopy(l)
    ln[:, 0] = np.sin(mtheta) * np.cos(phi)
    ln[:, 1] = np.sin(mtheta) * np.sin(phi)
    ln[:, 2] = np.cos(mtheta)

    #extrapolate to virtual detector
    ldn = np.sum(n * ln, axis=1)
    lda = np.sum((p0 - l0) * n, axis=1)/ldn
    ext_points = l0 + ln * lda.reshape(-1, 1)

    return ext_points
