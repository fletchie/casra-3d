
"""

The module for performing non-octree accelerated electrostatic calculations.

Module must be compiled prior to using casra_3d using the following command when in the casra_3d directory:
'python3 setup.py build_ext --inplace'

Author: Charles Fletcher
Atom Probe Tomography Group, Department of Materials, University of Oxford
May 2020

"""

import cython
import numpy as np
cimport numpy as np
from cython.parallel import prange
from cpython.exc cimport PyErr_CheckSignals

#external function declaration
cdef extern from "math.h" nogil:
    double sqrt(double m)

cdef extern from "math.h" nogil:
    double atan2(double a, double b)

cdef extern from "math.h" nogil:
    double fabs(double v)

cdef extern from "math.h" nogil:
    double log(double v)

#initialise integration variables
cdef double t = 1.0/3.0
cdef double s = (1.0 - sqrt(15.0))/7.0
cdef double r = (1.0 + sqrt(15.0))/7.0
cdef double w1 = 9.0/40.0
cdef double w2 = (155.0 + sqrt(15.0))/1200.0
cdef double w3 = (155.0 - sqrt(15.0))/1200.0
cdef double int1 = t + 2.0 * t * s
cdef double int2 = t - t * s
cdef double int3 = t + 2.0 * t * r
cdef double int4 = t - t * r
cdef double a0, a1, a2, b0, b1, b2, c0, c1, c2, d0, d1, d2, e0, e1, e2, f0, f1, f2, g0, g1, g2, Gij, Fij, Kij, Hij

#removes error checking code from compiled cython for matrix index behaviour
@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def matrix_construction(double[:, ::1] verts, long[:, ::1] faces, double[::1] ls2, double eps):

    """

    Method for constructing the BEM matrices for solving the boundary solution for the direct constant collocation BEM

    :param1 verts: The 2D array of panel vertices
    :param2 faces: The 2D array connectivity matrix of defining triangular elements
    :param3 ls2: The length of the longest side of each triangle element (for adaptive integration)
    :param4 eps: The tolerance for the adaptive integration

    :returns:
        :F: The 2D BEM F matrix
        :G: The 2D BEM G matrix

    """

    #convert these views to pointers to avoid unnecessary copy operation
    cdef int vc = len(faces)

    F = np.zeros((vc, vc), dtype = np.double)
    cdef double[:, ::1] F_view = F

    G = np.zeros((vc, vc), dtype = np.double)
    cdef double[:, ::1] G_view = G

    areas = np.zeros((vc), dtype = np.double)
    cdef double[:] areas_view = areas

    norms = np.zeros((vc, 4), dtype = np.double)
    cdef double[:, ::1] norms_view = norms

    cdef unsigned int same = 0

    cdef double x0, x1, x2, ya0, ya1, ya2, yb0, yb1, yb2, yc0, yc1, yc2, cpx, cpy, cpz,tdist2

    cdef Py_ssize_t i, j

    #for j in prange(vc, nogil = True):
    for j in range(vc):

        ya0 = verts[faces[j, 0], 0]
        ya1 = verts[faces[j, 0], 1]
        ya2 = verts[faces[j, 0], 2]

        yb0 = verts[faces[j, 1], 0]
        yb1 = verts[faces[j, 1], 1]
        yb2 = verts[faces[j, 1], 2]

        yc0 = verts[faces[j, 2], 0]
        yc1 = verts[faces[j, 2], 1]
        yc2 = verts[faces[j, 2], 2]

        areas_view[j] = 0.5 * ((((yb1 - ya1) * (yc2 - ya2) - (yb2 - ya2) * (yc1 - ya1))**2.0 + ((yb2 - ya2) * (yc0 - ya0) - (yb0 - ya0) * (yc2 - ya2))**2.0 + ((yb0 - ya0) * (yc1 - ya1) - (yb1 - ya1) * (yc0 - ya0))**2.0)**0.5)

        #calculate the cross product of vectors V1 = y0 - y1 and V2 =  y0 - y2
        norms_view[j, 0] = ((yb1 - ya1) * (yc2 - ya2) - (yb2 - ya2) * (yc1 - ya1))
        norms_view[j, 1] = ((yb2 - ya2) * (yc0 - ya0) - (yb0 - ya0) * (yc2 - ya2))
        norms_view[j, 2] = ((yb0 - ya0) * (yc1 - ya1) - (yb1 - ya1) * (yc0 - ya0))
        norms_view[j, 3] = sqrt((norms_view[j, 0]*norms_view[j, 0] + norms_view[j, 1]*norms_view[j, 1] + norms_view[j, 2]*norms_view[j, 2]))

        norms_view[j, 0] = norms_view[j, 0]/norms_view[j, 3]
        norms_view[j, 1] = norms_view[j, 1]/norms_view[j, 3]
        norms_view[j, 2] = norms_view[j, 2]/norms_view[j, 3]

    #by default is compiled for parallelisation
    for i in prange(vc, nogil = True):
    #for i in range(vc):
        x0 = (verts[faces[i, 0], 0] + verts[faces[i, 1], 0] + verts[faces[i, 2], 0])/3.0
        x1 = (verts[faces[i, 0], 1] + verts[faces[i, 1], 1] + verts[faces[i, 2], 1])/3.0
        x2 = (verts[faces[i, 0], 2] + verts[faces[i, 1], 2] + verts[faces[i, 2], 2])/3.0

        for j in range(vc):

            ya0 = verts[faces[j, 0], 0]
            ya1 = verts[faces[j, 0], 1]
            ya2 = verts[faces[j, 0], 2]

            yb0 = verts[faces[j, 1], 0]
            yb1 = verts[faces[j, 1], 1]
            yb2 = verts[faces[j, 1], 2]

            yc0 = verts[faces[j, 2], 0]
            yc1 = verts[faces[j, 2], 1]
            yc2 = verts[faces[j, 2], 2]

            cpx = (ya0 + yb0 + yc0) * 0.333333
            cpy = (ya1 + yb1 + yc1) * 0.333333
            cpz = (ya2 + yb2 + yc2) * 0.333333

            tdist2 = (x0 - cpx) * (x0 - cpx) + (x1 - cpy) * (x1 - cpy) + (x2 - cpz) * (x2 - cpz)

            #perform single point quadrature (change to 3 point in future?)
            G_view[i, j] = greens_function(x0, x1, x2, cpx, cpy, cpz) * areas_view[j]
            F_view[i, j] = norm_greens_function(x0, x1, x2, cpx, cpy, cpz, norms_view[j, 0], norms_view[j, 1], norms_view[j, 2])  * areas_view[j]

            #if below threshold distance perform seven point integration
            if ls2[j] > eps * tdist2:
                F_view[i, j] = num_Fij(x0, x1, x2, ya0, ya1, ya2, yb0, yb1, yb2, yc0, yc1, yc2, areas_view[j], norms_view[j, 0], norms_view[j, 1], norms_view[j, 2])
                G_view[i, j] = num_Gij(x0, x1, x2, ya0, ya1, ya2, yb0, yb1, yb2, yc0, yc1, yc2, areas_view[j])

    for j in range(vc):
        x0 = (verts[faces[j, 0], 0] + verts[faces[j, 1], 0] + verts[faces[j, 2], 0])/3.0
        x1 = (verts[faces[j, 0], 1] + verts[faces[j, 1], 1] + verts[faces[j, 2], 1])/3.0
        x2 = (verts[faces[j, 0], 2] + verts[faces[j, 1], 2] + verts[faces[j, 2], 2])/3.0

        ya0 = verts[faces[j, 0], 0]
        ya1 = verts[faces[j, 0], 1]
        ya2 = verts[faces[j, 0], 2]

        yb0 = verts[faces[j, 1], 0]
        yb1 = verts[faces[j, 1], 1]
        yb2 = verts[faces[j, 1], 2]

        yc0 = verts[faces[j, 2], 0]
        yc1 = verts[faces[j, 2], 1]
        yc2 = verts[faces[j, 2], 2]

        #set F diagonal matrix to exterior Laplace problem solution
        F_view[j, j] = - 1.5
        G_view[j, j] = anal_Gij(x0, x1, x2, ya0, ya1, ya2, yb0, yb1, yb2, yc0, yc1, yc2, areas_view[j])

    G = 2.0 * G

    PyErr_CheckSignals()
    return F, G, areas, norms

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cpdef cfield_calculation(double[::1] x, double[:, ::1] verts, long[:, ::1] faces, vector[long] cluster_faces, double[:, ::1] centres, double[::1] areas, double[:, ::1] norms, double[::1] BP, double[::1] BF, double[::1] ls2, int f_l, double eps):

        """

        Function for calculating the part of the BIE for non-admissable octree panels

        :param1 x: The point in domain at which to calculate the field
        :param2 verts: The 2D array of panel vertices
        :param3 faces: The 2D array connectivity matrix of defining triangular elements
        :param4 cluster_faces: vector of indices of panel elements included within the cluster
        :param5 centres: The 2D array of element centres
        :param6 areas: 1D array of triangle element areas
        :param7 norms: 2D array of triangle element normals
        :param8 BP: 1D array of boundary element potentials
        :param9 BF: 1D array of boundary element normal fluxes
        :param10 ls2: The length of the longest side of each triangle element (for adaptive integration)
        :param11 eps: The tolerance for the adaptive integration

        :returns:
            :Ex: x component of the electric field at point x
            :Ey: y component of the electric field at point x
            :Ez: z component of the electric field at point x

        """

        cdef double ya0, ya1, ya2, yb0, yb1, yb2, yc0, yc1, yc2, dist, div, dx, dy, dz

        cdef double Ex = 0.0
        cdef double Ey = 0.0
        cdef double Ez = 0.0

        cdef Py_ssize_t i, j

        cdef double Exa, Eya, Eza

        for j in range(f_l):

            i = cluster_faces[j]

            ya0 = verts[faces[i, 0], 0]
            ya1 = verts[faces[i, 0], 1]
            ya2 = verts[faces[i, 0], 2]

            yb0 = verts[faces[i, 1], 0]
            yb1 = verts[faces[i, 1], 1]
            yb2 = verts[faces[i, 1], 2]

            yc0 = verts[faces[i, 2], 0]
            yc1 = verts[faces[i, 2], 1]
            yc2 = verts[faces[i, 2], 2]

            dx = x[0] - centres[i, 0]
            dy = x[1] - centres[i, 1]
            dz = x[2] - centres[i, 2]

            dist = dx * dx + dy * dy + dz * dz

            Exa = - vecgrad_greens_function_opt(x[0], x[1], x[2], centres[i, 0], centres[i, 1], centres[i, 2], dx) * areas[i] * BF[i]
            Eya = - vecgrad_greens_function_opt(x[0], x[1], x[2], centres[i, 0], centres[i, 1], centres[i, 2], dy) * areas[i] * BF[i]
            Eza = - vecgrad_greens_function_opt(x[0], x[1], x[2], centres[i, 0], centres[i, 1], centres[i, 2], dz) * areas[i] * BF[i]

            #if single point quadrature condition not valid, perform 7-point quadrature integration
            if ls2[i] > eps * dist:
                ###changed this from Exa to Ex
                Exa = - num_Kijx(x[0], x[1], x[2], ya0, ya1, ya2, yb0, yb1, yb2, yc0, yc1, yc2, areas[i])  * BF[i]
                Eya = - num_Kijy(x[0], x[1], x[2], ya0, ya1, ya2, yb0, yb1, yb2, yc0, yc1, yc2, areas[i])  * BF[i]
                Eza = - num_Kijz(x[0], x[1], x[2], ya0, ya1, ya2, yb0, yb1, yb2, yc0, yc1, yc2, areas[i])  * BF[i]

            Ex += Exa
            Ey += Eya
            Ez += Eza

        Ex = -1.0/(4.0 * 3.14159265359) * Ex
        Ey = -1.0/(4.0 * 3.14159265359) * Ey
        Ez = -1.0/(4.0 * 3.14159265359) * Ez

        return Ex, Ey, Ez


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cpdef field_calculation(double[::1] x, double[:, ::1] verts, long[:, ::1] faces, double[:, ::1] centres, double[::1] areas, double[:, ::1] norms, double[::1] BP, double[::1] BF, double[::1] ls2, double eps):

        """

        Python callable function for calculating the electric field at a point within the domain

        :param1 x: The point in domain at which to calculate the field
        :param2 verts: The 2D array of panel vertices
        :param3 faces: The 2D array connectivity matrix of defining triangular elements
        :param4 centres: The 2D array of element centres
        :param5 areas: 1D array of triangle element areas
        :param6 norms: 2D array of triangle element normals
        :param7 BP: 1D array of boundary element potentials
        :param8 BF: 1D array of boundary element normal fluxes
        :param9 ls2: The length of the longest side of each triangle element (for adaptive integration)
        :param10 eps: The tolerance for the adaptive integration

        :returns:
            :Ex: x component of the electric field at point x
            :Ey: y component of the electric field at point x
            :Ez: z component of the electric field at point x

        """

        cdef int f_l = len(faces)
        cdef double ya0, ya1, ya2, yb0, yb1, yb2, yc0, yc1, yc2, dist, div, dx, dy, dz

        cdef double Ex = 0.0
        cdef double Ey = 0.0
        cdef double Ez = 0.0

        cdef Py_ssize_t i

        cdef double Exa, Eya, Eza

        #multithreading (as opposed to multiprocessing) takes advantage of the system hyperthreading
        #perhaps parallelising ion projection via multiprocessing while using multithreading to take advantage of hyperthreading
        #is the best approach
        if f_l > 500:
            for i in prange(f_l, nogil = True):

                ya0 = verts[faces[i, 0], 0]
                ya1 = verts[faces[i, 0], 1]
                ya2 = verts[faces[i, 0], 2]

                yb0 = verts[faces[i, 1], 0]
                yb1 = verts[faces[i, 1], 1]
                yb2 = verts[faces[i, 1], 2]

                yc0 = verts[faces[i, 2], 0]
                yc1 = verts[faces[i, 2], 1]
                yc2 = verts[faces[i, 2], 2]

                dx = x[0] - centres[i, 0]
                dy = x[1] - centres[i, 1]
                dz = x[2] - centres[i, 2]

                dist = dx * dx + dy * dy + dz * dz

                Exa = vecgrad_norm_greens_function_opt(x[0], x[1], x[2], centres[i, 0], centres[i, 1], centres[i, 2], norms[i, 0], norms[i, 1], norms[i, 2], dx, norms[i, 0]) * areas[i] * BP[i] - vecgrad_greens_function_opt(x[0], x[1], x[2], centres[i, 0], centres[i, 1], centres[i, 2], dx) * areas[i] * BF[i]
                Eya = vecgrad_norm_greens_function_opt(x[0], x[1], x[2], centres[i, 0], centres[i, 1], centres[i, 2], norms[i, 0], norms[i, 1], norms[i, 2], dy, norms[i, 1]) * areas[i] * BP[i] - vecgrad_greens_function_opt(x[0], x[1], x[2], centres[i, 0], centres[i, 1], centres[i, 2], dy) * areas[i] * BF[i]
                Eza = vecgrad_norm_greens_function_opt(x[0], x[1], x[2], centres[i, 0], centres[i, 1], centres[i, 2], norms[i, 0], norms[i, 1], norms[i, 2], dz, norms[i, 2]) * areas[i] * BP[i] - vecgrad_greens_function_opt(x[0], x[1], x[2], centres[i, 0], centres[i, 1], centres[i, 2], dz) * areas[i] * BF[i]

                #if single point quadrature condition not valid, perform 7-point quadrature integration
                if ls2[i] > eps * dist:
                    Exa = num_Hijx(x[0], x[1], x[2], ya0, ya1, ya2, yb0, yb1, yb2, yc0, yc1, yc2, areas[i], norms[i, 0], norms[i, 1], norms[i, 2])  * BP[i] - num_Kijx(x[0], x[1], x[2], ya0, ya1, ya2, yb0, yb1, yb2, yc0, yc1, yc2, areas[i])  * BF[i]
                    Eya = num_Hijy(x[0], x[1], x[2], ya0, ya1, ya2, yb0, yb1, yb2, yc0, yc1, yc2, areas[i], norms[i, 0], norms[i, 1], norms[i, 2])  * BP[i] - num_Kijy(x[0], x[1], x[2], ya0, ya1, ya2, yb0, yb1, yb2, yc0, yc1, yc2, areas[i])  * BF[i]
                    Eza = num_Hijz(x[0], x[1], x[2], ya0, ya1, ya2, yb0, yb1, yb2, yc0, yc1, yc2, areas[i], norms[i, 0], norms[i, 1], norms[i, 2])  * BP[i] - num_Kijz(x[0], x[1], x[2], ya0, ya1, ya2, yb0, yb1, yb2, yc0, yc1, yc2, areas[i])  * BF[i]

                Ex += Exa
                Ey += Eya
                Ez += Eza
        else:
            for i in range(f_l):

                ya0 = verts[faces[i, 0], 0]
                ya1 = verts[faces[i, 0], 1]
                ya2 = verts[faces[i, 0], 2]

                yb0 = verts[faces[i, 1], 0]
                yb1 = verts[faces[i, 1], 1]
                yb2 = verts[faces[i, 1], 2]

                yc0 = verts[faces[i, 2], 0]
                yc1 = verts[faces[i, 2], 1]
                yc2 = verts[faces[i, 2], 2]

                dx = x[0] - centres[i, 0]
                dy = x[1] - centres[i, 1]
                dz = x[2] - centres[i, 2]

                dist = dx * dx + dy * dy + dz * dz

                Exa = vecgrad_norm_greens_function_opt(x[0], x[1], x[2], centres[i, 0], centres[i, 1], centres[i, 2], norms[i, 0], norms[i, 1], norms[i, 2], dx, norms[i, 0]) * areas[i] * BP[i] - vecgrad_greens_function_opt(x[0], x[1], x[2], centres[i, 0], centres[i, 1], centres[i, 2], dx) * areas[i] * BF[i]
                Eya = vecgrad_norm_greens_function_opt(x[0], x[1], x[2], centres[i, 0], centres[i, 1], centres[i, 2], norms[i, 0], norms[i, 1], norms[i, 2], dy, norms[i, 1]) * areas[i] * BP[i] - vecgrad_greens_function_opt(x[0], x[1], x[2], centres[i, 0], centres[i, 1], centres[i, 2], dy) * areas[i] * BF[i]
                Eza = vecgrad_norm_greens_function_opt(x[0], x[1], x[2], centres[i, 0], centres[i, 1], centres[i, 2], norms[i, 0], norms[i, 1], norms[i, 2], dz, norms[i, 2]) * areas[i] * BP[i] - vecgrad_greens_function_opt(x[0], x[1], x[2], centres[i, 0], centres[i, 1], centres[i, 2], dz) * areas[i] * BF[i]

                #if single point quadrature condition not valid, perform 7-point quadrature integration
                if ls2[i] > eps * dist:
                    Exa = num_Hijx(x[0], x[1], x[2], ya0, ya1, ya2, yb0, yb1, yb2, yc0, yc1, yc2, areas[i], norms[i, 0], norms[i, 1], norms[i, 2])  * BP[i] - num_Kijx(x[0], x[1], x[2], ya0, ya1, ya2, yb0, yb1, yb2, yc0, yc1, yc2, areas[i])  * BF[i]
                    Eya = num_Hijy(x[0], x[1], x[2], ya0, ya1, ya2, yb0, yb1, yb2, yc0, yc1, yc2, areas[i], norms[i, 0], norms[i, 1], norms[i, 2])  * BP[i] - num_Kijy(x[0], x[1], x[2], ya0, ya1, ya2, yb0, yb1, yb2, yc0, yc1, yc2, areas[i])  * BF[i]
                    Eza = num_Hijz(x[0], x[1], x[2], ya0, ya1, ya2, yb0, yb1, yb2, yc0, yc1, yc2, areas[i], norms[i, 0], norms[i, 1], norms[i, 2])  * BP[i] - num_Kijz(x[0], x[1], x[2], ya0, ya1, ya2, yb0, yb1, yb2, yc0, yc1, yc2, areas[i])  * BF[i]

                Ex += Exa
                Ey += Eya
                Ez += Eza


        Ex = -1.0/(4.0 * 3.14159265359) * Ex
        Ey = -1.0/(4.0 * 3.14159265359) * Ey
        Ez = -1.0/(4.0 * 3.14159265359) * Ez

        return Ex, Ey, Ez

@cython.cdivision(True)
cdef double num_Gij(double x0, double x1, double x2, double ya0, double ya1, double ya2, double yb0, double yb1, double yb2, double yc0, double yc1, double yc2, double area) nogil:

    """
    degree 5 gaussian Cubature
    source: https://arxiv.org/pdf/1606.03743.pdf

    Function for calculating the numerical solution to the G matrix of the singular BIE
    Calculates field in domain

    :param 1 x0: x coordinate of field point (domain point)
    :param 2 x1: y coordinate of field point (domain point)
    :param 3 x2: z coordinate of field point (domain point)
    :param 4 ya0: x coordinate of triangle element corner 1
    :param 5 ya1: y coordinate of triangle element corner 1
    :param 6 ya2: z coordinate of triangle element corner 1
    :param 7 yb0: x coordinate of triangle element corner 2
    :param 8 yb1: y coordinate of triangle element corner 2
    :param 9 yb2: z coordinate of triangle element corner 2
    :param 10 yc0: x coordinate of triangle element corner 3
    :param 11 yc1: y coordinate of triangle element corner 3
    :param 12 yc2: z coordinate of triangle element corner 3
    :param 13 area: triangle element area

    :returns:
        :G(x, y): evaluated G matrix element

    """

    #a = (ya + yb + yc) * t
    a0 = (ya0 + yb0 + yc0) * t
    a1 = (ya1 + yb1 + yc1) * t
    a2 = (ya2 + yb2 + yc2) * t

    #b = ya * (t + 2.0 * t * s) + (yb + yc) * (t - t * s)
    b0 = ya0 * int1 + (yb0 + yc0) * int2
    b1 = ya1 * int1 + (yb1 + yc1) * int2
    b2 = ya2 * int1 + (yb2 + yc2) * int2

    #c = yb * (t + 2.0 * t * s) + (ya + yb) * (t - t * s)
    c0 = yb0 * int1 + (ya0 + yc0) * int2
    c1 = yb1 * int1 + (ya1 + yc1) * int2
    c2 = yb2 * int1 + (ya2 + yc2) * int2

    #d = yc * (t + 2.0 * t * s) + (yb + ya) * (t - t * s)
    d0 = yc0 * int1 + (yb0 + ya0) * int2
    d1 = yc1 * int1 + (yb1 + ya1) * int2
    d2 = yc2 * int1 + (yb2 + ya2) * int2

    #e = ya * (t + 2.0 * t * r) + (yb + yc) * (t - t * r)
    e0 = ya0 * int3 + (yb0 + yc0) * int4
    e1 = ya1 * int3 + (yb1 + yc1) * int4
    e2 = ya2 * int3 + (yb2 + yc2) * int4

    #f = yb * (t + 2.0 * t * r) + (ya + yc) * (t - t * r)
    f0 = yb0 * int3 + (ya0 + yc0) * int4
    f1 = yb1 * int3 + (ya1 + yc1) * int4
    f2 = yb2 * int3 + (ya2 + yc2) * int4

    #g = yc * (t + 2.0 * t * r) + (yb + ya) * (t - t * r)
    g0 = yc0 * int3 + (yb0 + ya0) * int4
    g1 = yc1 * int3 + (yb1 + ya1) * int4
    g2 = yc2 * int3 + (yb2 + ya2) * int4

    Gij  = greens_function(x0, x1, x2, a0, a1, a2) * w1
    Gij += greens_function(x0, x1, x2, b0, b1, b2) * w2
    Gij += greens_function(x0, x1, x2, c0, c1, c2) * w2
    Gij += greens_function(x0, x1, x2, d0, d1, d2) * w2
    Gij += greens_function(x0, x1, x2, e0, e1, e2) * w3
    Gij += greens_function(x0, x1, x2, f0, f1, f2) * w3
    Gij += greens_function(x0, x1, x2, g0, g1, g2) * w3

    return area * Gij

@cython.cdivision(True)
cdef double num_Fij(double x0, double x1, double x2, double ya0, double ya1, double ya2, double yb0, double yb1, double yb2, double yc0, double yc1, double yc2, double area, double n0, double n1, double n2) nogil:

    """
    degree 5 gaussian Cubature
    source: https://arxiv.org/pdf/1606.03743.pdf

    Function for calculating the numerical solution to the F matrix of the singular BIE
    Calculates field in domain

    :param 1 x0: x coordinate of field point (domain point)
    :param 2 x1: y coordinate of field point (domain point)
    :param 3 x2: z coordinate of field point (domain point)
    :param 4 ya0: x coordinate of triangle element corner 1
    :param 5 ya1: y coordinate of triangle element corner 1
    :param 6 ya2: z coordinate of triangle element corner 1
    :param 7 yb0: x coordinate of triangle element corner 2
    :param 8 yb1: y coordinate of triangle element corner 2
    :param 9 yb2: z coordinate of triangle element corner 2
    :param 10 yc0: x coordinate of triangle element corner 3
    :param 11 yc1: y coordinate of triangle element corner 3
    :param 12 yc2: z coordinate of triangle element corner 3
    :param 13 area: triangle element area
    :param 14 n0: x component of the triangle element normal
    :param 15 n1: y component of the triangle element normal
    :param 16 n2: z component of the triangle element normal

    :returns:
        :F(x, y): evaluated F matrix element

    """

    #a = (ya + yb + yc) * t
    a0 = (ya0 + yb0 + yc0) * t
    a1 = (ya1 + yb1 + yc1) * t
    a2 = (ya2 + yb2 + yc2) * t

    #b = ya * (t + 2.0 * t * s) + (yb + yc) * (t - t * s)
    b0 = ya0 * int1 + (yb0 + yc0) * int2
    b1 = ya1 * int1 + (yb1 + yc1) * int2
    b2 = ya2 * int1 + (yb2 + yc2) * int2

    #c = yb * (t + 2.0 * t * s) + (ya + yb) * (t - t * s)
    c0 = yb0 * int1 + (ya0 + yc0) * int2
    c1 = yb1 * int1 + (ya1 + yc1) * int2
    c2 = yb2 * int1 + (ya2 + yc2) * int2

    #d = yc * (t + 2.0 * t * s) + (yb + ya) * (t - t * s)
    d0 = yc0 * int1 + (yb0 + ya0) * int2
    d1 = yc1 * int1 + (yb1 + ya1) * int2
    d2 = yc2 * int1 + (yb2 + ya2) * int2

    #e = ya * (t + 2.0 * t * r) + (yb + yc) * (t - t * r)
    e0 = ya0 * int3 + (yb0 + yc0) * int4
    e1 = ya1 * int3 + (yb1 + yc1) * int4
    e2 = ya2 * int3 + (yb2 + yc2) * int4

    #f = yb * (t + 2.0 * t * r) + (ya + yc) * (t - t * r)
    f0 = yb0 * int3 + (ya0 + yc0) * int4
    f1 = yb1 * int3 + (ya1 + yc1) * int4
    f2 = yb2 * int3 + (ya2 + yc2) * int4

    #g = yc * (t + 2.0 * t * r) + (yb + ya) * (t - t * r)
    g0 = yc0 * int3 + (yb0 + ya0) * int4
    g1 = yc1 * int3 + (yb1 + ya1) * int4
    g2 = yc2 * int3 + (yb2 + ya2) * int4

    Fij  = norm_greens_function(x0, x1, x2, a0, a1, a2, n0, n1, n2) * w1
    Fij += norm_greens_function(x0, x1, x2, b0, b1, b2, n0, n1, n2) * w2
    Fij += norm_greens_function(x0, x1, x2, c0, c1, c2, n0, n1, n2) * w2
    Fij += norm_greens_function(x0, x1, x2, d0, d1, d2, n0, n1, n2) * w2
    Fij += norm_greens_function(x0, x1, x2, e0, e1, e2, n0, n1, n2) * w3
    Fij += norm_greens_function(x0, x1, x2, f0, f1, f2, n0, n1, n2) * w3
    Fij += norm_greens_function(x0, x1, x2, g0, g1, g2, n0, n1, n2) * w3

    return area * Fij


@cython.cdivision(True)
cdef double num_Kijx(double x0, double x1, double x2, double ya0, double ya1, double ya2, double yb0, double yb1, double yb2, double yc0, double yc1, double yc2, double area) nogil:

    """
    degree 5 gaussian Cubature
    source: https://arxiv.org/pdf/1606.03743.pdf

    Function for calculating the x component for the numerical solution to the K matrix of the hypersingular BIE
    Calculates field in domain

    :param 1 x0: x coordinate of field point (domain point)
    :param 2 x1: y coordinate of field point (domain point)
    :param 3 x2: z coordinate of field point (domain point)
    :param 4 ya0: x coordinate of triangle element corner 1
    :param 5 ya1: y coordinate of triangle element corner 1
    :param 6 ya2: z coordinate of triangle element corner 1
    :param 7 yb0: x coordinate of triangle element corner 2
    :param 8 yb1: y coordinate of triangle element corner 2
    :param 9 yb2: z coordinate of triangle element corner 2
    :param 10 yc0: x coordinate of triangle element corner 3
    :param 11 yc1: y coordinate of triangle element corner 3
    :param 12 yc2: z coordinate of triangle element corner 3
    :param 13 area: triangle element area

    :returns:
        :Kx(x, y, n): evaluated x component of the K integrated kernel

    """

    cdef double top = 0.0
    #a = (ya + yb + yc) * t
    a0 = (ya0 + yb0 + yc0) * t
    a1 = (ya1 + yb1 + yc1) * t
    a2 = (ya2 + yb2 + yc2) * t

    #b = ya * (t + 2.0 * t * s) + (yb + yc) * (t - t * s)
    b0 = ya0 * int1 + (yb0 + yc0) * int2
    b1 = ya1 * int1 + (yb1 + yc1) * int2
    b2 = ya2 * int1 + (yb2 + yc2) * int2

    #c = yb * (t + 2.0 * t * s) + (ya + yb) * (t - t * s)
    c0 = yb0 * int1 + (ya0 + yc0) * int2
    c1 = yb1 * int1 + (ya1 + yc1) * int2
    c2 = yb2 * int1 + (ya2 + yc2) * int2

    #d = yc * (t + 2.0 * t * s) + (yb + ya) * (t - t * s)
    d0 = yc0 * int1 + (yb0 + ya0) * int2
    d1 = yc1 * int1 + (yb1 + ya1) * int2
    d2 = yc2 * int1 + (yb2 + ya2) * int2

    #e = ya * (t + 2.0 * t * r) + (yb + yc) * (t - t * r)
    e0 = ya0 * int3 + (yb0 + yc0) * int4
    e1 = ya1 * int3 + (yb1 + yc1) * int4
    e2 = ya2 * int3 + (yb2 + yc2) * int4

    #f = yb * (t + 2.0 * t * r) + (ya + yc) * (t - t * r)
    f0 = yb0 * int3 + (ya0 + yc0) * int4
    f1 = yb1 * int3 + (ya1 + yc1) * int4
    f2 = yb2 * int3 + (ya2 + yc2) * int4

    #g = yc * (t + 2.0 * t * r) + (yb + ya) * (t - t * r)
    g0 = yc0 * int3 + (yb0 + ya0) * int4
    g1 = yc1 * int3 + (yb1 + ya1) * int4
    g2 = yc2 * int3 + (yb2 + ya2) * int4

    top = x0 - a0
    Kij = vecgrad_greens_function_opt(x0, x1, x2, a0, a1, a2, top) * w1
    top = x0 - b0
    Kij += vecgrad_greens_function_opt(x0, x1, x2, b0, b1, b2, top) * w2
    top = x0 - c0
    Kij += vecgrad_greens_function_opt(x0, x1, x2, c0, c1, c2, top) * w2
    top = x0 - d0
    Kij += vecgrad_greens_function_opt(x0, x1, x2, d0, d1, d2, top) * w2
    top = x0 - e0
    Kij += vecgrad_greens_function_opt(x0, x1, x2, e0, e1, e2, top) * w3
    top = x0 - f0
    Kij += vecgrad_greens_function_opt(x0, x1, x2, f0, f1, f2, top) * w3
    top = x0 - g0
    Kij += vecgrad_greens_function_opt(x0, x1, x2, g0, g1, g2, top) * w3

    return area * Kij

@cython.cdivision(True)
cdef double num_Kijy(double x0, double x1, double x2, double ya0, double ya1, double ya2, double yb0, double yb1, double yb2, double yc0, double yc1, double yc2, double area) nogil:

    """
    degree 5 gaussian Cubature
    source: https://arxiv.org/pdf/1606.03743.pdf

    function for calculating the y component for the numerical solution to the K matrix of the hypersingular BIE
    Calculates field in domain

    :param 1 x0: x coordinate of field point (domain point)
    :param 2 x1: y coordinate of field point (domain point)
    :param 3 x2: z coordinate of field point (domain point)
    :param 4 ya0: x coordinate of triangle element corner 1
    :param 5 ya1: y coordinate of triangle element corner 1
    :param 6 ya2: z coordinate of triangle element corner 1
    :param 7 yb0: x coordinate of triangle element corner 2
    :param 8 yb1: y coordinate of triangle element corner 2
    :param 9 yb2: z coordinate of triangle element corner 2
    :param 10 yc0: x coordinate of triangle element corner 3
    :param 11 yc1: y coordinate of triangle element corner 3
    :param 12 yc2: z coordinate of triangle element corner 3
    :param 13 area: triangle element area

    :returns:
        :Ky(x, y, n): evaluated y component of the K integrated kernel

    """

    cdef double top = 0.0
    #a = (ya + yb + yc) * t
    a0 = (ya0 + yb0 + yc0) * t
    a1 = (ya1 + yb1 + yc1) * t
    a2 = (ya2 + yb2 + yc2) * t

    #b = ya * (t + 2.0 * t * s) + (yb + yc) * (t - t * s)
    b0 = ya0 * int1 + (yb0 + yc0) * int2
    b1 = ya1 * int1 + (yb1 + yc1) * int2
    b2 = ya2 * int1 + (yb2 + yc2) * int2

    #c = yb * (t + 2.0 * t * s) + (ya + yb) * (t - t * s)
    c0 = yb0 * int1 + (ya0 + yc0) * int2
    c1 = yb1 * int1 + (ya1 + yc1) * int2
    c2 = yb2 * int1 + (ya2 + yc2) * int2

    #d = yc * (t + 2.0 * t * s) + (yb + ya) * (t - t * s)
    d0 = yc0 * int1 + (yb0 + ya0) * int2
    d1 = yc1 * int1 + (yb1 + ya1) * int2
    d2 = yc2 * int1 + (yb2 + ya2) * int2

    #e = ya * (t + 2.0 * t * r) + (yb + yc) * (t - t * r)
    e0 = ya0 * int3 + (yb0 + yc0) * int4
    e1 = ya1 * int3 + (yb1 + yc1) * int4
    e2 = ya2 * int3 + (yb2 + yc2) * int4

    #f = yb * (t + 2.0 * t * r) + (ya + yc) * (t - t * r)
    f0 = yb0 * int3 + (ya0 + yc0) * int4
    f1 = yb1 * int3 + (ya1 + yc1) * int4
    f2 = yb2 * int3 + (ya2 + yc2) * int4

    #g = yc * (t + 2.0 * t * r) + (yb + ya) * (t - t * r)
    g0 = yc0 * int3 + (yb0 + ya0) * int4
    g1 = yc1 * int3 + (yb1 + ya1) * int4
    g2 = yc2 * int3 + (yb2 + ya2) * int4

    top = x1 - a1
    Kij = vecgrad_greens_function_opt(x0, x1, x2, a0, a1, a2, top) * w1
    top = x1 - b1
    Kij += vecgrad_greens_function_opt(x0, x1, x2, b0, b1, b2, top) * w2
    top = x1 - c1
    Kij += vecgrad_greens_function_opt(x0, x1, x2, c0, c1, c2, top) * w2
    top = x1 - d1
    Kij += vecgrad_greens_function_opt(x0, x1, x2, d0, d1, d2, top) * w2
    top = x1 - e1
    Kij += vecgrad_greens_function_opt(x0, x1, x2, e0, e1, e2, top) * w3
    top = x1 - f1
    Kij += vecgrad_greens_function_opt(x0, x1, x2, f0, f1, f2, top) * w3
    top = x1 - g1
    Kij += vecgrad_greens_function_opt(x0, x1, x2, g0, g1, g2, top) * w3

    return area * Kij


@cython.cdivision(True)
cdef double num_Kijz(double x0, double x1, double x2, double ya0, double ya1, double ya2, double yb0, double yb1, double yb2, double yc0, double yc1, double yc2, double area) nogil:

    """
    degree 5 gaussian Cubature
    source: https://arxiv.org/pdf/1606.03743.pdf

    Function for calculating the z component for the numerical solution to the K matrix of the hypersingular BIE
    Calculates field in domain

    :param 1 x0: x coordinate of field point (domain point)
    :param 2 x1: y coordinate of field point (domain point)
    :param 3 x2: z coordinate of field point (domain point)
    :param 4 ya0: x coordinate of triangle element corner 1
    :param 5 ya1: y coordinate of triangle element corner 1
    :param 6 ya2: z coordinate of triangle element corner 1
    :param 7 yb0: x coordinate of triangle element corner 2
    :param 8 yb1: y coordinate of triangle element corner 2
    :param 9 yb2: z coordinate of triangle element corner 2
    :param 10 yc0: x coordinate of triangle element corner 3
    :param 11 yc1: y coordinate of triangle element corner 3
    :param 12 yc2: z coordinate of triangle element corner 3
    :param 13 area: triangle element area

    :returns:
        :Kz(x, y, n): evaluated z component of the K integrated kernel

    """
    #degree 5 gaussian Cubature
    #source: https://arxiv.org/pdf/1606.03743.pdf

    cdef double top = 0.0
    #a = (ya + yb + yc) * t
    a0 = (ya0 + yb0 + yc0) * t
    a1 = (ya1 + yb1 + yc1) * t
    a2 = (ya2 + yb2 + yc2) * t

    #b = ya * (t + 2.0 * t * s) + (yb + yc) * (t - t * s)
    b0 = ya0 * int1 + (yb0 + yc0) * int2
    b1 = ya1 * int1 + (yb1 + yc1) * int2
    b2 = ya2 * int1 + (yb2 + yc2) * int2

    #c = yb * (t + 2.0 * t * s) + (ya + yb) * (t - t * s)
    c0 = yb0 * int1 + (ya0 + yc0) * int2
    c1 = yb1 * int1 + (ya1 + yc1) * int2
    c2 = yb2 * int1 + (ya2 + yc2) * int2

    #d = yc * (t + 2.0 * t * s) + (yb + ya) * (t - t * s)
    d0 = yc0 * int1 + (yb0 + ya0) * int2
    d1 = yc1 * int1 + (yb1 + ya1) * int2
    d2 = yc2 * int1 + (yb2 + ya2) * int2

    #e = ya * (t + 2.0 * t * r) + (yb + yc) * (t - t * r)
    e0 = ya0 * int3 + (yb0 + yc0) * int4
    e1 = ya1 * int3 + (yb1 + yc1) * int4
    e2 = ya2 * int3 + (yb2 + yc2) * int4

    #f = yb * (t + 2.0 * t * r) + (ya + yc) * (t - t * r)
    f0 = yb0 * int3 + (ya0 + yc0) * int4
    f1 = yb1 * int3 + (ya1 + yc1) * int4
    f2 = yb2 * int3 + (ya2 + yc2) * int4

    #g = yc * (t + 2.0 * t * r) + (yb + ya) * (t - t * r)
    g0 = yc0 * int3 + (yb0 + ya0) * int4
    g1 = yc1 * int3 + (yb1 + ya1) * int4
    g2 = yc2 * int3 + (yb2 + ya2) * int4

    top = x2 - a2
    Kij = vecgrad_greens_function_opt(x0, x1, x2, a0, a1, a2, top) * w1
    top = x2 - b2
    Kij += vecgrad_greens_function_opt(x0, x1, x2, b0, b1, b2, top) * w2
    top = x2 - c2
    Kij += vecgrad_greens_function_opt(x0, x1, x2, c0, c1, c2, top) * w2
    top = x2 - d2
    Kij += vecgrad_greens_function_opt(x0, x1, x2, d0, d1, d2, top) * w2
    top = x2 - e2
    Kij += vecgrad_greens_function_opt(x0, x1, x2, e0, e1, e2, top) * w3
    top = x2 - f2
    Kij += vecgrad_greens_function_opt(x0, x1, x2, f0, f1, f2, top) * w3
    top = x2 - g2
    Kij += vecgrad_greens_function_opt(x0, x1, x2, g0, g1, g2, top) * w3

    return area * Kij

@cython.cdivision(True)
cdef double num_Hijx(double x0, double x1, double x2, double ya0, double ya1, double ya2, double yb0, double yb1, double yb2, double yc0, double yc1, double yc2, double area, double n0, double n1, double n2) nogil:

    """
    degree 5 gaussian Cubature
    source: https://arxiv.org/pdf/1606.03743.pdf

    Function for calculating the x component for the numerical solution to the H matrix of the hypersingular BIE
    Calculates field in domain

    :param 1 x0: x coordinate of field point (domain point)
    :param 2 x1: y coordinate of field point (domain point)
    :param 3 x2: z coordinate of field point (domain point)
    :param 4 ya0: x coordinate of triangle element corner 1
    :param 5 ya1: y coordinate of triangle element corner 1
    :param 6 ya2: z coordinate of triangle element corner 1
    :param 7 yb0: x coordinate of triangle element corner 2
    :param 8 yb1: y coordinate of triangle element corner 2
    :param 9 yb2: z coordinate of triangle element corner 2
    :param 10 yc0: x coordinate of triangle element corner 3
    :param 11 yc1: y coordinate of triangle element corner 3
    :param 12 yc2: z coordinate of triangle element corner 3
    :param 13 area: triangle element area
    :param 14 n0: x component of the triangle element normal
    :param 15 n1: y component of the triangle element normal
    :param 16 n2: z component of the triangle element normal

    :returns:
        :Hx(x, y, n): evaluated x component of the H integrated kernel

    """


    #a = (ya + yb + yc) * t

    cdef double topl = 0.0
    cdef double topr = 0.0

    a0 = (ya0 + yb0 + yc0) * t
    a1 = (ya1 + yb1 + yc1) * t
    a2 = (ya2 + yb2 + yc2) * t

    #b = ya * (t + 2.0 * t * s) + (yb + yc) * (t - t * s)
    b0 = ya0 * int1 + (yb0 + yc0) * int2
    b1 = ya1 * int1 + (yb1 + yc1) * int2
    b2 = ya2 * int1 + (yb2 + yc2) * int2

    #c = yb * (t + 2.0 * t * s) + (ya + yb) * (t - t * s)
    c0 = yb0 * int1 + (ya0 + yc0) * int2
    c1 = yb1 * int1 + (ya1 + yc1) * int2
    c2 = yb2 * int1 + (ya2 + yc2) * int2

    #d = yc * (t + 2.0 * t * s) + (yb + ya) * (t - t * s)
    d0 = yc0 * int1 + (yb0 + ya0) * int2
    d1 = yc1 * int1 + (yb1 + ya1) * int2
    d2 = yc2 * int1 + (yb2 + ya2) * int2

    #e = ya * (t + 2.0 * t * r) + (yb + yc) * (t - t * r)
    e0 = ya0 * int3 + (yb0 + yc0) * int4
    e1 = ya1 * int3 + (yb1 + yc1) * int4
    e2 = ya2 * int3 + (yb2 + yc2) * int4

    #f = yb * (t + 2.0 * t * r) + (ya + yc) * (t - t * r)
    f0 = yb0 * int3 + (ya0 + yc0) * int4
    f1 = yb1 * int3 + (ya1 + yc1) * int4
    f2 = yb2 * int3 + (ya2 + yc2) * int4

    #g = yc * (t + 2.0 * t * r) + (yb + ya) * (t - t * r)
    g0 = yc0 * int3 + (yb0 + ya0) * int4
    g1 = yc1 * int3 + (yb1 + ya1) * int4
    g2 = yc2 * int3 + (yb2 + ya2) * int4

    topr =  n0

    topl = x0 - a0
    Hij  = vecgrad_norm_greens_function_opt(x0, x1, x2, a0, a1, a2, n0, n1, n2, topl, topr) * w1
    topl = x0 - b0
    Hij += vecgrad_norm_greens_function_opt(x0, x1, x2, b0, b1, b2, n0, n1, n2, topl, topr) * w2
    topl = x0 - c0
    Hij += vecgrad_norm_greens_function_opt(x0, x1, x2, c0, c1, c2, n0, n1, n2, topl, topr) * w2
    topl = x0 - d0
    Hij += vecgrad_norm_greens_function_opt(x0, x1, x2, d0, d1, d2, n0, n1, n2, topl, topr) * w2
    topl = x0 - e0
    Hij += vecgrad_norm_greens_function_opt(x0, x1, x2, e0, e1, e2, n0, n1, n2, topl, topr) * w3
    topl = x0 - f0
    Hij += vecgrad_norm_greens_function_opt(x0, x1, x2, f0, f1, f2, n0, n1, n2, topl, topr) * w3
    topl = x0 - g0
    Hij += vecgrad_norm_greens_function_opt(x0, x1, x2, g0, g1, g2, n0, n1, n2, topl, topr) * w3

    return area * Hij

@cython.cdivision(True)
cdef double num_Hijy(double x0, double x1, double x2, double ya0, double ya1, double ya2, double yb0, double yb1, double yb2, double yc0, double yc1, double yc2, double area, double n0, double n1, double n2) nogil:

    """
    degree 5 gaussian Cubature
    source: https://arxiv.org/pdf/1606.03743.pdf

    Function for calculating the y component for the numerical solution to the H matrix of the hypersingular BIE
    Calculates field in domain

    :param 1 x0: x coordinate of field point (domain point)
    :param 2 x1: y coordinate of field point (domain point)
    :param 3 x2: z coordinate of field point (domain point)
    :param 4 ya0: x coordinate of triangle element corner 1
    :param 5 ya1: y coordinate of triangle element corner 1
    :param 6 ya2: z coordinate of triangle element corner 1
    :param 7 yb0: x coordinate of triangle element corner 2
    :param 8 yb1: y coordinate of triangle element corner 2
    :param 9 yb2: z coordinate of triangle element corner 2
    :param 10 yc0: x coordinate of triangle element corner 3
    :param 11 yc1: y coordinate of triangle element corner 3
    :param 12 yc2: z coordinate of triangle element corner 3
    :param 13 area: triangle element area
    :param 14 n0: x component of the triangle element normal
    :param 15 n1: y component of the triangle element normal
    :param 16 n2: z component of the triangle element normal

    :returns:
        :Hy(x, y, n): evaluated y component of the H integrated kernel

    """

    #a = (ya + yb + yc) * t

    cdef double topl = 0.0
    cdef double topr = 0.0

    a0 = (ya0 + yb0 + yc0) * t
    a1 = (ya1 + yb1 + yc1) * t
    a2 = (ya2 + yb2 + yc2) * t

    #b = ya * (t + 2.0 * t * s) + (yb + yc) * (t - t * s)
    b0 = ya0 * int1 + (yb0 + yc0) * int2
    b1 = ya1 * int1 + (yb1 + yc1) * int2
    b2 = ya2 * int1 + (yb2 + yc2) * int2

    #c = yb * (t + 2.0 * t * s) + (ya + yb) * (t - t * s)
    c0 = yb0 * int1 + (ya0 + yc0) * int2
    c1 = yb1 * int1 + (ya1 + yc1) * int2
    c2 = yb2 * int1 + (ya2 + yc2) * int2

    #d = yc * (t + 2.0 * t * s) + (yb + ya) * (t - t * s)
    d0 = yc0 * int1 + (yb0 + ya0) * int2
    d1 = yc1 * int1 + (yb1 + ya1) * int2
    d2 = yc2 * int1 + (yb2 + ya2) * int2

    #e = ya * (t + 2.0 * t * r) + (yb + yc) * (t - t * r)
    e0 = ya0 * int3 + (yb0 + yc0) * int4
    e1 = ya1 * int3 + (yb1 + yc1) * int4
    e2 = ya2 * int3 + (yb2 + yc2) * int4

    #f = yb * (t + 2.0 * t * r) + (ya + yc) * (t - t * r)
    f0 = yb0 * int3 + (ya0 + yc0) * int4
    f1 = yb1 * int3 + (ya1 + yc1) * int4
    f2 = yb2 * int3 + (ya2 + yc2) * int4

    #g = yc * (t + 2.0 * t * r) + (yb + ya) * (t - t * r)
    g0 = yc0 * int3 + (yb0 + ya0) * int4
    g1 = yc1 * int3 + (yb1 + ya1) * int4
    g2 = yc2 * int3 + (yb2 + ya2) * int4

    topr =  n1

    topl = x1 - a1
    Hij  = vecgrad_norm_greens_function_opt(x0, x1, x2, a0, a1, a2, n0, n1, n2, topl, topr) * w1
    topl = x1 - b1
    Hij += vecgrad_norm_greens_function_opt(x0, x1, x2, b0, b1, b2, n0, n1, n2, topl, topr) * w2
    topl = x1 - c1
    Hij += vecgrad_norm_greens_function_opt(x0, x1, x2, c0, c1, c2, n0, n1, n2, topl, topr) * w2
    topl = x1 - d1
    Hij += vecgrad_norm_greens_function_opt(x0, x1, x2, d0, d1, d2, n0, n1, n2, topl, topr) * w2
    topl = x1 - e1
    Hij += vecgrad_norm_greens_function_opt(x0, x1, x2, e0, e1, e2, n0, n1, n2, topl, topr) * w3
    topl = x1 - f1
    Hij += vecgrad_norm_greens_function_opt(x0, x1, x2, f0, f1, f2, n0, n1, n2, topl, topr) * w3
    topl = x1 - g1
    Hij += vecgrad_norm_greens_function_opt(x0, x1, x2, g0, g1, g2, n0, n1, n2, topl, topr) * w3

    return area * Hij

@cython.cdivision(True)
cdef double num_Hijz(double x0, double x1, double x2, double ya0, double ya1, double ya2, double yb0, double yb1, double yb2, double yc0, double yc1, double yc2, double area, double n0, double n1, double n2) nogil:

    """
    degree 5 gaussian Cubature
    source: https://arxiv.org/pdf/1606.03743.pdf

    Function for calculating the z component for the numerical solution to the H matrix of the hypersingular BIE
    Calculates field in domain

    :param 1 x0: x coordinate of field point (domain point)
    :param 2 x1: y coordinate of field point (domain point)
    :param 3 x2: z coordinate of field point (domain point)
    :param 4 ya0: x coordinate of triangle element corner 1
    :param 5 ya1: y coordinate of triangle element corner 1
    :param 6 ya2: z coordinate of triangle element corner 1
    :param 7 yb0: x coordinate of triangle element corner 2
    :param 8 yb1: y coordinate of triangle element corner 2
    :param 9 yb2: z coordinate of triangle element corner 2
    :param 10 yc0: x coordinate of triangle element corner 3
    :param 11 yc1: y coordinate of triangle element corner 3
    :param 12 yc2: z coordinate of triangle element corner 3
    :param 13 area: triangle element area
    :param 14 n0: x component of the triangle element normal
    :param 15 n1: y component of the triangle element normal
    :param 16 n2: z component of the triangle element normal

    :returns:
        :Hz(x, y, n): evaluated z component of the H integrated kernel

    """



    #a = (ya + yb + yc) * t

    cdef double topl = 0.0
    cdef double topr = 0.0

    a0 = (ya0 + yb0 + yc0) * t
    a1 = (ya1 + yb1 + yc1) * t
    a2 = (ya2 + yb2 + yc2) * t

    #b = ya * (t + 2.0 * t * s) + (yb + yc) * (t - t * s)
    b0 = ya0 * int1 + (yb0 + yc0) * int2
    b1 = ya1 * int1 + (yb1 + yc1) * int2
    b2 = ya2 * int1 + (yb2 + yc2) * int2

    #c = yb * (t + 2.0 * t * s) + (ya + yb) * (t - t * s)
    c0 = yb0 * int1 + (ya0 + yc0) * int2
    c1 = yb1 * int1 + (ya1 + yc1) * int2
    c2 = yb2 * int1 + (ya2 + yc2) * int2

    #d = yc * (t + 2.0 * t * s) + (yb + ya) * (t - t * s)
    d0 = yc0 * int1 + (yb0 + ya0) * int2
    d1 = yc1 * int1 + (yb1 + ya1) * int2
    d2 = yc2 * int1 + (yb2 + ya2) * int2

    #e = ya * (t + 2.0 * t * r) + (yb + yc) * (t - t * r)
    e0 = ya0 * int3 + (yb0 + yc0) * int4
    e1 = ya1 * int3 + (yb1 + yc1) * int4
    e2 = ya2 * int3 + (yb2 + yc2) * int4

    #f = yb * (t + 2.0 * t * r) + (ya + yc) * (t - t * r)
    f0 = yb0 * int3 + (ya0 + yc0) * int4
    f1 = yb1 * int3 + (ya1 + yc1) * int4
    f2 = yb2 * int3 + (ya2 + yc2) * int4

    #g = yc * (t + 2.0 * t * r) + (yb + ya) * (t - t * r)
    g0 = yc0 * int3 + (yb0 + ya0) * int4
    g1 = yc1 * int3 + (yb1 + ya1) * int4
    g2 = yc2 * int3 + (yb2 + ya2) * int4

    topr =  n2

    topl = x2 - a2
    Hij  = vecgrad_norm_greens_function_opt(x0, x1, x2, a0, a1, a2, n0, n1, n2, topl, topr) * w1
    topl = x2 - b2
    Hij += vecgrad_norm_greens_function_opt(x0, x1, x2, b0, b1, b2, n0, n1, n2, topl, topr) * w2
    topl = x2 - c2
    Hij += vecgrad_norm_greens_function_opt(x0, x1, x2, c0, c1, c2, n0, n1, n2, topl, topr) * w2
    topl = x2 - d2
    Hij += vecgrad_norm_greens_function_opt(x0, x1, x2, d0, d1, d2, n0, n1, n2, topl, topr) * w2
    topl = x2 - e2
    Hij += vecgrad_norm_greens_function_opt(x0, x1, x2, e0, e1, e2, n0, n1, n2, topl, topr) * w3
    topl = x2 - f2
    Hij += vecgrad_norm_greens_function_opt(x0, x1, x2, f0, f1, f2, n0, n1, n2, topl, topr) * w3
    topl = x2 - g2
    Hij += vecgrad_norm_greens_function_opt(x0, x1, x2, g0, g1, g2, n0, n1, n2, topl, topr) * w3

    return area * Hij


@cython.cdivision(True)
cdef inline double greens_function(double x0, double x1, double x2, double y0, double y1, double y2) nogil:
    return 1.0/(4.0 * 3.14159265359) * 1.0/sqrt((x0 - y0)*(x0 - y0) + (x1 - y1)*(x1 - y1) + (x2 - y2)*(x2 - y2))

@cython.cdivision(True)
cdef inline double norm_greens_function(double x0, double x1, double x2, double y0, double y1, double y2, double n0, double n1, double n2) nogil:
    return -1.0/(4.0 * 3.14159265359) * ((x0 - y0) * n0 + (x1 - y1) * n1 + (x2 - y2) * n2) * 1.0/sqrt(((x0 - y0)*(x0 - y0) + (x1 - y1)*(x1 - y1) + (x2 - y2)*(x2 - y2)) * ((x0 - y0)*(x0 - y0) + (x1 - y1)*(x1 - y1) + (x2 - y2)*(x2 - y2)) * ((x0 - y0)*(x0 - y0) + (x1 - y1)*(x1 - y1) + (x2 - y2)*(x2 - y2)))

@cython.cdivision(True)
cdef inline double vecgrad_greens_function(double x0, double x1, double x2, double y0, double y1, double y2, double nx0, double nx1, double nx2) nogil:
    return ((x0 - y0) * nx0 + (x1 - y1) * nx1 + (x2 - y2) * nx2) * 1.0/sqrt(((x0 - y0)*(x0 - y0) + (x1 - y1)*(x1 - y1) + (x2 - y2)*(x2 - y2)) * ((x0 - y0)*(x0 - y0) + (x1 - y1)*(x1 - y1) + (x2 - y2)*(x2 - y2)) * ((x0 - y0)*(x0 - y0) + (x1 - y1)*(x1 - y1) + (x2 - y2)*(x2 - y2)))

@cython.cdivision(True)
cdef inline double vecgrad_norm_greens_function(double x0, double x1, double x2, double y0, double y1, double y2, double n0, double n1, double n2, double nx0, double nx1, double nx2) nogil:
    return - 3.0 * ( (x0 - y0) * n0 + (x1 - y1) * n1 + (x2 - y2) * n2 ) * ( (x0 - y0) * nx0 + (x1 - y1) * nx1 + (x2 - y2) * nx2 ) * 1.0/sqrt(( (x0 - y0)*(x0 - y0) + (x1 - y1)*(x1 - y1) + (x2 - y2)*(x2 - y2) )**(5.0)) + ( n0 * nx0 + n1 * nx1 + n2 * nx2 ) * 1.0/sqrt(((x0 - y0)*(x0 - y0) + (x1 - y1)*(x1 - y1) + (x2 - y2)*(x2 - y2)) * ((x0 - y0)*(x0 - y0) + (x1 - y1)*(x1 - y1) + (x2 - y2)*(x2 - y2)) * ((x0 - y0)*(x0 - y0) + (x1 - y1)*(x1 - y1) + (x2 - y2)*(x2 - y2)) )

@cython.cdivision(True)
cdef inline double vecgrad_greens_function_opt(double x0, double x1, double x2, double y0, double y1, double y2, double top) nogil:
    return  top/sqrt(((x0 - y0)*(x0 - y0) + (x1 - y1)*(x1 - y1) + (x2 - y2)*(x2 - y2)) * ((x0 - y0)*(x0 - y0) + (x1 - y1)*(x1 - y1) + (x2 - y2)*(x2 - y2)) * ((x0 - y0)*(x0 - y0) + (x1 - y1)*(x1 - y1) + (x2 - y2)*(x2 - y2)))

@cython.cdivision(True)
cdef inline double vecgrad_norm_greens_function_opt(double x0, double x1, double x2, double y0, double y1, double y2, double n0, double n1, double n2, double topl, double topr) nogil:
    return topl * ( (x0 - y0) * n0 + (x1 - y1) * n1 + (x2 - y2) * n2 ) * 3.0/sqrt(((x0 - y0)*(x0 - y0) + (x1 - y1)*(x1 - y1) + (x2 - y2)*(x2 - y2) )**5.0) - topr/sqrt(((x0 - y0)*(x0 - y0) + (x1 - y1)*(x1 - y1) + (x2 - y2)*(x2 - y2)) * ((x0 - y0)*(x0 - y0) + (x1 - y1)*(x1 - y1) + (x2 - y2)*(x2 - y2)) * ((x0 - y0)*(x0 - y0) + (x1 - y1)*(x1 - y1) + (x2 - y2)*(x2 - y2)) )


@cython.cdivision(True)
cdef inline double anal_Gij(double x0, double x1, double x2, double ya0, double ya1, double ya2, double yb0, double yb1, double yb2, double yc0, double yc1, double yc2, double area) nogil:

    """

    Function for calculating the analytical solution to the G matrix (required for self interaction)
    The analytical solution for the constant panel implementation can be found here: doi:10.1016/j.enganabound.2009.10.002

    :param 1 x0: x coordinate of field point
    :param 2 x1: y coordinate of field point
    :param 3 x2: z coordinate of field point
    :param 4 ya0: x coordinate of triangle element corner 1
    :param 5 ya1: y coordinate of triangle element corner 1
    :param 6 ya2: z coordinate of triangle element corner 1
    :param 7 yb0: x coordinate of triangle element corner 2
    :param 8 yb1: y coordinate of triangle element corner 2
    :param 9 yb2: z coordinate of triangle element corner 2
    :param 10 yc0: x coordinate of triangle element corner 3
    :param 11 yc1: y coordinate of triangle element corner 3
    :param 12 yc2: z coordinate of triangle element corner 3
    :param 13 area: triangle element area

    :returns:
        :G(x, y): evaluated G matrix element

    """

    cdef double a0, a1, a2, nyb, nyc, nya, nda1, ndb1, ndc1, nda2, ndb2, ndc2

    a0 = ( (yb0 - ya0)**2.0 + (yb1 - ya1)**2.0 + (yb2 - ya2)**2.0 )**0.5
    a1 = ( (yc0 - yb0)**2.0 + (yc1 - yb1)**2.0 + (yc2 - yb2)**2.0 )**0.5
    a2 = ( (ya0 - yc0)**2.0 + (ya1 - yc1)**2.0 + (ya2 - yc2)**2.0 )**0.5

    nyb = ( (yb0 - x0)**2.0 + (yb1 - x1)**2.0 + (yb2 - x2)**2.0 )**0.5
    nyc = ( (yc0 - x0)**2.0 + (yc1 - x1)**2.0 + (yc2 - x2)**2.0 )**0.5
    nya = ( (ya0 - x0)**2.0 + (ya1 - x1)**2.0 + (ya2 - x2)**2.0 )**0.5

    nda1 = (yb0 - x0) * (yb0 - ya0)/a0 + (yb1 - x1) * (yb1 - ya1)/a0 + (yb2 - x2) * (yb2 - ya2)/a0
    ndb1 = (yc0 - x0) * (yc0 - yb0)/a1 + (yc1 - x1) * (yc1 - yb1)/a1 + (yc2 - x2) * (yc2 - yb2)/a1
    ndc1 = (ya0 - x0) * (ya0 - yc0)/a2 + (ya1 - x1) * (ya1 - yc1)/a2 + (ya2 - x2) * (ya2 - yc2)/a2

    nda2 = (ya0 - x0) * (yb0 - ya0)/a0 + (ya1 - x1) * (yb1 - ya1)/a0 + (ya2 - x2) * (yb2 - ya2)/a0
    ndb2 = (yb0 - x0) * (yc0 - yb0)/a1 + (yb1 - x1) * (yc1 - yb1)/a1 + (yb2 - x2) * (yc2 - yb2)/a1
    ndc2 = (yc0 - x0) * (ya0 - yc0)/a2 + (yc1 - x1) * (ya1 - yc1)/a2 + (yc2 - x2) * (ya2 - yc2)/a2

    L0 = log((nyb + nda1)/(nya + nda2))
    L1 = log((nyc + ndb1)/(nyb + ndb2))
    L2 = log((nya + ndc1)/(nyc + ndc2))

    return area * 1.0/(6 * 3.14159265359) * (L0/a0 + L1/a1 + L2/a2)
