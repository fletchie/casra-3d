
"""

Module containing functions for performing level set object setup and field initialisation

Author: Charles Fletcher
Atom Probe Tomography Group, Department of Materials, University of Oxford
May 2020
"""

import copy

from casra_3d.ls_operators import sdf as sdf
from casra_3d.io import model_writers as model_writers

try:
    import numpy as np
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python package numpy. " +
                              "Please make sure it is installed.")

def dimensions_from_geometry(geom, grid_width=2e-9, scale=1e-9):
    """

    function for setting up the simulation dictionary containing
    parameters and characteristics

    :param1 geom (dict): The simulation dictionary
    :param2 grid_width (float): The level set grid width (isotropic)
    :param3 scale (float): The initial model scale (scales .mstate geometries)

    :returns:
        None

    :raises:
        ValueError: is raised if  no geometry object in passed .mstate is flagged
                    as the specimen surface

    """
    xmin = np.inf

    for a in range(0, len(geom["objts"])):
        if geom["objts"][a]["surface"] == 1:
            print("model rescaling factor: " + str(scale))
            print("level set grid cell width: " + str(grid_width))
            xmin = (int(np.round(geom["objts"][a]["min_x"] * scale/grid_width * 1.2)) -
                    int(round(geom["parameters"]["initial_shank_extension"] *
                              np.tan(geom["parameters"]["shank_angle"]))))
            xmax = (int(np.round(geom["objts"][a]["max_x"] * scale/grid_width * 1.2)) +
                    int(round(geom["parameters"]["initial_shank_extension"] *
                              np.tan(geom["parameters"]["shank_angle"]))))
            ymin = (int(np.round(geom["objts"][a]["min_y"] * scale/grid_width * 1.2)) -
                    int(round(geom["parameters"]["initial_shank_extension"] *
                              np.tan(geom["parameters"]["shank_angle"]))))
            ymax = (int(np.round(geom["objts"][a]["max_y"] * scale/grid_width * 1.2)) +
                    int(round(geom["parameters"]["initial_shank_extension"] *
                              np.tan(geom["parameters"]["shank_angle"]))))
            zmin = (int(np.round(geom["objts"][a]["min_z"] * scale/grid_width) - 2.0) -
                    geom["parameters"]["initial_shank_extension"])
            zmax = int(np.round(geom["objts"][a]["max_z"] * scale/grid_width * 1.2))
            break

    if xmin != np.inf:
        geom["simulation_grid"] = {"xmin": xmin, "ymin": ymin, "zmin":zmin,
                                   "xmax": xmax, "ymax": ymax, "zmax": zmax,
                                   "x": xmax - xmin, "y": ymax - ymin, "z": zmax - zmin,
                                   "scale": scale, "cell_width": grid_width}
    else:
        raise ValueError("Level set grid dimensions could not be derived. " +
                         "please check that a geometry has been initialised that " +
                         "is flagged as being the sample surface.")

    #add grid scaled phase mesh coordinates
    for a in range(0, len(geom["objts"])):
        tverts = (geom["objts"][a]["verts"] * geom["simulation_grid"]["scale"]/
                  geom["simulation_grid"]["cell_width"])
        geom["objts"][a]["sverts"] = copy.deepcopy(tverts)

def initialise_field(geom, filename):

    """

    function for initialising the level set field

    Writes the level set field to .mstate (filename) if no previously built field detected

    :param1 geom (dict): The simulation dictionary
    :param2 filename (string): The .mstate filename

    :returns:
        None

    :raises:
        None

    """

    if isinstance(geom["field"], bool):
        field = np.zeros((geom["simulation_grid"]["x"],
                          geom["simulation_grid"]["y"],
                          geom["simulation_grid"]["z"]))
        print("level set field dimensions: " + str(field.shape))
        verts = np.zeros((0, 3), dtype=float)
        faces = np.zeros((0, 3), dtype=int)
        normals = np.zeros((0, 3), dtype=float)

        for a in range(0, len(geom["objts"])):
            tverts = (geom["objts"][a]["verts"] * geom["simulation_grid"]["scale"]/
                      geom["simulation_grid"]["cell_width"])
            tfaces = geom["objts"][a]["faces"]

            if geom["objts"][a]["surface"] == 1:

                lx = int(geom["simulation_grid"]["x"]/2)
                ly = int(geom["simulation_grid"]["y"]/2)
                lz = int(geom["simulation_grid"]["z"]/2)

                px = np.linspace(geom["objts"][a]["min_x"],
                                 geom["objts"][a]["max_x"],
                                 lx, endpoint=True)
                py = np.linspace(geom["objts"][a]["min_y"],
                                 geom["objts"][a]["max_y"],
                                 ly, endpoint=True)
                pz = np.linspace(geom["objts"][a]["min_z"],
                                 geom["objts"][a]["max_z"],
                                 lz, endpoint=True)

                XX, XY, XZ = np.meshgrid(px, py, pz)
                phase_grid = np.zeros((XX.shape[0] - 1, XX.shape[1] - 1, XX.shape[2] - 1),
                                      dtype="long")
                phase_grid[:, :, :] = geom["objts"][a]["phase_number"]

                tfaces = tfaces.astype("long")
                tnormals = np.cross(tverts[tfaces[:, 2], :] - tverts[tfaces[:, 1], :],
                                    tverts[tfaces[:, 1], :] - tverts[tfaces[:, 0], :])
                tnormals = tnormals/np.linalg.norm(tnormals, axis=1).reshape(-1, 1)
                tfaces = tfaces + len(verts)
                verts = np.vstack((verts, tverts))
                faces = np.vstack((faces, tfaces))
                normals = np.vstack((normals, tnormals))

            if geom["objts"][a]["mat_id"] == 0:
                tverts = (geom["objts"][a]["verts"] * geom["simulation_grid"]["scale"]/
                          geom["simulation_grid"]["cell_width"])
                tfaces = geom["objts"][a]["faces"]

                tfaces = tfaces.astype("long")
                tnormals = np.cross(tverts[tfaces[:, 2], :] - tverts[tfaces[:, 1], :],
                                    tverts[tfaces[:, 1], :] - tverts[tfaces[:, 0], :])
                tfaces = tfaces + len(verts)
                tnormals = -tnormals/np.linalg.norm(tnormals, axis=1).reshape(-1, 1)
                verts = np.vstack((verts, tverts))
                faces = np.vstack((faces, tfaces))
                normals = np.vstack((normals, tnormals))

        #construct local phase grid of material ids (matching to sqlite database)
        print("phase grid construction")
        for a in range(0, len(geom["objts"])):
            if geom["objts"][a]["surface"] == 0:
                pverts = geom["objts"][a]["verts"]
                pfaces = geom["objts"][a]["faces"]
                from casra_3d.io import model_writers as model_writers
                model_writers.write_vtk("mesh" + str(a) + ".vtk", pverts, pfaces)

                phase_grid = sdf.phase_grid(phase_grid, XX, XY, XZ, pverts, pfaces,
                                            geom["objts"][a]["phase_number"])

        phase_grid = np.transpose(phase_grid, axes=(1, 0, 2))

        x = geom["simulation_grid"]["x"]
        y = geom["simulation_grid"]["y"]
        z = geom["simulation_grid"]["z"]
        x0 = geom["simulation_grid"]["xmin"]
        y0 = geom["simulation_grid"]["ymin"]
        z0 = geom["simulation_grid"]["zmin"]

        #perform intial shank extension
        if geom["parameters"]["initial_shank_extension"] > 0:
            filt = (verts[:, 2] < (np.min(verts[:, 2]) +
                                   (1.5 * geom["simulation_grid"]["cell_width"]/
                                    geom["simulation_grid"]["scale"])))
            base_shift = (geom["parameters"]["initial_shank_extension"] *
                          geom["simulation_grid"]["cell_width"]/geom["simulation_grid"]["scale"])
            verts[filt, 2] -= base_shift
            base_centre = np.mean(verts[filt, :2], axis=0)

            #also need to expand x,y for shank
            angles = np.arctan2(verts[:, 1] - base_centre[1], verts[:, 0] - base_centre[0])
            verts[filt, 0] += (np.cos(angles[filt]) * base_shift *
                               np.tan(geom["parameters"]["shank_angle"]))
            verts[filt, 1] += (np.sin(angles[filt]) *base_shift *
                               np.tan(geom["parameters"]["shank_angle"]))

        #calculate initial signed distance field
        print("level set field construction")
        cp = np.mean(verts[faces], axis=1)
        field = sdf.sdf(verts, normals, faces, x, y, z, -x0, -y0, -z0)

        #write field and phase grid parameters to simulation dictionary
        geom["field"] = field
        geom["phase_grid"] = phase_grid
        geom["pgxmin"] = ((np.min(px)) * geom["simulation_grid"]["scale"]/
                          geom["simulation_grid"]["cell_width"])
        geom["pgxmax"] = ((np.max(px)) * geom["simulation_grid"]["scale"]/
                          geom["simulation_grid"]["cell_width"])
        geom["pgymin"] = ((np.min(py)) * geom["simulation_grid"]["scale"]/
                          geom["simulation_grid"]["cell_width"])
        geom["pgymax"] = ((np.max(py)) * geom["simulation_grid"]["scale"]/
                          geom["simulation_grid"]["cell_width"])
        geom["pgzmin"] = ((np.min(pz)) * geom["simulation_grid"]["scale"]/
                          geom["simulation_grid"]["cell_width"])
        geom["pgzmax"] = ((np.max(pz)) * geom["simulation_grid"]["scale"]/
                          geom["simulation_grid"]["cell_width"])

        #save field to .mstate
        model_writers.write_field_to_mstate(field, filename)
        #save phase grid to .mstate
        model_writers.write_pgrid_to_mstate(phase_grid, geom["pgxmin"], geom["pgxmax"],
                                            geom["pgymin"], geom["pgymax"],
                                            geom["pgzmin"], geom["pgzmax"], filename)
