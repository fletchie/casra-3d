start=$(date +'%s')
echo "Run SiGe multilayer reconstruction calibration pipeline"

python3 -m casra_3d.tools.ET_loader --phases "bulk:0.35:1;layer:0.84:0" --input_data c3d_input/sige_multilayer/sige_multilayer.tiff --parameters c3d_input/sige_multilayer/ET_parameters --mstate c3d_input/sige_multilayer/sige_multilayer.mstate --stretch 2.0

python3 c3d_input/sige_multilayer/optimise_model_multilayer.py
python3 c3d_input/sige_multilayer/vis_optimisation_multilayer.py

finish=$(date +'%s')
duration=$((finish-start))
hrs=$(( duration/3600 )); mins=$(( (duration-hrs*3600)/60)); secs=$(( duration-hrs*3600-mins*60 ))
printf 'Time spent: %02d:%02d:%02d\n' $hrs $mins $secs
