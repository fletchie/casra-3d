"""

Script generates data for performing unit tests

Authors: Charles Fletcher, Daniel Haley
Atom Probe Tomography Group, Department of Materials, University of Oxford
August 2020

"""

import sys
sys.path.append("")

try:
    import numpy as np
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python package numpy. " +
                              "Please make sure package is installed.")

from casra_3d.io import model_writers as model_writers

###generate UNSTRUCTURED_GRID vtk test file###
def write_unstructured_vtk_triangles_test(filename="tests/test_data/unstructured_grid_triangles_test.vtk"):
    vertices = [[0.0, 0.0, 0.0], [0.0, 0.0, 1.0], [0.0, 1.0, 1.0]]
    cells = [[0, 1, 2]]
    point_data = {"field": [0.3, 0.6, 1.1], "vals": [-0.1, -0.2, -0.3]}
    cell_data = {"temp": [[1.0, 3.0]]}
    model_writers.write_vtk(filename, vertices, cells, cell_data, point_data)

def write_unstructured_vtk_lines_test(filename="tests/test_data/unstructured_grid_lines_test.vtk"):
    vertices = [[0.0, 0.0, 0.0], [0.0, 0.0, 1.0], [0.0, 0.0, 2.0]]
    cells = [[0, 1], [1, 2]]
    point_data = {"field": [0.3, 0.6, 1.1], "vals": [-0.1, -0.2, -0.3]}
    cell_data = {"temp": [[1.0], [3.0]]}
    model_writers.write_vtk(filename, vertices, cells, cell_data, point_data)

def write_unstructured_vtk_points_test(filename="tests/test_data/unstructured_grid_points_test.vtk"):
    vertices = [[0.0, 0.0, 0.0], [0.0, 0.0, 1.0], [0.0, 0.0, 2.0]]
    cells = [[0], [1], [2]]
    point_data = {"field": [0.3, 0.6, 1.1], "vals": [-0.1, -0.2, -0.3]}
    cell_data = {"temp": [[1.0, 5.0], [3.0, -3.0], [5.0, 1.0]]}
    model_writers.write_vtk(filename, vertices, cells, cell_data, point_data)

######
###generate POLYDATA vtk test file###
def write_polydata_vtk_triangle_test(polydata_file="tests/test_data/polydata_test.vtk"):

    #define test polydata vtk
    polydata_test_str = """# vtk DataFile Version 4.2
vtk output
ASCII
DATASET POLYDATA
POINTS 3 float
0.0 0.0 0.0
0.0 0.0 1.0
0.0 1.0 1.0
POLYGONS 1 4
3 0 1 2
CELL_DATA 1
FIELD FieldData 1
field 2 1 float
0.0 1.0
POINT_DATA 3
SCALARS temp double 1
LOOKUP_TABLE default
0.3
0.4
0.5
    """

    #generate polydata test - save to file
    f = open(polydata_file, "w")
    f.write(polydata_test_str)
    f.close()
    ######

#if script directly called, generate test data
if __name__ == "__main__":
    print("Generating unit test data")
    write_unstructured_vtk_triangles_test()
    write_unstructured_vtk_lines_test()
    write_polydata_vtk_test()
    print("Complete")

def write_non_watertight_mesh(filename="tests/test_data/nonwatertight_test.vtk"):
    vertices = np.array([[1.0, 2.0, 3.0], [2.0, 2.0, 2.0], [1.0, 2.0, 5.0], [1.0, 4.0, 4.0]])
    cells = np.array([[0, 1, 2], [0, 1, 3]])
    model_writers.write_vtk(filename, vertices, cells, cell_data={}, point_data={})

def write_watertight_mesh(filename="tests/test_data/watertight_test.vtk"):
    vertices = np.array([[1.0, 2.0, 3.0], [2.0, 2.0, 2.0], [1.0, 2.0, 5.0], [1.0, 4.0, 4.0]])
    cells = np.array([[0, 1, 2], [0, 1, 3], [0, 2, 3], [1, 2, 3]])
    model_writers.write_vtk(filename, vertices, cells, cell_data={}, point_data={})
