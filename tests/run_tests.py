#!/usr/bin/env python3

"""

Script runs unit tests for package core functionality

Authors: Charles Fletcher, Daniel Haley
Atom Probe Tomography Group, Department of Materials, University of Oxford
August 2020

"""

import sys
sys.path.append("")

from casra_3d.io import model_loaders as model_loaders
from casra_3d.initialisation import tip_initialiser as tip_initialiser

import tests.test_data.generate_data as generate_data

try:
    import numpy as np
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python package numpy. " +
                              "Please make sure it is installed.")

print("test 1: write unstructured grid triangle vtk")
generate_data.write_unstructured_vtk_triangles_test()
print("test 1 complete")

print("test 2: read unstructured grid triangle vtk")
filename = "tests/test_data/unstructured_grid_triangles_test.vtk"
mesh = model_loaders.read_vtk(filename)
print("test 2 complete")

print("test 3: write unstructured grid lines vtk")
generate_data.write_unstructured_vtk_lines_test()
print("test 3 complete")

print("test 4: read unstructured grid lines vtk")
filename = "tests/test_data/unstructured_grid_lines_test.vtk"
mesh = model_loaders.read_vtk(filename)
#print(mesh.points)
#print(mesh.cells)
#print(mesh.cell_data)
#print(mesh.point_data)
print("test 4 complete")

print("test 5: write unstructured grid points vtk")
generate_data.write_unstructured_vtk_points_test()
print("test 5 complete")

print("test 6: read unstructured grid points vtk")
filename = "tests/test_data/unstructured_grid_points_test.vtk"
mesh = model_loaders.read_vtk(filename)
print("test 6 complete")

#test 7 preparation - generate polydata test data
generate_data.write_polydata_vtk_triangle_test()

print("test 7: polydata vtk")
polydata_file = "tests/test_data/polydata_test.vtk"
mesh = model_loaders.read_vtk(polydata_file)
print("test 7 complete")

print("test 8: test that test mesh is not watertight. \n \t" +
      "This does not confirm that the input " +
      "geometry is a manifold")

filename = "tests/test_data/nonwatertight_test.vtk"
generate_data.write_non_watertight_mesh()
mesh = model_loaders.read_vtk(filename)
verts = np.array(mesh.points)
faces = np.array(mesh.cells)

#test geometry
#verts = np.array([[1.0, 2.0, 3.0], [2.0, 2.0, 2.0], [1.0, 2.0, 5.0], [1.0, 4.0, 4.0]])
#faces = np.array([[0, 1, 2], [0, 1, 3]])

out = tip_initialiser.check_mesh_is_watertight(verts, faces, "test", test=1)

if out == 2:
    print("test 8 complete")
else:
    raise ValueError("test 8 failed")

print("test 9: test that test mesh is watertight. \n \t" +
      "This does not confirm that the input " +
      "geometry is a manifold")

filename = "tests/test_data/watertight_test.vtk"
generate_data.write_watertight_mesh()
mesh = model_loaders.read_vtk(filename)
verts = np.array(mesh.points)
faces = np.array(mesh.cells)

#test geometry
#verts = np.array([[1.0, 2.0, 3.0], [2.0, 2.0, 2.0], [1.0, 2.0, 5.0], [1.0, 4.0, 4.0]])
#faces = np.array([[0, 1, 2], [0, 1, 3], [0, 2, 3], [1, 2, 3]])

out = tip_initialiser.check_mesh_is_watertight(verts, faces, "test", test=1)
if out == 0:
    print("test 9 complete")
else:
    raise ValueError("test 9 failed")
