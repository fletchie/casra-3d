
"""

The ET processing submodule

Author: Charles Fletcher
Atom Probe Tomography Group, Department of Materials, University of Oxford
May 2020

"""

import copy
import sys
import os

try:
    import numpy as np
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python package numpy. Please make sure it is installed.")

try:
    import scipy
    import scipy.ndimage
    from scipy.ndimage import gaussian_filter
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python package scipy. Please make sure it is installed.")

try:
    from skimage import io
    from skimage import measure
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python package scikit-image. Please make sure it has been installed.")

try:
    import pandas as pd
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python package pandas. Please make sure it has been installed (e.g. apt or pip).")


from casra_3d.io import model_writers as model_writers
from casra_3d.mesh.cgal import cgal_linker as cgal_linker
import casra_3d.mesh.non_cgal.mesh_library as mesh_library

def generate_mesh_from_ET(data_filename, param_filename, phase_values, mstate_filename, stretch_coeff = 1):

    """

    Function for generating a .mstate input mesh file for the specimen from Electron Tomography TIFF stack data and defined threshold values

    :param1 data_filename: The ET data structure
    :param2 param_filename: The ET parameter tsv file
                            ###example structure (with all parameters)
                            ignore_size 10  The volume in nm^3 below which to ignore phase meshes
                            res_x   0.5   The x resolution in nm
                            res_y   0.5   The y resolution in nm
                            res_z   0.5   The z resolution in nm
                            simplification_resolution   2.0 The resolution to simplify meshes to
                            x_axis  0   The axis to use for the array loaded x axis
                            y_axis  1   The axis to use for the array loaded y axis
                            z_axis  2   The axis to use for the array loaded z axis
                            invz    1 Whether to invert the z axis (1 for yes)
                            clipf   10  Number of cells to clip from the top
                            clipb   10  Number of cells to clip from the bottom
                            invert  1 Whether to invert the array values (1 for yes)
                            top_flatten 1   Whether to flatten the mesh at the top
                            bottom_flatten 1   Whether to flatten the mesh at the bottom
                            ###
    :param3 phase_values: The string of the input isosurfaces to extract and what phase these should correspond to.
                          Structure is as followed phase_name1:contour_value:is_sample_surface;phase_name2:contour_value:is_sample_surface...
                          e.g. bulk:0.7:1;phase:0.8:0
                          contour values are between 0.0 and 1.0 (ET data is normalised on input)
    :param4 mstate_filename: The .mstate file name to output to

    :returns: 0 (writes to .mstate file)

    """

    #determine directory to output vtk phase and surface meshes
    mesh_dir = mstate_filename.split("/")
    if len(mesh_dir) == 1:
        mesh_dir = ""
    else:
        tmd = [mesh_dir[a] for a in range(0, len(mesh_dir) - 1)]
        mesh_dir = "/".join(tmd)

    #clear mstate file if it exists
    if os.path.exists(mstate_filename):
   	 f = open(mstate_filename, "w")
   	 f.close()
    else:
       	f = open(mstate_filename, "w+")
       	f.close()

    print("output directory (relative): " + mesh_dir)
    mesh_dir = mesh_dir + "/"
    print("parameter file: " + param_filename)
    print("output .mstate file: " + mstate_filename)

    #obtain ET mesh extraction parameters from tsv input
    gp = pd.read_csv(param_filename, sep = '\t', comment = '#')
    gp['index_col'] = 0
    gp = gp[['index_col', 'parameter', 'value']].pivot(index = 'index_col', columns = 'parameter', values = 'value')

    ignore_size = gp["ignore_size"][0]
    res_x = gp["res_x"][0]
    res_y = gp["res_y"][0]
    res_z = gp["res_z"][0]
    simplification_resolution = gp["simplification_resolution"][0]
    x_axis = int(gp["x_axis"][0])
    y_axis = int(gp["y_axis"][0])
    z_axis = int(gp["z_axis"][0])
    invz = bool(gp["invz"][0])

    #clip parameters removes certain amount from back and front
    clipf = int(gp["clipf"][0])
    clipb = int(gp["clipb"][0])
    invert = bool(gp["invert"][0])
    top_flatten = bool(gp["top_flatten"][0])
    bottom_flatten = bool(gp["bottom_flatten"][0])
    sfiles = 0

    try:
        im = io.imread(data_filename)
    except:
        print("Default TIFF reader failed. Trying medpy.")
        try:
            from medpy.io import load
            from medpy.io import save
            im, image_header = load(data_filename)
        except:
            raise ModuleNotFoundError("Python package medpy could not be imported. Please make sure it is installed.")

    #invert image (e.g. if bright field reconstruction)
    if invert == True:
        im = np.max(im) - im

    #reorient your image axis to define shank - 1,2,0
    contour_field = np.transpose(im, (x_axis, y_axis, z_axis))

    print("Loaded image shape")
    print("ET data shape: " + str(contour_field.shape))

    contour_field = contour_field.astype("double")

    #remove aberrations at start and end
    trim_contour_field = contour_field[:, :, (clipb):-clipf]
    if invz == True:
        trim_contour_field = trim_contour_field[:, :, ::-1]

    #smooth out data and subsample
    trim_contour_field = gaussian_filter(trim_contour_field, 6.0)

    #perform global normalisation
    trim_contour_field = (trim_contour_field - np.min(trim_contour_field))/(np.max(trim_contour_field) - np.min(trim_contour_field))

    #subsample data following smoothing
    sample = 3
    trim_contour_field = trim_contour_field[::sample, ::sample, ::sample]

    if bottom_flatten == 1:
        trim_contour_field[:, :, 0] = -200.0
    if top_flatten == 1:
        trim_contour_field[:, :, -1] = -200.0

    phase_values = phase_values.split(sep = ";")
    phases = {}
    for p in range(len(phase_values)):
        pvalues = phase_values[p].split(":")
        phases[pvalues[0]] = {"threshold": float(pvalues[1]), "surface": int(pvalues[2])}

    for phase in phases:
        threshold = phases[phase]["threshold"]
        verts, faces, normals, values = measure.marching_cubes_lewiner(trim_contour_field, threshold)

        verts = verts.astype("double")
        faces = faces.astype("long")

        ###construct face-face adjacency list
        face_face_graph = mesh_library.adjacency_list_face_faces(verts, faces)

        #perform depth-first search for identifying connected regions
        mesh_list = []
        mesh_listc = []
        inc_faces = np.zeros((len(faces)), dtype = int) + 1
        tfaceind = np.linspace(0, len(inc_faces), len(inc_faces), endpoint = False, dtype = int)

        if phases[phase]["surface"] == 0:
            print("output phase")
            #tverts, tfaces = cgal_linker.isotropic_remesh(verts, faces, 4)
            mvalues = np.array([np.max(verts[:, 2]) + 1e-2])
            fv = np.array([4.0])
            tverts, tfaces = cgal_linker.adaptive_remeshing(verts, faces, mvalues, fv, False)
            tverts[:, 0] -= xy_centre[0]
            tverts[:, 1] -= xy_centre[1]

            tverts[:, 0] *= stretch_coeff
            tverts[:, 1] *= stretch_coeff

            #output phase surface meshes
            cells = tfaces
            model_writers.write_vtk(mesh_dir + "output_phases.vtk", tverts * sample * 1e-9, cells)

            tverts[:, 0] /= stretch_coeff
            tverts[:, 1] /= stretch_coeff   

            tverts[:, 0] += xy_centre[0]
            tverts[:, 1] += xy_centre[1]
            
        while np.sum(inc_faces) != 0:
            tr = tfaceind[inc_faces == 1][0]
            connected = mesh_library.DFS_iterative(face_face_graph, tr)

            mesh_listc.append(connected)
            mesh = {}

            #obtain separate meshes - remove isolated vertices
            vind, rfaces = np.unique(faces[connected == 1], return_inverse = True)
            mesh["faces"] = rfaces.reshape(-1, 3)
            mesh["vertices"] = verts[vind]

            mesh_list.append(mesh)
            inc_faces = inc_faces - connected

        print(phases[phase]["surface"])
        if phases[phase]["surface"] == 1:
            if len(mesh_list) > 1:
                lv = -1
                lvlen = 0

                #find largest contour - corresponds to surface
                for a in range(0, len(mesh_list)):
                    if len(mesh_list[a]["faces"]) > lvlen:
                        lvlen = len(mesh_list[a]["faces"])
                        lv = a
                        verts = mesh_list[a]["vertices"]
                        faces = mesh_list[a]["faces"]

            verts = verts.astype("double")
            faces = faces.astype("long")

            print("Performing isotropic mesh simplification")
            #Perform an isotropic remeshing
            #verts, faces = cgal_linker.isotropic_remesh(verts, faces, 4)
            mvalues = np.array([np.max(verts[:, 2]) + 1e-2])
            fv = np.array([4.0])
            verts, faces = cgal_linker.adaptive_remeshing(verts, faces, mvalues, fv, False)
            print("mesh simplification complete")

            verts = verts.astype("double")
            faces = faces.astype("long")

            verts = verts.astype("double")
            faces = faces.astype("long")

            #identify translation to centre surface mesh within the model space
            xy_centre = [np.mean(verts[:, 0]),np.mean(verts[:, 1])]
            verts[:, 0] -= xy_centre[0]
            verts[:, 1] -= xy_centre[1]

            #ouput sample surface mesh
            cells = faces
            model_writers.write_vtk(mesh_dir + "raw_output_surface.vtk", verts * sample * 1e-9, cells)

            #write to mstate
            model_writers.write_mesh_to_mstate(phase, verts * sample, faces, mstate_filename, phases[phase]["surface"])

        else:
            for a in range(0, len(mesh_list)):
                verts = mesh_list[a]["vertices"]
                faces = mesh_list[a]["faces"]

                #run function to check if volume below threshold, if so then ignore
                verts[:, 0] *= res_x * stretch_coeff
                verts[:, 1] *= res_y * stretch_coeff
                verts[:, 2] *= res_z

                vol = abs(calculate_volume(verts, faces))
                print(vol)
                if vol <= ignore_size**3.:
                    print("ignore this phase")
                    continue

                print("centre:" + str(np.mean(verts[:, 2])))

                verts[:, 0] = verts[:, 0]/(res_x * stretch_coeff)
                verts[:, 1] = verts[:, 1]/(res_y * stretch_coeff)
                verts[:, 2] = verts[:, 2]/res_z
                verts = verts.astype("double")
                faces = faces.astype("long")

                #verts, faces = cgal_linker.isotropic_remesh(verts, faces, 3)
                mvalues = np.array([np.max(verts[:, 2]) + 1e-2])
                fv = np.array([3.0])
                verts, faces = cgal_linker.adaptive_remeshing(verts, faces, mvalues, fv, False)
                try:
                    verts[:, 0] -= xy_centre[0]
                    verts[:, 1] -= xy_centre[1]
                except NameError:
                    raise NameError("Please ensure that the phase defining the sample surface is listed first on input")

                sfiles += 1

                verts = verts.astype("double")
                faces = faces.astype("long")

                #expand phase to edges
                pcentre = np.mean(verts, axis = 0)
                verts -= pcentre
                verts[:, 0] = verts[:, 0] * stretch_coeff
                verts[:, 1] = verts[:, 1] * stretch_coeff
                verts += pcentre

                cells = faces
                model_writers.write_vtk(mesh_dir + "phase_"  + phase + str(sfiles) + ".vtk" , verts * sample * 1e-9, cells)

                #write to mstate
                print("phase name: " + phase + str(sfiles))
                model_writers.write_mesh_to_mstate(phase + str(sfiles), verts * sample, faces, mstate_filename, phases[phase]["surface"])

#calculate mesh volume
#calculates enclosed area via the 3d Shoelace formula generalisation
def calculate_volume(verts, faces):

    """

    Function for calculating the volume of a triangular mesh

    calculates enclosed area via the 3d Shoelace formula generalisation

    :param1 verts: Array of mesh vertices
    :param2 faces: Mesh faces
    :returns: volume

    """

    vol = 0.0
    for f in faces:
        vol = vol + 1.0/6.0 * np.linalg.det([verts[f[0]], verts[f[1]], verts[f[2]]])
    return vol

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--input_data', metavar='The ET input data (TIFF)', type = str, default = "c3d_input/diamond/recon.tif",
                        help='The ET input data (Tiff stack)')
    parser.add_argument('--parameters', metavar='Parameter file (tsv)', type = str, default = "c3d_input/diamond/ET_loader_parameters",
                        help='The tsv file containing the mesh extraction parameters')
    parser.add_argument('--phases', metavar='The phase values', type = str, default = "bulk:93.0:1;phase:150.5:0",
                        help='A string defining the particular material phases, image threshold values, and whether sample surface or not (sep by ;) e.g. "bulk:93.0:1;phase1:154.0:0"')
    parser.add_argument('--mstate', metavar='The generated mstate filename', type = str, default = "c3d_input/diamond/DET1.mstate",
                        help='The name of the generated mstate file for model intialisation')
    parser.add_argument('--stretch', metavar='stretch phases', type = float, default = 1.0,
                        help='Coefficient to laterally stretch phases by')

    args = parser.parse_args()

    #simulation parameter definition
    data_filename = args.input_data
    param_filename = args.parameters
    phase_values = args.phases
    mstate_filename = args.mstate
    stretch_coeff = args.stretch

    generate_mesh_from_ET(data_filename, param_filename, phase_values, mstate_filename, stretch_coeff = stretch_coeff)
