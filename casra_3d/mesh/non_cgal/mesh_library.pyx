
"""

The cython module for constructing the BEM matrices

Author: Charles Fletcher
Atom Probe Tomography Group, Department of Materials, University of Oxford
May 2020

"""

import cython

import numpy as np
cimport numpy as np

from cython.parallel import prange,parallel
from libcpp.vector cimport vector
from libcpp.set cimport set
from libcpp.algorithm cimport sort
from cython.operator cimport dereference as deref, preincrement as inc
from cpython.exc cimport PyErr_CheckSignals

#external function
cdef extern from "math.h" nogil:
    double sqrt(double m)

cdef extern from "math.h" nogil:
    double atan2(double a, double b)

cdef extern from "math.h" nogil:
    double fabs(double v)

cdef extern from "math.h" nogil:
    double log(double v)

cdef extern from "math.h" nogil:
    double log(double v)

@cython.cdivision(True)
cdef inline double clamp(double v, double lo, double hi) nogil:
  if v < lo:
    return lo
  if hi < v:
    return hi
  return v

ctypedef vector[int] int_vec
ctypedef vector[int_vec] int_vec_vec

ctypedef set[int] int_set
ctypedef vector[int_set] int_vec_set

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def calculate_normals(double[:, ::1] verts, long[:, ::1] faces):
    """

    Function for calculating panel normals (assuming righthand vertex ordering)

    :param1 verts: 2D array of mesh panel vertices
    :param2 faces: 2D array for mesh connectivity matrix
    :returns:
        :norms: The calculated 2D array of panel normals

    """

    norms = np.zeros((len(faces), 4), dtype = np.double)
    cdef double[:, ::1] norms_view = norms

    cdef double ya0, ya1, ya2, yb0, yb1, yb2, yc0, yc1, yc2

    cdef Py_ssize_t j

    for j in range(len(faces)):

        ya0 = verts[faces[j, 0], 0]
        ya1 = verts[faces[j, 0], 1]
        ya2 = verts[faces[j, 0], 2]

        yb0 = verts[faces[j, 1], 0]
        yb1 = verts[faces[j, 1], 1]
        yb2 = verts[faces[j, 1], 2]

        yc0 = verts[faces[j, 2], 0]
        yc1 = verts[faces[j, 2], 1]
        yc2 = verts[faces[j, 2], 2]

        #calculate the cross product of vectors V1 = y0 - y1 and V2 =  y0 - y2
        norms_view[j, 0] = ((yb1 - ya1) * (yc2 - ya2) - (yb2 - ya2) * (yc1 - ya1))
        norms_view[j, 1] = ((yb2 - ya2) * (yc0 - ya0) - (yb0 - ya0) * (yc2 - ya2))
        norms_view[j, 2] = ((yb0 - ya0) * (yc1 - ya1) - (yb1 - ya1) * (yc0 - ya0))
        norm_mag = sqrt((norms_view[j, 0]*norms_view[j, 0] + norms_view[j, 1]*norms_view[j, 1] + norms_view[j, 2]*norms_view[j, 2]))

        #normalise
        norms_view[j, 0] = norms_view[j, 0]/norm_mag
        norms_view[j, 1] = norms_view[j, 1]/norm_mag
        norms_view[j, 2] = norms_view[j, 2]/norm_mag
        norms_view[j, 3] = 1.0

    return norms

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def shortest_surface_distance(double[:, ::1] vertices, double[:, ::1] lcp, long[:, ::1] faces):

    """

    Python callable function calculates the shortest distance between a set of points (lcp) and a surface mesh

    :param1 vertices: 2D array of vertex coordinates for mesh
    :param2 lcp: 2D array of point coordinates to calculate distance to mesh with
    :param3 faces: 2D array representing the connectivity matrix of the mesh

    :returns:
        :shortest_dist: 1D array (same length as lcp) of shortest distances between lcp points and surface mesh

    """

    cdef double dist1 = 1e10
    cdef double dist2 = 1e10
    cdef double tdist = 0.0

    cdef double c1x = 0.0
    cdef double c1y = 0.0

    cdef double c2x = 0.0
    cdef double c2y = 0.0

    cdef long mv = 0

    cdef Py_ssize_t va, vb

    cdef long vbmax = len(lcp)
    cdef long vamax = len(faces)

    shortest_dist = np.zeros(vbmax, dtype = "double")
    cdef double[::1] shortest_dist_view = shortest_dist

    cdef double sb = 0.0
    cdef double sa = 0.0
    cdef double l = 0.0
    cdef double AB = 0.0

    cdef double sign = 0.0

    cdef double point[3]
    cdef double vec[3]
    cdef double edge1[3]
    cdef double edge2[3]

    cdef double det
    cdef double invdet
    cdef double v
    cdef double w
    cdef double u

    cdef double d00
    cdef double d01
    cdef double d11
    cdef double d20
    cdef double d21

    cdef double t
    cdef double s
    cdef double clp[3]

    cdef double numer
    cdef double denom

    cdef double tmp0
    cdef double tmp1


    for vb in range(vbmax):
        PyErr_CheckSignals()

        dist1 = 1e10
        for va in range(vamax):

            #calculate barycentric coordinates to triangle
            edge1[0] = vertices[faces[va, 1], 0] - vertices[faces[va, 0], 0]
            edge1[1] = vertices[faces[va, 1], 1] - vertices[faces[va, 0], 1]
            edge1[2] = vertices[faces[va, 1], 2] - vertices[faces[va, 0], 2]

            edge2[0] = vertices[faces[va, 2], 0] - vertices[faces[va, 0], 0]
            edge2[1] = vertices[faces[va, 2], 1] - vertices[faces[va, 0], 1]
            edge2[2] = vertices[faces[va, 2], 2] - vertices[faces[va, 0], 2]

            vec[0] = vertices[faces[va, 0], 0] - lcp[vb, 0]
            vec[1] = vertices[faces[va, 0], 1] - lcp[vb, 1]
            vec[2] = vertices[faces[va, 0], 2] - lcp[vb, 2]

            d00 = edge1[0]*edge1[0] + edge1[1]*edge1[1] + edge1[2]*edge1[2]
            d01 = edge1[0]*edge2[0] + edge1[1]*edge2[1] + edge1[2]*edge2[2]
            d11 = edge2[0]*edge2[0] + edge2[1]*edge2[1] + edge2[2]*edge2[2]
            d20 = vec[0]*edge1[0] + vec[1]*edge1[1] + vec[2]*edge1[2]
            d21 = vec[0]*edge2[0] + vec[1]*edge2[1] + vec[2]*edge2[2]

            det = (d00 * d11 - d01 * d01)

            s = d01*d21 - d11*d20
            t = d01*d20 - d00*d21

            if ((s + t) < det):
                if (s < 0.0):
                    if (t < 0.0):
                        if (d20 < 0.0):
                            s = clamp(-d20/d00, 0.0, 1.0)
                            t = 0.0
                        else:
                            s = 0.0
                            t = clamp(-d21/d11, 0.0, 1.0)
                    else:
                        s = 0.0
                        t = clamp(-d21/d11, 0.0, 1.0)
                elif (t < 0.0):
                    s = clamp(-d20/d00, 0.0, 1.0)
                    t = 0.0
                else:
                    invdet = 1.0/det
                    s = s*invdet
                    t = t*invdet
            else:
                if (s < 0.0):
                    tmp0 = d01+d20
                    tmp1 = d11+d21
                    if (tmp1 > tmp0):
                        numer = tmp1 - tmp0
                        denom = d00 - 2.0*d01 + d21
                        s = clamp(numer/denom, 0.0, 1.0)
                        t = 1.0-s
                    else:
                        t = clamp(-d21/d11, 0.0, 1.0)
                        s = 0.0

                elif (t < 0.0):
                    if ((d00 + d20) > (d01 + d21)):
                        numer = d11+d21 - d01 - d20
                        denom = d00 - 2.0*d01 + d11
                        s = clamp(numer/denom, 0.0, 1.0)
                        t = 1.0 - s
                    else:
                        s = clamp(-d21/d11, 0.0, 0.0)
                        t = 0.0
                else:
                    numer = d11+d21 - d01 - d20
                    denom = d00 - 2.0*d01 + d11
                    s = clamp(numer/denom, 0.0, 1.0)
                    t = 1.0 - s

            #calculate closest point
            clp[0] = vertices[faces[va, 0], 0] + s * edge1[0] + t * edge2[0]
            clp[1] = vertices[faces[va, 0], 1] + s * edge1[1] + t * edge2[1]
            clp[2] = vertices[faces[va, 0], 2] + s * edge1[2] + t * edge2[2]

            tdist = (lcp[vb, 0] - clp[0])*(lcp[vb, 0] - clp[0]) + (lcp[vb, 1] - clp[1])*(lcp[vb, 1] - clp[1]) + (lcp[vb, 2] - clp[2])*(lcp[vb, 2] - clp[2])

            #closest distance to points
            if tdist < dist1:
                dist1 = tdist
                mv = va

        shortest_dist_view[vb] = sqrt(dist1)

    return shortest_dist


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cpdef average_field(long[:, ::1] faces, double[::1] area, double[::1] field):

    """

    Python callable function for smoothing the average surface flux via a median filter
    This loss of accuracy compensates for the relatively poor conditioning of the collocation BEM

    :param1 faces: 2D array for the mesh connectivity matrix
    :param2 area: Array of panel areas
    :param3 field: The surface electric field panel fluxes
    :returns:
        :av_field: The median filtered panel fluxes
    """

    cdef int lf = len(faces)
    cdef int i = 0
    cdef int j = 0

    av_field = np.array([0.0] * len(field))
    cdef double[::1] av_field_view = av_field
    cdef double area_tot = 0.0

    cdef double area_min = min(area)
    cdef double area_max = max(area)

    cdef vector[double] double_vec

    cdef int l = 0

    cdef int size = 0
    cdef double med_f = 0

    #perform median filtering
    for i in range(0, lf):

        area_tot = 0.0
        l = 0
        double_vec.clear()
        #find neighbouring panels
        for j in range(0, lf):
            if faces[i, 0] == faces[j, 0] or faces[i, 0] == faces[j, 1] or faces[i, 0] == faces[j, 2] or faces[i, 1] == faces[j, 0] or faces[i, 1] == faces[j, 1] or faces[i, 1] == faces[j, 2] or faces[i, 2] == faces[j, 0] or faces[i, 2] == faces[j, 1] or faces[i, 2] == faces[j, 2]:
                double_vec.push_back(field[j])
        #sort neighbouring panel fluxes
        sort(double_vec.begin(), double_vec.end())

        size = double_vec.size()

        #select middle (median) flux value from neighbours list
        if ((size % 2) == 0):
            med_f = (double_vec[size/2] + double_vec[(size/2) - 1])/2.0

        else:
            med_f = double_vec[size/2]


        av_field_view[i] = med_f

    return av_field

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cpdef adjacency_list_vertex_faces(double[:, ::] verts, long[:, ::] faces):

    """

    Python callable function for calculating the mesh vertex (neighbouring faces for a vertex) adjacency list

    :param1 verts: 2D array of mesh vertices
    :param2 faces: mesh connectivity matrix
    :returns:
        :listth: vertex adjacency list

    """

    cdef int f = 0
    cdef int lv =  len(verts)
    cdef int lf = len(faces)

    cdef int_vec el
    el = int_vec(0)

    cdef int_vec_vec adjacency_listf
    adjacency_listf = int_vec_vec(lv, el)

    for f in range(0, lf):
        adjacency_listf[faces[f, 0]].push_back(f)
        adjacency_listf[faces[f, 1]].push_back(f)
        adjacency_listf[faces[f, 2]].push_back(f)

    listth = adjacency_listf

    return listth

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cpdef adjacency_list_face_faces(double[:, ::] verts, long[:, ::] faces):

    """

    Python callable function for calculating the mesh faces (neighbouring faces for a face) adjacency list

    :param1 verts: 2D array of mesh vertices
    :param2 faces: mesh connectivity matrix
    :returns:
        :listth: faces adjacency list

    """

    cdef int lv =  len(verts)
    cdef int lf = len(faces)

    cdef int_vec el
    el = int_vec(0)

    cdef int_vec_vec adjacency_listf
    adjacency_listf = int_vec_vec(lv, el)

    cdef int f
    cdef int fl

    #construct vertex adjacency list
    for f in range(0, lf):
        adjacency_listf[faces[f, 0]].push_back(f)
        adjacency_listf[faces[f, 1]].push_back(f)
        adjacency_listf[faces[f, 2]].push_back(f)

    cdef int_set el2
    el2 = int_set()
    cdef int_vec_set adjacency_listff
    adjacency_listff = int_vec_set(lf, el2)

    #use vertex adjacency list to calculate face adjacency list
    for f in range(0, lf):
        for fl in range(0, adjacency_listf[faces[f, 0]].size()):
            adjacency_listff[f].insert(adjacency_listf[faces[f, 0]][fl])
        for fl in range(0, adjacency_listf[faces[f, 1]].size()):
            adjacency_listff[f].insert(adjacency_listf[faces[f, 1]][fl])
        for fl in range(0, adjacency_listf[faces[f, 2]].size()):
            adjacency_listff[f].insert(adjacency_listf[faces[f, 2]][fl])

    return adjacency_listff

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cpdef DFS_iterative(face_face_graph, long v):

    """

    Python callable function for performing an iterative depth-first search (DFS) for determining detached mesh components.

    :param1 face_face_graph: The face-face adjacency list
    :param2 v: Initial face index for the algorithm
    :returns:
        :connected: 1D array of mesh component identity labels for each panel

    """

    cdef int w
    connected = np.zeros((len(face_face_graph)), dtype = long)
    cdef long[:] connected_view = connected

    cdef vector[long] S
    S.push_back(v)

    while S.size() != 0:
        v = S.back()
        S.pop_back()
        if connected_view[v] == 0:
            connected_view[v] = 1
            for w in face_face_graph[v]:
                S.push_back(w)

    return connected

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cpdef adjacency_list_vertex_vertices(double[:, ::] verts, long[:, ::] faces):

    """

    Python callable function for calculating the mesh vertex (neighbouring vertices for a vertex) adjacency list

    :param1 verts: 2D array of mesh vertices
    :param2 faces: mesh connectivity matrix
    :returns:
        :listth: vertex adjacency list

    """

    cdef int lv =  len(verts)
    cdef int lf = len(faces)

    cdef int_set el
    el = int_set()

    cdef int_vec_set adjacency_listf
    adjacency_listf = int_vec_set(lv, el)

    for f in range(0, lf):
        adjacency_listf[faces[f, 0]].insert(faces[f, 1])
        adjacency_listf[faces[f, 0]].insert(faces[f, 2])
        adjacency_listf[faces[f, 1]].insert(faces[f, 2])
        adjacency_listf[faces[f, 1]].insert(faces[f, 0])
        adjacency_listf[faces[f, 2]].insert(faces[f, 0])
        adjacency_listf[faces[f, 2]].insert(faces[f, 1])

    listth = adjacency_listf

    return listth

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cpdef average_normals(double[:, ::] verts, long[:, ::] faces, double[:, ::1] norms):

    """

    Python callable function for averaging neighbouring panel normals to determine a vertex normal

    :param1 verts: The mesh vertices
    :param2 faces: The mesh connectivity_matrix
    :param3 norms: The mesh panel normals
    :returns:
        :anorms: 2D array of averaged vertex normals

    """

    cdef int lv =  len(verts)
    cdef int lf = len(faces)

    cdef int_set el
    el = int_set()

    cdef int_vec_set adjacency_listf
    adjacency_listf = int_vec_set(lv, el)

    anorms = np.zeros((lv, 3), dtype = np.double)
    cdef double[:, ::1] anorms_view = anorms

    npanels = np.zeros((lv), dtype = np.double)
    cdef double[::1] npanels_view = npanels

    for f in range(0, lf):
        adjacency_listf[faces[f, 0]].insert(faces[f, 1])
        adjacency_listf[faces[f, 0]].insert(faces[f, 2])
        adjacency_listf[faces[f, 1]].insert(faces[f, 2])
        adjacency_listf[faces[f, 1]].insert(faces[f, 0])
        adjacency_listf[faces[f, 2]].insert(faces[f, 0])
        adjacency_listf[faces[f, 2]].insert(faces[f, 1])

        anorms_view[faces[f, 0], 0] += norms[f, 0]
        anorms_view[faces[f, 0], 1] += norms[f, 1]
        anorms_view[faces[f, 0], 2] += norms[f, 2]

        anorms_view[faces[f, 1], 0] += norms[f, 0]
        anorms_view[faces[f, 1], 1] += norms[f, 1]
        anorms_view[faces[f, 1], 2] += norms[f, 2]

        anorms_view[faces[f, 2], 0] += norms[f, 0]
        anorms_view[faces[f, 2], 1] += norms[f, 1]
        anorms_view[faces[f, 2], 2] += norms[f, 2]

        npanels_view[faces[f, 0]] += 1.0
        npanels_view[faces[f, 1]] += 1.0
        npanels_view[faces[f, 2]] += 1.0

    for f in range(0, lv):
        anorms_view[f, 0] = anorms_view[f, 0]/npanels_view[f]
        anorms_view[f, 1] = anorms_view[f, 1]/npanels_view[f]
        anorms_view[f, 2] = anorms_view[f, 2]/npanels_view[f]

    listth = adjacency_listf

    return anorms

#calculate mesh volume
#calculates enclosed area via the 3d Shoelace formula generalisation
def calculate_volume(double[:, ::1] verts, long[:, ::1] faces):

    """

    Function for calculating mesh volume via 3D Shoelace formula

    :param1 verts: The input mesh vertices
    :param2 faces: the input mesh faces

    returns: mesh volume

    """

    cdef double vol = 0.0
    cdef int f
    cdef double p0x
    cdef double p0y
    cdef double p0z
    cdef double p1x
    cdef double p1y
    cdef double p1z
    cdef double p2x
    cdef double p2y
    cdef double p2z
    cdef double det
    #Sum over the tetrahedral determinant (signed volumes)
    for f in range(len(faces)):
        det = 0.0
        p0x = verts[faces[f, 0], 0]
        p0y = verts[faces[f, 0], 1]
        p0z = verts[faces[f, 0], 2]
        p1x = verts[faces[f, 1], 0]
        p1y = verts[faces[f, 1], 1]
        p1z = verts[faces[f, 1], 2]
        p2x = verts[faces[f, 2], 0]
        p2y = verts[faces[f, 2], 1]
        p2z = verts[faces[f, 2], 2]
        #calculate tetrahedron determinant
        det = p0x * (p1y * p2z - p2y * p1z) - p0y * (p1x * p2z - p2x * p1z) + p0z * (p1x * p2y - p2x * p1y)
        vol = vol + 1.0/6.0 * det
    return vol

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def exp_void_region(int[:, :, ::1] surf_nonsurf, int lenx, int leny, int lenz):

    """

    Python callable function for eroding/expanding mask for the level set field gaussian smoothing
    This expanded regions is masked during Gaussian smoothing to avoid unphysical level set movement prior to surface evaporation.

    :param1 surf_nonsurf: 3D array of whether level set grid cell is sample surface (1) or not (0)
    :param2 lenx: The level set grid x dimension
    :param3 leny: The level set grid y dimension
    :param4 lenz: The level set grid z dimension
    :returns:
        :nsurf_nonsurf: 3D eroded (expanded) masking array

    """

    cdef int a, b, c

    nsurf_nonsurf = np.zeros((lenx, leny, lenz), dtype = "int") + 1
    cdef long[:, :, ::1] nsurf_nonsurf_view = nsurf_nonsurf

    for a in range(1,  lenx - 2):
        for b in range(1, leny - 2):
            for c in range(1, lenz - 2):
                if surf_nonsurf[a, b, c] * surf_nonsurf[a + 1, b, c] * surf_nonsurf[a-1, b, c]  * surf_nonsurf[a, b + 1, c] * surf_nonsurf[a + 1, b + 1, c] * surf_nonsurf[a-1, b + 1, c] * surf_nonsurf[a, b - 1, c] * surf_nonsurf[a + 1, b - 1, c] * surf_nonsurf[a-1, b - 1, c] == 0:
                    nsurf_nonsurf[a, b, c] = 0
                if surf_nonsurf[a, b, c + 1] * surf_nonsurf[a + 1, b, c + 1] * surf_nonsurf[a-1, b, c + 1]  * surf_nonsurf[a, b + 1, c + 1] * surf_nonsurf[a + 1, b + 1, c + 1] * surf_nonsurf[a-1, b + 1, c + 1] * surf_nonsurf[a, b - 1, c + 1] * surf_nonsurf[a + 1, b - 1, c + 1] * surf_nonsurf[a-1, b - 1, c + 1] == 0:
                    nsurf_nonsurf[a, b, c] = 0
                if surf_nonsurf[a, b, c - 1] * surf_nonsurf[a + 1, b, c - 1] * surf_nonsurf[a-1, b, c - 1]  * surf_nonsurf[a, b + 1, c - 1] * surf_nonsurf[a + 1, b + 1, c - 1] * surf_nonsurf[a-1, b + 1, c - 1] * surf_nonsurf[a, b - 1, c - 1] * surf_nonsurf[a + 1, b - 1, c - 1] * surf_nonsurf[a-1, b - 1, c - 1] == 0:
                    nsurf_nonsurf[a, b, c] = 0

    return nsurf_nonsurf
