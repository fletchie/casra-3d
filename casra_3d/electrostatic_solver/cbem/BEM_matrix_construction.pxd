"""

Header for BEM_matrix_construction module (allows for cimports)

Author: Charles Fletcher
Atom Probe Tomography Group, Department of Materials, University of Oxford
May 2020

"""

from libcpp.vector cimport vector

cpdef cfield_calculation(double[::1] x, double[:, ::1] verts, long[:, ::1] faces, vector[long] cluster_faces, double[:, ::1] centres, double[::1] areas, double[:, ::1] norms, double[::1] BP, double[::1] BF, double[::1] ls2, int f_l, double eps)
