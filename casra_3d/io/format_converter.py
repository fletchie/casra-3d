#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Module containing functions for converting file types (e.g. .epos to .vtk)

Author: Charles Fletcher, Daniel Haley
Atom Probe Tomography Group, Department of Materials, University of Oxford
August 2020

"""

import csv
import copy
import sys

try:
    import numpy as np
except ModuleNotFoundError:
    raise ModuleNotFoundError("Could not import Python package numpy. Please make sure it is installed.")


from casra_3d.point_projection_reconstruction import point_projection_mapping as ppm
from casra_3d.point_projection_reconstruction import loader as loader
from casra_3d.io import model_loaders as model_loaders
from casra_3d.io import model_writers as model_writers

def convert_epos_vtk(filename, out_dir, rangefile, sample=0.1, sep_el=["C:1"]):

    """

    function for converting reconstructed coordinates in a .epos file to a .vtk file.

    :param1 filename (str): The .epos filename
    :param2 out_dir (str): The output directory. Output .vtk files are dumped
                           in this directory.
    :param3 rangefile (str): The corresponding reconstruction range file
    :param4 sample (float): The fraction of rows to sample from the
                            .epos file (between 0[none] and 1[all]).
    :param5 sep_el (list): List containing ion names (contained in the rangefile)
                           to also output filtered point clouds for

    :returns:
        :returncode (int): 0 if successful (no Exception raised)

    :raises:
        ValueError: No reconstruction ion position data found in the passed
                    .epos file

    """

    #read in epos data
    dx, dy, da, VP, pslep, sx, sy, sz = loader.chunk_epos_reader(filename,
                                                                 int(1.0/sample))
    #read range file and range loaded data
    ions, rfile = loader.read_rrng(rangefile)
    chem, ions, ic = ppm.range(da, rfile)
    output_ion_indexes = []

    for a in range(0, len(sep_el)):
        output_ion_indexes.append(ions.index(sep_el[a]) + 1)

    chem = np.array(chem)

    if len(sx) == 0:
        raise ValueError("No attached reconstruction position data located in " +
                         "epos file: " + filename)
    else:
        print("Original coordinates detected and outputed.")
        points = np.vstack((sx, sy, sz)).T
        point_ind = np.linspace(0, len(points), len(points),
                                endpoint=False).astype(int)
        cells = point_ind.reshape(-1, 1)
        point_data = {"chemistry": chem}
        print("outputing original coordinates to: " + out_dir + "/original_pos.vtk")
        model_writers.write_vtk(out_dir + "/original_pos.vtk", points,
                                cells, point_data=point_data)
        print("Original coordinates detected and outputed.")

        #output filtered point clouds for specific elements found in rangefile
        for a in range(0, len(sep_el)):
            if len(sx[chem == sep_el[a]]) > 0:
                points = np.vstack((sx[chem == sep_el[a]],
                                    sy[chem == sep_el[a]],
                                    sz[chem == sep_el[a]])).T
                point_ind = np.linspace(0, len(points), len(points),
                                        endpoint=False).astype(int)
                cells = point_ind.reshape(-1, 1)
                cell_data = {"chemistry": np.array([1] * len(points))}
                name = out_dir + "/original_" + sep_el[a] + "_pos.vtk"

                print("outputing original coordinates to: " + name)
                model_writers.write_vtk(name, points, cells,
                                        cell_data=cell_data)

    print("epos -> vtk conversion completed")
    return 0


def convert_vtk_pos(input_vtk_file, output_pos_file, output_rrng_file,
                materials_db="", el_dictionary={}, simulated=True, mesh=None):

    """

    Function for converting a .vtk reconstruction file to a .pos file and .rrng file

    :param1 input_vtk_file (str): The input .vtk reconstruction filename
    :param2 output_pos_file (str): The output .pos reconstruction filename
    :param3 output_rrng_file (str)): The .rrng filename
    :param4 materials_db (str): The materials database
                                (only required for reconstructions using experimental data
                                 i.e. simulated=False)
    :param5 el_dictionary (dict): The dictionary matching up ion integer numbers
                                  (from the `chemistry` attribute within the .vtk)
                                  with ion labels (required for simulated data
                                  i.e. simulated=True)
                                  e.g. {"14": "iron", "-1": "unknown"}
    :param6 simulated (bool): Where .vtk reconstruction is simulated (True)
                              or experimental data (False)

    :returns:
        :returncode (int): 0 if successful (no Exception raised)

    :raises:
        FileNotFoundError: No materials database file found
        IOError: Could not connect to passed SQLite database
        ValueError: No element dictionary has been passed
        ValueError: Specific ion id not found in element dictionary (el_dictionary)

    """

    #load .vtk reconstruction
    if mesh == None:
        mesh = model_loaders.read_vtk(input_vtk_file)
    atoms = np.array(mesh.points)
    atom_properties = mesh.point_data
    atom_chemistry = atom_properties["chemistry"]

    uatom_chemistry = np.unique(atom_chemistry)
    uchem = np.arange(0, len(uatom_chemistry), 1, dtype=int)

    if "Da" not in atom_properties:
        #connect to database to match up element ids to element labels
        if simulated is False:
            if materials_db == "":
                raise FileNotFoundError("no materials database filename passed " +
                                        "to function. Please make sure database is passed.")

            try:
                conn = sqlite3.connect(materials_db)
            except:
                raise IOError("Cannot connect to database. Please make " +
                              "sure database filename is correct.")

            c = conn.cursor()
            el_ids = c.execute('SELECT element_id, element FROM element_ids').fetchall()
            ids = np.array([a[0] for a in el_ids])
            els = np.array([a[1] for a in el_ids], dtype='object')
            els = els[np.sort(ids)]
            ids = ids[np.sort(ids)]

            #create .rrng file
            f = open(output_rrng_file, "w")
            f.write("[Ions]\n")
            f.write("Number=" + str(len(uatom_chemistry)) + "\n")

            for a in range(len(uatom_chemistry)):
                f.write("Ion" + str(a + 1) + "=" + els[uchem[a]] + "\n")

            f.write("[Ranges]\n")
            f.write("Number=" + str(len(uatom_chemistry)) + "\n")

            for a in range(len(uatom_chemistry)):
                f.write("Range" + str(a+1) + "= " + str(uatom_chemistry[a] - 1e-4) +
                        " " + str(uatom_chemistry[a] + 1e-4) + " Vol:0.1 " +
                        els[uchem[a]] + ":1 Color:" + str(660033) + "\n")
            #create .rrng file
        else:
            if el_dictionary == {}:
                raise ValueError("no element dictionary has been passed. " +
                                 "Please make sure this dictionary is passed " +
                                 "labelling all ions!")


            f = open(output_rrng_file, "w")
            f.write("[Ions]\n")
            f.write("Number=" + str(len(uatom_chemistry)) + "\n")
            for a in range(len(uatom_chemistry)):
                try:
                    b = el_dictionary[str(uatom_chemistry[a])]
                except:
                    print("element dictionary passed was " + str(el_dictionary))
                    print("try: " + str(el_dictionary)[:-1] + ",'" +
                          str(uatom_chemistry[a]) + "':`desired_label`}")
                    raise ValueError("Ion number " + str(uatom_chemistry[a]) +
                                     " does not have a matching label in the element " +
                                     "dictionary argument. Please make sure this ion key " +
                                     "in the element dictionary value pair.")


            for a in range(len(uatom_chemistry)):
                f.write("Ion" + str(a + 1) + "=" + el_dictionary[str(uatom_chemistry[a])] + "\n")

            f.write("[Ranges]\n")
            f.write("Number=" + str(len(uatom_chemistry)) + "\n")
            b = 0
            for a in range(len(uatom_chemistry)):
                f.write("Range" + str(a+1) + "= " + str(float(uatom_chemistry[a]) - 1e-4) + " " +
                        str(float(uatom_chemistry[a]) + 1e-4) + " Vol:0.1 " +
                        el_dictionary[str(uatom_chemistry[a])] + ":1 Color:" +
                        str(660033) + "\n")
            f.close()

        #rescale spatial dimensions to nm + reformat data
        pos_data = np.hstack((atoms * 1e9, np.array(atom_properties["chemistry"].astype("float")
                                                   ).reshape(-1, 1)))
        pos_data = pos_data.flatten().tolist()

        #export to .pos
        export_pos(output_pos_file, pos_data)
    else:
        pos_data = np.hstack((atoms * 1e9, np.array(atom_properties["Da"]
                                                   ).reshape(-1, 1)))
        pos_data = pos_data.flatten().tolist()

        #export to .pos
        export_pos(output_pos_file, pos_data)

    print("vtk -> pos conversion completed")
    return 0
