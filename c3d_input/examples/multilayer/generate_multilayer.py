#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#add current working directory to system path
import os,sys,inspect
sys.path.insert(0,'')

from casra_3d.geometries import generate_model_geometry as generate_model_geometry
import numpy as np

###example commmands###
filename = "c3d_input/examples/multilayer/multilayer.mstate"
directory = "c3d_input/examples/multilayer"

#generate tip geometry
generate_model_geometry.generate_tip(3.0, 1.0, 8.0, filename, directory, name = "bulk", offset = 0.2)

#generate multilayer
generate_model_geometry.generate_cuboid(-10.0, 10.0, -10.0, 10.0, 7.4, 8.2, 0.0, 0.0, 0.0, filename, directory, "phase1")
