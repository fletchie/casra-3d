/*

Main script for CGAL wrapper

Author: Charles Fletcher
Atom Probe Tomography Group, Department of Materials, University of Oxford
May 2020

*/

#include <iostream>
#include <CGAL/Simple_cartesian.h>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Surface_mesh.h>
#include <CGAL/Polygon_mesh_processing/remesh.h>
#include <CGAL/Polygon_mesh_processing/border.h>
#include <boost/function_output_iterator.hpp>

#include <CGAL/Fixed_alpha_shape_3.h>
#include <CGAL/Fixed_alpha_shape_vertex_base_3.h>
#include <CGAL/Fixed_alpha_shape_cell_base_3.h>
#include <CGAL/Delaunay_triangulation_3.h>

#include <list>

#if defined(CGAL_LINKED_WITH_TBB)
#define TAG CGAL::Parallel_tag
#else
#define TAG CGAL::Sequential_tag
#endif

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Surface_mesh<K::Point_3> cgalMesh;
typedef CGAL::Simple_cartesian<double> Kernel;
typedef Kernel::Point_2 Point_2;
typedef Kernel::Segment_2 Segment_2;

typedef CGAL::Fixed_alpha_shape_vertex_base_3<K> Vb;
typedef CGAL::Fixed_alpha_shape_cell_base_3<K> Fb;

typedef CGAL::Triangulation_data_structure_3<Vb,Fb> Tds;
typedef CGAL::Delaunay_triangulation_3<K,Tds, CGAL::Fast_location> Delaunay;

typedef CGAL::Fixed_alpha_shape_3<Delaunay> Fixed_alpha_shape_3;

typedef K::Point_3 Point;
typedef Fixed_alpha_shape_3::Cell_handle Cell_handle;

#include "cgal_remesh.h"
#include <vector>

typedef std::vector<long> vec;

int main()
{
  return 0;
}

int test_return(int d){
  int c = d;
  return c + d;
}

  tetra_container alpha_shape(double* points, unsigned int pts, unsigned int dim, double alpha) {

    /*

    Function for calling CGAL alpha shape function

    :param1 points: The mesh vertices
    :param2 pts: The number of panels
    :param3 dim: The spatial dimension
    :param4 alpha: The alpha value for defining the surface
    :returns:
      :tetra: Pointer to the tetra container instance

    */

    double x, y, z;

    std::list<Point> lp;
    std::list<Cell_handle> cells;

    tetra_container tetra;

    std::map<Point, size_t> pointsout_original;

    //create cgal mesh from vertices and connectivity data
    for (auto i = 0u; i < pts; i++) {
      x = points[dim * i];
      y = points[dim * i + 1];
      z = points[dim * i + 2];
      lp.push_back(Point(x, y, z));
      pointsout_original[Point(x, y, z)] = i;
      std::vector<double> point;
      point.push_back(x);
      point.push_back(y);
      point.push_back(z);
      tetra.npoints.push_back(point);
    }

    //std::list<Facet> facets;
    Fixed_alpha_shape_3 as(lp.begin(), lp.end(), alpha);
    as.get_alpha_shape_cells(std::back_inserter(cells), Fixed_alpha_shape_3::INTERIOR);

    //find indexes of original
    for (auto cell:cells) {
        size_t v0 = pointsout_original.find(cell->vertex(0)->point())->second;
        size_t v1 = pointsout_original.find(cell->vertex(1)->point())->second;
        size_t v2 = pointsout_original.find(cell->vertex(2)->point())->second;
        size_t v3 = pointsout_original.find(cell->vertex(3)->point())->second;
        tetra.findices.push_back(v0);
        tetra.findices.push_back(v1);
        tetra.findices.push_back(v2);
        tetra.findices.push_back(v3);
    }

    return tetra;
  }

mesh_container iso_remesh(double* points, long* faces, int el, int fsize, int pts, int dim, double edge_coll){

    uint  i;
    double x, y, z;
    double i0, i1, i2;

    cgalMesh cgal_mesh;
    std::vector<cgalMesh::Vertex_index> created_points;

    //create cgal mesh from vertices and connectivity data
    for (i = 0; i < pts; i++) {
      x = points[dim * i];
      y = points[dim * i + 1];
      z = points[dim * i + 2];

      cgalMesh::Vertex_index u = cgal_mesh.add_vertex(K::Point_3(x, y, z));
      created_points.push_back(u);
    }

    //add faces to cgal mesh
    for (i = 0; i < el; i++) {
      x = faces[fsize * i];
      y = faces[fsize * i + 1];
      z = faces[fsize * i + 2];

      cgal_mesh.add_face(created_points[x], created_points[y], created_points[z]);
    }

    CGAL::Polygon_mesh_processing::isotropic_remeshing(cgal_mesh.faces(), edge_coll, cgal_mesh, CGAL::Polygon_mesh_processing::parameters::number_of_iterations(2).number_of_relaxation_steps(5));

    mesh_container Mesh;

    //Get vertices ...
    for (cgalMesh::Vertex_index vi : cgal_mesh.vertices()) {
        K::Point_3 pt = cgal_mesh.point(vi);
        std::vector<double> point;
        point.push_back((double)pt.x());
        point.push_back((double)pt.y());
        point.push_back((double)pt.z());
        Mesh.npoints.push_back(point);
        Mesh.vindices.push_back(vi);
    }

    for (cgalMesh::Face_index face_index : cgal_mesh.faces()) {
      CGAL::Vertex_around_face_circulator<cgalMesh> vcirc(cgal_mesh.halfedge(face_index), cgal_mesh), done(vcirc);
      do Mesh.findices.push_back(*vcirc++); while (vcirc != done);
    }

  return Mesh;
}

  mesh_container sample_remeshing(double* points, long* faces, int el, int fsize, unsigned int pts, unsigned int dim, std::vector<double> z_regions, std::vector<double> edge_coll){

      /*

      Function for calling CGALs isotropic remeshing function

      :param1 points: pointer to 2D array of mesh vertices
      :param2 faces: pointer to the mesh connectivity matrix
      :param3 el: The number of panels
      :param4 fsize: The vertices per panel (e.g. value is 3 for triangle elements)
      :param5 pts: The number of vertices
      :param6 dim: The spatial dimension
      :param7 z_regions: 1D array of z values between which to remesh with the corresponding value at the same index in edge_coll
      :param8 edge_coll: 1D array of average panel lengths for the remeshing algorithm to aim for
      :returns:
        :Mesh: pointer to the Mesh container instance

      */

      double x, y, z;

      cgalMesh cgal_mesh;
      std::vector<cgalMesh::Vertex_index> created_points;
      z_regions.push_back(-1000.0);

      //create cgal mesh from vertices and connectivity data
      for (auto i = 0u; i < pts; i++) {
        x = points[dim * i];
        y = points[dim * i + 1];
        z = points[dim * i + 2];

        cgalMesh::Vertex_index u = cgal_mesh.add_vertex(K::Point_3(x, y, z));
        created_points.push_back(u);
      }

      //add faces to cgal mesh
      for (auto i = 0; i < el; i++) {
        x = faces[fsize * i];
        y = faces[fsize * i + 1];
        z = faces[fsize * i + 2];

        cgal_mesh.add_face(created_points[x], created_points[y], created_points[z]);
      }

      std::vector<cgalMesh::Face_index> selected_faces;

      for (auto f = 0u; f < edge_coll.size(); f++) {

        selected_faces.clear();
        for (cgalMesh::Face_index face : cgal_mesh.faces()) {
          cgalMesh::Halfedge_index pv = cgal_mesh.halfedge(face);
          K::Point_3 pt1 = cgal_mesh.point(target(pv, cgal_mesh));
          K::Point_3 pt2 = cgal_mesh.point(target(next(pv, cgal_mesh), cgal_mesh));
          K::Point_3 pt3 = cgal_mesh.point(target(prev(pv, cgal_mesh), cgal_mesh));

          //if (pt1.z() < z_regions[f] && pt2.z() < z_regions[f] && pt3.z() < z_regions[f]) {
          if (pt1.z() < z_regions[f] && pt2.z() < z_regions[f] && pt3.z() < z_regions[f] && pt1.z() >= (z_regions[f+1] - 10.0) && pt2.z() >= (z_regions[f+1] - 10.0) && pt3.z() >= (z_regions[f+1] - 10.0)) {
            selected_faces.push_back(face);
          }
        }

        CGAL::Polygon_mesh_processing::isotropic_remeshing(selected_faces, edge_coll[f], cgal_mesh, CGAL::Polygon_mesh_processing::parameters::number_of_iterations(2).number_of_relaxation_steps(2));

        }

    mesh_container Mesh;

    std::vector<double> point;
	point.resize(3);
	Mesh.npoints.reserve(cgal_mesh.vertices().size());
	Mesh.vindices.reserve(cgal_mesh.vertices().size());
    //Get vertices ...
    for (cgalMesh::Vertex_index vi : cgal_mesh.vertices()) {
        K::Point_3 pt = cgal_mesh.point(vi);
	point[0] = (double)pt.x();
	point[1] = (double)pt.y();
	point[2] = (double)pt.z();
        Mesh.npoints.push_back(point);
        Mesh.vindices.push_back(vi);
    }

    //get face indices
    for (cgalMesh::Face_index face_index : cgal_mesh.faces()) {
      CGAL::Vertex_around_face_circulator<cgalMesh> vcirc(cgal_mesh.halfedge(face_index), cgal_mesh), done(vcirc);
      do Mesh.findices.push_back(*vcirc++); while (vcirc != done);
  }

    return Mesh;

  }
