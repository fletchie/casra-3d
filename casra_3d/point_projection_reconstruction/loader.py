
"""

Module containing methods for reading in and writing data to APT reconstruction files
e.g. .epos, .ato

Author: Charles Fletcher
Atom Probe Tomography Group, Department of Materials, University of Oxford
May 2020

"""

import re
import struct
import sys
import csv

try:
    import numpy as np
except ModuleNotFoundError:
    raise ModuleNotFoundError("The Python package Numpy cannot be found. Please make sure it has been installed (e.g. via apt or pip)")

def read_parameters_from_file(global_parameters_filename):

    """

    Function for reading in the reconstruction parameters from text file (tab separated).

    :param1 global_parameters_filename: String giving reconstruction parameters file location
    :returns:
        :read_dict: Dictionary with keys (parameter label) and values (parameter value)

    """

    read_dict = {}
    with open(global_parameters_filename) as f:
        reader = csv.reader(f, delimiter='\t')
        for row in reader:
            if len(row) > 1:
                if row[0][0] != "#":
                    if row[0] in read_dict:
                        raise ValueError("definition for parameter '" + row[0] + "' detected twice in the parameters file. Not sure which value to use. Make sure each parameter is only defined once!")
                    read_dict[row[0]] = row[1]
    return read_dict


#return filesize of fileobject
def getSize(fileobject):
    """

    Returns the binary file size

    :param1 fileobject: The read file object
    :returns:
        size - The file object size (e.g. number of lines)

    """
    fileobject.seek(0,2) # move the cursor to the end of the file
    size = fileobject.tell()
    fileobject.seek(0) # move the cursor to the beginning of the file
    return size

def read_epos(filename):

    """

    A python reader for the .epos APT data file. Once unpacked the 1D array d contains
    the following elements:

    'x': d[0::11], #reconstructed x-coordinate of ion within sample
    'y': d[1::11], #reconstructed y-coordinate of ion within sample
    'z': d[2::11], #reconstructed z-coordinate of ion within sample
    'Da': d[3::11],
    'ns': d[4::11],
    'DC_kV': d[5::11],
    'pulse_kV': d[6::11],
    'det_x': d[7::11],
    'det_y': d[8::11]})
    'pslep': d[9::11], # pulses since last event pulse
    'ipp': d[10::11]}) # ions per pulse

    The method also calculates and prints the instrument flightpath, variance in the ion flight path, and the detector radius.

    :param1 filename: The .epos filename
    :returns:
        det_x
        det_y
        Da
        pulse voltage
        pslep
        x
        y
        z

    """

    if filename.split(".")[-1] != "epos":
        raise TypeError("Incorrect filetype detected (based off name). Method only supports .epos file types. Please call appropriate reader.")

    # read in the data
    try:
        f = open(filename, "rb")
    except:
        raise FileNotFoundError("Could not open or read file: " + filename + "\n Please make sure the correct file (.epos) and location has been passed.")

    n = int(getSize(f)/4) #length of integers (bytes)
    rs = int(n / 11) #number of entries/rows
    byte = f.read()
    d = struct.unpack('>'+'fffffffffII'*rs, byte)
                    # '>' denotes 'big-endian' byte order

    #calculate instrument flightpath:
    voltage = np.array(d[5::11]) + np.array(d[6::11])
    print("estimated instrument flightpath: " + str(np.mean(((2.0 * voltage * 1.6e-19/(np.array(d[3::11])* 1.67e-27) * (1e-9 * np.array(d[4::11]))**2.0))**0.5)))
    est_det_rad = np.mean([abs(np.min(np.array(d[7::11]) * 1e-3)), abs(np.max(np.array(d[7::11]) * 1e-3)), abs(np.min(np.array(d[8::11]) * 1e-3)), abs(np.max(np.array(d[8::11])) * 1e-3)])
    print("estimated detector radius: " + str(est_det_rad))
    VP = np.array(d[5::11]) + np.array(d[6::11])
    return np.array(d[7::11]) * 1e-3, np.array(d[8::11]) * 1e-3, np.array(d[3::11]), VP, d[9::11], d[0::11], d[1::11], d[2::11]

#return filesize of fileobject
def getSize(fileobject):
    """

    Return the file size

    :param1 fileobject: The read file object
    :returns:
        size - The file object size (e.g. number of lines)

    """
    fileobject.seek(0,2) # move the cursor to the end of the file
    size = fileobject.tell()
    fileobject.seek(0) # move the cursor to the beginning of the file
    return size

def read_epos_file_header(filename, header_size, skip = 0):

    """

    A python reader for the .epos APT data file. Only reads an initial number of ions (`header_size`).
    Once unpacked the 1D array d contains
    the following elements:

    'x': d[0::11], #reconstructed x-coordinate of ion within sample
    'y': d[1::11], #reconstructed y-coordinate of ion within sample
    'z': d[2::11], #reconstructed z-coordinate of ion within sample
    'Da': d[3::11],
    'ns': d[4::11],
    'DC_kV': d[5::11],
    'pulse_kV': d[6::11],
    'det_x': d[7::11],
    'det_y': d[8::11]})
    'pslep': d[9::11], # pulses since last event pulse
    'ipp': d[10::11]}) # ions per pulse

    The method also calculates and prints the instrument flightpath, variance in the ion flight path, and the detector radius.

    :param1 filename: The .epos filename
    :param2 header_size: The number of ions to read in
    :returns:
        det_x
        det_y
        Da
        pulse voltage
        pslep
        x
        y
        z

    """

    if filename.split(".")[-1] != "epos":
        raise TypeError("Incorrect filetype detected (based off name). Method only supports .epos file types. Please call appropriate reader.")

    # read in the data
    try:
        f = open(filename, "rb")
    except:
        raise FileNotFoundError("Could not open or read file: " + filename + "\n Please make sure the correct file (.epos) and location has been passed.")


    n = int(getSize(f)/4) #length of integers (bytes)
    print(getSize(f)/4)
    rs = int(n / 11) #number of entries/rows
    print(n / 11)

    byte = f.read()[(4 * skip * 11):(4 * (skip + header_size) * 11)]
    d = struct.unpack('>'+'fffffffffII'*header_size, byte)
                    # '>' denotes 'big-endian' byte order

    #calculate instrument flightpath:
    voltage = np.array(d[5::11]) + np.array(d[6::11])
    print("estimated instrument flightpath: " + str(np.mean(((2.0 * voltage * 1.6e-19/(np.array(d[3::11])* 1.67e-27) * (1e-9 * np.array(d[4::11]))**2.0))**0.5)))
    est_det_rad = np.mean([abs(np.min(np.array(d[7::11]) * 1e-3)), abs(np.max(np.array(d[7::11]) * 1e-3)), abs(np.min(np.array(d[8::11]) * 1e-3)), abs(np.max(np.array(d[8::11])) * 1e-3)])
    print("estimated detector radius: " + str(est_det_rad))
    VP = np.array(d[5::11]) + np.array(d[6::11])
    return np.array(d[7::11]) * 1e-3, np.array(d[8::11]) * 1e-3, np.array(d[3::11]), VP, d[9::11], d[0::11], d[1::11], d[2::11]


def chunk_read_epos(filename, sample = 10):

    """

    A python reader for the .epos APT data file.
    Chunks reading of file to handle large file sizes
    Also samples ions (sample rate defined by `sample`)
    Once unpacked the 1D array d contains
    the following elements:

    'x': d[0::11], #reconstructed x-coordinate of ion within sample
    'y': d[1::11], #reconstructed y-coordinate of ion within sample
    'z': d[2::11], #reconstructed z-coordinate of ion within sample
    'Da': d[3::11],
    'ns': d[4::11],
    'DC_kV': d[5::11],
    'pulse_kV': d[6::11],
    'det_x': d[7::11],
    'det_y': d[8::11]})
    'pslep': d[9::11], # pulses since last event pulse
    'ipp': d[10::11]}) # ions per pulse

    The method also calculates and prints the instrument flightpath, variance in the ion flight path, and the detector radius.

    :param1 filename: The .epos filename
    :param2 sample: The sample rate (defaults to one in every 10)
    :returns:
        det_x
        det_y
        Da
        pulse voltage
        pslep
        x
        y
        z

    """

    if filename.split(".")[-1] != "epos":
        raise TypeError("Incorrect filetype detected (based off name). Method only supports .epos file types. Please call appropriate reader.")

    # read in the data
    try:
        f = open(filename, "rb")
    except:
        raise FileNotFoundError("Could not open or read file: " + filename + "\n Please make sure the correct file (.epos) and location has been passed.")

    n = int(getSize(f)/4) #length of integers (bytes)
    rs = int(n / 11) #number of entries/rows

    d = []

    #chunk data reading to 44 bytes i.e. row by row (while sampling rows)
    for a in np.arange(0, (n * 4)/(11 * 4), sample, dtype = int):
        f.seek(a * 11 * 4)
        byte = f.read(44)
        unpacked = struct.unpack('>'+'fffffffffII'*1, byte)
        d.extend(unpacked)
                    # '>' denotes 'big-endian' byte order

    #calculate instrument flightpath:
    voltage = np.array(d[5::11]) + np.array(d[6::11])
    print("estimated instrument flightpath: " + str(np.mean(((2.0 * voltage * 1.6e-19/(np.array(d[3::11])* 1.67e-27) * (1e-9 * np.array(d[4::11]))**2.0))**0.5)))
    est_det_rad = np.mean([abs(np.min(np.array(d[7::11]) * 1e-3)), abs(np.max(np.array(d[7::11]) * 1e-3)), abs(np.min(np.array(d[8::11]) * 1e-3)), abs(np.max(np.array(d[8::11])) * 1e-3)])
    print("estimated detector radius: " + str(est_det_rad))

    VP = np.array(d[5::11]) + np.array(d[6::11])
    return np.array(d[7::11]) * 1e-3, np.array(d[8::11]) * 1e-3, np.array(d[3::11]), VP, d[9::11], np.array(d[0::11])*1e-9, np.array(d[1::11])*1e-9, np.array(d[2::11])*1e-9

def read_ato(filename):

    """

    A python reader for the .ato APT data file. Once unpacked the 1D array d contains
    the following elements:

    'x': d[8::14], #reconstructed x-coordinate of ion within sample
    'y': d[9::14], #reconstructed y-coordinate of ion within sample
    'z': d[10::14], #reconstructed z-coordinate of ion within sample
    'Da': d[11::14]
    'pn': d[13::14]
    'DC_kV': d[14::14]
    'TOF': d[15::14], ###micro seconds
    'det_x': d[16::14]
    'det_y': d[17::14]
    'pulse_kV': d[19::14]

    The method also calculates and prints the instrument flightpath, variance in the ion flight path, and the detector radius.

    :param1 filename: The .ato filename
    :returns:
        det_x
        det_y
        Da
        pulse voltage
        pslep
        x
        y
        z

    """

    if filename.split(".")[-1] != "ato":
        raise TypeError("Incorrect filetype detected (based off name). Method only supports .ato file types. Please call appropriate reader.")

    # read in the data
    try:
        f = open(filename, "rb")
    except:
        raise FileNotFoundError("Could not open or read file: " + filename + "\n Please make sure the correct file (.ato) and location has been passed.")

    n = int(getSize(f)/4) #length of integers (bytes)
    rs = int(n / 14) #number of entries/rows

    byte = f.read()

    ##note constant bytes (head) at start of binary file
    d = struct.unpack('<'+'cccccccc' + 'ffffIIffffffff'*rs, byte)
    # '<' denotes 'little-endian' byte order

    # unpack data
    #x,y,z,mass,clusID,pIndex,Vdc,TOF,dx,dy,dz,Vp,shank,FouR,FouI
    voltage = 1e3 * (np.array(d[19::14]) + np.array(d[14::14]))
    print("estimated instrument flightpath: " + str(np.mean(((2 * voltage * 1.6e-19/(np.array(d[11::14])* 1.67e-27) * (1e-6 * np.array(d[15::14]))**2.0))**0.5)))
    est_det_rad = np.mean([abs(np.min(np.array(d[16::14]) * 1e-2)), abs(np.max(np.array(d[16::14]) * 1e-2)), abs(np.min(np.array(d[16::14]) * 1e-2)), abs(np.max(np.array(d[16::14])) * 1e-2)])
    print("estimated detector radius: " + str(est_det_rad))
    VP = np.array(d[19::14]) + np.array(d[14::14])
    return np.array(d[16::14]) * 1e-2, np.array(d[17::14]) * 1e-2, np.array(d[11::14]), VP, np.array(d[13::14]), np.array(d[8::14]), np.array(d[9::14]), np.array(d[10::14])


def chunk_read_ato(filename, sample = 10):

    """

    A python reader for the .ato APT data file. Once unpacked the 1D array d contains
    Chunks reading of file to handle large file sizes
    Also samples ions (sample rate defined by `sample`)
    the following elements:

    'x': d[8::14], #reconstructed x-coordinate of ion within sample
    'y': d[9::14], #reconstructed y-coordinate of ion within sample
    'z': d[10::14], #reconstructed z-coordinate of ion within sample
    'Da': d[11::14]
    'pn': d[13::14]
    'DC_kV': d[14::14]
    'TOF': d[15::14], ###micro seconds
    'det_x': d[16::14]
    'det_y': d[17::14]
    'pulse_kV': d[19::14]

    The method also calculates and prints the instrument flightpath, variance in the ion flight path, and the detector radius.

    :param1 filename: The .ato filename
    :param2 sample: The sample rate (defaults to one in every 10)
    :returns:
        det_x
        det_y
        Da
        pulse voltage
        pslep
        x
        y
        z

    """

    if filename.split(".")[-1] != "ato":
        raise TypeError("Incorrect filetype detected (based off name). Method only supports .ato file types. Please call appropriate reader.")

    # read in the data
    try:
        f = open(filename, "rb")
    except:
        raise FileNotFoundError("Could not open or read file: " + filename + "\n Please make sure the correct file (.ato) and location has been passed.")

    n = int(getSize(f)/4) #length of integers (bytes)
    rs = int(n / 14) #number of entries/rows
    #byte = f.read()

    d = []

    #chunk data reading to 56 bytes i.e. row by row (while sampling rows)
    for a in np.arange(8, (n * 4)/(14 * 4) + 8, sample, dtype = int):
        f.seek(a * 14 * 4)
        byte = f.read(14 * 4)
        if len(byte) == 56:
            d.extend(struct.unpack('<'+'ffffIIffffffff'*1, byte))
                    # '<' denotes 'little-endian' byte order

    # unpack data
    #x,y,z,mass,clusID,pIndex,Vdc,TOF,dx,dy,dz,Vp,shank,FouR,FouI

    voltage = 1e3 * (np.array(d[19::14]) + np.array(d[14::14]))
    print("estimated instrument flightpath: " + str(np.mean(((2 * voltage * 1.6e-19/(np.array(d[11::14])* 1.67e-27) * (1e-6 * np.array(d[15::14]))**2.0))**0.5)))
    est_det_rad = np.mean([abs(np.min(np.array(d[16:14]) * 1e-2)), abs(np.max(np.array(d[16::14]) * 1e-2)), abs(np.min(np.array(d[16::14]) * 1e-2)), abs(np.max(np.array(d[16::14])) * 1e-2)])
    print("estimated detector radius: " + str(est_det_rad))

    VP = np.array(d[19::14]) + np.array(d[14::14])
    return np.array(d[16::14]) * 1e-2, np.array(d[17::14]) * 1e-2, np.array(d[11::14]), VP, np.array(d[13::14]), np.array(d[8::14]), np.array(d[9::14]), np.array(d[10::14])



def read_rrng(f):

    """

    Method for reading range files

    :param1 f: The .range filename
    :returns:
        ions - Pandas dataframe of containing the ion number and its corresponding name (ionic compound)
        rrngs - Pandas dataframe with following structure:
                'ionic number','lower Da range','upper Da range','vol fraction','composition','colour'

    """

    try:
        rf = open(f,'r').readlines()
    except:
        raise FileNotFoundError("Could not open or read file: " + f + "\n Please make sure the correct file (.range) and location has been passed.")

    print("called range reader")
    rions = []
    rrrngs = []
    ion_header = False
    rrng_header = False

    for line in rf:
        line = line.strip("\n")
        line = line.strip(" ")
        idx = line.find("[Ions]")
        if (idx != -1):
            ion_header = True
            continue
        if (ion_header == True):
            idx = line.find("Ion")
            if (idx != -1):
                rions.append(line.split("="))
                continue
            idx = line.find("[Ranges]")
            if (idx != -1):
                ion_header = False
                rrng_header = True
                continue
        if (rrng_header == True):
            idx = line.find("Range")
            if (idx != -1):
                rline = line.split("=")[1]
                rrline = rline.split(" ")
                if (len(rrline) >= 5):
                    nline = [rline]
                    nline.extend(rrline[:5])
                    rrrngs.append(nline)

    ions = {}
    for a in range(len(rions)):
        ions[rions[a][0]] = rions[a][1]

    rrngs = {}
    for a in range(len(rrrngs)):
        rrngs[rrrngs[a][0]] = {}
        rrngs[rrrngs[a][0]]["lower"] = float(rrrngs[a][1])
        rrngs[rrrngs[a][0]]["upper"] = float(rrrngs[a][2])
        rrngs[rrrngs[a][0]]["vol"] = float(rrrngs[a][3].split(":")[1])
        rrngs[rrrngs[a][0]]["comp"] = rrrngs[a][4]
        rrngs[rrrngs[a][0]]["colour"] = rrrngs[a][5]

    if len(ions) == 0:
        raise ValueError("Warning: no ions have been read! Please ensure correct file has been past.")

    return ions,rrngs

def read_reconstruction_parameters(param_filename, dx):

    """

    Method for reading in parameters for the point projection reconstruction algorithm.

    :param1 param_filename: The .tsv parameters file
    :param2 dx: 1D array of length matching the number of ions

    :returns:
        parameter_dict - dictionary containing reconstruction parameters

    """

    #define default parameter values
    dICF = 1.3
    dscale = 1e-9
    dR0 = 35e-9
    dalpha = 10.0
    dvol_frac = 0.2 * (1e-9)**3.0
    dIx = 0
    dIy = 0
    dflipxy = 0
    dlower = 0
    dupper = 0
    doffx = 0.0
    doffy = 0.0
    doffz = 0.0
    ddetector_efficiency = 1.0
    dflight_path = 1.0
    ddet_rad = 0.04

    #read in the reconstruction parameter file
    try:
        gp = read_parameters_from_file(param_filename)
    except:
        raise FileNotFoundError("reconstruction parameters in file: " + param_filename + " could not be read. Please check this is the correct file and location.")

    try:
        ICF = float(gp["eps"])
    except:
        print("No Image Compression Factor parameter (ICF) found. Using default value of " + str(dICF))
        ICF = dICF

    try:
        scale = float(gp["scale"])
    except:
        print("No scale factor parameter (scale) found. Using default value of " + str(dscale))
        scale = dscale

    try:
        R0 = float(gp["R0"]) * 1e-9
    except:
        print("No initial radius parameter (R0) found. Using default value of " + str(dR0))
        R0 = dR0

    try:
        alpha = float(gp["alpha"]) * np.pi/180.0
    except:
        print("No shank angle parameter (alpha) found. Using default value of " + str(dalpha))
        alpha =dalpha * np.pi/180.0

    try:
        vol_frac = float(gp["ionic_volume"]) * (1e-9)**3.0
    except:
        print("No ionic volume parameter (ionic_volume) found. Using default value of " + str(dvol_frac))
        vol_frac = dvol_frac

    try:
        Ix = float(gp["Ix"])
    except:
        print("No invert X dimension parameter (Ix) found. Using default value of " + str(dIx) + " (i.e. no inversion).")
        Ix = dIx

    try:
        Iy = float(gp["Iy"])
    except:
        print("No invert Y dimension parameter (Iy) found. Using default value of " + str(dIy) + " (i.e. no inversion).")
        Iy = dIy

    try:
        flipxy = int(gp["flipxy"])
    except:
        print("No flip XY dimensions parameter (flipxy) found. Using default value of " + str(dflipxy) + " (i.e. no flip.).")
        flipxy = dflipxy

    try:
        lower = int(gp["lower"])
    except:
        print("No lower ion index parameter (lower) found. Corresponds to number of ions at start not included in reconstruction. Using default value of " + str(dlower) + " (no lower limit for ions excluded in reconstruction).")
        lower = dlower
    try:
        upper = int(gp["upper"])
        upper = len(dx) - upper
    except:
        print("No upper ion index parameter (upper) found. Corresponds to number of ions at end not included in reconstruction. Using default value of " + str(dupper) + " (no upper limit for ions excluded in reconstruction).")
        upper = len(dx) - dupper

    try:
        offx = float(gp["offx"])
    except:
        print("No offset in dimension x parameter (offx) found. Using default value of " + str(doffx))
        offx = doffx

    try:
        offy = float(gp["offy"])
    except:
        print("No offset in dimension y parameter (offy) found. Using default value of " + str(doffy))
        offy = doffy

    try:
        offz = float(gp["offz"])
    except:
        print("No offset in dimension z parameter (offz) found. Using default value of " + str(doffz))
        offz = doffz

    try:
        detector_efficiency = float(gp["detector_efficiency"])
    except:
        print("No detector efficiency parameter (detector_efficiency) found. Using default value of " + str(ddetector_efficiency))
        detector_efficiency = ddetector_efficiency

    try:
        flight_path = float(gp["L"])
    except:
        print("No flight path instrument parameter (L) found. Using the default value of " + str(dflight_path) + "m")
        flight_path = dflight_path

    try:
        det_rad = float(gp["det_rad"])
    except:
        print("No detector radius parameter (det_rad) found. Using default value of " + str(ddet_rad) + " m")
        det_rad = ddet_rad

    #define parameters dictionary
    parameters = {}
    parameters["ICF"] = ICF
    parameters["R0"] = R0
    parameters["alpha"] = alpha
    parameters["scale"] = scale
    parameters["ionic_volume"] = vol_frac
    parameters["Ix"] = Ix
    parameters["Iy"] = Iy
    parameters["flipxy"] = flipxy
    parameters["lower"] = lower
    parameters["upper"] = upper
    parameters["offx"] = offx
    parameters["offy"] = offy
    parameters["offz"] = offz
    parameters["detector_efficiency"] = detector_efficiency
    parameters["flight_path"] = flight_path
    parameters["det_rad"] = det_rad

    return parameters
