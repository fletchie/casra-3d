"""

Module for performing Sussman reinitialization of the level set field

Author: Charles Fletcher, Daniel Haley
Atom Probe Tomography Group, Department of Materials, University of Oxford
August 2020

"""

import copy

try:
    import numpy as np
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python package numpy. " +
                              "Please make sure it has been installed.")

def sussman_reinit(field, forward_x, forward_y, forward_z,
                   backward_x, backward_y, backward_z, re_its=20):

    """

    Function for performing Sussman reinitialization of the level set
    field (restoring to signed distance form)

    Method described in: M. Sussman, S. Smereka, and S. Osher. J. Comput. Phys., vol. 114, pp. 146–159, 1994.
    Integrator described in: E. Rouy and A. Tourin. SIAM J. Numer. Anal., vol. 29, no. 3, pp. 867–884, 1992.

    :param1 field (np.array, (L1,L2,L3), dtype=float): The 3D level set field array
    :param2 forward_x (np.array, (L1,L2,L3), dtype=float): 3D array for forward
                                                           difference x operator
    :param3 forward_y (np.array, (L1,L2,L3), dtype=float): 3D array for forward
                                                           difference y operator
    :param4 forward_z (np.array, (L1,L2,L3), dtype=float): 3D array for forward
                                                           difference z operator
    :param5 backward_x (np.array, (L1,L2,L3), dtype=float): 3D array for backward
                                                            difference x operator
    :param6 backward_y (np.array, (L1,L2,L3), dtype=float): 3D array for backward
                                                            difference y operator
    :param7 backward_z (np.array, (L1,L2,L3), dtype=float): 3D array for backward
                                                            difference z operator
    :param8 re_its (int): Number of reinitialization iterations to perform
                          (currently fixed but plan adaptive implementation
                          in the future)

    :returns:
        :field (np.array, (L1,L2,L3), dtype=float): The reinitialized level set field

    :raises:
        None

    """


    dt = 0.1

    #add small numerical perturbation to ensure convergence
    pzeta = 0.5
    sign_field = field/(field**2.0 + pzeta)**0.5

    field_0 = copy.deepcopy(field)

    Gsq = np.zeros(field_0.shape)
    pv = 0.0

    for a in range(0, re_its):
        forward_x[:-1, :, :] = field[1:, :, :] - field[:-1, :, :]
        forward_y[:, :-1, :] = field[:, 1:, :] - field[:, :-1, :]
        forward_z[:, :, :-1] = field[:, :, 1:] - field[:, :, :-1]

        backward_x[1:, :, :] = field[1:, :, :] - field[:-1, :, :]
        backward_y[:, 1:, :] = field[:, 1:, :] - field[:, :-1, :]
        backward_z[:, :, 1:] = field[:, :, 1:] - field[:, :, :-1]

        #upwind method gradients

        dxfp = np.maximum(forward_x, 0.0)
        dxfm = np.minimum(forward_x, 0.0)
        dxbp = np.maximum(backward_x, 0.0)
        dxbm = np.minimum(backward_x, 0.0)

        dyfp = np.maximum(forward_y, 0.0)
        dyfm = np.minimum(forward_y, 0.0)
        dybp = np.maximum(backward_y, 0.0)
        dybm = np.minimum(backward_y, 0.0)

        dzfp = np.maximum(forward_z, 0.0)
        dzfm = np.minimum(forward_z, 0.0)
        dzbp = np.maximum(backward_z, 0.0)
        dzbm = np.minimum(backward_z, 0.0)

        G = 0.0
        G += ((np.maximum(dxbp**2.0, dxfm**2.0) + np.maximum(dybp**2.0, dyfm**2.0) +
               np.maximum(dzbp**2.0, dzfm**2.0))**0.5 - 1.0) * (field_0 > 0.0)
        G += ((np.maximum(dxbm**2.0, dxfp**2.0) + np.maximum(dybm**2.0, dyfp**2.0) +
               np.maximum(dzbm**2.0, dzfp**2.0))**0.5 - 1.0) * (field_0 < 0.0)

        dphi = sign_field * G

        field -= dphi * dt

        #check convergence
        if np.sum(abs(dphi))/(field.shape[0] * field.shape[1] * field.shape[2]) < dt * 0.4:
            break

    return field
