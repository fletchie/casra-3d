
"""

Submodule containing functions for for handling model parameter inputs

Author: Charles Fletcher
Atom Probe Tomography Group, Department of Materials, University of Oxford
May 2020

"""

import sqlite3
import os
import csv
import copy
import math

from typing import List

try:
    import numpy as np
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python package numpy. " +
                              "Please make sure it is installed.")

#FIXME: Transform requires (scipy >=1.4)
#from scipy.spatial.transform import Rotation as rot
class Rotation:


    def compute_matrix(self) :
        assert(np.linalg.norm(self.axis) > 0)

        assert(self.angle <=2.1*3.141596254)

        ct = math.cos(self.angle)
        st = math.sin(self.angle)
        o_ct = 1-math.cos(self.angle)

        self.matrix[0,0] = ct + self.axis[0]*self.axis[0]*o_ct
        self.matrix[0,1] = self.axis[0]*self.axis[1]*o_ct -self.axis[2]*st
        self.matrix[0,2] = self.axis[0]*self.axis[2]*o_ct + self.axis[1]*st

        self.matrix[1,0] = self.axis[1]*self.axis[0]*o_ct + self.axis[2]*st
        self.matrix[1,1] = ct + self.axis[1]*self.axis[1]*o_ct
        self.matrix[1,2] = self.axis[1]*self.axis[2]*o_ct - self.axis[0]*st


        self.matrix[2,0] = self.axis[2]*self.axis[0]*o_ct - self.axis[1]*st
        self.matrix[2,1] = self.axis[2]*self.axis[1]*o_ct + self.axis[0]*st
        self.matrix[2,2] = ct + self.axis[2]*self.axis[2]*o_ct

    def from_rotvec(self,new_axis : List[float], new_angle : float):
        assert(len(new_axis) ==3)
        self.axis = new_axis
        self.angle=new_angle
        self.compute_matrix()

    def invert(self):
        self.angle=-self.angle
        self.matrix.transpose()

    def apply(self,items):
        for i in items:
            i = self.matrix*i


    def __init__(self):
        self.axis = [0.0,0.0,0.0]
        self.angle = 0.0
        self.matrix=np.array(3,3)



def read_parameters_from_file(global_parameters_filename):

    """

    Function for reading in model parameters from text file (tab separated).

    :param1 global_parameters_filename (str): String giving parameters file location

    :returns:
        :read_dict: Dictionary with keys (parameter label) and values (parameter value)

    :raises:
        ValueError: Parameter value listed twice in parameters file. Cannot identify the
                    the correct parameter value to use.

    """

    read_dict = {}
    with open(global_parameters_filename) as f:
        reader = csv.reader(f, delimiter='\t')
        for row in reader:
            if len(row) > 1:
                if row[0][0] != "#":
                    if row[0] in read_dict:
                        raise ValueError("definition for parameter '" + row[0] +
                                         "' detected twice in the parameters file. " +
                                         "Not sure which value to use. Make sure each " +
                                         "parameter is only defined once!")

                    read_dict[row[0]] = row[1]
    return read_dict


def rotate_tip_axis_to_world(model_geom, tip_axis : List[ float ] ):
    assert(len(tip_axis) == 3)

    #Normalise tip axis
    tip_axis_norm = [ i*i for i in tip_axis ];
    tip_axis_norm=math.sqrt(tip_axis_norm)

    #Construct rotation matrix using [0,0,1]xtip_axis_norm as the
    # rotation direction axis
    rotation_axis = np.cross(tip_axis, [0,0,1])
    rotation_angle = np.angle(tip_axis,[0,0,1])

    #TODO: Once scipy.spatial.transform is widely available (probably 2022)
    # remove in favour of scipy rotation
    rot=Rotation()
    rot.from_rotvec(rotation_axis,rotation_angle)

    #compute centre point of rotation
    for o in range(len(model_geom["objts"])):
        if model_geom["objts"][0]["surface"] == 1:
            verts = model_geom["objts"][o]["verts"]
            cp = np.mean(verts, axis=0)

    #perform rotation of specimen phases
    for o in range(len(model_geom["objts"])):
        verts = model_geom["objts"][o]["verts"] - cp
        rot.apply(verts)
        verts=verts +cp



def rotate_specimen_geometry(model_geom, alpha, beta, gamma):

    """

    Function for performing Euler angle rotation of initial model geometry

    :param1 model_geom (dict): The model geometry simulation dictionary
    :param2 alpha (float): Euler angle 1 (yaw)
    :param3 beta (float): Euler angle 2 (pitch)
    :param4 gamma (float): Euler angle 3 (roll)

    :returns:
        None

    :raises:
        None

    """

    #define Euler rotation matrices
    R_roll = np.array([[1, 0, 0],
                       [0.0, np.cos(gamma), -np.sin(gamma)],
                       [0.0, np.sin(gamma), np.cos(gamma)]])
    R_pitch = np.array([[np.cos(beta), 0.0, np.sin(beta)],
                        [0.0, 1.0, 0.0],
                        [-np.sin(beta), 0.0, np.cos(beta)]])
    R_yaw = np.array([[np.cos(alpha), -np.sin(alpha), 0.0],
                      [np.sin(alpha), np.cos(alpha), 0.0],
                      [0.0, 0.0, 1.0]])

    #identify centre point of rotation
    for o in range(len(model_geom["objts"])):
        if model_geom["objts"][0]["surface"] == 1:
            verts = model_geom["objts"][o]["verts"]
            cp = np.mean(verts, axis=0)

    #perform rotation of specimen phases
    for o in range(len(model_geom["objts"])):
        verts = model_geom["objts"][o]["verts"] - cp
        #perform Euler angle rotation
        verts = np.matmul(R_roll, verts.T).T
        verts = np.matmul(R_pitch, verts.T).T
        verts = np.matmul(R_yaw, verts.T).T
        tverts = verts + cp

        tverts = np.ascontiguousarray(tverts)

        from casra_3d.io import model_writers as model_writers
        model_writers.write_vtk("mesh" + str(o) + ".vtk", tverts, model_geom["objts"][o]["faces"])

        min_x, min_y, min_z = np.min(tverts, axis=0)
        max_x, max_y, max_z = np.max(tverts, axis=0)

        model_geom["objts"][o]["min_x"] = min_x
        model_geom["objts"][o]["max_x"] = max_x
        model_geom["objts"][o]["min_y"] = min_y
        model_geom["objts"][o]["max_y"] = max_y
        model_geom["objts"][o]["min_z"] = min_z
        model_geom["objts"][o]["max_z"] = max_z
        model_geom["objts"][o]["verts"] = tverts

def read_mref_from_file(mref_filename):

    """

    Function for reading in the material references from text file (tab separated).

    :param1 mref_filename (str): String giving materials reference (mref) file location
    :returns:
        :read_dict (dict): Dictionary with keys (phase label) and
                           values (dict of phase properties and values)

    :raises:
        ValueError: phase listed twice in .mref file. Cannot identify which value to use.

    """

    read_dict = {}
    col_names = []
    with open(mref_filename) as f:
        reader = csv.reader(f, delimiter='\t')
        h = 0
        for row in reader:
            if len(row) > 1:
                if row[0][0] != "#":
                    if h == 0:
                        col_names = row
                        h += 1
                        continue
                    if row[0] in read_dict:
                        raise ValueError("definition for phase '" + row[0] +
                                         "' detected twice in the mref file. " +
                                         "Not sure which value to use. Make sure " +
                                         "each phase is only defined once!")

                    read_dict[row[0]] = {}
                    for a in range(1, len(row)):
                        read_dict[row[0]][col_names[a]] = row[a]
    return read_dict

def load_parameters(global_parameters_filename):

    """

    Function for loading model parameters from file `global_parameters_filename`.
    If values cannot be found then default values for parameters are used.

    :param1 global_parameters_filename (str): String giving parameters file location

    :returns:
        :global_parameters (dict): dictionary of parameters and values

    raises:
        KeyError: Particular parameter value not found, sets a default value and continues
        FileNotFoundError: Cannot open or read parsed in parameters file

    """

    global_parameters = dict()

    #set default parameter values
    dbeta = 20.0
    damplitude = 5e-4
    dflightpath = 0.12
    dscale = 1e-8
    dCFL = 0.2
    dBEM_tol = 100.0
    dmapping_frac = 10
    dinital_blurring = 0
    dgrid_width = 2e-9
    dadaptive = True
    dvoltage_curve = True
    dbase_disable = -1
    datomic_volume = 1.0
    dtotal_ions = 1000
    dmin_shank_length = -1.0
    dinitial_shank_extension = 0
    dshank_angle = 0.0
    dreinit = 0
    drot_alpha = 0.0 * np.pi/180.0
    drot_beta = 0.0 * np.pi/180.0
    drot_gamma = 0.0 * np.pi/180.0

    try:
        read_dict = read_parameters_from_file(global_parameters_filename)
        try:
            global_parameters['beta'] = float(read_dict['beta'])
        except KeyError:
            print("No beta input value detected (beta). " +
                  "Using default value of " + str(dbeta))
            global_parameters['beta'] = dbeta

        try:
            global_parameters['amplitude'] = float(read_dict['amplitude'])
        except KeyError:
            print("No amplitude input value detected (amplitude). " +
                  "Using default value of " + str(damplitude))
            global_parameters['amplitude'] = damplitude

        try:
            global_parameters['flightpath'] = float(read_dict['flightpath'])
        except KeyError:
            print("No flightpath input value detected (flightpath). " +
                  "Setting detector plane to default value " + str(dflightpath))
            global_parameters['flightpath'] = dflightpath

        try:
            global_parameters['scale'] = float(read_dict['scale'])
        except KeyError:
            print("No scale parameter detected (scale). Using default value of " +
                  str(dscale * 1e9) + " nm")
            global_parameters['scale'] = dscale

        try:
            global_parameters['CFL'] = float(read_dict['CFL'])
        except KeyError:
            print("No CFL parameter detected (CFL). " +
                  "Using default value of " + str(dCFL))
            global_parameters['CFL'] = dCFL

        try:
            global_parameters['BEM_tol'] = float(read_dict['BEM_tol'])
        except KeyError:
            print("No BEM_tol parameter detected (BEM_tol). " +
                  "Using default value of " + str(dBEM_tol))
            global_parameters['BEM_tol'] = dBEM_tol

        try:
            global_parameters['mapping_frac'] = int(read_dict['mapping_frac'])
        except KeyError:
            print("No mapping fraction detected (mapping_frac) - model iterations " +
                  "between ion projections. Setting to default value of " + str(dmapping_frac))
            global_parameters['mapping_frac'] = dmapping_frac

        try:
            global_parameters['initial_blurring'] = int(read_dict['initial_blurring'])
        except KeyError:
            print("No initial field blurring parameter detected (initial_blurring). " +
                  "Setting to default value of " + str(dinital_blurring))
            global_parameters['initial_blurring'] = dinital_blurring
        try:
            global_parameters['grid_width'] = float(read_dict['grid_width'])
        except KeyError:
            print("No grid width parameter detected (grid_width). " +
                  "Using default value of " + str(dgrid_width * 1e9) + " nm")
            global_parameters['grid_width'] = dgrid_width
        try:
            global_parameters['adaptive'] = check_true_false(read_dict["adaptive"])
        except KeyError:
            print("No adaptive grid parameter detected (adaptive). " +
                  "Using default value of " + str(dadaptive))
            global_parameters['adaptive'] = dadaptive
        try:
            global_parameters['voltage_curve'] = check_true_false(read_dict["voltage_ramping"])
        except KeyError:
            print("No voltage ramping parameter detected (voltage_ramping). " +
                  "Using default value of " + str(dvoltage_curve))
            global_parameters['voltage_curve'] = dvoltage_curve
        try:
            global_parameters['base_disable'] = int(read_dict["base_disable"])
        except KeyError:
            print("No base disable parameter detected (base_disable). " +
                  "Using default value of " + str(dbase_disable)  +
                  "(evolve apex for evaporation rate above threshold).")
            global_parameters['base_disable'] = dbase_disable
        try:
            global_parameters['atomic_volume'] = float(read_dict["atomic_volume"])
        except KeyError:
            print("No atomic volume parameter detected (atomic_volume). " +
                  "Using default value of " + str(datomic_volume) + " nm^3")
            global_parameters['atomic_volume'] = datomic_volume
        try:
            global_parameters['total_ions'] = int(read_dict["total_ions"])
        except KeyError:
            print("No number for total ions detected (total_ions). " +
                  "Using default value of " + str(dtotal_ions))
            global_parameters['total_ions'] = dtotal_ions
        try:
            global_parameters['min_shank_length'] = float(read_dict["min_shank_length"])
        except KeyError:
            print("No number minimum shank length value detected (min_shank_length). " +
                  "Using default value of " + str(dmin_shank_length) + " (off).")
            global_parameters['min_shank_length'] = dmin_shank_length
        try:
            global_parameters['initial_shank_extension'] = int(read_dict["initial_shank_extension"])
            if global_parameters['initial_shank_extension'] < 0:
                global_parameters['initial_shank_extension'] = 0
        except KeyError:
            print("No initial shank extension value detected (initial_shank_extension). " +
                  "Using default value of " + str(dinitial_shank_extension) + " (e.g. None/off).")
            global_parameters['initial_shank_extension'] = dinitial_shank_extension
        try:
            global_parameters['shank_angle'] = float(read_dict["shank_angle"]) * np.pi/180.0
        except KeyError:
            print("No shank angle value detected (shank_angle), " +
                  "which is required for shank extension. Using " +
                  "default value of " + str(dshank_angle) + ". A value for this parameter can " +
                  "be estimated via the 'tools.fit_point_projection_model' submodule.")
            global_parameters['shank_angle'] = dshank_angle
        try:
            global_parameters['reinit'] = int(read_dict["reinit"])
        except KeyError:
            print("No reinitialisation detected (reinit), " +
                  "which is required for field reinitialisation. " +
                  "Using default value of " + str(dreinit) + " (off).")
            global_parameters['reinit'] = dreinit
        try:
            global_parameters['rot_alpha'] = float(read_dict["rot_alpha"]) * np.pi/180.0
        except KeyError:
            print("No default specimen Euler rotation (yaw) set (rot_alpha), " +
                  "Using default value of " + str(drot_alpha) + ".")
            global_parameters['rot_alpha'] = drot_alpha
        try:
            global_parameters['rot_beta'] = float(read_dict["rot_beta"]) * np.pi/180.0
        except KeyError:
            print("No default specimen Euler rotation (roll) set (rot_beta), " +
                  "Using default value of " + str(drot_beta) + ".")
            global_parameters['rot_beta'] = drot_beta
        try:
            global_parameters['rot_gamma'] = float(read_dict["rot_gamma"]) * np.pi/180.0
        except KeyError:
            print("No default specimen Euler rotation (pitch) set (rot_gamma), " +
                  "Using default value of " + str(drot_gamma) + ".")
            global_parameters['rot_gamma'] = drot_gamma
    except FileNotFoundError:
        raise FileNotFoundError("no parameter file found.")

    return global_parameters

def check_true_false(input_param):

    """

    Check whether parameter is true or false from input

    :param1 input_param (str): The input parameter value

    :return:
        :tf (bool): Whether input parameter is true or false
    """

    if (input_param.lower() == "true" or input_param.lower() == "t" or
        input_param.lower() == "yes" or input_param.lower() == "y" or
        input_param.lower() == "1"):

        return True
    else:
        return False

def load_mref(mref_filename, geom):

    """

    Function for reading material reference from file `global_parameters_filename`.

    :param1 mref_filename (str): String giving mref file location
    :param2 geom (dict): The model geometry setup dictionary

    :returns:
        :returncode: 0

    :raises:
        FileNotFoundError: The parsed .mref filename could not be opened/read

    """

    try:
        mref = read_mref_from_file(mref_filename)

    except FileNotFoundError:
        raise FileNotFoundError("The file: " + str(mref_filename) + " was not found.")

    #check the number of phases matches the loaded geometry
    for o in range(0, len(geom["objts"])):
        if geom["objts"][o]["name"] not in geom["objts"][o]["name"]:
            print("no reference corresponding to defined phase: " +
                  str(geom["objts"][o]["name"]) + " can be found. \n Please " +
                  "ensure a phase with this name is contained in the passed .mref file.")
        else:
            matched_row = mref[geom["objts"][o]["name"]]
            geom["objts"][o]["mat_id"] = int(matched_row["mat_id"])
            geom["objts"][o]["alpha"] = float(matched_row["alpha"]) * np.pi/180.0
            geom["objts"][o]["beta"] = float(matched_row["beta"]) * np.pi/180.0
            geom["objts"][o]["gamma"] = float(matched_row["gamma"]) * np.pi/180.0
            geom["objts"][o]["color"] = matched_row["color"]

    return 0

def load_database_parameters(materials_db, geom, free_params):

    """

    Method for loading the phase parameters from the materials DB.
    In case of model calibration these parameters simply
    provide an initial guess.

    :param1 materials_db (str): filename of the the materials database
    :param2 geom (dict): The model geometry setup dictionary
    :param3 free_params (dict): dictionary containing free parameters
                                (overides parameters stored in database)
    :returns:
        :returncode: 0

    :raises:
        FileNotFoundError: The parsed materials database could not be opened/read
        IOError: Cannot connect to SQL database. Parsed database may be corrupted.
        OperationalError: Could not query parsed SQLite database. Likely not an SQL database.

    """

    if os.path.exists(materials_db) is False:
        raise FileNotFoundError("No database can be found at directory: " +
                                str(materials_db) + ". /n Please ensure the " +
                                "passed directory is correct.")

    try:
        conn = sqlite3.connect(materials_db)
        c = conn.cursor()
    except:
        raise IOError("Cannot connect to sqlite materials database. " +
                      "Is file type correct or database corrupted?")
    for o in range(0, len(geom["objts"])):
        try:
            a = c.execute('SELECT evaporation_field, amorphous FROM main where ' +
                          'mat_id = ' + str(geom["objts"][o]["mat_id"])).fetchall()

            #check if database parameters are model free parameters, if so then overide
            if geom["objts"][o]["name"] in free_params:
                geom["objts"][o]["field"] = free_params[geom["objts"][o]["name"]]
                print(geom["objts"][o]["field"])

            else:
                geom["objts"][o]["field"] = a[0][0]
            geom_bool = a[0][1].lower() in ['true']

            #load phase crystallography
            geom["objts"][o]["amorphous"] = geom_bool
            if geom_bool is False:
                a = c.execute('SELECT mx,my,mz,mag FROM crystallographic_structure "' +
                              '"where mat_id = ' + str(geom["objts"][o]["mat_id"])).fetchall()
                if not a:
                    print("No crystallographic information found for material with mat_id: " +
                          str(geom["objts"][o]["mat_id"]) + ". Assuming amorphous.")
                    geom["objts"][o]["amorphous"] = True
                else:
                    crystallographic_parameters = np.array(a)
                    geom["objts"][o]["amorphous"] = False

                    ###apply grain rotation
                    #construct rotation matrixes - yaw, pitch and roll
                    gamma = geom["objts"][o]["gamma"]
                    alpha = geom["objts"][o]["alpha"]
                    beta = geom["objts"][o]["beta"]

                    print(gamma, alpha, beta)

                    R_roll = np.array([[1, 0, 0], [0.0, np.cos(gamma), -np.sin(gamma)],
                                       [0.0, np.sin(gamma), np.cos(gamma)]])
                    R_pitch = np.array([[np.cos(beta), 0.0, np.sin(beta)], [0.0, 1.0, 0.0],
                                        [-np.sin(beta), 0.0, np.cos(beta)]])
                    R_yaw = np.array([[np.cos(alpha), -np.sin(alpha), 0.0],
                                      [np.sin(alpha), np.cos(alpha), 0.0],
                                      [0.0, 0.0, 1.0]])

                    m = np.array([crystallographic_parameters[:, 0],
                                  crystallographic_parameters[:, 1],
                                  crystallographic_parameters[:, 2]]).T

                    #expand Miller indices
                    cmy = copy.deepcopy(m)
                    cmy[:, 0] = -cmy[:, 0]
                    m = np.vstack((m, cmy))
                    mag = np.hstack((crystallographic_parameters[:, 3],
                                     crystallographic_parameters[:, 3]))
                    cmy = copy.deepcopy(m)
                    cmy[:, 1] = -cmy[:, 1]
                    m = np.vstack((m, cmy))
                    mag = np.hstack((mag, mag))
                    cmy = copy.deepcopy(m)
                    cmy[:, 2] = -cmy[:, 2]
                    m = np.vstack((m, cmy))
                    mag = np.hstack((mag, mag))

                    #perform Euler angle rotation
                    mr = np.matmul(R_roll, m.T).T
                    mp = np.matmul(R_pitch, mr.T).T
                    my = np.matmul(R_yaw, mp.T).T

                    geom["objts"][o]["mx"] = my[:, 0]
                    geom["objts"][o]["my"] = my[:, 1]
                    geom["objts"][o]["mz"] = my[:, 2]
                    geom["objts"][o]["mag"] = mag

        except sqlite3.OperationalError:
            raise sqlite3.OperationalError("The query failed to find the 'main' table, " +
                                           "likely because the passed directory: " +
                                           str(materials_db) +
                                           " is not a sqlite database " +
                                           "(i.e. the connection failed).")

    #close the database connection
    conn.close()

    return 0

def check_geometry(verts, faces):

    """

    Checks for consistency of the specimen surface geometry

    :param1 verts (np.array, (M, 3), dtype=double): The input mesh vertices
    :param2 faces (np.array, (N, 3), dtype=int): The input mesh connectivity matrix

    :returns:
        None

    :raises:
        None

    """

    xmax = np.max(verts[:, 0])
    xmin = np.min(verts[:, 0])
    ymax = np.max(verts[:, 1])
    ymin = np.min(verts[:, 1])
    zmax = np.max(verts[:, 2])
    zmin = np.min(verts[:, 2])

    cp = np.mean(verts[faces], axis=1)

    #check one - check that input geometry is not sideways, warn user otherwise
    if (xmax - xmin) > 1.2 * (zmax - zmin) or (ymax - ymin) > 1.2 * (zmax - zmin):
        print("WARNING: input specimen sideways dimensions greater than height. \n" +
              "This is unusual for typical specimen geometries. Is your specimen" +
              "oriented correctly?")

    #check two - check that input geometry is correct way round, warn user otherwise
    cp_top = cp[cp[:, 2] > (zmin + (zmax - zmin) * 0.75)]
    cp_bottom = cp[cp[:, 2] < (zmin + (zmax - zmin) * 0.25)]

    bbtop_xmin = np.min(cp_top[:, 0])
    bbtop_xmax = np.max(cp_top[:, 0])
    bbtop_ymin = np.min(cp_top[:, 1])
    bbtop_ymax = np.max(cp_top[:, 1])
    bbtop_area = (bbtop_ymax - bbtop_ymin) * (bbtop_xmax - bbtop_xmin)

    bbbottom_xmin = np.min(cp_bottom[:, 0])
    bbbottom_xmax = np.max(cp_bottom[:, 0])
    bbbottom_ymin = np.min(cp_bottom[:, 1])
    bbbottom_ymax = np.max(cp_bottom[:, 1])
    bbbottom_area = (bbbottom_ymax - bbbottom_ymin) * (bbbottom_xmax - bbbottom_xmin)

    if bbtop_area * 1.2 > bbbottom_area:
        print("WARNING: The inputted specimen apex bounding box appears significantly larger " +
              "than the base bounding box. \nThis is unusual for typical specimen " +
              "geometries. Is your specimen oriented correctly (e.g. not upside down)?")

def check_mesh_is_watertight(verts, faces, phase_name, surface=0, test=0):

    """

    Checks whether the particular input geometry is
    closed/watertight (does not contain single edges/holes).
    Checks apply to both the specimen surface and phases.
    *Note that additional tests are required to confirm that the inputted geometry
     is a manifold.*

    :param1 verts (np.array, (M, 3), dtype=double): The input mesh vertices
    :param2 faces (np.array, (N, 3), dtype=int): The input mesh connectivity matrix
    :param3 phase_name (str): The inputted mesh phase name
    :param4 surface (int): Whether the inputted mesh is the specimen surface (1)
                           or not (0)
    :param5 test (int): Whether check is test (1) or not (0)

    :returns:
        :(int): 0 if successful, 1 if zero length mesh, 2 if not watertight.

    :raises:
        :ValueError: inputted mesh empty (zero length)
        :ValueError: inputted mesh contains disconnected edges e.g. not closed

    """

    edge_dict = {}

    for f in range(len(faces)):
        e0 = np.sort([faces[f, 1], faces[f, 0]]).astype("str")
        e1 = np.sort([faces[f, 2], faces[f, 1]]).astype("str")
        e2 = np.sort([faces[f, 0], faces[f, 2]]).astype("str")

        se0 = "-".join(e0)
        se1 = "-".join(e1)
        se2 = "-".join(e2)

        if se0 in edge_dict.keys():
            edge_dict[se0] = edge_dict[se0] + 1
        else:
            edge_dict[se0] = 1

        if se1 in edge_dict.keys():
            edge_dict[se1] = edge_dict[se1] + 1
        else:
            edge_dict[se1] = 1

        if se2 in edge_dict.keys():
            edge_dict[se2] = edge_dict[se2] + 1
        else:
            edge_dict[se2] = 1

    for keys, values in edge_dict.items():
        if values == 1 and surface == 1 and test == 0:
            raise ValueError("input specimen surface mesh found to not " +
                             "be watertight. \nThis is an invalid mesh. " +
                             "Please check input.")
        elif values == 1 and test == 0:
            raise ValueError("phase " + phase_name + " input mesh found to not " +
                             "be watertight. \nThis is an invalid mesh. Please " +
                             "check input.")
        elif values == 1:
            return 2

    if len(edge_dict) == 0:
        if surface == 1 and test == 0:
            raise ValueError("Specimen surface mesh empty. Please check inputs.")
        elif test == 0:
            raise ValueError("phase " + phase_name + " mesh empty. Please check inputs.")
        else:
            return 1

    return 0

def check_tip_detector_proximity(verts, faces, detector_pos, scale, test=0):
    """

    Function to check whether specimen size is greater than the flightpath

    :param1 verts (np.array, (M, 3), dtype=double): The input mesh vertices
    :param2 faces (np.array, (N, 3), dtype=int): The input mesh connectivity matrix
    :param3 detector_pos (list): The detector position
    :param4 scale (float): The specimen scale
    :param5 test (int): Whether test (1) or not (0)

    :returns:
        :(int): 0 if successful,
                1 if detector scale similar to specimen scale,
                2 if detector placed within specimen mesh

    :raises:
        None
    """

    detector_pos = np.array(detector_pos)
    cp = np.mean(verts[faces], axis=1) * scale
    max_z = np.max(cp[:, 2])
    min_z = np.min(cp[:, 2])
    sp_len = max_z - min_z

    sp_det_dist = np.min(np.linalg.norm(cp - detector_pos, axis = 1))

    if detector_pos[2] < max_z and test == 1:
        raise ValueError("Detector placed inside specimen")
    elif detector_pos[2] < max_z:
        return 2

    if sp_det_dist/sp_len < 1.0:
        return 1

    return 0
