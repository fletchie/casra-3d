"""

script for sweeping over and optimising two nominated model evaporation fields
for the SiGe finFET dataset

Reruns model various times, performing regular grid search over the nominated
evaporation fields

Author: Charles Fletcher
Atom Probe Tomography Group, Department of Materials, University of Oxford
March 2021

"""

import os
import sys
import time
from multiprocessing import Pool
import multiprocessing
from functools import partial
import time as timer

sys.path.append("")

try:
    import numpy as np
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Package numpy. Please make sure it is installed.")
    
from casra_3d import tip_simulation as tip_simulation
from casra_3d import perform_projection as perform_projection
from casra_3d.reconstruction import landmark_fitting as landmark_fitting
from casra_3d.reconstruction import model_reconstruction as model_reconstruction
from casra_3d.reconstruction import evaluate_similarity as evaluate_similarity
from casra_3d.reconstruction import tune_image_transfer as tune_image_transfer

def overlap_function(regular_grid_cell, nominated_phases):

    #####inputs/outputs#####
    #pool_id = (multiprocessing.current_process()._identity[0] - 1) % pools
    pool_id = 0

    #experiment input parameters
    apt_data = "c3d_input/sige_finfet/sige_finfet.ato"
    rangefile = "c3d_input/sige_finfet/sige_finfet.rrng"

    det_dist = 0.102
    det_rad  = 0.038

    ##evaporate model parameters
    model_filename = "c3d_input/sige_finfet/sige_finfet.mstate"
    out_filename = "c3d_output/sige_finfet/sweep" + str(pool_id)
    global_parameters_filename = "c3d_input/sige_finfet/parameters"
    mref_filename = "c3d_input/sige_finfet/sige_finfet.mref"
    materials_db = "c3d_input/materials"
    its = 2000
    reset_field = False
    terminate_height = 2.7e-7

    if not os.path.exists("c3d_output/sige_finfet/sweep" + str(pool_id)):
        os.makedirs("c3d_output/sige_finfet/sweep" + str(pool_id))

    ##project model parameters
    filename = "c3d_output/sige_finfet/sweep" + str(pool_id) + "/sweep" + str(pool_id) + "_init.mstate"
    out_filename = "c3d_output/sige_finfet/sweep" + str(pool_id)
    dirname = "c3d_output/sige_finfet/sweep" + str(pool_id)

    ##image transfer parameters
    projection_data = "c3d_output/sige_finfet/sweep" + str(pool_id) + "/projection_data/projection_data.tsv"
    stability_data = "c3d_output/sige_finfet/sweep" + str(pool_id) + "/projection_data/stability_data.tsv"

    model_landmarks_roi ="B-phase3:enter,C-phase2:enter,D-phase1:enter".split(",")
    exp_landmarks_roi = "48000,130000,2995000".split(",")
    exp_landmarks_roi = [int(b) for b in exp_landmarks_roi]

    ##landmark placement parameters
    mapping_data = "c3d_output/sige_finfet/sweep" + str(pool_id) + "/projection_data/projection_data.tsv"
    model_landmarks ="B-phase3:enter,C-phase2:enter,D-phase1:enter".split(",")
    exp_landmarks = "48000,130000,2995000".split(",")
    exp_landmarks = [int(b) for b in exp_landmarks]

    landmark_calibration_file = "c3d_output/sige_finfet/sweep" + str(pool_id) + "/recon/landmark_calibration.rec"
    projection_calibration_file = "c3d_output/sige_finfet/sweep" + str(pool_id) + "/recon/projection_calibration.rec"

    ###model driven reconstruction parameters
    out_recon_file = "c3d_output/sige_finfet/sweep" + str(pool_id) + "/recon/recon.vtk"

    #evaluate MI parameters
    filename = "c3d_output/sige_finfet/sweep" + str(pool_id) + "/sweep" + str(pool_id) + "_init.mstate"
    out_dir = "c3d_output/sige_finfet/sweep" + str(pool_id) + "/recon"
    micro_elements = "bulk:0.66-O;phase1:1-Si;phase2:0.5-Ge;phase3:1-Ge;phase4:1-Si"
    bc = 100
    bw = 2
    detector_efficiency = 0.5
    out_recon_file = "c3d_output/sige_finfet/sweep" + str(pool_id) + "/recon/recon.vtk"
    model_scale = 3.5e-8

    #set model free parameters
    free_params = {}
    for a in range(0, len(nominated_phases)):
        #handle multiple free phases having the same field
        nom_phase_spl = nominated_phases[a].split(";")
        for b in range(0, len(nom_phase_spl)):
            free_params[nom_phase_spl[b]] = regular_grid_cell[2 + a]


    ###perform specimen evaporation simulation
    its = tip_simulation.evaporate_model(model_filename, out_filename, global_parameters_filename,
                                         mref_filename, materials_db, its, reset_field,
                                         free_params, termination_height = terminate_height)

    ###perform ion projection from model
    perform_projection.project(filename, out_filename, global_parameters_filename,
                               mref_filename, materials_db, its, dirname,
                               para = True, real_projection = False,
                               weighted_projection = False)

    #perform specimen alignment and ICF calibration
    optimised_parameters = tune_image_transfer.optimise_image_transfer(projection_data, stability_data,
                                                                       det_dist, det_rad, apt_data,
                                                                       rangefile, model_landmarks_roi,
                                                                       exp_landmarks_roi, nbins = 100)

    #define image transfer list of optimum parameters
    image_transfer = [det_dist, optimised_parameters["tdx"], optimised_parameters["tdy"],
                      optimised_parameters["theta"], optimised_parameters["ICF"]]

    landmark_fitting.landmark(mapping_data, det_rad, dirname, model_landmarks,
                              exp_landmarks, landmark_calibration_file,
                              projection_calibration_file, image_transfer)

    #radius to filter
    #filt_rad = 0.036
    filt_rad = np.inf
    mapping_data = "c3d_output/sige_finfet/sweep" + str(pool_id) + "/projection_data/projection_data_diffeo/projection_data.tsv"

    model_reconstruction.recon(mapping_data, dirname, landmark_calibration_file,
                               projection_calibration_file, apt_data, rangefile,
                               out_recon_file, materials_db, image_transfer, filt_rad = filt_rad)
    print("reconstructed")

    material_db = "c3d_input/materials"
    dd_output = "c3d_output/sige_finfet/sweep0/spread_diffeo.tsv"
    reconstruction_composition_calculation.calculate_compositions(out_recon_file, out_recon_file, material_db = material_db, bw = 1.2e-9,
                                                                  bc = 90, dd_output = dd_output, detection_efficiency = 0.5,
                                                                  simulated = False)

    #####evaluate reconstruction MI
    overlap_value_diff, overlap_value_MI, spread, normalised_spread, normalised_spread_bins, return_code = evaluate_similarity.similarity(filename, out_recon_file,
                                                                                                                                          out_dir, materials_db, bw,
                                                                                                                                          bc, micro_elements,
                                                                                                                                          detector_efficiency, model_scale)
    
    ###write optimization outputs###
    f = open(output_dir + "/optimisation_results.tsv", "a+")
    for a in range(len(nominated_phases)):
        f.write(nominated_phases[a] + "\t" + str(regular_grid_cell[2 + a]) + "\t" + str(image_transfer[1]) +
                "\t" + str(image_transfer[2]) + "\t" + str(image_transfer[3]) + "\t" + str(image_transfer[4]) +
                "\t" + str(overlap_value_MI) + "\t" + str(optimised_parameters["MI"]) + "\t" + str(spread) + "\t" + str(normalised_spread) + "\t" + str(normalised_spread_bins) + "\n")
    f.close()

###default parameters

init_this_time = timer.time()

#fguess = [29864444444.4444, 29500000000]
fguess = [48000000000.0, 30200000000.0]
fguess = [59360000000, 35400000000]
nominated_phases = ["bulk", "phase2"]

grid_sampling_x = 8
grid_sampling_y = 8
pools = int(os.cpu_count()/2)

output_dir = "c3d_output/sige_finfet/optimisation"

#create directory for outputting optimisation results
if not os.path.exists(output_dir):
    os.makedirs(output_dir, exist_ok=True)

#create/clear previous optimisation results
f = open(output_dir + "/optimisation_results.tsv", "w+")
f.close()

#construct regular grid
if grid_sampling_x > 1:
    F00 = np.linspace(fguess[0] - fguess[0] * 0.3,
                      fguess[0] + fguess[0] * 0.3,
                      num = grid_sampling_x, endpoint = True)
else:
    F00 = np.array([fguess[0]])
if grid_sampling_y > 1:
    F01 = np.linspace(fguess[1] - fguess[1] * 0.3,
                      fguess[1] + fguess[1] * 0.3,
                      num = grid_sampling_y, endpoint = True)
else:
    F01 = np.array([fguess[1]])

ids = np.tile(np.linspace(0, pools - 1, pools), int(len(F00)/pools + 1))
F00, F01 = np.meshgrid(F00,F01)

ids = np.tile(ids, len(F01.flatten()))[:len(F01.flatten())]
unid = np.linspace(1, len(ids), len(ids))

regular_grid = np.vstack((ids, unid, F00.flatten(), F01.flatten())).T

#non-parallelised version
for a in range(len(regular_grid)):
    overlap_function(regular_grid[a], nominated_phases)

print("complete")
print(timer.time() - init_this_time)
