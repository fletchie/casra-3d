import sys
sys.path.append("")
import os
import glob
import copy
from multiprocessing import Pool
from functools import partial

try:
    import numpy as np
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Package numpy. Please make sure it is installed.")

try:
    import scipy.ndimage
    import scipy.interpolate
    import scipy.optimize
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Package scipy. Please make sure it is installed.")

from casra_3d.io import model_loaders as model_loaders
from casra_3d.io import model_writers as model_writers
from casra_3d.reconstruction import landmark_fitting as landmark_fitting
from casra_3d.image_processing import interp as interp
from casra_3d.reconstruction import evaluate_similarity as evaluate_similarity
from casra_3d.core import ion_projection as ion_projection
from casra_3d.core import calculate_hitmap as calculate_hitmap


def trajectory_stability(sv0, sv1, sv2, dv0, dv1, dv2):

    """
    Calculates the jacobian determinant of the forward mapping

    Method taken from Polygon Mesh Processing, Botsch
    https://math.stackexchange.com/questions/2930816/gradient-in-a-triangle-unclear-notation

    :param1 sv0 (np.array): 2D array of launch vertices from panels' corner 1
    :param2 sv1 (np.array): 2D array of launch vertices from panels' corner 2
    :param3 sv2 (np.array): 2D array of launch vertices from panels' corner 3
    :param4 sv3 (np.array): 2D array of detector impact vertices from panels' corner 1
    :param5 sv4 (np.array): 2D array of detector impact vertices from panels' corner 2
    :param6 sv5 (np.array): 2D array of detector impact vertices from panels' corner 3
    :returns:
        :stability: 1D arry of calculated local Jacobian determinants
        :lcp: The surface panel centre points for which the Jacobian is defined
        :dcp: The detector element centre points for which the Jacobian is defined
        :conformal: The local conformality conditions
    """

    stability = []
    conformal = []
    J00 = []
    J10 = []
    J01 = []
    J11 = []

    for a in range(len(sv0)):

        sp0 = sv0[a]
        sp1 = sv1[a]
        sp2 = sv2[a]

        ds0 = dv0[a]
        ds1 = dv1[a]
        ds2 = dv2[a]

        #calculate local triangle basis
        X = (sp1 - sp0)/np.linalg.norm(sp1 - sp0)
        n = np.cross(X, sp2 - sp0)
        n = n/np.linalg.norm(n)
        Y = np.cross(n, X)

        X = np.array([1.0, 0.0, 0.0])
        Y = np.array([0.0, 1.0, 0.0])

        X = X - np.sum(X * n) * n
        Y = Y - np.sum(Y * n) * n - np.sum(Y * X) * X

        Xi = 0.0
        Yi = 0.0
        Xj = np.sum((sp1 - sp0) * X)/(np.sum(X * X))
        Yj = np.sum((sp1 - sp0) * Y)/(np.sum(Y * Y))
        Xk = np.sum((sp2 - sp0) * X)/(np.sum(X * X))
        Yk = np.sum((sp2 - sp0) * Y)/(np.sum(Y * Y))

        #Heron's formula
        da = np.linalg.norm(sp1 - sp0)
        db = np.linalg.norm(sp2 - sp1)
        dc = np.linalg.norm(sp0 - sp2)
        s = (da + db + dc)/2.0
        A = np.sqrt(s * (s - da) * (s - db) * (s - dc))

        M = np.array([[Yj - Yk, Yk - Yi, Yi - Yj], [Xk - Xj, Xi - Xk, Xj - Xi]])
        #M = np.array([[Y[1] - Y[2], Y[2] - Y[0]], [X[2] - X[1], X[0] - X[2]]])
        M = 1.0/(2.0 * A) * np.matmul(M, np.array([[ds0[0], ds1[0], ds2[0]], [ds0[1], ds1[1], ds2[1]]]).T).T
        #du = 1.0/(2.0 * A) * np.matmul(M, np.array([ds[0], ds[1]]))
        #calculate partial Jacobian
        J = M[0, 0] * M[1, 1] - M[1, 0] * M[0, 1]
        stability.append(J)

        C0 = M[0, 0] - M[1, 1]
        C1 = M[0, 1] + M[1, 0]
        conformal.append(abs(C0) + abs(C1))
        J00.append(M[0, 0])
        J10.append(M[1, 0])
        J01.append(M[0, 1])
        J11.append(M[1, 1])

    lcp = (sv0 + sv1 + sv2)/3.0
    dcp = (dv0 + dv1 + dv2)/3.0

    return stability, lcp, dcp, conformal, J00, J10, J01, J11


def calculate_sim_chemmap(n1, projection, det_rad, nbins):

    filt = projection["num"] == n1
    chem = projection["chemistry"][filt]
    dcpp = projection["detector_space"][filt]

    x = np.linspace(-det_rad * 1.5, det_rad * 1.5, nbins, endpoint=True)
    XX, XY = np.meshgrid(x, x)

    dint = scipy.interpolate.griddata(dcpp[:, :2], chem, (XX, XY),
                                      fill_value=0, method='nearest')

    dint = dint.flatten()
    dint = dint.reshape(nbins, nbins)

    return dint, XX, XY

def calculate_sim_hitmap(stability, det_rad, nbins, n1):

    filt = stability["num"] == n1
    dv0 = stability["dv0"][filt]
    dv1 = stability["dv1"][filt]
    dv2 = stability["dv2"][filt]
    sv0 = stability["sv0"][filt]
    sv1 = stability["sv1"][filt]
    sv2 = stability["sv2"][filt]
    print(dv0[:10], dv1[:10], dv2[:10])

    jacobian, lcp, dcp, conformal, J00, J10, J01, J11 = trajectory_stability(sv0, sv1, sv2,
                                                                             dv0, dv1, dv2)
    stability["jacobian"][filt] = jacobian

    jacobian = np.array(stability["jacobian"][filt])

    demag = abs(-1.0/np.array(jacobian))
    evap_rate = np.array(stability["evap_rate"])[filt]

    grid_size = nbins + 1

    #print("nan: ", np.sum(np.isnan(dv0)), np.sum(np.isnan(dv1)), np.sum(np.isnan(dv2)), np.sum(np.isnan(demag)))
    dint = calculate_hitmap.calculate_hitmap(sv0, sv1, sv2, dv0, dv1, dv2, demag,
                                             evap_rate, det_rad, grid_size = grid_size)[0]

    #import matplotlib.pyplot as plt
    #plt.imshow(dint)
    #plt.savefig("savethis.png")
    #plt.close()
    #sys.exit()
    dint = scipy.ndimage.gaussian_filter(dint, sigma=1)

    dx = np.linspace(-det_rad, det_rad, grid_size, endpoint = True)
    eXX, eXY = np.meshgrid(dx, dx)
    XX = (eXX[1:, 1:] + eXX[:-1, :-1])/2.0
    XY = (eXY[1:, 1:] + eXY[:-1, :-1])/2.0

    dint = dint.reshape(nbins, nbins)

    return dint, XX, XY

def calculate_sim_hitmap_old(n1, stability, det_rad, nbins):

    filt = stability["num"] == n1
    dv0 = stability["dv0"][filt]
    dv1 = stability["dv1"][filt]
    dv2 = stability["dv2"][filt]

    dcp = (dv0 + dv1 + dv2)/3.0
    jacobian = np.array(stability["jacobian"][filt])

    mag = - 1.0/np.array(jacobian)
    evap_rate = np.array(stability["evap_rate"])[filt]
    density = mag * evap_rate

    x = np.linspace(-det_rad * 1.5, det_rad * 1.5, nbins, endpoint=True)
    XX, XY = np.meshgrid(x, x)

    dcpp = copy.deepcopy(dcp)
    pdensity = copy.deepcopy(density)

    #remove positive values
    if np.max(density) < 0.0:
        dcpp = dcpp[density < 0.0]
        pdensity = pdensity[density < 0.0]

    dint = scipy.interpolate.griddata(dcpp[:, :2], pdensity, (XX, XY),
                                      fill_value=0, method='nearest')

    dint = scipy.ndimage.gaussian_filter(dint, sigma=1)
    dint = dint.flatten()

    #calculate cumulative distribution function
    data_sorted = np.sort(dint)
    ds_ind = np.argsort(dint)

    # calculate the proportional values of samples
    p = 1.0 * np.arange(len(data_sorted)) / (len(data_sorted) - 1)

    #filter out high density outliers
    md = np.max(data_sorted[p < 0.95])
    dint[ds_ind * (p > 0.95)] = md

    #normalise
    #dint = dint/np.sum(dint)

    dint = dint.reshape(nbins, nbins)

    return dint, XX, XY

def create_hitmap_stack(stability, projection, det_rad, nbins):
    nv = np.sort(np.unique(projection["num"]))
    chem_stack = []

    print("construct detector space density")
    count = os.cpu_count()
    if count > 2:
        pool = Pool(count)
        partial_ = partial(calculate_sim_hitmap, stability, det_rad, nbins)
        hit_stack = pool.map(partial_, nv)
        pool.close()
        pool.join()
        hit_stack = np.array(hit_stack)[:, 0, :, :]

    else:
        hit_stack = []
        for a in nv:
            hmap, XX, XY = calculate_sim_hitmap(a, stability, det_rad, nbins)
            hit_stack.append(hmap)

    print("detector space density construction complete")

    for a in nv:
        cmap, XX, XY = calculate_sim_chemmap(a, projection, det_rad, nbins)
        chem_stack.append(cmap)

    hit_stack = np.array(hit_stack)
    hit_stack = np.transpose(hit_stack, axes = (1, 2, 0))
    chem_stack = np.array(chem_stack)
    chem_stack = np.transpose(chem_stack, axes = (1, 2, 0))

    return hit_stack, chem_stack, XX, XY

def sub_volumes(hit_stack, det_rad, XX, XY, xc, yc):
    vols = np.array([0.0] * len(hit_stack[0, 0, :]))

    det_filt = np.zeros(XX.shape)
    det_filt[((XX - xc)**2.0 + (XY - yc)**2.0) <= det_rad**2.0] = 1.0
    for a in range(len(hit_stack[0, 0, :])):
        vols[a] = np.sum(-hit_stack[:, :, a] * det_filt)
    vols = np.cumsum(vols)
    return vols

def calculate_landmark_volumes(sim_map_frames, proj_frames, vols):
    landmark_vols = []
    l = 0
    for a in range(0, len(proj_frames)):
        if sim_map_frames[l] == proj_frames[a]:
            l += 1
            landmark_vols.append(vols[a])
            if l == len(sim_map_frames):
                break

    return landmark_vols

def calculate_ionic_volumes(landmark_vols, exp_landmarks):
    ionic_vols = []
    for a in range(1, len(landmark_vols)):
        iv = (landmark_vols[a] - landmark_vols[a - 1])/(exp_landmarks[a] - exp_landmarks[a - 1])
        ionic_vols.append(iv)
    return ionic_vols

def event_at_frame(vols, ionic_vols, exp_landmarks, sim_map_frames):
    f = int(sim_map_frames[0]/5)
    iv = ionic_vols[0]
    vol = vols[f]
    fat = []
    frms = []
    lm = 0
    for a in range(exp_landmarks[0], exp_landmarks[-1]):
        vol += iv
        if vol > (vols[f + 1]):
            f = f + 1
            fat.append(a)
            frms.append(f)
        if a > exp_landmarks[lm + 1]:
            lm += 1
            if lm == len(exp_landmarks):
                break
            iv = ionic_vols[lm]
    return fat, frms

def plot_hist(ions, chem):
    import matplotlib.pyplot as plt
    #find indexes of ions to filter
    lions = ["Ge:1"]

    fv = []
    for i in range(len(ions)):
        if ions[i] in lions:
            fv.append(i + 1)

    fdv = np.isin(chem, fv)
    v = np.linspace(0, len(fdv), len(fdv))
    v = v[fdv]
    hist, bin_edges = np.histogram(v, bins = 200)
    bc = (bin_edges[1:] + bin_edges[:-1])/2.0
    plt.figure(figsize = (35, 10))
    plt.plot(bc, np.log(hist))
    plt.xticks(np.linspace(min(bc), max(bc), 35, endpoint = True))
    plt.savefig("hist.png")

def generate_experiment_slices(fat, frms, hit_stack, chem_stack, rsl, dx,
                               dy, chem, ions, nbins, dmask, det_rad):

    #construct experiment hitmaps to compare to
    dmask = dmask.reshape(nbins, nbins)

    l = 0
    exp_comp = []
    for a in range(len(frms)):
        if frms[a] == rsl[l]:
            exp_comp.append(fat[a])
            l += 1
            if l == len(rsl):
                break

    #find indexes of ions to filter
    lions = ["Si:2", "SiO:1", "Si:1"]
    lions = ["Ge:1"]

    #remove trace ions (< 200)
    #lions = ['Si:2', 'Ge:2', 'B:1', 'Pt:1', 'Si:1', 'Ge:1', 'SiO:1', 'Ga:1']

    #lions = ["Si:2"]
    #lions = ["SiO:1"]
    #lions = ["Si:1"]

    fv = []
    for i in range(len(ions)):
        if ions[i] in lions:
            fv.append(i + 1)

    #construct hitmap
    exp_hitmap = []
    for a in range(len(exp_comp)):
        b_low = exp_comp[a] - 20000
        b_up = exp_comp[a] + 20000

        #filter out non-selected ions
        xv = dx[b_low:b_up]
        yv = dy[b_low:b_up]
        fdv = np.isin(chem[b_low:b_up], fv)
        dxv = xv[fdv]
        dyv = yv[fdv]
        #v = np.linspace(0, len(fdv), len(fdv))
        #v = v[fdv]
        #hist, bin_edges = np.histogram(v, bins = 100)
        #plt.plot(hist)
        #plt.show()

        H, xedges, yedges = np.histogram2d(dxv, dyv, bins= nbins,
                                       range=[[-det_rad * 0.9, det_rad * 0.9],
                                       [-det_rad * 0.9, det_rad * 0.9]])

        #H = scipy.ndimage.gaussian_filter(H, sigma = 2)

        #H[dmask == 0] = 0
        #H = H/10000

        Hn, xedges, yedges = np.histogram2d(xv, yv, bins = nbins,
                                            range=[[-det_rad * 0.9, det_rad * 0.9],
                                            [-det_rad * 0.9, det_rad * 0.9]])
        #transform into composition
        #H = scipy.ndimage.gaussian_filter(H, sigma = 2)
        #Hn = scipy.ndimage.gaussian_filter(Hn, sigma = 2)

        H = H/(Hn + 1e-10)
        H = scipy.ndimage.gaussian_filter(H, sigma = 2)
        H[dmask == 0] = 0

        exp_hitmap.append(H)

    return exp_hitmap, exp_comp

def generate_simulation_slices(chem_stack, rsl, nncoords, nbins):

    sim_hitmaps = []
    for a in range(len(rsl)):
        f = rsl[a]
        shm = chem_stack[:, :, f]
        shm = np.ascontiguousarray(shm)
        coords0 = np.ascontiguousarray(nncoords[:, 0].reshape((nbins, nbins)))
        coords1 = np.ascontiguousarray(nncoords[:, 1].reshape((nbins, nbins)))

        hmnew = interp.interp2D(shm, coords0, coords1)
        sim_hitmaps.append(hmnew)
    sim_hitmaps = np.array(sim_hitmaps)
    #sim_hitmaps = scipy.ndimage.gaussian_filter(sim_hitmaps, sigma = 1)
    return sim_hitmaps

def measure_similarity(sim_hitmaps, exp_hitmaps, dmask, nbins):

    dmask = dmask.reshape(nbins, nbins)
    MI = 0.0
    nm = np.sum(dmask != 0)

    exp_hitmaps = np.array(exp_hitmaps)
    sim_hitmaps = np.array(sim_hitmaps)

    exp_hit_flat = exp_hitmaps[:, dmask != 0]
    sim_hitmap_flat = sim_hitmaps[:, dmask != 0]

    exp_hit_flat = exp_hit_flat.flatten()
    sim_hitmap_flat = sim_hitmap_flat.flatten()

    joint_histogram, x_edges, y_edges = np.histogram2d(exp_hit_flat, sim_hitmap_flat, bins=[40, 40])
    MI += evaluate_similarity.mutual_information(joint_histogram)/nm

    return MI

def match_sim_exp_volumes(exp_landmarks_mono, sim_landmarks):
    A0 = (exp_landmarks_mono[-1] - exp_landmarks_mono[0])/(sim_landmarks[-1] - sim_landmarks[0])
    res = scipy.optimize.minimize(vol_diff, A0, args = (exp_landmarks_mono, sim_landmarks), method='nelder-mead', options={'xatol': 1e-8, 'disp': True})
    return res

def vol_diff(A, exp_landmarks_mono, sim_landmarks):
    num = len(sim_landmarks)
    return 1.0/num * np.sum((A * sim_landmarks - exp_landmarks_mono) * (A * sim_landmarks - exp_landmarks_mono))

def exp_landmarks_monotomic(exp_landmarks, chem, ic):
    exp_landmarks_mono = []
    rt = 0
    b = 0

    for a in range(exp_landmarks[-1] + 100000):
        if chem[a] > 0:
            rt += ic[chem[a] - 1]
            if a >= exp_landmarks[b]:
                b += 1
                exp_landmarks_mono.append(rt)
        if len(exp_landmarks_mono) == len(exp_landmarks):
            break

    return exp_landmarks_mono

def FD_sweep(stability, stability_ICF, projection, projection_ICF,
             ICF_range, x_sweep, y_sweep, z_angle, det_rad, det_dist, nbins,
             model_landmarks, exp_landmarks, dmask, coords, scoords,
             dx, dy, chem, ions, rsc):

    MI_vals = np.zeros((0))
    params = np.zeros((0, 4))
    for ICF in ICF_range:
        print("Image compression factor: " + str(ICF))
        stability_ICF["dv0"] = ion_projection.transfer_function(stability["dv0"], stability["vel0"], det_dist, kappa=ICF)
        stability_ICF["dv1"] = ion_projection.transfer_function(stability["dv1"], stability["vel1"], det_dist, kappa=ICF)
        stability_ICF["dv2"] = ion_projection.transfer_function(stability["dv2"], stability["vel2"], det_dist, kappa=ICF)
        projection_ICF["detector_space"] = ion_projection.transfer_function(projection["detector_space"], projection["vel"], det_dist, kappa=ICF)

        hit_stack, chem_stack, dXX, dXY = create_hitmap_stack(stability_ICF, projection_ICF, det_rad, nbins)
        hit_stack = abs(hit_stack)
        proj_frames = np.unique(projection["num"])

        #function for determining frames of the landmark - this should be within translation loop (leave out for now)
        sim_map_frames = landmark_fitting.determine_ion_frame(model_landmarks, projection_ICF, det_rad)

        print("construct detector space density")

        #parallelise sweep
        count = os.cpu_count()
        if count > 2:
            t = np.linspace(0, len(x_sweep), len(x_sweep), endpoint = False, dtype = int)
            pool = Pool(count)
            partial_ = partial(alignment_transform, hit_stack, det_rad, dXX, dXY, sim_map_frames,
                               proj_frames, exp_landmarks, scoords, rsc, x_sweep, y_sweep, z_angle,
                               chem_stack, dx, dy, nbins, dmask, ions, chem, coords, ICF)
            data = pool.map(partial_, t)
            pool.close()
            pool.join()
            data = np.array(data)
            data = data.reshape(data.shape[0] * data.shape[1], data.shape[2])
            params = np.concatenate((params, data[:, :4]), axis = 0)
            MI_vals = np.append(MI_vals, data[:, 4])
        else:
            #perform x, y sweep
            for t in range(len(x_sweep)):
                print(100.0 * t/len(x_sweep))
                tx = x_sweep[t]
                for ty in y_sweep:
                    vols = abs(sub_volumes(hit_stack, det_rad, dXX, dXY, 0.0, 0.0))
                    landmark_vols = calculate_landmark_volumes(sim_map_frames, proj_frames, vols)
                    ionic_vols = calculate_ionic_volumes(landmark_vols, exp_landmarks)
                    fat, frms = event_at_frame(vols, ionic_vols, exp_landmarks, sim_map_frames)

                    rsl = np.linspace(frms[0], frms[-1], 30, endpoint = True, dtype = int)

                    exp_hitmaps, exp_comp = generate_experiment_slices(fat, frms, hit_stack, chem_stack, rsl,
                                                                       dx, dy, chem, ions, nbins, dmask, det_rad)

                    tcoords = np.array([tx, ty])/rsc

                    #perform rotation sweep
                    for theta in z_angle:
                        R = np.array([[np.cos(theta), -np.sin(theta)], [np.sin(theta), np.cos(theta)]])
                        ncoords = np.matmul(R, (coords + scoords).T).T - scoords
                        nncoords = ncoords + tcoords

                        param_arr = np.array([[ICF, tcoords[0] * rsc, tcoords[1] * rsc, theta]])
                        params = np.concatenate((params, param_arr), axis = 0)
                        sim_hitmaps = generate_simulation_slices(chem_stack, rsl, nncoords, nbins)
                        MI = np.array(measure_similarity(sim_hitmaps, exp_hitmaps, dmask, nbins))
                        MI_vals = np.append(MI_vals, np.array([MI]))

    max_ind = np.argmax(MI_vals)
    min_ind = np.argmin(MI_vals)
    params = np.array(params)

    return params, max_ind, min_ind, MI_vals

def alignment_transform(hit_stack, det_rad, dXX, dXY, sim_map_frames, proj_frames,
                        exp_landmarks, scoords, rsc, x_sweep, y_sweep, z_angle,
                        chem_stack, dx, dy, nbins, dmask, ions, chem, coords, ICF, t):
    #perform x, y sweep
    tx = x_sweep[t]
    out = []

    for ty in y_sweep:
        vols = abs(sub_volumes(hit_stack, det_rad, dXX, dXY, 0.0, 0.0))
        landmark_vols = calculate_landmark_volumes(sim_map_frames, proj_frames, vols)
        ionic_vols = calculate_ionic_volumes(landmark_vols, exp_landmarks)
        fat, frms = event_at_frame(vols, ionic_vols, exp_landmarks, sim_map_frames)

        rsl = np.linspace(frms[0], frms[-1], 30, endpoint = True, dtype = int)

        exp_hitmaps, exp_comp = generate_experiment_slices(fat, frms, hit_stack, chem_stack, rsl,
                                                           dx, dy, chem, ions, nbins, dmask, det_rad)

        tcoords = np.array([tx, ty])/rsc

        #perform rotation sweep
        for theta in z_angle:
            R = np.array([[np.cos(theta), -np.sin(theta)], [np.sin(theta), np.cos(theta)]])
            ncoords = np.matmul(R, (coords + scoords).T).T - scoords
            nncoords = ncoords + tcoords

            sim_hitmaps = generate_simulation_slices(chem_stack, rsl, nncoords, nbins)
            MI = measure_similarity(sim_hitmaps, exp_hitmaps, dmask, nbins)
            out.append([ICF, tcoords[0] * rsc, tcoords[1] * rsc, theta, MI])

    return out

def vol_metric(ionic_vols, landmark_vols):
    return np.sum(abs(ionic_vols - np.mean(ionic_vols)))/(landmark_vols[-1] - landmark_vols[0])

def vol_metric_weighted(ionic_vols, landmark_vols):

    mean_vol = 0.0
    for a in range(len(ionic_vols)):
        mean_vol += ionic_vols[a] * (landmark_vols[a + 1] - landmark_vols[a])
    mean_vol /= (landmark_vols[-1] - landmark_vols[0])

    weighted_res = 0.0
    for a in range(len(ionic_vols)):
        weighted_res += abs(ionic_vols[a] - mean_vol) * (landmark_vols[a + 1] - landmark_vols[a])
    weighted_res /= (landmark_vols[-1] - landmark_vols[0])

    return weighted_res


def ICF_vol_sweeper(stability_ICF, stability, projection_ICF, projection, ICF_range, det_rad, det_dist, nbins, model_landmarks, exp_landmarks_mono, sv):

    ICF_sim = []
    ICF_sweep = np.linspace(ICF_range[0], ICF_range[-1], sv, endpoint = True)

    for ICF in ICF_sweep:
        stability_ICF["dv0"] = ion_projection.transfer_function(stability["dv0"], stability["vel0"], det_dist, kappa=ICF)
        stability_ICF["dv1"] = ion_projection.transfer_function(stability["dv1"], stability["vel1"], det_dist, kappa=ICF)
        stability_ICF["dv2"] = ion_projection.transfer_function(stability["dv2"], stability["vel2"], det_dist, kappa=ICF)
        projection_ICF["detector_space"] = ion_projection.transfer_function(projection["detector_space"], projection["vel"], det_dist, kappa=ICF)

        hit_stack, chem_stack, dXX, dXY = create_hitmap_stack(stability_ICF, projection_ICF, det_rad, nbins)
        proj_frames = np.unique(projection["num"])

        #function for determining frames of the landmark - this should be within translation loop (leave out for now)
        sim_map_frames = landmark_fitting.determine_ion_frame(model_landmarks, projection_ICF, det_rad)

        vols = sub_volumes(hit_stack, det_rad, dXX, dXY, 0.0, 0.0)
        landmark_vols = calculate_landmark_volumes(sim_map_frames, proj_frames, vols)
        ionic_vols = calculate_ionic_volumes(landmark_vols, exp_landmarks_mono)
        ICF_sim.append(vol_metric_weighted(ionic_vols, landmark_vols))

    return ICF_sim, ICF_sweep

def visualise_hitmaps(params, max_ind, stability_ICF, projection_ICF, stability,
                      projection, coords, scoords, dx, dy, chem, ions, dmask, det_rad, nbins, det_dist,
                      model_landmarks, exp_landmarks, rsc):

    try:
        import matplotlib.pyplot as plt
    except ModuleNotFoundError:
        raise ModuleNotFoundError("Cannot find Package matplotlib. Please make sure it is " +
                                  "installed to call functions requiring graphical outputs.")

    ICF = params[max_ind][0]

    stability_ICF["dv0"] = ion_projection.transfer_function(stability["dv0"], stability["vel0"], det_dist, kappa=ICF)
    stability_ICF["dv1"] = ion_projection.transfer_function(stability["dv1"], stability["vel1"], det_dist, kappa=ICF)
    stability_ICF["dv2"] = ion_projection.transfer_function(stability["dv2"], stability["vel2"], det_dist, kappa=ICF)
    projection_ICF["detector_space"] = ion_projection.transfer_function(projection["detector_space"], projection["vel"], det_dist, kappa=ICF)

    hit_stack, chem_stack, dXX, dXY = create_hitmap_stack(stability_ICF, projection_ICF, det_rad, nbins)
    hit_stack = abs(hit_stack)
    proj_frames = np.unique(projection["num"])

    #function for determining frames of the landmark - this should be within translation loop (leave out for now)
    sim_map_frames = landmark_fitting.determine_ion_frame(model_landmarks, projection_ICF, det_rad)

    vols = abs(sub_volumes(hit_stack, det_rad, dXX, dXY, 0.0, 0.0))
    landmark_vols = calculate_landmark_volumes(sim_map_frames, proj_frames, vols)
    ionic_vols = calculate_ionic_volumes(landmark_vols, exp_landmarks)
    fat, frms = event_at_frame(vols, ionic_vols, exp_landmarks, sim_map_frames)

    rsl = np.linspace(frms[0], frms[-1], 30, endpoint = True, dtype = int)

    exp_hitmaps, exp_comp = generate_experiment_slices(fat, frms, hit_stack, chem_stack, rsl, dx,
                                                       dy, chem, ions, nbins, dmask, det_rad)

    tcoords = np.array([params[max_ind][1], params[max_ind][2]])/rsc
    theta = params[max_ind][3]
    R = np.array([[np.cos(theta), -np.sin(theta)], [np.sin(theta), np.cos(theta)]])
    ncoords = np.matmul(R, (coords + scoords).T).T - scoords
    nncoords = ncoords + tcoords
    sim_hitmaps = generate_simulation_slices(chem_stack, rsl, nncoords, nbins)

    if not os.path.exists("hm"):
        os.makedirs("hm")
    else:
        files = os.listdir('hm')
        for f in files:
            os.remove("hm/" + f)

    for a in range(len(sim_hitmaps)):
        plt.imshow(sim_hitmaps[a])
        plt.savefig("hm/sim" + str(a) + ".png")
        plt.close()
        plt.imshow(exp_hitmaps[a])
        plt.savefig("hm/exp" + str(a) + ".png")
        plt.close()

#plot ICF max values
def ICF_plot(params, MI_vals, name):

    try:
        import matplotlib.pyplot as plt
    except ModuleNotFoundError:
        raise ModuleNotFoundError("Cannot find Package matplotlib. Please make sure it is " +
                                  "installed to call functions requiring graphical outputs.")

    if not os.path.exists("var"):
        os.makedirs("var")

    ICFr = np.unique(params[:, 0])
    ICFy = []
    MI_vals = np.array(MI_vals).flatten()
    for a in range(len(ICFr)):
        ICFy.append(np.max(MI_vals[params[:, 0] == ICFr[a]]))

    plt.plot(ICFr, ICFy)
    plt.xlabel(r"$\kappa$ (linear compression) (a.u.)", fontsize = 16)
    plt.ylabel("Microstructual Similarity (MI)", fontsize = 16)
    plt.xticks(fontsize = 14)
    plt.yticks(fontsize = 14)
    plt.tight_layout()
    plt.savefig(name, dpi = 300)
    plt.close()

#plot angle max values
def rot_plot(params, MI_vals, name):

    try:
        import matplotlib.pyplot as plt
    except ModuleNotFoundError:
        raise ModuleNotFoundError("Cannot find Package matplotlib. Please make sure it is " +
                                  "installed to call functions requiring graphical outputs.")

    if not os.path.exists("var"):
        os.makedirs("var")

    angr = np.unique(params[:, 3])
    angy = []
    MI_vals = np.array(MI_vals).flatten()
    for a in range(len(angr)):
        angy.append(np.max(MI_vals[params[:, 3] == angr[a]]))

    plt.xlabel(r"$\theta$ (angle) (degrees)", fontsize = 16)
    plt.ylabel("Microstructual Similarity (MI)", fontsize = 16)
    plt.xticks(fontsize = 14)
    plt.yticks(fontsize = 14)
    plt.plot(angr * 180.0/np.pi, angy)
    plt.tight_layout()
    plt.savefig(name, dpi = 300)
    plt.close()

def tr_plot(params, MI_vals, x_sweep, y_sweep, name):

    try:
        import matplotlib.pyplot as plt
    except ModuleNotFoundError:
        raise ModuleNotFoundError("Cannot find Package matplotlib. Please make sure it is " +
                                  "installed to call functions requiring graphical outputs.")

    if not os.path.exists("var"):
        os.makedirs("var")

    #plot transform max values
    trd = np.unique(params[:, 1:3], axis = 0)
    trr = []
    MI_vals = np.array(MI_vals).flatten()
    for a in range(len(trd)):
        trr.append(np.max(MI_vals[(params[:, 1] == trd[a, 0]) * (params[:, 2] == trd[a, 1])]))

    dx = (x_sweep[1]-x_sweep[0])/2.0
    dy = (y_sweep[1]-y_sweep[0])/2.0
    extent = [x_sweep[0]-dx, x_sweep[-1]+dx, y_sweep[0]-dy, y_sweep[-1]+dy]

    trr = np.array(trr)
    plt.imshow(trr.reshape(len(x_sweep), len(y_sweep)).T, origin = "lower", extent = extent)
    plt.xlabel("translation x-pos (mm)", fontsize = 16)
    plt.ylabel("translation y-pos (mm)", fontsize = 16)
    cbar = plt.colorbar()
    cbar.set_label('Microstructual Similarity (MI)', fontsize = 16)
    cbar.ax.tick_params(labelsize=14)
    plt.tight_layout()
    plt.savefig(name, dpi = 300)
    plt.close()

def plot_hit_stacks(params, max_ind, stability, projection, stability_ICF,
                    projection_ICF, coords, scoords, dx, dy, chem, ions, dmask,
                    det_dist, det_rad, nbins, model_landmarks, exp_landmarks, rsc):

    ICF = params[max_ind][0]

    stability_ICF["dv0"] = ion_projection.transfer_function(stability["dv0"], stability["vel0"], det_dist, kappa=ICF)
    stability_ICF["dv1"] = ion_projection.transfer_function(stability["dv1"], stability["vel1"], det_dist, kappa=ICF)
    stability_ICF["dv2"] = ion_projection.transfer_function(stability["dv2"], stability["vel2"], det_dist, kappa=ICF)
    projection_ICF["detector_space"] = ion_projection.transfer_function(projection["detector_space"], projection["vel"], det_dist, kappa=ICF)

    hit_stack, chem_stack, dXX, dXY = create_hitmap_stack(stability_ICF, projection_ICF, det_rad, nbins)
    hit_stack = abs(hit_stack)
    proj_frames = np.unique(projection["num"])

    #function for determining frames of the landmark - this should be within translation loop (leave out for now)
    sim_map_frames = landmark_fitting.determine_ion_frame(model_landmarks, projection_ICF, det_rad)

    vols = abs(sub_volumes(hit_stack, det_rad, dXX, dXY, 0.0, 0.0))
    landmark_vols = calculate_landmark_volumes(sim_map_frames, proj_frames, vols)
    ionic_vols = calculate_ionic_volumes(landmark_vols, exp_landmarks)
    fat, frms = event_at_frame(vols, ionic_vols, exp_landmarks, sim_map_frames)

    rsl = np.linspace(frms[0], frms[-1], 40, endpoint = True, dtype = int)

    exp_hitmaps, exp_comp = generate_experiment_slices(fat, frms, hit_stack, chem_stack, rsl,
                                                       dx, dy, chem, ions, nbins, dmask, det_rad)

    exp_hitmaps = np.array(exp_hitmaps)

    tcoords = np.array([params[max_ind][1], params[max_ind][2]])/rsc
    #tcoords = np.array([0.0, 0.0])

    theta = params[max_ind][3]
    R = np.array([[np.cos(theta), -np.sin(theta)], [np.sin(theta), np.cos(theta)]])
    ncoords = np.matmul(R, (coords + scoords).T).T - scoords
    nncoords = ncoords + tcoords
    sim_hitmaps = generate_simulation_slices(chem_stack, rsl, nncoords, nbins)

    sim_hitmaps = np.transpose(sim_hitmaps, axes = (1, 2, 0))
    exp_hitmaps = np.transpose(exp_hitmaps, axes = (1, 2, 0))

    x = sim_hitmaps.shape[0]
    y = sim_hitmaps.shape[1]
    z = sim_hitmaps.shape[2]
    x = np.linspace(-det_rad * 1.5, det_rad * 1.5, x)
    y = np.linspace(-det_rad * 1.5, det_rad * 1.5, y)
    z = -np.linspace(0, det_rad * 4.0, z)
    XX, XY, XZ = np.meshgrid(x, y, z)

    points = np.vstack((XY.flatten(), XX.flatten(), XZ.flatten())).T
    cells = np.linspace(0, len(points), len(points),
                        endpoint=False, dtype=int).reshape(-1, 1)
    model_writers.write_vtk("stack/sim_stack.vtk", points, cells, point_data = {"chem": sim_hitmaps.flatten()})

    x = sim_hitmaps.shape[0]
    y = sim_hitmaps.shape[1]
    z = sim_hitmaps.shape[2]
    x = np.linspace(-det_rad * 0.9, det_rad * 0.9, x)
    y = np.linspace(-det_rad * 0.9, det_rad * 0.9, y)
    z = -np.linspace(0, det_rad * 4.0, z)
    XX, XY, XZ = np.meshgrid(x, y, z)
    points = np.vstack((XY.flatten(), XX.flatten(), XZ.flatten())).T

    cells = np.linspace(0, len(points), len(points),
                        endpoint=False, dtype=int).reshape(-1, 1)
    model_writers.write_vtk("stack/exp_stack.vtk", points, cells, point_data = {"si": exp_hitmaps.flatten()})

def optimise_image_transfer(trajectory_data, stability_data, det_dist, det_rad, apt_data,
                            rangefile, model_landmarks, exp_landmarks, nbins = 100):

    """

    Function for optimising specimen alignment and image compression

    :param1 trajectory_data (str): The trajectory_data (projection_data) model .tsv output filename
    :param2 stability_data (str): The stability_data (projection_data) model .tsv output filename
    :param3 det_dist (float): The instrument flight path (in metres)
    :param4 det_rad (float): The instrument detector radius (in metres)
    :param5 apt_data (str): The apt_data filename
    :param6 rangefile (str): The apt data rangefile
    :param7 model_landmarks (list[str]): The model landmarks
    :param8 exp_landmarks (list[int]): The experiment detector sequence landmarks
    :param9 nbins (int): The histogram grid size for MI calculation (nbins x nbins)

    :returns:
        :converged_params (dict): The converged alignment parameters. Keys given below.
            "MI": The maximum MI value
            "ICF": The best fit ICF
            "tdx": The best fit dx detector translation
            "tdy": The best fit dy detector translation
            "theta": The best fit detector rotation

    """

    #print(trajectory_data)
    #print(stability_data)
    #print(det_rad)
    #print(det_dist)
    #print(model_landmarks)
    #print(exp_landmarks)
    #print(apt_data)
    #print(rangefile)
    #print(nbins)
    #sys.exit()

    #load simulation model data
    projection = {"num": [], "sample_space": [], "detector_space": [], "vel": [],
                  "chemistry": [], "ion_phase": [], "trajectories": [],
                  "panel_index": []}

    model_loaders.load_tsv_ion_trajectories(projection, trajectory_data)
    stability = model_loaders.load_tsv_stability_trajectories(stability_data)

    dx, dy, sx, sy, sz, chem, ions, ic, VP, da, da_p, experimental = model_loaders.load_apt_data(apt_data, rangefile, 1.0)

    dx = dx[chem != 0]
    dy = dy[chem != 0]
    chem = chem[chem != 0]

    rad2 = dx**2.0 + dy**2.0
    dx = dx[rad2 < det_rad**2.0]
    dy = dy[rad2 < det_rad**2.0]
    chem = chem[rad2 < det_rad**2.0]

    stability_ICF = copy.deepcopy(stability)
    projection_ICF = copy.deepcopy(projection)

    ###create detected simulation subvolume
    x = np.linspace(-det_rad * 0.9, det_rad * 0.9, nbins, endpoint=True)
    xmin = -det_rad * 1.5
    xmax = det_rad * 1.5

    #calculate detector bin spacing
    rsc = (xmax - xmin)/(nbins - 1)

    x = (nbins - 1) * x/(xmax - xmin)
    x[-1] -= 1e-8
    rot_shift = (nbins - 1) * xmin/(xmax - xmin)
    x -= rot_shift
    XX, XY = np.meshgrid(x, x)
    XXf = XX.flatten()
    XYf = XY.flatten()
    coords = np.vstack((XXf, XYf)).T

    #central axis to rotate around
    scoords = np.array([rot_shift, rot_shift])

    #create detector mask
    mx = np.linspace(-det_rad * 0.9, det_rad * 0.9, nbins, endpoint=True)
    mXX, mXY = np.meshgrid(mx, mx)
    dmask = mXX**2.0 + mXY**2.0 < det_rad**2.0
    dmask = dmask.flatten()

    #the regular grid to sweep over
    x_sweep = np.linspace(-30.0, 30.0, 12) * det_rad/100.0
    y_sweep = np.linspace(-30.0, 30.0, 12) * det_rad/100.0
    z_angle = np.linspace(-180.0, 180.0, 10) * np.pi/180.0
    ICF_range = np.linspace(0.6, 0.8, 5, endpoint = True)

    #the regular grid to sweep over
    x_sweep = np.linspace(-30.0, 30.0, 16) * det_rad/100.0
    y_sweep = np.linspace(-30.0, 30.0, 16) * det_rad/100.0
    z_angle = np.linspace(-180.0, 180.0, 14) * np.pi/180.0
    ICF_range = np.linspace(0.6, 1.0, 6, endpoint = True)


    #coarse sweep
    params, max_ind, min_ind, MI_vals = FD_sweep(stability, stability_ICF,
                                        projection, projection_ICF,
                                        ICF_range, x_sweep, y_sweep,
                                        z_angle, det_rad, det_dist,
                                        nbins, model_landmarks,
                                        exp_landmarks, dmask,
                                        coords, scoords,
                                        dx, dy, chem, ions, rsc)

    ICF_plot(params, MI_vals, "var/ICF_coarse.png")
    rot_plot(params, MI_vals, "var/rot_coarse.png")
    tr_plot(params, MI_vals, x_sweep, y_sweep, "var/tr_coarse.png")
    print("alignment tuning complete")
    #coarse search grid resolution
    ddx = (x_sweep[1] - x_sweep[0]) * 1.5
    ddy = (y_sweep[1] - y_sweep[0]) * 1.5
    ddtheta = z_angle[1] - z_angle[0]
    ddicf = (ICF_range[1] - ICF_range[0]) * 1.2

    x_sweep = np.linspace(params[max_ind][1] - ddx, params[max_ind][1] + ddx, 8)
    y_sweep = np.linspace(params[max_ind][2] - ddy, params[max_ind][2] + ddy, 8)
    z_angle = np.linspace(params[max_ind][3] - ddtheta, params[max_ind][3] + ddtheta, 15)
    ICF_range = np.linspace(params[max_ind][0] - ddicf, params[max_ind][0] + ddicf, 7, endpoint = True)

    #fine sweep
    params, max_ind, min_ind, MI_vals = FD_sweep(stability, stability_ICF,
                                        projection, projection_ICF,
                                        ICF_range, x_sweep, y_sweep,
                                        z_angle, det_rad, det_dist,
                                        nbins, model_landmarks,
                                        exp_landmarks, dmask,
                                        coords, scoords,
                                        dx, dy, chem, ions, rsc)

    #fine search grid resolution
    ddx = x_sweep[1] - x_sweep[0]
    ddy = y_sweep[1] - y_sweep[0]
    ddtheta = z_angle[1] - z_angle[0]
    ddicf = ICF_range[1] - ICF_range[0]

    x_sweep = np.linspace(params[max_ind][1] - ddx, params[max_ind][1] + ddx, 10)
    y_sweep = np.linspace(params[max_ind][2] - ddy, params[max_ind][2] + ddy, 10)
    z_angle = np.linspace(params[max_ind][3] - ddtheta, params[max_ind][3] + ddtheta, 20)
    ICF_range = np.linspace(params[max_ind][0] - ddicf, params[max_ind][0] + ddicf, 10, endpoint = True)

    #finer sweep
    params, max_ind, min_ind, MI_vals = FD_sweep(stability, stability_ICF,
                                        projection, projection_ICF,
                                        ICF_range, x_sweep, y_sweep,
                                        z_angle, det_rad, det_dist,
                                        nbins, model_landmarks,
                                        exp_landmarks, dmask,
                                        coords, scoords,
                                        dx, dy, chem, ions, rsc)

    ###uncomment for partial maximisation wrt specific variables
    ###plots generated in current directory in 'var' folder
    ICF_plot(params, MI_vals, "var/ICF_fine.png")
    rot_plot(params, MI_vals, "var/rot_fine.png")
    tr_plot(params, MI_vals, x_sweep, y_sweep, "var/tr_fine.png")

    ###uncomment for detector and model .vtk detector stacks
    ###plots generated in current directory in 'stack' folder
    plot_hit_stacks(params, max_ind, stability, projection, stability_ICF,
                    projection_ICF, coords, scoords, dx, dy, chem, ions, dmask,
                    det_dist, det_rad, nbins, model_landmarks, exp_landmarks, rsc)

    ###umcomment for model and detector .png hitmaps
    ###plots generated in current directory in 'hm folder
    visualise_hitmaps(params, max_ind, stability_ICF, projection_ICF, stability,
                      projection, coords, scoords, dx, dy, chem, ions, dmask, det_rad, nbins, det_dist,
                      model_landmarks, exp_landmarks, rsc)

    ICF_vol_calib = False
    if ICF_vol_calib == True:
        exp_landmarks_mono = exp_landmarks_monotomic(exp_landmarks, chem, ic)
        ICF_sim, ICF_sweep_vals = ICF_vol_sweeper(stability_ICF, stability, projection_ICF, projection,
                                                  ICF_range, det_rad, det_dist, nbins,
                                                  model_landmarks[1:], exp_landmarks_mono[1:], 20)

    return {"MI": MI_vals[max_ind], "ICF": params[max_ind][0], "tdx": params[max_ind][1],
            "tdy": params[max_ind][2], "theta": params[max_ind][3] * 180.0/np.pi}

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--projection_data', metavar='projection data', type = str, default = "",
                        help='.tsv containing projection data')
    parser.add_argument('--stability_data', metavar='stability data', type = str, default = "",
                        help='.tsv containing stability data')
    parser.add_argument('--det_radius', metavar='detector radius', type = float, default = 0.038,
                        help='The detector radius in metres')
    parser.add_argument('--flight_path', metavar='flight path', type = float, default = 0.102,
                        help='The instrument flight path in metres')
    parser.add_argument('--model_landmarks', metavar='simulated landmark points', type = str,
                        default = "B-layer5:enter,C-layer4:enter,E-layer3:enter,D-layer2:enter",
                        help='landmark points. Information on how to define this string '+
                             'can be found in the module docstring and in the Casra_3d wiki.')
    parser.add_argument('--exp_landmarks', metavar='precalculated experimental landmark points (ion number)',
                        type = str, default = "730000,1080000,1610000,2270000",
                        help='precalculated experimental landmark points, separated by ,')
    parser.add_argument('--apt_data', metavar='atom probe data', type = str, default = "",
                        help='atom probe data: either .epos, .ato, or .tsv')
    parser.add_argument('--rangefile', metavar='range file', type = str, default = "",
                        help='The range file for labelling ions within the reconstruction')
    parser.add_argument('--nbins', metavar='bin number', type = int, default = 100,
                        help='The detector distezation dimension for MI calculations (nbins x nbins)')

    args = parser.parse_args()

    #simulation parameter definition
    trajectory_data = args.projection_data
    stability_data = args.stability_data
    det_rad = args.det_radius
    det_dist = args.flight_path
    model_landmarks = args.model_landmarks.split(",")
    exp_landmarks = args.exp_landmarks.split(",")
    exp_landmarks = [int(a) for a in exp_landmarks]
    apt_data = args.apt_data
    rangefile = args.rangefile
    nbins = args.nbins

    ###default parameter values - uncomment
    #trajectory_data = "c3d_output/IMEC2/projection_data/projection_data.tsv"
    #stability_data = "c3d_output/IMEC2/projection_data/stability_data.tsv"

    #trajectory_data = "c3d_output/IMEC2/sweep0/projection_data/projection_data.tsv"
    #stability_data = "c3d_output/IMEC2/sweep0/projection_data/stability_data.tsv"
    #det_dist = 0.102
    #det_rad = 0.038
    #nbins =  100
    #apt_data = "c3d_input/IMEC2/D11_MT1_UV7500.ato"
    #rangefile = "c3d_input/IMEC2/imec_new.rrng"

    #model_landmarks ="B-layer5:enter,C-layer4:enter,E-layer3:enter,D-layer2:enter".split(",")
    #exp_landmarks = "730000,1080000,1610000,2270000".split(",")
    #exp_landmarks = [int(a) for a in exp_landmarks]

    optimise_image_transfer(trajectory_data, stability_data, det_dist, det_rad, apt_data,
                            rangefile, model_landmarks, exp_landmarks, nbins = nbins)
