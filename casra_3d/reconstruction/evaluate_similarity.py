"""

Submodule for calculating the similarity between the model geometry and reconstructed geometry
Two different metrics are evaluated - a simple distance metric and a metric based off mutual information

February 2020
Author: Charles Fletcher
Atom Probe Tomography Group, Department of Materials, University of oxford
"""

import os
import copy
import sqlite3

try:
    import numpy as np
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python module Numpy. Please make sure it is installed.")

try:
    from scipy.ndimage import gaussian_filter
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python module Scipy. Please make sure it is installed.")

try:
    import sklearn.neighbors
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python module Scikit-learn. Please make sure it is installed.")

try:
    from casra_3d.io import model_loaders as model_loaders
    from casra_3d.io import model_writers as model_writers
    from casra_3d.ls_operators import sdf as sdf
    from casra_3d.image_processing import interp as interp

except ModuleNotFoundError:
    raise ModuleNotFoundError("failed to import casra_3d modules. Make sure you are calling casra as a module with the -m argument.")


def mutual_information(hgram):
    """

    Function calculates mutual information for a np joint histogram

    :param1 hgram: The numpy joint histogram
    :returns:
        :MI: The Mutual Information

    """
    # Convert bins counts to probability values
    pxy = hgram / float(np.sum(hgram))
    px = np.sum(pxy, axis=1) # marginal for x over y
    py = np.sum(pxy, axis=0) # marginal for y over x
    px_py = px[:, None] * py[None, :] # Broadcast to multiply marginals
    # Now we can do the calculation using the pxy, px_py 2D arrays
    nzs = pxy > 0 # Only non-zero pxy values contribute to the sum
    return np.sum(pxy[nzs] * np.log(pxy[nzs] / px_py[nzs]))

###function for calculating composition differences
#must perform difference between the same species
#returned array is ordered by model elements - given in array cv
def dist_image_difference(model_el_map, recon_el_map, cv, els, uchem):

    """

    Function for calculating a simple difference metric between two composition grids

    :param1 model_el_map: 3D array of local composition estimates from the model
    :param2 recon_el_map: 3D array of local composition estimates from the reconstruction (calculated via KDE)
    :returns:
        :diff: 3D array of absolute difference values
        :total_sum_recon: 3D array of local reconstruction density

    """

    diff = np.zeros((model_el_map.shape[0], model_el_map.shape[1], model_el_map.shape[2], model_el_map.shape[3]))
    tot_sum_recon = np.sum(recon_el_map, axis = 0)
    tot_sum_mod = np.sum(model_el_map, axis = 0)

    for a in range(model_el_map.shape[0]):
        for b in range(recon_el_map.shape[0]):
            if str(cv[a]) == str(els[int(uchem[b])]):
                print("model: " + str(cv[a]))
                print("experiment: " + str(els[int(uchem[b])]))
                diff[a] = abs(model_el_map[a, :, :, :] - recon_el_map[b, :, :, :]) * (tot_sum_recon > 1e-5)
    return diff, tot_sum_recon

#function for calculating the mutual information between the model and reconsruction
def MI_image_difference(model_phase_map, recon_el_map, filt):

    """

    Function for calculating the mutual information

    :param1 model_el_map: 3D array of model phases (as phase ids)
    :param2 recon_el_map: 3D array of local composition estimates from the reconstruction (calculated via KDE)
    :returns:
        :val: The Mutual Information value


    """

    val = 0.0
    for a in range(0, len(recon_el_map)):
        joint_histogram, x_edges, y_edges = np.histogram2d(recon_el_map[a][filt],
                                                           model_phase_map[filt],
                                                           bins=[50, np.unique(model_phase_map)])
        val += mutual_information(joint_histogram)
    return val


def similarity(filename, recon_file, out_dir, materials_db, bw, bc,
               microstructure_compositions, detector_efficiency, scale = 3.5e-8):

    """

    Function for calculating the similarity between the Model and reconstruction.
    This similarity is calculated both
    by a simple Hausdorff difference and Mutual information.

    :param1 filename (str): The input .mstate model geometry filename
    :param2 recon_file (str): The reconstruction .vtk filename
    :param3 out_dir (str): The output directory
    :param4 materials_db (str): The materials database
    :param5 bw (float): The KDE Kernel bandwidth
    :param6 bc (int): The number of bins per dimension
    :param7 microstructure_compositions (list): Estimates for the phase
            microstructure (required for the sum of squared differences metric)
            example:
            'bulk:0.33-Si,0.66-O;phase1:1-Si;phase2:0.5-Si,0.5-Ge;phase3:1-Ge;phase4:1-Si'
            leave blank to default to using just the Mutual Information metric
    :returns:
        :squared_differences: unsummed squared difference value (local grid)
        :mutual_information_val: The mutual information metric value
        :error code: 0 implies success, 1 otherwise

    """

    microstructure = microstructure_compositions.split(";")
    uniq_elements = []

    micro = {}

    #connect to the materials database
    try:
        conn = sqlite3.connect(materials_db)
    except:
        print("Could not connect to the materials database. " +
              "Please make sure the correct location has been inputed.")
        print("Attempted location: " + materials_db)
        #exit with error code
        return 0.0, 0.0, 1

    #obtain element ids from db
    c = conn.cursor()
    el_ids = c.execute('SELECT element_id, element FROM element_ids').fetchall()
    ids = np.array([a[0] for a in el_ids])
    els = np.array([a[1] for a in el_ids], dtype = 'object')
    ids = ids[np.sort(ids)]

    #process compositions
    try:
        for a in range(0, len(microstructure)):
            micro_split = microstructure[a].split(":")
            micro_name = micro_split[0]
            composition = micro_split[1]
            elements = composition.split(",")
            micro[micro_name] = {}
            micro[micro_name]["comp"] = {}
            for b in range(len(elements)):
                sel = elements[b].split("-")
                micro[micro_name]["comp"][sel[1]] = float(sel[0])
                uniq_elements.append(sel[1])
    except:
        print("Could not read in phase compositions. " +
              "Please make sure different phase compositions are separated by ;")
        print("passed compositions: " + microstructure_compositions)
        return 0.0, 0.0, 1

    #this is the order of element compositions for overlap calculation arrays
    cv = list(np.unique(uniq_elements))

    #find unique elements present in model and order by database id
    mod_el = []
    for a in range(len(cv)):
        mod_el.append(ids[els == cv[a]][0])
    mod_el = np.array(mod_el)

    #import model driven reconstruction
    try:
        mesh = model_loaders.read_vtk(recon_file)
        atoms = np.array(mesh.points)
    except:
        print("Cannot read mesh: " + recon_file)
        return 0.0, 0.0, 1

    #elements of uchem correspond to ids in materials db
    uchem = np.unique(mesh.point_data["chemistry"])

    minx = np.min(atoms[:, 0])/1e-9
    maxx = np.max(atoms[:, 0])/1e-9
    miny = np.min(atoms[:, 1])/1e-9
    maxy = np.max(atoms[:, 1])/1e-9
    minz = np.min(atoms[:, 2])/1e-9
    maxz = np.max(atoms[:, 2])/1e-9

    ############model geometry section#######################
    print("Calculate model composition")
    model_geom = model_loaders.load_mstate(filename)

    ind = 0
    name = ""
    for ob in model_geom["objts"]:
        if ob["surface"] == 1:
            name = ob["name"]
            break
        ind += 1

    for key in micro:
        for ob in model_geom["objts"]:
            if ob["name"] == key:
                micro[key]["verts"] = ob["verts"]
                micro[key]["faces"] = ob["faces"]
                micro[key]["surface"] = ob["surface"]

    #construct grid for KDE of compositions
    x = np.linspace(minx, maxx, bc)
    y = np.linspace(miny, maxy, bc)
    z = np.linspace(minz, maxz, bc)

    XX, XY, XZ = np.meshgrid(x, y, z)
    compositions = np.zeros((len(cv), bc, bc, bc))
    outside = np.zeros((bc, bc, bc))

    #initialise 3D array of local phase type (corresponding to phase ids from materials_db)
    model_phase_map = np.zeros((bc, bc, bc))

    ph = 0.0
    for key in micro:
        if micro[key]["surface"] == 1:
            verts = np.ascontiguousarray(micro[key]["verts"] * 1e9 * scale)
            faces = np.ascontiguousarray(micro[key]["faces"])
            px = np.ascontiguousarray(x)
            py = np.ascontiguousarray(y)
            pz = np.ascontiguousarray(z)
            span = sdf.pointsinside(px, py, pz, verts, faces, len(faces))
            outside = (1.0 - copy.deepcopy(span))
            for el in micro[key]["comp"]:
                #micro[key]["comp"][el] - composition value
                #el - dictionary key or element type (label)
                model_phase_map = ph * span
                compositions[cv.index(el), :, :, :] = copy.deepcopy(span) * micro[key]["comp"][el]
        ph += 1.0

    ph = 0.0
    for key in micro:
        if micro[key]["surface"] == 0:
            verts = np.ascontiguousarray(micro[key]["verts"] * 1e9 * scale)
            faces = np.ascontiguousarray(micro[key]["faces"])
            px = np.ascontiguousarray(x)
            py = np.ascontiguousarray(y)
            pz = np.ascontiguousarray(z)
            span = sdf.pointsinside(px, py, pz, verts, faces, len(faces))
            for a in range(compositions.shape[0]):
                compositions[a, :, :, :] = compositions[a, :, :, :]*(1.0 - span)
                model_phase_map = model_phase_map * (1.0 - span)
            for el in micro[key]["comp"]:
                compositions[cv.index(el), :, :, :] += copy.deepcopy(span) * micro[key]["comp"][el]
                model_phase_map += ph * span
        ph += 1

    #smooth the compositions - gaussian filter approximately kernel smoothing
    smooth_compositions = copy.deepcopy(compositions)
    for a in range(smooth_compositions.shape[0]):
        compositions[a, :, :, :][outside == 1] = 0
        smooth_compositions[a, :, :, :] = gaussian_filter(compositions[a, :, :, :], sigma = 5)
        smooth_compositions[a, :, :, :][outside == 1] = 0

    compositions = np.transpose(compositions, axes = (0, 2, 1, 3))
    smooth_compositions = np.transpose(smooth_compositions, axes = (0, 2, 1, 3))
    model_phase_map = np.transpose(model_phase_map, axes = (1, 0, 2))

    ###output simulated sample space
    #coords = np.vstack((XX.flatten()*1e-9, XY.flatten()*1e-9, XZ.flatten()*1e-9)).T
    #point_ind = np.linspace(0, len(coords), len(coords), endpoint = False).astype(int)
    #cells = point_ind.reshape(-1, 1)
    #model_writers.write_vtk("simulated_comp.vtk", coords, cells,
    #                        point_data = {"phase": model_phase_map.flatten()})


    ############experimental data section#######################
    print("calculate experimental compositions")

    x = np.linspace(minx, maxx, bc)
    y = np.linspace(miny, maxy, bc)
    z = np.linspace(minz, maxz, bc)
    XX, XY, XZ = np.meshgrid(x, y, z)

    chem = np.zeros((len(uchem), bc, bc, bc))

    print("estimate reconstruction compositions")
    for uc in range(len(uchem)):
        catoms = atoms[np.array(mesh.point_data["chemistry"]).flatten() == uchem[uc]]
        kde = sklearn.neighbors.KernelDensity(kernel="epanechnikov",
                                              bandwidth= bw).fit(catoms/1e-9)
        print("score atoms: chem - " + str(uc))
        ndv = kde.score_samples(np.vstack((XX.flatten(),
                                           XY.flatten(),
                                           XZ.flatten())).T)
        ndv = ndv.reshape((bc, bc, bc))
        ndv = np.exp(ndv) * len(catoms)
        ndv = np.transpose(ndv, axes = (0, 1, 2))
        chem[uc, :, :, :] = ndv
        print("fitted")

    ecompositions = chem/(np.sum(chem, axis = 0) + 1e-10)

    ecompositions[:, np.sum(chem, axis = 0) < 1e-10 * len(atoms)] = 0.0

    #ensure in demo experimental and model compositions are the same length
    #ecompositions = ecompositions[:len(compositions), :, :, :]

    recon_compositions = {}
    recon_compositions["density"] = np.sum(chem, axis = 0).flatten()/detector_efficiency

    #add new KDE composition properties - matching element ids to labels via database
    #for uc in range(len(uchem)):
    #    recon_compositions[els[int(uchem[uc])]] = ecompositions[uc, :, :, :].flatten()

    for uc in range(len(uchem)):
        recon_compositions[str(uc)] = ecompositions[uc, :, :, :].flatten()

    ###output simulated detector space
    #coords = np.vstack((XX.flatten()*1e-9, XY.flatten()*1e-9, XZ.flatten()*1e-9)).T
    #point_ind = np.linspace(0, len(coords), len(coords), endpoint = False).astype(int)
    #cells = point_ind.reshape(-1, 1)
    #model_writers.write_vtk("experimental_comp.vtk", coords,
    #                        cells, point_data = recon_compositions)

    #calculate local element difference map between experiment and simulation - euclidean metric
    #diff, tot_sum_recon = dist_image_difference(smooth_compositions, ecompositions, cv, els, uchem)

    tot_sum_recon = np.sum(ecompositions, axis = 0)

    #calculate MI based phase-composition similarity measurement
    mutual_information_val = MI_image_difference(model_phase_map, ecompositions, tot_sum_recon > 1e-5)
    diff = mutual_information_val
    distance_to_minimise = np.sum(diff)/np.sum(tot_sum_recon > 1e-5)

    ###output difference vector (result from similarity distance metric) to 3D point mesh
    #coords = np.vstack((XX.flatten(), XY.flatten(), XZ.flatten())).T
    #point_ind = np.linspace(0, len(coords), len(coords), endpoint = False).astype(int)
    #cells = {"vertex": point_ind.reshape(-1, 1)}
    #meshio.write_points_cells("difference1.vtk", coords, cells, point_data = {"si": diff[0].flatten(), "o": diff[1].flatten(), "Ge": diff[2].flatten()})

    ##remove edge voxels from density histogram
    binnedndv, bin_edges2 = np.histogramdd(atoms/1e-9, bins = (bc, bc, bc))
    binnedndv = binnedndv.astype("long")
    ndv = np.sum(chem, axis = 0)/detector_efficiency

    ndv, binnedndv = interp.erode_recon(ndv, binnedndv, bc, bc, bc, -1000.0)
    ndv, binnedndv = interp.erode_recon(ndv, binnedndv, bc, bc, bc, -1000.0)
    ndv, binnedndv = interp.erode_recon(ndv, binnedndv, bc, bc, bc, -1000.0)
    ndvf = ndv.flatten()[ndv.flatten() > -500.0]
    binnedndvf = binnedndv.flatten()[binnedndv.flatten() != 0]

    #calculate normalised standard deviation for reconstruction density metric
    hist, bin_edges = np.histogram(ndvf, bins = 200)
    bin_centres = (bin_edges[1:] + bin_edges[:-1])/2.0
    total = np.sum(hist[1:])
    mean = (1.0/total) * np.sum(hist[1:] * bin_centres[1:])
    spread = (1.0/total) * np.sum(hist[1:] * (bin_centres[1:] - mean)**2.0)
    spread = (spread)**0.5
    nsd = spread/mean

    hist, bin_edges = np.histogram(binnedndvf, bins = 200)
    bin_centres = (bin_edges[1:] + bin_edges[:-1])/2.0
    total = np.sum(hist[1:])
    mean = (1.0/total) * np.sum(hist[1:] * bin_centres[1:])
    spread = (1.0/total) * np.sum(hist[1:] * (bin_centres[1:] - mean)**2.0)
    spread = (spread)**0.5
    bnsd = spread/mean

    ###output this value to the optimiser csv
    if len(out_dir) > 0:
        evaluation_filename = out_dir + "/evaluation.tsv"

        if not os.path.exists(out_dir):
            os.makedirs(out_dir)

        #write distance outputs to evaluration_filename
        f = open(evaluation_filename, "w+")
        f.write("distance \t " + str(distance_to_minimise))
        f.close()

    return distance_to_minimise, mutual_information_val, spread, nsd, bnsd, 0

if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--tip_geom', metavar='tip geometry', type = str, default = "",
                        help='The .mstate file directory defining the tip geometry')
    parser.add_argument('--out_dir', metavar='output directory', type = str, default = "",
                        help='The output directory in which to write the similarity metric results. If string is zero length then result is not outputed.')
    parser.add_argument('--recon', metavar='reconstruction', type = str, default = "",
                        help='The model driven reconstruction')
    parser.add_argument('--microstructure_compositions', metavar='estimates for element compositions in microstructure', type = str, default = "",
                        help='The output directory in which to dump the simulation output files. Different phase compositions are separated by a semicolon (;). Different elements for a single phae are separated by a comma (,). Argument example: bulk:0.33-Si,0.66-O;phase1:1-Si;phase2:0.5-Si,0.5-Ge;phase3:1-Ge;phase4:1-Si')
    parser.add_argument('--bandwidth', metavar='The Kernel bandwidth', type = float, default = 1e-9,
                        help='The bandwidth of the kernel to use for density estimation')
    parser.add_argument('--grid_resolution', metavar='The grid width for sampling', type = int, default = 100,
                        help='The width in grid cells used for sampling the KDE')
    parser.add_argument('--detector_efficiency', metavar='The detector efficiency', type = float, default = 0.5,
                        help='The atom probe detector efficiency (value between 0 and 1). Required for accurate density/composition calculations.')
    parser.add_argument('--model_scale', metavar='The model scale', type = float, default = 3.5e-8,
                        help='The scale to use for the model')

    args = parser.parse_args()
    filename = args.tip_geom
    recon_file = args.recon
    out_dir = args.out
    bw = args.bandwidth
    bc = args.grid_resolution
    microstructure_compositions = args.microstructure_compositions
    detector_efficiency = args.detector_efficiency
    model_scale = args.model_scale


    if len(filename) == 0:
        raise ValueError("mstate file must be parsed to --tip_geom argument")
    if len(recon_file) == 0:
        raise ValueError("output reconstruction filname must be parsed to --recon argument")
    if len(out_dir) == 0:
        raise ValueError("output directory must be parsed to --out argument")

    similarity(filename, recon_file, out_dir, bw, bc, microstructure_compositions, detector_efficiency, model_scale)
