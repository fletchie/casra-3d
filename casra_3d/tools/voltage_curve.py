
"""

Submodule for outputing voltage curve from simulated data

Author: Charles Fletcher
Atom Probe Tomography Group, Department of Materials, University of Oxford
May 2020
"""

import os

from casra_3d.io import model_loaders as model_loaders

try:
    import numpy as np
except ModuleNotFoundError:
    raise ModuleNotFoundError("The python package numpy could not be found. " +
                              "Please make sure it is installed.")

try:
    import matplotlib.pyplot as plt
except ModuleNotFoundError:
    raise ModuleNotFoundError("The python package matplotlib could not be found. " +
                              "Please make sure it is installed.")

try:
    import scipy
    import scipy.ndimage
except ModuleNotFoundError:
    raise ModuleNotFoundError("The python package scipy could not be found. " +
                              "Please make sure it is installed.")

def generate_voltage_curve(dirname, output_directory):

    """

    Method for generating and plotting the voltage curve for simulated model data
    (stored in .vtks within `dirname`).

    :param1 dirname (str): The directory containing the .vtk model files
    :param2 output_directory (str): The output directory in which to save the
                                    voltage curve plot

    :returns:
        None

    :raises:
        None

    """

    surface = {"verts": [], "faces": [], "cp": [], "phase": [], "areas": [],
               "normals": [], "pot": [], "flux": [], "smflux": [], "time": [],
               "sample_surface": [], "smoothed_velocity": [], "volume": [],
               "voltage": []}

    model_loaders.load_simulation(dirname, surface)

    pvol = surface["volume"][0]
    vols = []
    stable_voltage = []

    for v in range(1, len(surface["volume"])):
        print(abs(surface["volume"][v] - pvol) * 1e27)
        if abs(surface["volume"][v] - pvol) * 1e27 < 0.05:
            del vols[-1]
            del stable_voltage[-1]
            pvol = surface["volume"][v]

        else:
            vols.append(surface["volume"][v])
            stable_voltage.append(surface["voltage"][v])
            pvol = surface["volume"][v]

    svoltage = scipy.ndimage.maximum_filter(stable_voltage, 3)

    print("saving raw voltage curve to: " + output_directory + "/voltage_curve.png")

    #create output directory for voltage curves if it doesn't yet exist
    if not os.path.exists(output_directory):
        print("creating directory: " + output_directory)
        os.makedirs(output_directory)

    #output raw voltage plot
    plt.plot(vols, stable_voltage)
    plt.xlim(max(vols), min(vols))
    plt.ylim(0, max(stable_voltage))
    plt.xlabel("Evaporated volume (nm^-3)")
    plt.ylabel("raw voltage (V)")
    plt.savefig(output_directory + "/raw_voltage_curve.png")
    plt.close()

    plt.plot(np.array(surface["volume"]))
    plt.xlabel("iteration count")
    plt.ylabel("volume evaporated (nm^-3)")
    plt.savefig(output_directory + "/model_vol.png")
    plt.close()

    print("saving smoothed voltage curve to: " + output_directory + "/smoothed_voltage_curve.png")

    #output smoothed voltage plot
    plt.plot(vols, svoltage)
    plt.xlim(max(vols), min(vols))
    plt.ylim(0, max(svoltage))
    plt.xlabel("Evaporated volume (nm^-3)")
    plt.ylabel("smoothed voltage (V)")
    plt.savefig(output_directory + "/smoothed_voltage_curve.png")
    plt.close()

    print("Voltage curve output Completed")

if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--dirname', metavar='dirname', type=str, default='',
                        help='The directory containing the .vtk simulated evaporation data.')

    parser.add_argument('--output', metavar='output', type=str, default='output',
                        help='The output directory.')

    args = parser.parse_args()
    dirname_cl = args.dirname
    output_directory_cl = args.output

    generate_voltage_curve(dirname_cl, output_directory_cl)
