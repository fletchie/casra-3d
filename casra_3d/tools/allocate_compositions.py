"""

Module for randomly allocating ionic compositions to simulated phases
within reconstructions. Enables direct comparison with experimental data.

Author: Charles Fletcher
Atom Probe Tomography Group, Department of Materials, University of Oxford
August 2020

"""

import copy
import json

from casra_3d.io import model_loaders as model_loaders
from casra_3d.io import model_writers as model_writers
from casra_3d.io import format_converter as format_converter

try:
    import numpy as np
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python package numpy. " +
                              "Please make sure package is installed.")

def allocate_compositions(input_recon, comps, output_recon, rangefile):

    """

    Function randomly allocates particular ionic species to element_ids
    based off input compositions for element_ids

    :param1 input_recon (str): The input .vtk reconstruction filename
    :param2 comps (dict of dicts): dict of compositions for element_ids
    :param3 output_recon (str): The output .pos reconstruction filename
    :param4 rangefile (str): The output .rrng filename

    :returns:
        None

    :raises:
        None

    """

    mesh = model_loaders.read_vtk(input_recon)

    chem = np.array(mesh.point_data["chemistry"])
    nchem = copy.deepcopy(chem).astype("str")

    for key,values in comps.items():
        f = chem == float(key)
        ckey = []
        cvalues = []
        cv = 0.0
        for key2, values2 in values.items():
            ckey.append(key2)
            cv += values2
            cvalues.append(cv)

        cvalues = np.array(cvalues)
        cvalues = cvalues/cvalues[-1]
        ac = np.random.rand(len(chem))

        for a in range(len(ckey)):
            b = len(ckey) - a - 1
            nchem[(ac < cvalues[b]) * f] = ckey[b]

    uchem = np.unique(nchem)

    el_dictionary = {}
    for a in range(len(uchem)):
        v = a + 1
        el_dictionary[str(v)] = uchem[a]
        nchem[nchem == uchem[a]] = v

    mesh.point_data = {"chemistry": nchem}
    materials_db = ""

    nchem = nchem.astype("float")

    format_converter.convert_vtk_pos("", output_recon, rangefile,
                                     materials_db, el_dictionary=el_dictionary,
                                     simulated=True, mesh=mesh)

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--input_recon', metavar='input recon', type=str,
                        default="",
                        help='The input .vtk reconstruction filename')
    parser.add_argument('--comps', metavar='comps', type=str, default="",
                        help='The compositions e.g.' +
                        '{"22.0":{"W":1.0},' +
                        '"23.0": {"Re": 1.0}}')
    parser.add_argument('--output_recon', metavar='output recon', type=str,
                        default="The output .pos reconstruction filename",
                        help='The output reconstruction file')
    parser.add_argument('--rangefile', metavar='The range file', type=str,
                        default="",
                        help='The output .rrng filename')
    args = parser.parse_args()

    #parameter definition
    input_recon = args.input_recon
    comps = args.comps
    output_recon = args.output_recon
    rangefile = args.rangefile

    EMPTY = ""

    if input_recon is EMPTY:
        raise ValueError("input_recon file must be parsed to --tip_geom argument")
    if comps is EMPTY:
        raise ValueError("comps must be parsed to --out argument")
    if output_recon is EMPTY:
        raise ValueError("output_recon file must be parsed to --parameters argument")
    if rangefile is EMPTY:
        raise ValueError("rangefile file must be parsed to --mref argument")

    comps = json.loads(comps)

    allocate_compositions(input_recon, comps, output_recon, rangefile)
