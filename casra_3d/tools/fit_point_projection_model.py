
"""

Module for fitting point projection geometrical assumptions to a sample geometry (as .vtk)

Author: Charles Fletcher
Atom Probe Tomography Group, Department of Materials, University of Oxford
May 2020
"""

import math
import sys

try:
    import numpy as np
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python package numpy. Please make sure package is installed.")

"""

Module containing functions for least squares fitting of a point projection model to a mesh

Author: Charles Fletcher
Atom Probe Tomography Group, Department of Materials, University of Oxford
May 2020

"""

try:
    import scipy.spatial
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python package scipy. Please make sure package is installed.")

def sphereFit(spX,spY,spZ):

    """

    Function for fitting sphere to set of 3D points via least squares

    :param1 spX: x coordinates
    :param2 spY: y coordinates
    :param3 spZ: z coordinates

    :returns:
        :radius: circle radius
        :C[0]: circle x coord centre
        :C[1]: circle y coord centre
        :C[2]: circle z coord centre

    """

    spX = np.array(spX)
    spY = np.array(spY)
    spZ = np.array(spZ)
    A = np.zeros((len(spX),4))
    A[:,0] = spX*2
    A[:,1] = spY*2
    A[:,2] = spZ*2
    A[:,3] = 1

    f = np.zeros((len(spX),1))
    f[:,0] = (spX*spX) + (spY*spY) + (spZ*spZ)
    C, residules, rank, singval = np.linalg.lstsq(A,f)

    t = (C[0]*C[0])+(C[1]*C[1])+(C[2]*C[2])+C[3]
    radius = math.sqrt(t)

    return radius, C[0], C[1], C[2]

def circleFit(spX,spY):

    """

    Function for fitting circle to set of (projected) 2D points via least squares

    :param1 spX: x coordinates
    :param2 spY: y coordinates

    :returns:
        :radius: circle radius
        :C[0]: circle x coord centre
        :C[1]: circle y coord centre

    """

    spX = np.array(spX)
    spY = np.array(spY)
    A = np.zeros((len(spX),3))
    A[:,0] = spX*2
    A[:,1] = spY*2
    A[:,2] = 1

    f = np.zeros((len(spX),1))
    f[:,0] = (spX*spX) + (spY*spY)
    C, residules, rank, singval = np.linalg.lstsq(A,f)

    t = (C[0]*C[0])+(C[1]*C[1])+C[2]
    radius = math.sqrt(t)

    return radius, C[0], C[1]

def shank_fit(verts, faces, apex_radius, shank_frac):

    """

    Function for calculating the shank mesh and angle
    Apex radius must have been previously calculated

    :param1 verts: 2D array of sample mesh vertices
    :param2 faces: 2D sample mesh connectivity matrix
    :param3 apex_radius: Previously calculated apex radius (`apex_radius_fit`)
    :param4 shank_frac: fraction of sample used to estimate shank geometry

    :returns:
        :spoints: 2D array of shank geometry mesh points
        :sfaces: 2D  connectivity matrix for shank geometry mesh

    """

    zmax = max(verts[:, 2])

    #calculate panel centres
    cp = np.mean(verts[faces], axis = 1)

    #calculate coordinates for estimating top and bottom circle defining the shank geometry
    mid = np.max(cp[:, 2]) * (1.0 - shank_frac) + np.min(cp[:, 2]) * shank_frac
    top_circle = cp[(cp[:, 2] > (zmax - apex_radius - apex_radius * 0.1)) * (cp[:, 2] < (zmax - apex_radius + apex_radius * 0.1))]
    bottom_circle = cp[(cp[:, 2] > (mid - apex_radius * 0.1)) * (cp[:, 2] < mid + apex_radius * 0.1)]

    #fit circle to sample geometry by least squares near top and bottom of sample
    top_rad, cxt, cyt = circleFit(top_circle[:, 0], top_circle[:, 1])
    bottom_rad, cxb, cyb = circleFit(bottom_circle[:, 0], bottom_circle[:, 1])
    Lz = max(top_circle[:, 2]) - min(bottom_circle[:, 2])

    #fit circles to top and bottom of shank to derive shank angle
    ang = np.arctan2(top_rad - bottom_rad, Lz)
    print("estimated shank angle: " + str(abs(ang)) + " (rad)")

    #output points defining the shank convex hull
    spoints = []
    for theta in np.linspace(0.0, 2.0 * np.pi, endpoint = False):

        x = top_rad * np.cos(theta) + cxt[0]
        y = top_rad * np.sin(theta) + cyt[0]
        z = np.mean(top_circle[:, 2])
        spoints.append([x, y, z])

    for theta in np.linspace(0.0, 2.0 * np.pi, endpoint = False):
        x = bottom_rad * np.cos(theta) + cxb[0]
        y = bottom_rad * np.sin(theta) + cyb[0]
        z = np.mean(bottom_circle[:, 2])
        spoints.append([x, y, z])

    #fit convex hull to top and bottom circles to construct shank geometry
    spoints = np.array(spoints)
    chull = scipy.spatial.ConvexHull(spoints)
    sfaces = chull.simplices

    return spoints, sfaces
