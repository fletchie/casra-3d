#!/bin/bash

start=$(date +'%s')
echo "Run multilayer model pipeline"

if [ ! -d casra_3d ] ; then
	echo " please run script from top level folder of casra repository: could not find casra_3d in current dir."
	exit 1
fi

python3 c3d_input/examples/multilayer/generate_multilayer.py 
python3 -m casra_3d.tip_simulation --mat_db c3d_input/materials --tip_geom c3d_input/examples/multilayer/multilayer.mstate --mref c3d_input/examples/multilayer/multilayer.mref --parameters c3d_input/examples/multilayer/parameters --it 10 --out c3d_output/examples/multilayer
#python3 -m casra_3d.perform_projection --tip_geom c3d_input/examples/multilayer/multilayer.mstate --dirname c3d_output/examples/multilayer --mref c3d_input/examples/multilayer/multilayer.mref --it 250 --parameters c3d_input/examples/multilayer/parameters --parallelisation True --out c3d_output/examples/multilayer
#python3 -m casra_3d.tools.reconstruction_mapping --mapping_data c3d_output/examples/multilayer/projection_data/projection_data.tsv --det_radius 0.038 --dirname c3d_output/examples/multilayer/reconstruction
finish=$(date +'%s')
duration=$((finish-start))
hrs=$(( duration/3600 )); mins=$(( (duration-hrs*3600)/60)); secs=$(( duration-hrs*3600-mins*60 ))
printf 'Time spent: %02d:%02d:%02d\n' $hrs $mins $secs

