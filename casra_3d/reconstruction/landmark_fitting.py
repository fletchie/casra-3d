
"""

Submodule for generating the calibration files required for reconstruction

In this module the model landmarks are passed into the The string model_landmarks defines the type of landmark. It is defined as the following structure:

    (A) fixed landmark: `landmark_name-frame_number`
    (B) dynamic landmark: `landmark_name-model_phase_name:enter or `landmark_name-model_phase_name:exit`

    When calling the function `landmarks` directly, the model_landmarks are then passed in as mlandmarks = [landmark1, landmark2, landmark3,...]
    When calling the function `landmarks` via the command line, the model_landmarks are then passed in as the string 'landmark1,landmark2,landmark3,...'
    where landmarks are either fixed (A) or dynamic (B)

Author: Charles Fletcher
Atom Probe Tomography Group, Department of Materials, University of Oxford
May 2020

"""

import os
import csv
import sys
import copy

try:
    import numpy as np
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Package numpy. Please make sure it is installed.")

try:
    import scipy as sp
    import scipy.spatial
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Package scipy. Please make sure it is installed.")

from casra_3d.io import model_loaders as model_loaders
from casra_3d.mesh.cgal import cgal_linker as cgal_linker
from casra_3d.io import model_writers as model_writers
from casra_3d.core import ion_projection as ion_projection

def landmark(projection, det_radius, dirname, model_landmarks, exp_landmarks,
             landmark_calibration_file, projection_calibration_file,
             image_transfer = [0.102, 0.0, 0.0, 0.0, 0.75], alpha = 4e-9):

    """

    Outputs the landmark configuration file and projection configuration file required for reconstruction

    The string model_landmarks defines the type of landmark. It is defined as the following structure:

        (A) fixed landmark: `landmark_name-frame_number`
        (B) dynamic landmark: `landmark_name-model_phase_name:enter or `landmark_name-model_phase_name:exit`

        the model_landmarks are then passed in as the list mlandmarks = [landmark1, landmark2, landmark3;...]
        where landmarks are either fixed (A) or dynamic (B)

    :param1 projection: The ion projection data dictionary
    :param2 det_radius: The detector radius
    :param3 dirname: The directory where the simulated evaporation data (.vtk) is located.
    :param4 model_landmarks: The input list defining the model landmarks for constraining reconstruction. Information on the list element format can be found above in the function docstring and the Casra_3d wiki.
    :param5 exp_landmarks: List of experimental landmarks (detected event indexes) corresponding to the model landmarks parsed in param4.
    :param6 landmark_calibration_file: The output landmark calibration filename (required for performing model driven reconstruction)
    :param7 projection_calibration_file: The output projection calibration filename (required for performing model driven reconstruction)
    :param8: image_transfer (list): List of image transfer parameters.
                                    This variable should have the following structure (list)
                                    [flight path, dx-transform, dy-transform, theta, ICF]
    :param8 alpha: The alpha parameter for generating the alpha shape. The default value works well for APT sized samples. For more info see https://doc.cgal.org/latest/Alpha_shapes_3/index.html

    :returns: 0

    """
    #if input is string, treat as trajectory data filename
    if type(projection) is str:
        mapping_data = projection
        projection = {"num": [], "sample_space": [], "detector_space": [], "vel": [],
                      "chemistry": [], "ion_phase": [], "trajectories": [], "panel_index": []}
        model_loaders.load_tsv_ion_trajectories(projection, mapping_data)
        #print(np.unique(projection["num"]))

    if len(image_transfer) != 5:
        raise ValueError("input parameter 'image_transfer' has the wrong length. " +
                         "\n This variable should have the following structure (list)" +
                         "\n [flight path, dx-transform, dy-transform, theta, ICF]")

    det_dist = float(image_transfer[0])
    ts = np.array([float(image_transfer[1]), float(image_transfer[2])])
    theta = float(image_transfer[3]) * np.pi/180.0
    ICF = float(image_transfer[4])
    
    #perform detector transfer
    projection["detector_space"] = ion_projection.transfer_function(projection["detector_space"], projection["vel"], det_dist, kappa=ICF)

    coords = projection["detector_space"]

    #rotate trajectory mapping - inverse rigid body transformation
    fl = copy.deepcopy(coords[:, 0])
    coords[:, 0] = coords[:, 1]
    coords[:, 1] = fl

    R = np.array([[np.cos(theta), -np.sin(theta)], [np.sin(theta), np.cos(theta)]]).T
    coords[:, :2] = np.matmul(R, (coords[:, :2]).T).T
    inv_ts = np.matmul(R, ts)

    coords[:, :2] = coords[:, :2] - inv_ts

    fl = copy.deepcopy(coords[:, 0])
    coords[:, 0] = coords[:, 1]
    coords[:, 1] = fl

    projection["detector_space"][:, :2] = coords[:, :2]
    ###########

    #load model evaporation
    #surface = {"verts": [], "faces": [], "cp": [], "phase": [], "areas": [], "normals": [], "pot": [],
    #           "flux": [], "smflux": [], "time": [], "sample_surface": [], "smoothed_velocity": [],
    #           "volume": []}

    #model_loaders.load_simulation(dirname, surface)

    #identify model frames where evaporation of phases is initiated or completed
    #desired landmarks are given in `model_landmarks`
    landmarks = determine_ion_frame(model_landmarks, projection, det_radius)

    #calculate evaporated volume between ion projections
    volume2 = calculate_projection_evaporation_volumes(projection, landmarks, det_radius, alpha)

    volume2.insert(0, 0)
    volume2 = np.cumsum(volume2)

    volume = []
    uniq = np.unique(projection["num"]).astype(int)

    for v in range(0, len(uniq)):
        #if v % (uniq[v+1] - uniq[v]) != 0:
        #    print("Manual user-defined landmark " + str(uniq[v]) + "has not been placed on an ion projection frame.")
        if np.sum(landmarks == uniq[v]) > 0:
            volume.append(volume2[v])

    det_rad = np.array([det_radius] * len(volume))

    #formatting landmark configuration file data
    print(volume)

    try:
        lm = np.vstack((np.array(landmarks).astype(int), np.array(exp_landmarks).astype(int), volume, det_rad)).T
    except ValueError:
        raise ValueError("landmark identification failed. Please make sure that the input phases are correctly named and in the correct sequential order (first-to-last encountered). \n If manually placing landmarks, then make sure the landmark is defined on a frame of the model where ions were projected!")

    directory = "/".join(landmark_calibration_file.split("/")[:-1])
    if not os.path.exists(directory):
        os.makedirs(directory)

    #output the landmark configuration file
    print("writing the landmark configuration file to path: " + landmark_calibration_file)
    f = open(landmark_calibration_file, "w+")
    fieldnames = ["simulation_landmarks", "experiment_landmarks", "volume", "detector_radius"]
    f.write("\t".join(fieldnames))
    f.write("\n")
    for a in range(0, len(lm)):
        line = lm[a, :].tolist()
        line = [str(b) for b in line]
        f.write("\t".join(line))
        if a != len(lm) - 1:
            f.write("\n")
    f.close()

    #formatting projection configuration file data
    lm = np.vstack((np.unique(projection["num"]).astype(int), volume2)).T

    directory = "/".join(projection_calibration_file.split("/")[:-1])
    if not os.path.exists(directory):
        os.makedirs(directory)

    #output the projection configuration file
    print("writing the projection configuration file to path: " + projection_calibration_file)
    f = open(projection_calibration_file, "w+")
    fieldnames = ["projection_frame", "volume"]
    f.write("\t".join(fieldnames))
    f.write("\n")
    for a in range(0, len(lm)):
        line = lm[a, :].tolist()
        line = [str(b) for b in line]
        f.write("\t".join(line))
        if a != len(lm) - 1:
            f.write("\n")
    f.close()

###function for converting the dynamic model landmarks (defined by entering/exiting phases) into list of model frames
def determine_ion_frame(mlandmarks, projection, det_rad):

    """

    Function for converting a string of fixed (model frames) and dynamic model landmarks (defined by entering/exiting phases)
    into a list of model frames

    The string mlandmarks defines the type of landmark. It is defined as the following structure:

        (A) fixed landmark: `landmark_name-frame_number`
        (B) dynamic landmark: `landmark_name-model_phase_name:enter or `landmark_name-model_phase_name:exit`

        the model_landmarks are then passed in as the list mlandmarks = [landmark1, landmark2, landmark3;...]
        where landmarks are either fixed (A) or dynamic (B)

        e.g. A-0;B-phase3:enter;C-phase2:enter;D-phase1:enter (fixed,dynamic,dynamic,dynamic)

    :param1 mlandmarks: The string defining the model landmarks
    :param2 projection: The list of projection simulated data
    :param3 det_rad: The detector radius

    :returns:
        :landmarks: the list of output simulated landmarks (simulation frame numbers)

    """

    #determine which landmarks are fixed and not fixed
    model_landmarks = {}
    for a in range(len(mlandmarks)):
        spl_landmark = mlandmarks[a].split("-")
        phase_name = spl_landmark[1].split(":")[0]
        spl_landmark2 = mlandmarks[a].split(":")
        if len(spl_landmark2) > 1:
            model_landmarks[str(a)] = {"name": spl_landmark[0], "fixed": False, "phase": phase_name, "type": spl_landmark2[1]}
        else:
            model_landmarks[str(a)] = {"name": spl_landmark[0], "fixed": True, "it": spl_landmark[1]}


    landmarks = []
    for lm in model_landmarks:
        if model_landmarks[str(lm)]["fixed"] == False:
            if model_landmarks[str(lm)]["type"] == "enter":
                for num in np.unique(projection["num"]):
                    det_space = projection["detector_space"][projection["num"] == num, :]
                    phase_type = np.array(projection["ion_phase"])[projection["num"] == num]
                    phase_type = phase_type[(abs(det_space[:, 0]) < 1e10) * (abs(det_space[:, 1]) < 1e10)]
                    det_space = det_space[(abs(det_space[:, 0]) < 1e10) * (abs(det_space[:, 1]) < 1e10)]
                    fdet = det_space[:, 0]**2.0 + det_space[:, 1]**2.0 < det_rad**2.0
                    filtered_phases = phase_type[fdet]
                    #print(filtered_phases)
                    #print(model_landmarks[str(lm)]["phase"])
                    #print(num)
                    if np.sum(np.unique(filtered_phases) == model_landmarks[str(lm)]["phase"]) > 0:
                        landmarks.append(int(num))
                        break

            elif model_landmarks[str(lm)]["type"] == "exit":
                enter = False
                for num in np.unique(projection["num"]):
                    det_space = projection["detector_space"][projection["num"] == num, :]
                    phase_type = np.array(projection["ion_phase"])[projection["num"] == num]
                    phase_type = phase_type[(abs(det_space[:, 0]) < 1e10) * (abs(det_space[:, 1]) < 1e10)]
                    det_space = det_space[(abs(det_space[:, 0]) < 1e10) * (abs(det_space[:, 1]) < 1e10)]
                    fdet = det_space[:, 0]**2.0 + det_space[:, 1]**2.0 < det_rad**2.0
                    filtered_phases = phase_type[fdet]
                    if np.sum(np.unique(filtered_phases) == model_landmarks[str(lm)]["phase"]) > 0:
                        enter =True
                    elif np.sum(np.unique(filtered_phases) == model_landmarks[str(lm)]["phase"]) == 0 and enter == True:
                        landmarks.append(int(num))
                        break

        elif model_landmarks[str(lm)]["fixed"] == True:
            landmarks.append(int(model_landmarks[str(lm)]["it"]))

    print("landmark placement complete")
    landmarks = np.array(landmarks)
    print("identified model frames: " + str(landmarks))

    #landmarks sorted in frame order (i.e. model time)
    return np.sort(landmarks)

def calculate_projection_evaporation_volumes(mapping, landmarks, FOV, alpha):

    """

    Calculates the FOV evaporated volume for each ion projection step

    :param1 self: The level-set space object instance
    :param2 sample_points: The detector coordinates at which to take the isolines
    :param3 demo_directory: The output demo_directory
    :returns:
        :volume: A list of evaporated volumes within the FOV between adjacent projections

    """

    volume = []
    uniq = np.unique(mapping["num"])
    
    for num in range(0, len(uniq) - 1):
        sample_space = mapping["sample_space"][mapping["num"] == uniq[num]]
        det_space = mapping["detector_space"][mapping["num"] == uniq[num]]

        #filter out instabilities
        sample_space = sample_space[(abs(det_space[:, 0]) < 1e10)*(abs(det_space[:, 1]) < 1e10)]
        det_space = det_space[(abs(det_space[:, 0]) < 1e10) * (abs(det_space[:, 1]) < 1e10)]
        
        point1 = []

        for i in range(0, len(det_space)):
            if (det_space[i, 0]**2.0 + det_space[i, 1]**2.0) < FOV**2.0:
                point1.append(sample_space[i])

        point1 = np.array(point1)

        sample_space = mapping["sample_space"][mapping["num"] == uniq[num + 1]]
        det_space = mapping["detector_space"][mapping["num"] == uniq[num + 1]]

        #filter out instabilities
        sample_space = sample_space[(abs(det_space[:, 0]) < 1e10)*(abs(det_space[:, 1]) < 1e10)]
        det_space = det_space[(abs(det_space[:, 0]) < 1e10) * (abs(det_space[:, 1]) < 1e10)]

        point2 = []

        #only include points within the Field-Of-View (FOV)
        for i in range(0, len(det_space)):
            if (det_space[i, 0]**2.0 + det_space[i, 1]**2.0) < FOV**2.0:
                point2.append(sample_space[i])

        point2 = np.array(point2)


        #transform the second projection trajectory points a set distance down the analysis axis to produce a more
        #spatially isotropic point cloud providing improved alpha shape behaviour
        ##This value is assigned to be the average nearest neighbour distance of the first projection
        dist_mat = scipy.spatial.distance_matrix(point2, point2, p = 2)
        np.fill_diagonal(dist_mat, np.inf)
        #determine point nearest neighbour distances
        nn_distances = np.min(dist_mat, axis = 0)
        #calculate average nearest neighbour distance
        ann_distance = np.mean(nn_distances) * 1.0
        #perform shift
        shift = ann_distance
        ##original implementation used a constant shift value of 3nm
        #shift = 3e-9
        point2[:, 2] -= ann_distance
        p1l = len(point1)

        points = np.vstack((point1, point2))
        points = points.astype(float)

        #cells = np.linspace(0, len(points), len(points), dtype = int, endpoint = False).reshape(-1, 1)
        #model_writers.write_vtk("vols/vol" + str(num) + ".vtk", points, cells = cells)
        #sys.exit()

        #call Cgal to generate the alpha shape
        points, simplices = cgal_linker.generate_alpha_shape(points, 3, alpha**2.0)

        #scale back trajectory points 3nm down to obtain true shape matching original data
        points[p1l:, 2] += ann_distance

        #calculate volume
        vol = 0.0
        for s in simplices:
            vol += abs(np.sum((points[s[0]] - points[s[3]]) * np.cross(points[s[1]] - points[s[3]], points[s[2]] - points[s[3]])))/6.0
        volume.append(vol)

    return volume

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--mapping_data', metavar='filename', type = str, default = "",
                        help='.tsv containing mapping data')
    parser.add_argument('--image_transfer', metavar='detector transfer function', type = str,
                        default = "0.102, 0.0, 0.0, 0.0, 0.8",
                        help='List of image transfer parameters. This variable should have the ' +
                             'following structure (list) [flight path, dx-transform, dy-transform, theta, ICF]')
    parser.add_argument('--det_radius', metavar='detector radius', type = float, default = 0.038,
                        help='The detector radius')
    parser.add_argument('--alpha_value', metavar='alpha value', type = float, default = 6e-9,
                        help='The alpha parameter for generating the alpha shape. The default value works well for APT sized samples. For more info see https://doc.cgal.org/latest/Alpha_shapes_3/index.html')
    parser.add_argument('--model_landmarks', metavar='simulated landmark points', type = str, default = "",
                        help='landmark points. Information on how to define this string can be found in the module docstring and in the Casra_3d wiki.')
    parser.add_argument('--exp_landmarks', metavar='precalculated experimental landmark points (ion number)', type = str, default = "",
                        help='precalculated experimental landmark points, separated by ,')
    parser.add_argument('--dirname', metavar='directory name', type = str, default = "",
                        help='The simulation data directory name')
    parser.add_argument('--landmark_calib_file', metavar='the landmark calibration file', type = str, default = "",
                        help='The output file name for the landmark calibration file (required input for the model driven reconstruction)')
    parser.add_argument('--projection_calib_file', metavar='the projection calibration file', type = str, default = "",
                        help='The output file name for the projection calibration file (required input for the model driven reconstruction)')
    args = parser.parse_args()

    #simulation parameter definition
    mapping_data = args.mapping_data
    det_radius = args.det_radius
    alpha = args.alpha_value
    dirname = args.dirname
    model_landmarks = args.model_landmarks.split(",")
    exp_landmarks = args.exp_landmarks.split(",")
    exp_landmarks = [int(a) for a in exp_landmarks]
    landmark_calibration_file = args.landmark_calib_file
    projection_calibration_file = args.projection_calib_file
    image_transfer = args.image_transfer.split(",")
    image_transfer = [float(a) for a in image_transfer]

    if len(mapping_data) == 0:
        raise ValueError("model mapping data tsv must be parsed to --mapping_data argument")
    if len(landmark_calibration_file) == 0:
        raise ValueError("output landmark calibration filename must be parsed to --landmark_calib_file argument")
    if len(projection_calibration_file) == 0:
        raise ValueError("output projection calibration filename must be parsed to --projection_calib_file argument")
    if len(dirname) == 0:
        raise ValueError("simulation directory must be parsed to --dirname argument")
    if len(model_landmarks) == 0:
        raise ValueError("model landmark string must be parsed to --model_landmarks argument. Structure should be like 'A-0,B-phase3:enter,C-phase2:enter,D-phase1:enter'. See function docstring for more info.")
    if len(exp_landmarks) == 0:
        raise ValueError("experimental landmark string must be parsed to --exp_landmarks argument. Structure should be like '1000,2000,3000,10000'. More info can be found in the function docstring.")

    projection = {"num": [], "sample_space": [], "detector_space": [], "vel": [], "chemistry": [], "ion_phase": [], "trajectories": [], "panel_index": []}
    model_loaders.load_tsv_ion_trajectories(projection, mapping_data)

    landmark(projection, det_radius, dirname, model_landmarks, exp_landmarks, landmark_calibration_file, projection_calibration_file, image_transfer, alpha)
