"""

Script for visualising the finFET reconstruction parameter sweep

Author: Charles Fletcher
Atom Probe Tomography Group, Department of Materials, University of Oxford
June 2021

"""

import numpy as np
import os
import matplotlib.pyplot as plt
import pandas
import scipy.interpolate
from mpl_toolkits.mplot3d import Axes3D
from scipy.interpolate import Rbf

output_dir = "c3d_output/sige_finfet/optimisation"

#create directory for outputting optimisation results
if not os.path.exists(output_dir):
    os.makedirs(output_dir, exist_ok=True)

filename = output_dir + "/optimisation_results.tsv"
phases = ["bulk", "phase2"]

df = pandas.read_csv(filename, sep = "\t", header = None)

nx = 6
ny = 6

f1 = df[0] == phases[0]
f2 = df[0] == phases[1]

vals = np.vstack((df[1][f1], df[1][f2], df[6][f1])).T
F00 = np.linspace(np.min(df[1][f1]), np.max(df[1][f1]), num = nx)
F01 = np.linspace(np.min(df[1][f2]), np.max(df[1][f2]), num = ny)
X, Y = np.meshgrid(F00, F01)

dx = (F00[1]/1e9-F00[0]/1e9)/2.
dy = (F01[1]/1e9-F01[0]/1e9)/2.
extent = [F00[0]/1e9-dx, F00[-1]/1e9+dx, F01[0]/1e9-dy, F01[-1]/1e9+dy]

fig1 = plt.figure()
ax1 = fig1.add_subplot(111)
ims = ax1.imshow(np.array(df[6][f1]).reshape(nx, ny), origin='lower', extent = extent)
cbar = plt.colorbar(ims)
cbar.ax.tick_params(labelsize=12)
cbar.set_label(label='Microstructual Similarity (MI)', size=16)
#ax1.set_xticklabels(np.round(F00/1e9, 2))
plt.xlabel("SiO2 evaporation field", fontsize = 16)
plt.ylabel("SiGe evaporation field", fontsize = 16)
plt.xticks(fontsize = 14)
plt.yticks(fontsize = 14)

plt.xlim(extent[0], extent[1])
plt.ylim(extent[2], extent[3])
plt.tight_layout()

plt.savefig("c3d_output/sige_finfet/optimisation/MI_similarity_sample_space.png", dpi = 300)
plt.close()

fig1 = plt.figure()
ax1 = fig1.add_subplot(111)
ims = ax1.imshow(np.array(df[7][f1]).reshape(nx, ny), origin='lower', extent = extent)
cbar = plt.colorbar(ims)
cbar.ax.tick_params(labelsize=12)
cbar.set_label(label='Microstructual Similarity (MI)', size=16)
#ax1.set_xticklabels(np.round(F00/1e9, 2))
plt.xlabel("SiO2 evaporation field", fontsize = 16)
plt.ylabel("SiGe evaporation field", fontsize = 16)
plt.xticks(fontsize = 14)
plt.yticks(fontsize = 14)

plt.xlim(extent[0], extent[1])
plt.ylim(extent[2], extent[3])
plt.tight_layout()

plt.savefig("c3d_output/sige_finfet/optimisation/MI_similarity_detector_space.png", dpi = 300)