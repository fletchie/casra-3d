"""

Module for evolving level set model e.g. simulating field evaporation

Authors: Charles Fletcher, Daniel Haley
Atom Probe Tomography Group, Department of Materials, University of Oxford
May 2020
"""


import os
import math
import sys
import tempfile
import numpy as np

from casra_3d.io import model_loaders as model_loaders
from casra_3d.io import model_writers as model_writers
from casra_3d.initialisation import tip_initialiser as tip_initialiser
from casra_3d.initialisation import field_initialiser as field_initialiser
from casra_3d.core import run as run

try:
    from scipy.ndimage.filters import gaussian_filter
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python package scipy. Please make sure it has been" +
                              "installed.")

#check python version
VER = sys.version_info
if VER[0] != 3 or VER[1] < 6:
    print("WARNING: Python version " + str(VER[0]) + "." + str(VER[1]) +
          " detected. Package has so far only been tested with Python versions > 3.6." +
          " May result in inconsistant behaviour.")

def evaporate_model(filename, out_dir, global_parameters_filename, mref_filename,
                    materials_db, its, reset_field, free_params={},
                    termination_height=-10.0, is_vtk=False):

    """

    The method for simulating the field evaporation of a particular geometry
    (contained within the input .mstate file).The simulated evaporation terminates
    after either the total number of iterations or termination height condition has
    been reached (whichever comes first).Model iterations are outputted as .vtk
    files in the directory defined by out_dir.

    :param1 filename (str): The input .mstate file defining the model geometry
    :param2 out_dir (str): the output directory
    :param3 global_parameters_filename (str): The tsv of model parameters (see help for
                                        tip_initialiser or wiki for more information).
    :param4 mref_filename (str): The tsv file matching phase names to database material ids.
                           Also contains Euler angles for grain rotation.
    :param5 materials_db (str): The SQLite materials database
    :param6 its (int): The total number of iterations (termination condition)
    :param7 reset_field (bool): Whether to regenerate the level set field or use stored field
                         if possible
    :param8 free_params (dict): Dictionary containing free parameters (set by parameter
                         optimisation methods rather than the materials database).
    :param9 termination_height (float): The maximum tip height after which to stop evaporation
                                (termination condition)

    :returns:
        :it (int): the iteration number

    :raises:
        None

    """

    print("Input tip geometry directory: " + filename)
    print("Output directory: " + out_dir)

    #create output directory
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    if is_vtk:
        #FIXME: Hack to re-use existing reader/writers, but we should be able to
        # translate without disk IO
        mesh = model_loaders.read_vtk(filename)
        tmpFileName = tempfile.mkstemp()[1]
        model_writers.write_mesh_to_mstate("bulk",
                                           np.array(mesh.points),
                                           np.array(mesh.cells),
                                           tmpFileName, 1)

        model_geom = model_loaders.load_mstate(tmpFileName, reset_field)

    else :
        model_geom = model_loaders.load_mstate(filename, reset_field)

    if(not len(model_geom["objts"]) ):
        print("Input tip geometry empty")
        raise

    ###load model parameters
    model_parameters = tip_initialiser.load_parameters(global_parameters_filename)
    model_geom["parameters"] = model_parameters

    ###perform input mesh checks
    for a in range(len(model_geom["objts"])):
        #check mesh is watertight
        res = tip_initialiser.check_mesh_is_watertight(model_geom["objts"][a]["verts"],
                                                       model_geom["objts"][a]["faces"],
                                                       model_geom["objts"][a]["name"],
                                                       surface=model_geom["objts"][a]["surface"])

        if res == 1:
            print("WARNING: Mesh " + model_geom["objts"][a]["name"] + "is zero length")
        elif res == 2:
            raise ValueError("Mesh " + model_geom["objts"][a]["name"] + "is not " +
                             "watertight (mesh is open)")

        if model_geom["objts"][a]["surface"] == 1:
            sverts = model_geom["objts"][a]["verts"]
            sfaces = model_geom["objts"][a]["faces"]

    #check specimen orientation is consistent
    if model_parameters['base_disable'] != 0:
        tip_initialiser.check_geometry(sverts, sfaces)

    ###perform initial rotation of specimen geometry and phases
    alpha = model_geom["parameters"]["rot_alpha"]
    beta = model_geom["parameters"]["rot_beta"]
    gamma = model_geom["parameters"]["rot_gamma"]
    tip_initialiser.rotate_specimen_geometry(model_geom, alpha, beta, gamma)

    if reset_field is True:
    #reset field if reset switch selected
        print("Resetting level set field")
        model_geom["field"] = False

    ###load mref file
    tip_initialiser.load_mref(mref_filename, model_geom)

    ###load database parameters
    tip_initialiser.load_database_parameters(materials_db, model_geom, free_params)

    ###obtain dimensions of specimen geometry
    field_initialiser.dimensions_from_geometry(model_geom,
                                               grid_width=model_parameters["grid_width"],
                                               scale=model_parameters["scale"])

    #only constructs field if no field exists in the .mstate file already (or field reset set to 1)
    field_initialiser.initialise_field(model_geom, filename)
    print("Construction complete")

    if model_parameters["base_disable"] == 1:
        model_geom["simulation_grid"]["inactive"] = int(model_geom["field"].shape[2] * 0.2)
    elif model_parameters["base_disable"] == 0:
        model_geom["simulation_grid"]["inactive"] = 0
    else:
        model_geom["simulation_grid"]["inactive"] = -1
    if model_parameters["initial_blurring"] > 0:
        model_geom["field"][:, :, :] = gaussian_filter(model_geom["field"][:, :, :],
                                                       model_parameters["initial_blurring"])
    if model_parameters["base_disable"] != 0:
        model_geom["field"][:, :, :2] = -1.0

    ###model initialisation
    surface = {"verts": [], "faces": [], "cp": [], "phase": [], "areas": [],
               "normals": [], "pot": [], "flux": [], "smflux": [], "time": [],
               "sample_surface": [], "smoothed_velocity": [], "volume": [],
               "voltage": [], "temperature": [], "iteration_stability": []}

    field, it = run.run_model(model_geom, its, 0, surface,
                              adaptive=model_parameters["adaptive"],
                              voltage_ramping=model_parameters["voltage_curve"],
                              terminate_height=termination_height)

    model_writers.output_evolution_vtk(out_dir, surface, model_geom, filename,
                                       field, start=0, end=math.inf)

    return it

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--tip_geom', metavar='tip geometry', type=str, default="",
                        help='The .mstate file directory defining the tip geometry. ' +
                             'Can also be a .vtk file if --vtk 1.')
    parser.add_argument('--out', metavar='output directory', type=str, default="",
                        help='The output directory in which to dump the simulation output files')
    parser.add_argument('--parameters', metavar='parameters', type=str, default="",
                        help='The .tsv file containing simulation parameter values')
    parser.add_argument('--mref', metavar='materials reference file', type=str, default="",
                        help='The .mref file linking phases in .mstate to the materials db')
    parser.add_argument('--mat_db', metavar='materials SQLite database', type=str,
                        default="c3d_input/materials", help='The materials db')
    parser.add_argument('--it', metavar='it', type=int, default=41,
                        help='Number of iterations to perform before termination')
    parser.add_argument('--termination_height', metavar='termination height',
                        type=float, default=-10.0,
                        help='The maximum simulated sample height below ' +
                        ' which to terminate the simulation (overides it).')
    parser.add_argument('--reset', metavar='reset', type=int, default=0,
                        help='Reset the level set field')
    parser.add_argument('--vtk', metavar='vtk', type=int, default=0,
                        help='Input mesh is VTK (1) or mstate (0)')
    args = parser.parse_args()

    #simulation parameter definition
    filename = args.tip_geom
    out_dir = args.out
    global_parameters_filename = args.parameters
    mref_filename = args.mref
    materials_db = args.mat_db
    its = args.it
    terminate_height = args.termination_height
    reset_field = bool(args.reset)
    is_vtk = args.vtk


    EMPTY = ""

    if filename is EMPTY:
        raise ValueError("mstate file must be parsed to --tip_geom argument")
    if out_dir is EMPTY:
        raise ValueError("output directory must be parsed to --out argument")
    if global_parameters_filename is EMPTY:
        raise ValueError("model parameters file must be parsed to --parameters argument")
    if mref_filename is EMPTY:
        raise ValueError("model reference file must be parsed to --mref argument")

    evaporate_model(filename, out_dir, global_parameters_filename, mref_filename,
                    materials_db, its, reset_field,
                    termination_height=terminate_height, is_vtk=is_vtk)
