
"""

Module containing file loaders

Authors: Charles Fletcher, Daniel Haley
Atom Probe Tomography Group, Department of Materials, University of Oxford
May 2020

"""
import re
import os
import csv
import sys
from pathlib import Path
from typing import Sequence, TypeVar, Dict, List, Union

try:
    import numpy as np
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python package numpy. " +
                              "Please make sure package is installed.")

from casra_3d.point_projection_reconstruction import point_projection_mapping as ppm
from casra_3d.point_projection_reconstruction import loader as loader

def progressbar(it, prefix="", size=10, file=sys.stdout):
    """
    progress bar function
    """
    def show(j):
        x = int(j)
        file.write("%s[%s%s] %i/%i\r" % (prefix, "#"*x, "."*(size-x), j, size) + "\r")
        file.flush()
    show(it)
    file.flush()

class Mesh:

    """

    Class for creating triangle mesh instances

    """

    def __init__(self, points: List[List[float]], cells: List[List[int]],
                 point_data, cell_data: Dict[str, List[List[float]]]):

        """

        :param1 self (class Mesh): The Python class Mesh instance to be initialised
        :param2 points (np.array, (M, 3), dtype=float): mesh instance vertices
        :param3 cells (np.array, (N, 3), dtype=int): mesh instance connectivity matrix
        :param4 point_data (dict): mesh instance point data
        :param5 cell_data (dict): mesh instance cell data

        :returns:
            None

        :raises:
            None

        """

        self.points = points
        self.cells = cells
        self.point_data = point_data
        self.cell_data = cell_data

#functions for handling file sorting (for loading in model iteration files in sequence)
def tryint(s : int) -> int:
    """try to convert to integer"""
    try:
        return int(s)
    except ValueError:
        return s

def alphanum_key(s : str):
    """ Turn a string into a list of string and number chunks.
        "z23a" -> ["z", 23, "a"]
    """
    return [tryint(c) for c in re.split('([0-9]+)', s)]


def load_apt_data(filename, rangefile="", sample_rate=1.0):
    """

    Function for reading in APT data. Method detects whether file type is
    .tsv, .epos, or .ato and runs appropriate subroutine. Therefore,
    the file extension is critical for normal functionality of the method.

    :param1 filename (str): The APT data file
    :param2 rangefile (str): The range file (required for .epos and .ato files)
    :param3 sample_rate (float): The ion sampling rate
    :returns:
        :dx (list): detector x coordinate (m)
        :dy (list): detector y coordinate (m)
        :sx (list): sample x coordinate (m). Returns empty array if unapplicable.
        :sy (list): sample y coordinate (m). Returns empty array if unapplicable.
        :sz (list): sample z coordinate (m). Returns empty array if unapplicable.
        :chem (list): The ion chemistry (integer)
        :ions (list): The corresponding ion label to the chem integer values (by index)
        :ic (list): The number of nuclei making up each ion
        :VP (list): The pulse voltage data (only returned for experimental APT data
                    - obtainable from model .vtks for simulated data)

    :raises:
        FileNotFoundError: could not open/read from parsed APT data file (param1 filename)
        FileNotFoundError: could not open/read from parsed rangefile (param2 rangefile)
        TypeError: Input file (param1 filename) has wrong file extension
                   (not .tsv, .ato, or .epos)

    """

    sample_num = int(1.0/sample_rate)
    experimental = True

    ##check if simulated data (tsv), or if experimental data (epos)
    #case of .tsv file
    if filename.split(".")[-1] == "tsv":
        num = []
        dx = []
        dy = []
        sx = []
        sy = []
        sz = []
        chem = []
        VP = []
        try:
            print(filename)
            with open(filename, 'r') as csvfile:
                reader = csv.reader(csvfile, delimiter='\t', quotechar='|')
                hd = next(reader, None)
                for row in reader:
                    c = row[10]
                    if c == "None":
                        c = -1
                    num.append(int(row[0]))
                    dx.append(float(row[1]))
                    dy.append(float(row[2]))
                    sx.append(float(row[7]))
                    sy.append(float(row[8]))
                    sz.append(float(row[9]))
                    chem.append(int(c))

            num = np.array(num)
            dx = np.array(dx)
            dy = np.array(dy)
            sx = np.array(sx)
            sy = np.array(sy)
            sz = np.array(sz)
            chem = np.array(chem)

            ions = []
            ic = num

            #perform sampling
            num = num[::sample_num]
            dx = dx[::sample_num]
            dy = dy[::sample_num]
            sx = sx[::sample_num]
            sy = sy[::sample_num]
            sz = sz[::sample_num]
            chem = chem[::sample_num]
            da = chem
            da_p = False
            experimental = False

        except:
            raise FileNotFoundError("could not find or read from the file: " + filename + " \n Please make sure all details are correct.")

    #case of .epos file
    elif filename.split(".")[-1] == "epos":
        if len(rangefile) == 0:
            raise FileNotFoundError("Please make sure a range file has been passed to " +
                                    "the method (required for .epos file types).")

        ions, rfile = loader.read_rrng(rangefile)
        dx, dy, da, VP, pslep, sx, sy, sz = loader.chunk_read_epos(filename, sample_num)
        sx = sx * 1e-9
        sy = sy * 1e-9
        sz = sz * 1e-9
        chem, ions, ic = ppm.range(da, rfile)
        da_p = True

    #case of .ato file
    elif filename.split(".")[-1] == "ato":
        if len(rangefile) == 0:
            raise FileNotFoundError("Please make sure a range file has been passed " +
                                    "to the method (required for .epos file types).")
        ions, rfile = loader.read_rrng(rangefile)
        dx, dy, da, VP, tsld, sx, sy, sz = loader.read_ato(filename)
        #convert
        sx = sx * 1e-9
        sy = sy * 1e-9
        sz = sz * 1e-9
        chem, ions, ic = ppm.range(da, rfile)
        da_p = True
    else:
        raise TypeError("input file not .tsv or .pos. Please input file with correct suffex.")

    print("dataset loaded successfully")
    return dx, dy, sx, sy, sz, chem, ions, ic, VP, da, da_p, experimental

def load_mstate(filename, reset_field=False):

    """

    Loads the simulated geometries (i.e. non ET initialised simulations)

    :param1 filename (str): the .mstate filename
    :param2 reset_field (bool): Whether to reset the field (clear mstate FIELD
                                and PGRID values)
    :return:
        :geom (dict): The loaded simulation geometry (in a dictionary)

    :raises:
        None

    """

    f = open(filename, "r")
    text = f.read()

    #find all objects in .mstate file
    find_objts = [m.start() for m in re.finditer('OBJT:', text)]

    geom = {"objts" : []}

    for o in range(0, len(find_objts)):
        obj = find_objts[o]

        if o != len(find_objts) - 1:
            obj2 = find_objts[o+1]
            split_text = text[obj+6:obj2].split("\n")
            objt_name = split_text[0]
            vert_count = int(split_text[1])
            face_count = int(split_text[2])
            surface = int(split_text[3])
            verts = np.array([i.split("\t") for i in
                              split_text[4:4+vert_count]]).astype(float)
            faces = np.array([i.split("\t") for i in
                              split_text[5+vert_count:5+vert_count+face_count]]).astype(int)
            geom["objts"].append({"name":objt_name, "verts":verts, "faces":faces,
                                  "surface": surface, "phase_number": o})
        else:
            split_text = text[obj+6:].split("\n")
            objt_name = split_text[0]
            vert_count = int(split_text[1])
            face_count = int(split_text[2])
            surface = int(split_text[3])
            verts = np.array([i.split("\t") for i in
                              split_text[4:4+vert_count]]).astype(float)
            faces = np.array([i.split("\t") for i in
                              split_text[5+vert_count:5+vert_count+face_count]]).astype(int)
            geom["objts"].append({"name":objt_name, "verts":verts, "faces":faces,
                                  "surface": surface, "phase_number": o})

        #calculate bounding box values
        #append to geometry structure
        (geom["objts"][-1]["min_x"], geom["objts"][-1]["min_y"],
         geom["objts"][-1]["min_z"], geom["objts"][-1]["max_x"],
         geom["objts"][-1]["max_y"],
         geom["objts"][-1]["max_z"]) = calculate_bounding_box(verts, faces)

    find_field = [m.start() for m in re.finditer('FIELD:', text)]

    if len(find_field) > 1 and reset_field is False:
        raise ValueError("more than one scalar field found " +
                         "(field choice ambigious - aborting).")
    if len(find_field) == 1 and reset_field is False:
        print("Previously saved scalar field detected. Loading field...")
        geom["field"] = read_field_to_mstate(filename)
        print("Field loaded successfully!")
    else:
        print("No field detected. Must be constructed prior to evaporation simulation.")
        geom["field"] = False

    #save phase grid to .mstate
    find_pgrid = [m.start() for m in re.finditer('PGRID:', text)]
    if len(find_field) == 1 and reset_field is False:
        (geom["phase_grid"], geom["pgxmin"],
         geom["pgxmax"], geom["pgymin"],
         geom["pgymax"], geom["pgzmin"],
         geom["pgzmax"]) = read_pgrid_to_mstate(filename)

    else:
        geom["phase_grid"] = False

    return geom

def load_simulation(dirname: str, surface, its: int=-1):

    """

    Load simulation from output .vtk data found in dirname

    :param1 dirname (str): The directory to read simulation files from
    :param2 surface (dict): The simulation dictionary
    :param3 its (int): The number of frames to read in (-1 -> all)

    :returns:
        None

    :raises:
        ValueError: field cannot be found
        IndexError: The maximum model frames in the directory (param1 dirname)
                    have been loaded (continue)

    """

    if os.path.exists(dirname + "/msim"):
        simulations = os.listdir(dirname + "/msim")
        simulations.sort(key=alphanum_key)
        if its == -1:
            its = len(simulations)
        print("loading surface evolution meshes...")
        progress = 0
        tic = 0
        for a in range(0, its):
            try:
                file_here = os.path.isfile(dirname + "/msim/" + simulations[a])
                tic += 1
            except IndexError:
                print("Maximum of " + str(tic) + " msim files found. " +
                      "Reading in these files.")
                break
        if its < tic:
            tic = its

        itc = int(tic/10)
        for a in range(0, tic):
            try:
                mesh = read_vtk_light(dirname + "/msim/" + simulations[a])
                surface["verts"].append(np.array(mesh.points))
                surface["faces"].append(np.array(mesh.cells))
                surface["pot"].append(np.array(mesh.cell_data["potential"]
                                              ).flatten())
                surface["flux"].append(np.array(mesh.cell_data["flux"]
                                               ).flatten())
                surface["smflux"].append(np.array(mesh.cell_data["smflux"]).flatten())
                surface["phase"].append(np.array(mesh.cell_data["phase"]).flatten())
                surface["areas"].append(np.array(mesh.cell_data["areas"]).flatten())
                surface["normals"].append(np.array(mesh.cell_data["normals"]))
                surface["sample_surface"].append(np.array(mesh.cell_data["sample_surface"]
                                                         ).flatten())
                surface["smoothed_velocity"].append(np.array(mesh.cell_data["smoothed_velocity"]
                                                            ).flatten())
                surface["volume"].append(abs(np.array(mesh.cell_data["volume"]
                                                     ).flatten())[0])
                try:
                    temp_it_stab = abs(np.array(mesh.cell_data["iteration_stability"]).flatten())[0]
                    surface["iteration_stability"].append(temp_it_stab)
                except KeyError:
                    pass
                try:
                    surface["time"].append(abs(np.array(mesh.cell_data["time"]).flatten())[0])
                except KeyError:
                    pass
                try:
                    surface["voltage"].append(abs(np.array(mesh.cell_data["voltage"]
                                                          ).flatten())[0])
                except KeyError:
                    pass
            except IndexError:
                print("maximum model frames in directory loaded.")
                break
            if a % itc == 0:
                progress += 1
                progressbar(progress, prefix="", size=10, file=sys.stdout)
        print("Meshes successfully loaded")
    else:
        raise FileNotFoundError("No msim directory detected in : " + dirname)

def load_trajectories(dirname: str, projection):

    """

    load ion trajectories in .vtk files in dirname directory

    :param1 dirname (str): The directory containng the input
                           trajectory .vtk files
    :param2 projection (dict): The projection data dictionary

    :returns:
        None

    :raises:
        FileNotFoundError: Cannot find parsed in dirname

    """

    if os.path.exists(dirname + "/mtraj"):
        trajectories = os.listdir(dirname + "/mtraj")
        trajectories.sort(key=alphanum_key)

        for a in range(0, len(trajectories)):
            mesh = read_vtk(dirname + "/mtraj/" + trajectories[a])
            traj = mesh.points
            traj_inds = mesh.cells

            ion_trajectories = [[]]

            #separate out ion trajectories
            for a in range(1, len(traj_inds)):
                if abs((traj_inds[a, 0] - traj_inds[a - 1, 0])) == 1:
                    ion_trajectories[-1].append(traj[a - 1])

            projection["points"].append(ion_trajectories)
            projection["verts"].append(ion_trajectories)

    else:
        raise FileNotFoundError("No mtraj directory detected in : " + dirname)


def load_tsv_stability_trajectories(stability_data):

    """

    Method for loading projection stability data from .tsv

    :param1 stability_data (string): The stability data tsv filename

    :returns: 0
    :param1 stability (dict): The stability data dictionary

    """

    stability = {"num": [], "sv0": [], "sv1": [], "sv2": [], "dv0": [], "dv1": [], "dv2": [],
                 "vel0": [], "vel1": [], "vel2": [], "jacobian": [], "conformality": [],
                 "J00": [], "J10": [], "J01": [], "J11": [], "evap_rate": [], "panel_index": []}

    with open(stability_data, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t', quotechar='|')
        next(reader, None)
        for row in reader:
            stability["num"].append(int(row[0]))
            stability["sv0"].append([float(row[1]), float(row[2]), float(row[3])])
            stability["sv1"].append([float(row[4]), float(row[5]), float(row[6])])
            stability["sv2"].append([float(row[7]), float(row[8]), float(row[9])])
            stability["dv0"].append([float(row[10]), float(row[11]), float(row[12])])
            stability["dv1"].append([float(row[13]), float(row[14]), float(row[15])])
            stability["dv2"].append([float(row[16]), float(row[17]), float(row[18])])
            stability["vel0"].append([float(row[19]), float(row[20]), float(row[21])])
            stability["vel1"].append([float(row[22]), float(row[23]), float(row[24])])
            stability["vel2"].append([float(row[25]), float(row[26]), float(row[27])])
            stability["jacobian"].append(float(row[28]))
            stability["conformality"].append(float(row[29]))
            stability["J00"].append(float(row[30]))
            stability["J10"].append(float(row[31]))
            stability["J01"].append(float(row[32]))
            stability["J11"].append(float(row[33]))
            stability["evap_rate"].append(float(row[34]))

    stability["num"] = np.array(stability["num"])
    stability["sv0"] = np.array(stability["sv0"])
    stability["sv1"] = np.array(stability["sv1"])
    stability["sv2"] = np.array(stability["sv2"])
    stability["dv0"] = np.array(stability["dv0"])
    stability["dv1"] = np.array(stability["dv1"])
    stability["dv2"] = np.array(stability["dv2"])
    stability["vel0"] = np.array(stability["vel0"])
    stability["vel1"] = np.array(stability["vel1"])
    stability["vel2"] = np.array(stability["vel2"])
    stability["jacobian"] = np.array(stability["jacobian"])
    stability["J00"] = np.array(stability["J00"])
    stability["J10"] = np.array(stability["J10"])
    stability["J01"] = np.array(stability["J01"])
    stability["J11"] = np.array(stability["J11"])
    stability["evap_rate"] = np.array(stability["evap_rate"])

    return stability


def read_field_to_mstate(filename: str):
    """

    Function for reading the stored level set field from the .mstate file
    If not detected, signed distance field will be rebuilt

    :param1 filename (str): The .mstate filename

    :returns:
        :field (np.array, (L1,L2,L3), dtype=float): the 3D level set field array

    :raises:
        None

    """
    f = open(filename, "r")
    text = f.read()
    find_objts = [m.start() for m in re.finditer('FIELD:', text)]
    if len(find_objts) == 0:
        print("No stored level set field found. Field must be rebuilt. " +
              "This might take some time.")

    else:
        field_text = text[find_objts[0]:].split("\n")
        name = field_text[0]

        x = int(field_text[1])
        y = int(field_text[2])
        z = int(field_text[3])

        field = np.zeros((x, y, z))

        for a in range(0, z):
            for b in range(0, x):
                mat_row = field_text[4 + a*x+b].split(" ")
                for c in range(0, y):
                    field[b, c, a] = float(mat_row[c])

        return field

def read_pgrid_to_mstate(filename: str):

    """

    Function for reading the stored phase grid from the .mstate file
    If not detected, phase_grid will be rebuilt

    :param1 filename (str): The .mstate filename

    :returns:
        :pgrid (np.array, (F1,F2,F3), dtype=float): 3D phase grid of material ids
        :pgxmin (float): phase grid minimum x coordinate
        :pgxmax (float): phase grid maximum x coordinate
        :pgymin (float): phase grid minimum y coordinate
        :pgymax (float): phase grid maximum y coordinate
        :pgzmin (float): phase grid minimum z coordinate
        :pgzmax (float): phase grid maximum z coordinate

    :raises:
        None

    """

    f = open(filename, "r")
    text = f.read()
    find_objts = [m.start() for m in re.finditer('PGRID:', text)]
    if not find_objts:
        print("No phase grid found. This must be regenerated and " +
              "may take some time.")
    else:
        field_text = text[find_objts[0]:].split("\n")
        name = field_text[0]
        x = int(field_text[1])
        y = int(field_text[2])
        z = int(field_text[3])

        pgxmin = float(field_text[4])
        pgxmax = float(field_text[5])
        pgymin = float(field_text[6])
        pgymax = float(field_text[7])
        pgzmin = float(field_text[8])
        pgzmax = float(field_text[9])

        pgrid = np.zeros((x, y, z))

        for a in range(0, z):
            for b in range(0, x):
                mat_row = field_text[10 + a*x+b].split(" ")
                for c in range(0, y):
                    pgrid[b, c, a] = float(mat_row[c])

        return pgrid, pgxmin, pgxmax, pgymin, pgymax, pgzmin, pgzmax

def load_tsv_ion_trajectories(projection, mapping_data: str):

    """

    Method for loading projection data from .tsv

    :param1 projection (dict): The projection data dictionary
    :param2 mapping_data (str): The filename for the .tsv trajectory data

    :returns:
        None

    :raises:
        None

    """

    with open(mapping_data, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t', quotechar='|')
        next(reader, None)
        for row in reader:
            projection["num"].append(int(row[0]))
            projection["detector_space"].append(np.array([float(row[1]),
                                                          float(row[2]),
                                                          float(row[3])]))
            projection["vel"].append(np.array([float(row[4]),
                                               float(row[5]),
                                               float(row[6])]))
            projection["sample_space"].append(np.array([float(row[7]),
                                                        float(row[8]),
                                                        float(row[9])]))
            projection["chemistry"].append(float(row[10]))
            projection["ion_phase"].append(row[11])
            panel_indexes = row[12].split(",")
            panel_indexes = [int(a) for a in panel_indexes]
            projection["panel_index"].append(panel_indexes)

    projection["num"] = np.array(projection["num"])
    projection["detector_space"] = np.array(projection["detector_space"])
    projection["vel"] = np.array(projection["vel"])
    projection["sample_space"] = np.array(projection["sample_space"])
    projection["chemistry"] = np.array(projection["chemistry"])
    projection["ion_phase"] = projection["ion_phase"]
    projection["panel_index"] = projection["panel_index"]


def calculate_bounding_box(verts, faces):

    """

    Function for calculating a meshes bounding box

    :param1 verts (np.array, (M, 3), dtype=float): The mesh vertices
    :param2 faces (np.array, (N, 3), dtype=int): The mesh connectivity matrix

    :returns:
        :min_x (float): The minimum x bounding box coordinate
        :min_y (float): The minimum y bounding box coordinate
        :min_z (float): The minimum z bounding box coordinate
        :max_x (float): The maximum x bounding box coordinate
        :max_y (float): The maximum y bounding box coordinate
        :max_z (float): The maximum z bounding box coordinate

    :raises:
        None

    """
    min_x, min_y, min_z = np.min(verts, axis=0)
    max_x, max_y, max_z = np.max(verts, axis=0)

    return min_x, min_y, min_z, max_x, max_y, max_z

#Return False if a string contains characters other than search_chars
def str_contains_only( input_str: str, search_chars: str) -> bool:

    """Returns false if a string contains characters other than search_chars"""

    for i in input_str:
        found = False
        for j in search_chars:
            if(i == j):
                found = True
                break

        if(not found):
            return False

    return True

#Returns either a Mesh or a bool (on failure, returns false)
# Union[ Mesh, bool] ?
def read_vtk(filename: str):

    """

    Function for reading in ASCII vtk files
    Supports both UNSTRUCTURED_GRID and POLYDATA vtk file formats

    :param1 filename (str): The vtk filename

    :returns:
        :mesh (class Mesh): Mesh object instance if successful
        :returncode (int): 0 if successful

    :raises:
        FileNotFoundError: Cannot open input file (param1 filename)
        ValueError: VTK Header missing
        ValueError: File does not appear to be ASCII formatted VTK
        ValueError: File does not appear to be ASCII formatted VTK Dataset
        ValueError: Non-triplet POINTS header
        ValueError: negative POINTS count int header
        ValueError: premature EOF/no POINTS found
        ValueError: Invalid character in POINTS data
        ValueError: Point data did not appear to be 3D
        ValueError: POLYGONS/CELLS section misformatted
        ValueError: POLYGONS/CELLS section holds non-triangular entities
        ValueError: Negative POLYGONS/CELLS count
        ValueError: Premature EOF in POLYGONS/CELLS
        ValueError: Non-integer value in polygon values
        ValueError: Fragment misformatted
        ValueError: non-triangular entity
        ValueError: FIELD misformatted
        ValueError: Premature EOF in FIELD. Still expecting FIELD entries
        ValueError: Negative FIELD entry count
        ValueError: Invalid character in POINTS data
        ValueError: field data did not appear to be match stated dimension in FIELD header

    """

    try:
        f = open(filename, 'r')
    except:
        raise FileNotFoundError("Cannot open/read file: " + filename)

    #There is a file description available at
    # https://lorensen.github.io/VTKExamples/site/VTKFileFormats/#simple-legacy-formats
    # however, this doesn't actually match what one finds in the file.

    #Example deviation

    #Docs say:
    #POINTS n float
    #p1x p1y p1z
    #...
    #pnx pny pnz

    #What you actually get:
    #POINTS 12050 float
    #6018.84 6353.22 942.429 6018.84 6252.3 1014.93 5889.11 6353.22 1014.93
    #...
    #(Pn/3)x ...

    #Where the last line has 6 entries. Total does sum to ~12050 however

    line = f.readline()

    #check for VTK header
    if(line.find("vtk DataFile Version") < 0):
        raise ValueError("VTK Header missing")

    #Skip dead line
    line = f.readline()

    line = f.readline().strip()

    if(line != "ASCII"):
        raise ValueError("File does not appear to be ASCII formatted VTK")

    fragments = f.readline().strip().split(" ")
    if(fragments[0] != "DATASET"):
        raise ValueError("File does not appear to be ASCII formatted VTK Dataset")

    points = []
    tris = []
    cell_data = {}
    point_data = {}

    file_pos = f.tell()
    #Obtain fragments
    while True:
        #Strip comments and leading/trailing whitespace
        fragments = f.readline().strip().split('#')[0].split(" ")

        #EOF Detection - check tell moved during readline
        if (file_pos == f.tell()):
            break
        else:
            file_pos = f.tell()

        if not len(fragments):
            continue

        #Skip ignorable sections
        if(fragments[0] == "DATA" or fragments[0] == "METADATA" or
           fragments[0] == "INFORMATION" or fragments[0] == "NAME"):
           continue

        #Read POINTS section
        if(fragments[0] == "POINTS"):
            if(len(fragments) != 3):
                raise ValueError("Non-triplet POINTS header")

            nPts = int(fragments[1])
            if(nPts <= 0):
                raise ValueError("negative POINTS count int header")

            #Read POINTS payload
            while nPts > 0:
                pts_entry = f.readline().strip()

                #Check for EOF
                if(file_pos == f.tell()):
                    raise ValueError("premature EOF/no POINTS found")
                else:
                    file_pos = f.tell()
                #Valid real number values
                if not str_contains_only( pts_entry, "0123456789. Ee-+"):
                    raise ValueError("Invalid character in POINTS data : " + pts_entry)

                fragments=pts_entry.split(" ")
                if len(fragments) % 3 != 0:
                    raise ValueError("Point data did not appear to be 3D")

                #Re-wrap into triplets, and append to points
                nPts -= len(fragments)//3
                ptsLine = [fragments[i*3:i*3+3] for i in range(len(fragments)//3)]

                #Record points for output
                for i in ptsLine:
                    thisPt = [float(x) for x in i]
                    points.append(thisPt)

            continue

        #Read polygon section
        if (fragments[0] == "POLYGONS" or fragments[0] == "CELLS"):
            #get dimension
            cdimension =int(fragments[2])//int(fragments[1]) - 1

            if (len(fragments) != 3):
                raise ValueError("POLYGONS/CELLS section misformatted : " + str(fragments))

            nPolys = int(fragments[1])
            #Each triangle needs 3 entries + 1 descriptor
            nCellEntries = int(fragments[2])//(cdimension+1)

            if int(fragments[2])//(3+1) != nPolys and fragments[0] == "POLYGONS":
                raise ValueError("POLYGONS section holds non-triangular entities : " +
                                 str(nCellEntries) + " cells, but " + str(nPolys) + "Polys")

            #Disallow negative poly counts
            if nPolys < 0:
                raise ValueError("Negative POLYGONS/CELLS count")

            #Loop over each line in POLYONS/CELLS section
            while nPolys:
                pts_entry = f.readline().strip()

                #Check for EOF
                if file_pos == f.tell():
                    raise ValueError(f"Premature EOF in POLYGONS/CELLS, still " +
                                     "expecting {nPolys}")
                else:
                    file_pos = f.tell()

                #Should only contain integers (indices)
                for j in pts_entry:
                    if not((j >='0' and j <='9') or j == ' '):
                        raise ValueError("Non-integer value in polygon values:" +
                                         pts_entry)

                nPolys-=1

                fragments = pts_entry.split(" ")

                if len(fragments) != (cdimension + 1):
                    raise ValueError("Fragment misformatted")

                if int(fragments[0]) != cdimension:
                    raise ValueError("non-triangular entity : " + str(fragments))

                tris.append([int(j) for j in fragments[1:4]])

        if fragments[0] == "FIELD":
            fieldArrayCount = int(fragments[2])

            #Loop over each array type
            while fieldArrayCount:
                #Read description line
                fragments = f.readline().strip().split(" ")

                #Metadata sections seem to be a thing, at the end of field
                # blocks
                if(fragments[0] == "METADATA" or fragments[0] == "INFORMATION"):
                    break;

                if len(fragments) != 4:
                    raise ValueError("FIELD misformatted:" + str(fragments))

                #Check for EOF
                if file_pos == f.tell():
                    raise ValueError(f"Premature EOF in field, still " +
                                     "expecting {numEntries} entries")
                else:
                    file_pos = f.tell()

                fieldArrayCount -= 1
                fieldName = fragments[0]
                fieldDimension = int(fragments[1])
                thisField = []
                numEntries = int(fragments[2]) * fieldDimension

                if numEntries <=0:
                    raise ValueError("Zero or negative FIELD entry count")

                while numEntries > 0:
                    norm_entry = f.readline().strip()
                    #Check for EOF
                    if file_pos == f.tell():
                        raise ValueError(f"Premature EOF in field, still " +
                                         "expecting {numEntries} entries")
                    else:
                        file_pos = f.tell()

                    #Should only contain integers (indices)
                    #Valid real number values
                    if not str_contains_only(norm_entry, "0123456789. Ee-+"):
                        raise ValueError("Invalid character in field data : " + norm_entry)

                    fragments = norm_entry.split(" ")
                    newEntries=[float(x) for x in fragments]

                    thisField.append(newEntries)
                    numEntries -=len(newEntries)

                cell_data[fieldName] = thisField

            continue

        if fragments[0] == "POINT_DATA":
            pac = int(fragments[1])
            pointDataSection = True

            #entered point data Section
            while pointDataSection == True:
                pointarraycount = pac
                thisPointdata = f.readline().strip().split(" ")

                #currently only recognises SCALARS field, ignores otherwise
                if thisPointdata[0] == "SCALARS":
                    pdName = thisPointdata[1]
                    pdType = thisPointdata[2]
                    pdDimension = int(thisPointdata[3])
                    lookup_table = f.readline().strip().split(" ")

                    #only default lookup table for scalars currently supported
                    if lookup_table[1] == "default":
                        thisPD = []
                        while pointarraycount > 0:
                            fragments = f.readline().strip().split(" ")

                            #first try reading in line by line (e.g. dimensions match)
                            if len(fragments) == pdDimension:
                                thisPD.append([float(x) for x in fragments])
                                pointarraycount -= 1

                                #next try reading in whole line (applies for 1D quantities)
                            elif pdDimension == 1 and len(fragments) == pointarraycount:
                                thisPD = [ [float(x) for x in fragments] ]
                                pointarraycount = 0

                            else: #if neither method works, cannot decipher point_data structure
                                raise ValueError("point data did not appear to be match " +
                                                 "stated dimension in POINT_DATA header")
                    point_data[pdName] = thisPD
                else:
                    pointDataSection = False

    print(f"VTK read complete: {len(points)} points, " +
          f"{len(tris)} tris, {len(point_data)} point_data, " +
          f"{len(cell_data)} cell_data")

    return Mesh(points, tris, point_data, cell_data)

def read_vtk_light(filename: str):

    """

    function for reading in ASCII vtk files written by CASRA 3D
    Supports only UNSTRUCTURED_GRID file formats

    :param1 filename (str): The vtk filename

    :returns:
        :mesh (class Mesh): Mesh object instance

    :raises:
        FileNotFoundError: Cannot open input file (param1 filename)
        ValueError: unsupported .vtk type
        ValueError: Only default lookup table is currently supported
        ValueError: input point_data dimension mismatches vertex dimension
                    (invalid vtk file)
        ValueError: invalid cell data (invalid vtk file)
        ValueError: cell data mismatches number of cells (invalid vtk file)
        ValueError: point data mismatches number of points (invalid vtk file)

    """

    try:
        f = open(filename, 'r')
    except:
        raise FileNotFoundError("Cannot open/read file: " + filename)

    skip = 0
    verts = []
    faces = []
    pnum = 0
    cnum = 0
    pdata = False
    cdata = False
    point_data = {}
    cell_data = {}
    active_point_data_field = ""

    for line in f:
        if skip > 0:
            skip -= 1
            continue

        #ignore comments
        if line[0] == "#":
            continue

        #make sure vtk file is of UNSTRUCTURED_GRID grid
        if len(line) > 7 and line[:7] == "DATASET":
            if line[8:-1] != "UNSTRUCTURED_GRID":
                raise ValueError("vtk type " + str(line[8:-1]) +
                                 " is currently unsupported by this loader")
            continue

        #check if header detected for points section
        if len(line) > 8 and line[:7] == "POINTS ":
            point_input = line.split(" ")
            pnum = int(point_input[1])
            ptype = point_input[2][:-1]
            continue

        #add point (vertex)
        if pnum > 0:
            pnum -= 1
            pline = line[:-1].split(" ")
            vertex = [float(i) for i in pline if len(i) != 0]
            verts.append(vertex)
            continue

        #check if header detected for cells section
        if len(line) > 6 and line[:6] == "CELLS ":
            cell_input = line.split(" ")
            cnum = int(cell_input[1])
            ctype = int(cell_input[2])
            continue

        #add cell (face)
        if cnum > 0:
            cnum -= 1
            cline = line[:-1].split(" ")
            face = [int(i) for i in cline[1:]]
            faces.append(face)
            continue

        #Check if header detected for point_data section
        if len(line) > 11 and line[:11] == "POINT_DATA ":
            cdata = False
            pdata = True
            pds = line[:-1].split(" ")
            pts = int(pds[1])
            ncell_field = 0
            continue

        #Check if header detected for cell_data section
        if len(line) > 10 and line[:10] == "CELL_DATA ":
            pdata = False
            cdata = True
            pds = line[:-1].split(" ")
            pts = int(pds[1])
            ncell_field = 0
            continue

        #read in point data
        if pdata == True:
            if len(line) > 8 and line[:8] == "SCALARS ":
                sline = line[:-1].split(" ")
                active_point_data_field = sline[1]
                point_data[active_point_data_field] = []
                continue

            #current reader assumes a default lookup table
            elif len(line) > 13 and line[:13] == "LOOKUP_TABLE ":
                if line[13:-1] != "default":
                    raise ValueError("currently only a default lookup table " +
                                     "is supported by vtk reader")
                continue

            else:
                field_vals = line[:-1].split(" ")
                field_vals = [float(a) for a in field_vals if len(a) != 0]
                if len(field_vals) != len(verts):
                    raise ValueError("point_data: " + active_point_data_field +
                                     " dimension mismatches number of mesh vertices")
                else:
                    point_data[active_point_data_field] = field_vals
                continue

        #read in cell data
        if cdata == True:
            if len(line) > 6 and line[:6] == "FIELD ":
                sline = line[:-1].split(" ")
                ncell_field = int(sline[2]) + 1
            elif ncell_field > 0:
                sline = line[:-1].split(" ")
                if len(sline) == 4 and len(sline[-1]) > 0 and type(sline[-1]) == str:
                    active_cell_data_field = sline[0]
                    tuple_size = int(sline[1])
                    cell_data[active_cell_data_field] = []
                    ncell_field -= 1
                else:
                    field_val = line[:-1].split(" ")
                    field_val = [float(v) for v in field_val if len(v) > 0]
                    if len(field_val) != tuple_size:
                        raise ValueError("Incorrect number of components detected " +
                                         "when reading cell_data field: " +
                                         active_cell_data_field)

                    cell_data[active_cell_data_field].append(field_val)

    #check cell data fields are the correct length
    for key, values in cell_data.items():
        if len(values) != len(faces):
            raise ValueError("loaded cell_data field: " + key +
                             " mismatches number of cells")

    #check point data fields are the correct length
    for key, values in point_data.items():
        if len(values) != len(verts):
            raise ValueError("loaded point_data field: " + key +
                             " mismatches number of vertices")

    f.close()

    #create mesh instance
    mesh = Mesh(verts, faces, point_data, cell_data)

    return mesh
