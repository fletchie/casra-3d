"""

Script for generating visualisations for the SiGe multilayer system
parameter tuning sweep results

Author: Charles Fletcher, June 2021, University of Oxford

"""

import numpy as np
import matplotlib.pyplot as plt
import pandas

filename = "c3d_output/sige_multilayer/optimisation/optimisation_results.tsv"
out_filename = "c3d_output/sige_multilayer/optimise-field.png"
phases = ["bulk", "layer1;layer6"]

n = 12

df = pandas.read_csv(filename, sep = "\t", header = None)

f1 = df[0] == phases[0]
f2 = df[0] == phases[1]

vals = np.vstack((df[1][f1], df[1][f2], df[7][f1])).T
F00 = np.linspace(np.min(vals[:, 0]), np.max(vals[:, 0]), num = 100)
F01 = np.linspace(np.min(vals[:, 1]), np.max(vals[:, 1]), num = 100)
X, Y = np.meshgrid(F00, F01)

print("Outputing parameter sweep to: " + out_filename)

fig, host = plt.subplots(figsize=(7,4)) # (width, height) in inches
par1 = host.twinx()
host.set_xlabel("Si$_{0.3}$Ge$_{0.7}$ evaporation field (Vnm$^{-1}$)", fontsize = 15)
host.set_ylabel("Microstructural Similarity (a.u.)", fontsize = 15)
par1.set_ylabel("Normalised standard \n deviation of density (a.u.)", fontsize = 15)
color1 = "#1f77b4"
color2 = "orange"
p1, = host.plot(df[1][f1][:n]* 1e-9, df[7][f1][:n], color = color1, label = "MI Similarity")
p2, = par1.plot(df[1][f1][:n]* 1e-9, df[9][f1][:n], color = color2, label = "Normalised density \nspread")
va = np.array(df[1][f1][:n]* 1e-9)
#par1.scatter(va[np.argmax(df[7][f1][:n])], [0.276], color = "green", label = "Optimised conventional reconstruction \ndensity spread", s = 100)
lns = [p1, p2]
par1.legend(handles=lns, loc='upper right', prop={"size": 12})
host.yaxis.label.set_color(p1.get_color())
par1.yaxis.label.set_color(p2.get_color())
host.tick_params(labelsize = 14)
par1.tick_params(labelsize = 14)
plt.tight_layout()
plt.savefig(out_filename, dpi = 300)
