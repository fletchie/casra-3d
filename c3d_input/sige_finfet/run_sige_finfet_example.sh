start=$(date +'%s')
echo "Run SiGe finFET reconstruction calibration pipeline"

python3 c3d_input/sige_finfet/generate_sige_finfet_model.py
python3 c3d_input/sige_finfet/optimise_model_finfet.py
python3 c3d_input/sige_finfet/vis_optimisation_finfet.py

finish=$(date +'%s')
duration=$((finish-start))
hrs=$(( duration/3600 )); mins=$(( (duration-hrs*3600)/60)); secs=$(( duration-hrs*3600-mins*60 ))
printf 'Time spent: %02d:%02d:%02d\n' $hrs $mins $secs
