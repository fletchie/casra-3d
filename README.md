**For the latest APT data reconstruction and automated parameter tuning code - see the [recon_calibration](https://gitlab.com/fletchie/casra-3d/-/tree/recon_calibration) repository branch**

# A brief Summary of Continuum APT Simulation and Reconstruction Aid in 3D (CASRA 3D)

CASRA 3D is a 3D simulation tool for continuum modelling of Atom Probe Tomography (APT) experiments, building on our earlier 2D APT modelling tool Casra. The tool can either be used as a command line program or imported into Python as a package. CASRA 3D contains a number of modules including functionality to simulate specimen field evaporation, perform ion projection and synthetic APT data generation, perform conventional point-projection reconstructions of synthetic or experimental data, and model-driven reconstructions of synthetic or experimental data.

# Experimental Dataset Availability

It is planned that correlative experimental datasets for semiconductor structures will be made available here in the near future. These will include:

* [An SiGe finFET device structure](c3d_input/sige_finfet)
* [An SiGe multi-quantum well structure](c3d_input/sige_multilayer)

Example pipelines will also be provided (see above links), demonstrating the model-driven reconstruction protocol and model parameter tuning through similarity optimisation. 

# Introduction to Casra 3D

Casra 3D is a tool targeted at APT users which primarily aims to allow rapid simulation of APT experiments in comparison to FEM based models, and allow users to harness the simulated trajectory data to drive distortion-correcting reconstruction of experimental datasets. In APT, the apex of a tip shaped sample is field evaporated by an applied electric field and these evaporated ions collected on a 2D detector. The sample shape behaves as a magnifying lens allowing the original position of ions to be estimated from its position in the arrival sequence of ions and its 2D detector coordinates in an imaging process is known as reconstruction. 

The conventional point-projection reconstruction approach used assumes that ion trajectories obey a mathematical projection between a hemispherical sample apex and detector plane. While often a reasonable approximation for samples consisting of homogeneous material, in reality the sample geometry is known to dynamically evolve depending on the instantaneous surface electric field and a set of local material parameters. In addition experiments are often performed in a 'laser mode' and in these cases the non-uniform incidence of a laser provides additional evaporation effects.

At its core, Casra 3D provides a 3D model that attempts to simulate this change in the sample geometry during an APT experiment based off the community's current knowledge of the physics of field evaporation. While APT simulation efforts have typically focused on trying to capture the entire range of phenomena and image distortions observed during field evaporation, Casra 3D instead attempts to model the specimen as a continuum. While this framework fails to capture the atomic-scale phenomena occuring during single evaporation events, it does allow for the rapid simulation of APT experiments at realistic lengthscales. This in turn opens up the possibility of calibrating such models to real experimental data, and using these calibrated models to derive a realistic ion projection or trajectory mapping for generating reconstructions.

Currently, in order to construct the initial model, users must provide a geometry for the sample (either manually defined using provided analytic functions or provided by electron tomography), values for phase-specific field evaporation parameters, and information on the instrument geometry (flightpath and detector radius).

The methods contained within this tool are still in the early stages of development and a wide range of future extensions and improvements are planned: from straightforward documentation, to implementing more accurate numerical schemes, incorporating new phenomena and evaporation physics, and finally the further automation of the data processing and model calibration. Please get in contact if you run into a problem or would like certain functionality to be included. 

# Initial Setup

This tool is best suited to running on a Unix-based system, with the setup process outlined below tested with Ubuntu. If using Windows, the tool can be compiled and run from an Ubuntu terminal (which can be installed from the Microsoft Application Store). 

## Python setup
Casra_3d requires Python 3.6 or above (although so far it has only been thoroughly tested with Python 3.6). In order to setup the tool a number of dependencies must be installed. These include:

required by core modules:
* numpy
* scipy
* skimage
* cython
* CGAL
* tbb

required by non-core modules:
* matplotlib
* _pandas_
* _medpy_ (for processing .rec Electron Tomography dat files)

optional dependencies:
* plotly (required for interactive plots)

The italicized dependencies are currently only required for the `tools.ET_loader` and `tools.species distribution` (only for interactive plots) submodules, and are set to be removed from `tools.ET_loader` entirely in future releases. 

To install these on :

* To install dependencies via apt on the command line:

```{bash}
$ sudo apt install libcgal-dev python3-skimage python3-scipy python3-numpy python3-matplotlib python3-pandas python3-plotly cython3 libtbb-dev 
```

Otherwise, Python packages can also be installed via pip or conda-forge. When installing dependencies on computing clusters, CGAL can also be installed through conda-forge. 

## Cython Compilation

In order to use casra 3d, the package must first be built. This can be done by running the setup.py file when in the casra_3d directory (see command below).

```{bash}
python3 setup.py build_ext --inplace
```

This setup.py file (similar to a c make file) will generate the compiled Cython and c++ within the correct directories.


## Core Modules

The module structure in Casra 3D is as follows

### Core modules:

**casra_3d**  
├── perform_projection.py - [x]  
├── **point_projection_reconstruction** - [x]  
│   ├── method.py - [x]  
├── **reconstruction**  
│   ├── landmark_fitting.py - [x]  
│   ├── model_reconstruction.py - [x]  
├── tip_simulation.py - [x]  


### Non-core modules:
  
├── **tools**  
    ├── anisotropy_plot.py - [x]  
    ├── detector_stack.py - [x]  
    ├── ET_loader.py - [x]  
    ├── evaluate_similarity.py - [x]  
    ├── fit_point_projection_model.py   
    ├── reconstruction_composition_calculation.py - [x]  
    ├── reconstruction_mapping.py - [x]  
    ├── species_distribution.py - [x]  
    └── voltage_curve.py - [x]  
    ├── species_distribution.py - [x]  
├── **geometries**  
    ├── generate_model_geometry.py   

Further information regarding specific module functions can be found through its docstrings (`python casra_3d.module.__doc__`). Additionally, modules marked with - [x] contain command line functionality where certain module functionality can be called directly from the command line. Information regarding the module's command line functionality and the possible inputs and outputs can be found via the shell command `bash Python3 -m casra_3d.desired_module -h`. 

It is worth noting that only front-end modules have been reported here. A number of functions in mid/back-end modules are also directly accesseable (such as data readers and writers). More information about these functions can be found through the docstrings. 

# Simulation Setup 

## The Materials database

The materials database contains the evaporation parameters associated with particular phases. This includes information regarding evaporation fields and crystallographic parameters. The database structure is outlined below:

![database structure](https://gitlab.com/fletchie/tip_simulations/raw/package/casra_3d/wiki/database_schema.png)

New materials can be added to the database by modifying the sqlite database fields through an appropriate editor e.g. sqlitestudio for Windows or SQLbrowser for Ubuntu. 

The main table contains unique material ids corresponding to particular material phases. The isotropic component of the evaporation field is also defined here. The crystallographic structure table contains the information required to construct anisotropic evaporation field functions. Finally, element\_ids contains a table mapping particular ions within reconstructions to a unique species\_id. 

## Initialising the Sample Geometry (The .mstate File)

In order to simulate the evaporation of a specific sample, the user must initialise the tip geometry. Currently, there are two possible approaches to model initialisation:

* Analytically constructing the tip geometry using the [geometries module](#https://gitlab.com/fletchie/tip_simulations/raw/package/casra_3d/geometries)
* Inputting the tip geometry as a 3D TIFF stack or .rec file through [ET_loader](#https://gitlab.com/fletchie/tip_simulations/raw/package/casra_3d/tools/ET_loader.py). These can be obtained from experiment such as Electron Tomography.

Once generated, the .mstate file is passed via the command line argument `-mstate` when calling `python tip_simulation.py` or `python perform_projection.py`. Remember that further information on how to use these modules can be found via `bash Python3 -m casra_3d.desired_module -h` or `python casra_3d.module.__doc__`. 

The output of these modules is what is known as a *.mstate* txt file. This file stores a series of meshes, each defining a separate phase within the model. The structure of an example file is given below:

```{bash}
OBJT:  
phasename1  
N (the vertex count)  
M (the faces count)  
1 (whether the sample surface ,value=1, or not, value=0)  
v{0,x} v{0,y} v{0,z}  
...  
v{N-1,x} v{N-1,y} v{N-1,z}  
  
f{0,v1} f{0,v2} f{0,v3}  
...  
f{M,v1} f{M,v2} f{M,v3}  

OBJT:  
phasename2  
...  
```

On running the model, additional fields are also saved to the .mstate file to avoid having to be rebuild these fields every time the same sample geometry is run. These include the initial level-set signed distance field and the grid of local phases. These fields are saved in the following structure below any OBJT fields:

```{bash}
FIELD:  
dimx (number of cells along x dimension)  
dimy (number of cells along y dimension)  
dimz (number of cells along z dimension)  

F{0,0,0} F{0,0,1} F{0,0,2} ...  
F{0,1,0} F{0,1,1} F{0,1,2} ...  
...  
F{dimx,dimy,0} F{dimx,dimy,1} ...  

PGRID:  
dimx (number of cells along x dimension)  
dimy (number of cells along y dimension)  
dimz (number of cells along z dimension)  
xmin (unscaled minimum x coordinate from input model)  
xmax (unscaled maximum x coordinate from input model)  
ymin (unscaled minimum y coordinate from input model)  
ymax (unscaled maximum z coordinate from input model)  
zmin (unscaled minimum y coordinate from input model)  
zmax (unscaled maximum z coordinate from input model)  

PG{0,0,0} PG{0,0,1} PG{0,0,2} ...  
PG{0,1,0} PG{0,1,1} PG{0,1,2} ...  
...  
PG{dimx,dimy,0} PG{dimx,dimy,1} ...  
```

These fields will be automatically constructed when first running the program if undetected within the .mstate file. 

## Materials Reference File

Once the .mstate file has been defined, the particular phases have to be assigned particular material parameters. This is achieved through the *.mref* tsv file which matches particular phases within the *.mstate* file to unique ids within a materials sqlite database. This allows particular phases, both monatomic and compound, to be initialised through a single integer by referencing a database containing all the required evaporation parameters. Model phases that define voids are initialised by linking the phase\_name to the vacuum mat_id 0 in the *.mref* file.

Once generated, the .mref file is passed via the command line argument `-mref` when calling `python tip_simulation.py` or `python perform_projection.py`.

The *.mref* file has the following structure: 

```{bash}
| phase_name        |      mat_id        |  alpha    | beta | gamma |  
vacuum	0	0	0	0  
phasename1	2	0	0	0  
phasename2	3	0	0	0  
```

The phase\_name column refers to the phase names within *.mstate* file. The mat\_id column contains the unique ids in the *main* table of the materials database. The angles alpha, beta, and gamma (provided in degrees not radians!) correspond to grain rotation/misorientation (only important if the linked material is crystalline and has an anisotropic evaporation field).


```
## Global Parameters 

Additional simulation parameters are contained within a parameters tsv structured file. The parameters file is passed via the command line argument `-parameters` when calling `python tip_simulation.py` or `python perform_projection.py`. An example parameters file demonstrating the structure, and containing all possible global arguments and a brief description, is given below:  

```{bash}

#This is a file defining simulation parameters. Undefined values will resort to default values:

parameter	value	description
beta	20.0	temperature parameter in field evaporation law
amplitude	5e-5	amplitude parameter in field evaporation law
gamma	1.0	The angular dependent scaling constant within the anisotropic law. In future versions this will be temperature dependent
delta	1.0	The facet dependent scaling constant within the anisotropic law
scale	0.3e-9	The lengthscale set by level-set grid
flightpath	0.12	The instrument flightpath (set by default to to that of the Lawatap)
CFL	0.2	The CFL time fraction per integration
BEM_tol	225.0	The Distance at which to switch from 7-panel integration to 1-panel integration
mapping_frac	5	Number of iterations between ion projections
initial_blurring	1	The level-set field blurring applied prior to running the simulation
grid_width	2e-9	The grid width parameter
shank_detached	0	Whether to detach the shank in the simulation
base_disable	-1	Whether to evolve or fix shank evolution (1 - fraction of shank disabled, 0 - full tip evolution, -1 - only evolve apex (default))
atomic_volume	0.25	The ionic volume (in nm^3) used during ion projection (relevant when setting weighted_projection = True and real_projection = True for the command line arguments in perform_projection.py)
min_shank_length	3.0e-7	The minimum tip length allowed, after which the shank will be extended from the model base. Ensures consistent trajectory lensing. Negative value implies no extension.
shank_angle	8.87	The shank angle to use for performing the model shank extension. A good estimate can be obtained using functions in the fit_point_projection_model.
initial_shank_extension	20	The number of level set grid cells to initially extend the model shank by
total_ions	10000	The total number of ions to project off the sample surface (relevant when setting weighted_projection = True and real_projection = False for the command line arguments in perform_projection.py)
adaptive_grid	1	Whether to use an adaptive mesh for the tip (1) or not (0)
reinit	30	The level set field reinitialisation rate	

```

# Running Examples

In order to familiarise the user with the program, a number of examples are included. The authors recommend going through the Jupyter Notebook examples that can be found within the `getting\_started` directory. These examples can also be run through a shell script (.sh) that can be found in the example directories in `c3d\_input/example`. These scripts call the same examples but from the command line. 

The examples can be called via executing the following shell scripts:

## FinFET example

Simulates evaporation of a finFET geometry (as seen in Melkonyan et. al. 2017) up to a certain height, performs uniform ion projection, and generates detector isolines capturing the local distortions (the sample detector mapping).

```{bash}

sh c3d_input/examples/finfet/run_finfet.sh

```

## Grain Boundary example

Simulates the evaporation of a horizontal grain boundary for an anisotropic tungsten material. A synthetic APT dataset is then generated from this geometry and a calibrated point-projection reconstruction is performed.

```{bash}

sh c3d_input/examples/grain_boundary/run_grain_boundary.sh

```

## multilayer example

Simulates the evaporation of a multilayer geometry up to a certain number of iterations, performs uniform ion projection, and generates detector isolines capturing the local distortions (the sample detector mapping).

```{bash}

sh c3d_input/examples/finfet/run_finfet.sh

```


## precipitates example

Simulates the evaporation of a central precipitate structure and generates a synthetic APT dataset. A uniform ion projection is also performed and this trajectory mapping information is used to perform a model driven reconstruction of the synthetic APT dataset (outputted in `c3d\_output/examples/precipitate/recon`). A point projection reconstruction is also performed (outputted in `c3d\_output/examples/precipitate/pprecon`). Landmark calibration has been set automatically for this example and determined by the authors by looking at the output file species\_distribution.html (or species\_distribution.png if plotly is not installed) (generated in `c3d_output/examples/precipitate`).

```{bash}

sh c3d_input/examples/precipitate/run_precipitate.sh

```

## void example

Simulates the evaporation of a void structure.

```{bash}

sh c3d_input/examples/void/run_void.sh

```


# Getting in Touch

If you run into any issues or have any questions about Casra 3d, then please do not hesitate to get in touch either through gitlab or email: _charles.fletcher@materials.ox.ac.uk_.


