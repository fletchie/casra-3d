
"""

The module for performing ion projection for given data returned by the simulated
model evaporation (stored in the .vtk output).

Authors: Charles Fletcher, Daniel Haley
Atom Probe Tomography Group, Department of Materials, University of Oxford
May 2020

"""

import sys
import os

from casra_3d.io import model_loaders as model_loaders
from casra_3d.io import model_writers as model_writers
from casra_3d.initialisation import tip_initialiser as tip_initialiser
from casra_3d.initialisation import field_initialiser as field_initialiser
from casra_3d.core import run as run

#check python version
VER = sys.version_info
if VER[0] != 3 or VER[1] < 6:
    print("WARNING: Python version " + str(VER[0]) + "." + str(VER[1]) +
          " detected. Package has so far only been tested with Python versions > 3.6." +
          " May result in inconsistant behaviour.")

def project(filename, out_dir, global_parameters_filename, mref_filename,
            materials_db, its, dirname,
            weighted_projection=False, para=True,
            real_projection=True, write_trajectories=True, real_detector=False):

    """

    Function for performing ion projection from the sample surface every set
    number of model iterations (the mapping_frac parameter)

    :param1 filename (str): The .mstate file defining the sample geometry
    :param2 out_dir (str): The output directory in which to dump the .vtk simulation output files
    :param3 global_parameters_filename (str): The tsv of model parameters (see help for
                                              tip_initialiser or wiki for more information).
    :param4 mref_filename (str): The tsv file matching phase names to database material ids.
                                 Also contains Euler angles for grain rotation.
    :param5 materials_db (str): The SQLite materials database
    :param6 its (int): The total number of iterations (termination condition)
    :param7 dirname (str): The directory in which the simulated model evaporation
                           data (.vtks) are located.
    :param8 weighted_projection (bool): Whether to perform a weighted ion projection
    :param9 para (bool): Whether to parallelise the ion projection (option only
                         applies to weighted_projection = False -
                         e.g. deriving reconstruction mapping)
    :param10 real_projection (bool): Whether to perform ion projection proportional
                                     to the evaporated model volume
    :param11 write_trajectories (bool): Whether to write trajectories to VTK.
    :returns:
        :(int): 0

    :raises:
        None

    """

    model_parameters = tip_initialiser.load_parameters(global_parameters_filename)
    model_geom = model_loaders.load_mstate(filename)
    model_geom["parameters"] = model_parameters

    tip_initialiser.load_mref(mref_filename, model_geom)
    field_initialiser.dimensions_from_geometry(model_geom,
                                               grid_width=model_parameters["grid_width"],
                                               scale=model_parameters["scale"])

    print("Input tip geometry directory: " + filename)
    print("Output directory: " + out_dir)

    surface = {"verts": [], "faces": [], "cp": [], "phase": [], "areas": [],
               "normals": [], "pot": [], "flux": [], "smflux": [], "time": [],
               "sample_surface": [], "smoothed_velocity": [], "volume": [],
               "iteration_stability": []}

    model_loaders.load_simulation(dirname, surface, its)

    ###perform input mesh checks
    for a in range(len(model_geom["objts"])):
        #check detector is not inside specimen
        if model_geom["objts"][a]["surface"] == 1:
            det_pos = [0.0, 0.0, model_parameters["flightpath"]]
            res = tip_initialiser.check_tip_detector_proximity(model_geom["objts"][a]["verts"],
                                                               model_geom["objts"][a]["faces"],
                                                               det_pos,
                                                               model_parameters["scale"])
        if res == 1:
            print("WARNING: detector flightpath on a similar scale to specimen size. " +
                  "Are you sure the specimen scale or flightpath has been set correctly")
        elif res == 2:
            raise ValueError("Detector placed inside specimen mesh.")

    ###model initialisation
    projection = {"num": [], "sample_space": [], "detector_space": [],
                  "vel": [], "chemistry": [], "ion_phase": [],
                  "trajectories": [], "panel_index": []}

    stability_info = {"num": [],
    "sv0": [],
    "sv1": [],
    "sv2": [],
    "dv0": [],
    "dv1": [],
    "dv2": [],
    "vel0": [],
    "vel1": [],
    "vel2": [],
    "jacobian": [],
    "conformality": [],
    "J00": [],
    "J10": [],
    "J01": [],
    "J11": [],
    "evap_rate": [],
    "panel_index": []}

    if its > len(surface["verts"]):
        its = len(surface["verts"])

    total_ions = model_parameters["total_ions"]

    if weighted_projection == True:
        run.run_rate_weighted_projection(model_geom, model_parameters["flightpath"],
                                         1e-5, 1e-5, projection, surface, 0, its,
                                         mapping_frac=model_parameters['mapping_frac'],
                                         atomic_volume=model_parameters['atomic_volume'],
                                         BEM_adist=model_parameters['BEM_tol'],
                                         total_ions=total_ions,
                                         real_projection=real_projection,
                                         real_detector=real_detector)
    else:
        run.run_projection(model_geom, model_parameters["flightpath"],
                           1e-5, 1e-5, projection, stability_info, surface,
                           0, its, mapping_frac=model_parameters['mapping_frac'],
                           BEM_adist=model_parameters['BEM_tol'], para=para,
                           real_detector=real_detector)


    if not os.path.exists(out_dir + "/projection_data"):
        os.makedirs(out_dir  + "/projection_data")

    model_writers.output_projection_tsv(projection,
                                        filename=out_dir + "/projection_data")

    try:
        #save stability info if generated
        if len(stability_info["num"]) > 0:
            print("output stability data")
            model_writers.output_stability_data_tsv(stability_info,
                                                    filename=out_dir + "/projection_data")
    except:
        pass

    model_writers.output_trajectories_vtk(projection, filename=out_dir + "/projection_data")
    model_writers.output_hitmap_vtk(projection, filename=out_dir + "/projection_data")

if __name__ == "__main__":

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--tip_geom', metavar='tip geometry', type=str, default="",
                        help='The .mstate file directory defining the tip geometry')
    parser.add_argument('--out', metavar='output directory', type=str, default="",
                        help='The output directory in which to dump the simulation output files')
    parser.add_argument('--parameters', metavar='parameters', type=str, default="",
                        help='The .tsv file containing simulation parameter values')
    parser.add_argument('--mref', metavar='materials reference file', type=str, default="",
                        help='The .mref file linking phases in .mstate to the materials db')
    parser.add_argument('--mat_db', metavar='materials SQLite database', type=str,
                        default="c3d_input/materials",
                        help='The materials db')
    parser.add_argument('--it', metavar='it', type=int, default=11,
                        help='The projection to perform up before termination')
    parser.add_argument('--dirname', metavar='directory name', type=str, default="",
                        help='The simulation directory name')
    parser.add_argument('--weighted_projection', metavar='weighted_projection',
                        type=str, default="False",
                        help='Whether to simulate APT like data (True) or ' +
                        'perform uniform projection (False)')
    parser.add_argument('--write_trajectories', metavar='write_trajectories',
                        type=str, default="True",
                        help='Whether to write individual ion trajectories to VTK')
    parser.add_argument('--parallelisation', metavar='parallelisation', type=str, default="False",
                        help='Whether to parallelise the ion projection')
    parser.add_argument('--real_projection', metavar='real projection', type=str, default="False",
                        help='For weighted_projection = True, Whether to project a number of' +
                        'ions proportional to the evaporated volume (atomic_volume) or a total ' +
                        'number of ions')
    parser.add_argument('--real_detector', metavar='real detector', type=str, default="False",
                        help='Whether to launch ions all the way up to the detector (True) or not (False).' +
                              'In the case of "False", a transfer function will be applied following a threshold' +
                              'distance.')
    args = parser.parse_args()

    #simulation parameter definition
    filename = args.tip_geom
    out_dir = args.out
    global_parameters_filename = args.parameters
    mref_filename = args.mref
    materials_db = args.mat_db
    its = args.it
    dirname = args.dirname

    EMPTY = ""

    if filename is EMPTY:
        raise ValueError("mstate file must be parsed to --tip_geom argument")
    if out_dir is EMPTY:
        raise ValueError("output directory must be parsed to --out argument")
    if global_parameters_filename is EMPTY:
        raise ValueError("model parameters file must be parsed to --parameters argument")
    if mref_filename is EMPTY:
        raise ValueError("model reference file must be parsed to --mref argument")
    if dirname is EMPTY:
        raise ValueError("model simulation directory must be parsed to --dirname argument")

    if (args.parallelisation == "True" or
            args.parallelisation == "true" or
            args.parallelisation == "1"):
        parallelisation = True
    else:
        parallelisation = False
    if (args.weighted_projection == "True" or
            args.weighted_projection == "true" or
            args.weighted_projection == "1"):
        weighted_projection = True
    else:
        weighted_projection = False
    if (args.real_projection == "True" or
            args.real_projection == "true" or
            args.real_projection == "1"):
        real_projection = True
    else:
        real_projection = False
    if (args.write_trajectories == "True" or
            args.write_trajectories == "true" or
            args.write_trajectories == "1"):
        write_trajectories = True
    else:
        write_trajectories = False
    if (args.real_detector == "True" or
            args.real_detector == "true" or
            args.real_detector == "1"):
        real_detector = True
    else:
        real_detector = False

    project(filename, out_dir, global_parameters_filename, mref_filename,
            materials_db, its, dirname, weighted_projection=weighted_projection,
            para=parallelisation, real_projection=real_projection,
            write_trajectories=write_trajectories, real_detector=real_detector)
