#!/bin/bash

if [ ! -d casra_3d ] ; then
	echo " please run script from top level folder of casra repository: could not find casra_3d in current dir."
	exit 1
fi

start=$(date +'%s')
echo "Run oxide tracer model pipeline"

#Example directories
EXDIRIN=c3d_input/examples/oxide_tracer
EXDIROUT=c3d_output/examples/oxide_tracer


#Generate finfet structure/model
python3 ${EXDIRIN}/generate_oxide_tracer.py  || { echo "Failed to generate oxide tracer structure" ; exit 1 ; }

#simulate finfet evaporation
python3 -m casra_3d.tip_simulation --mat_db c3d_input/materials --tip_geom ${EXDIRIN}/oxide_tracer.mstate --mref ${EXDIRIN}/oxide_tracer.mref --parameters ${EXDIRIN}/parameters --it 1000 --out ${EXDIROUT} --termination_height 4.1e-7 || { echo "oxide tracer evaporation failed" ; exit 1 ; }

#project ions to derive reconstruction mapping
python3 -m casra_3d.perform_projection --tip_geom ${EXDIRIN}/oxide_tracer.mstate --dirname ${EXDIROUT} --mref ${EXDIRIN}/oxide_tracer.mref --it 1000 --parameters ${EXDIRIN}/parameters --parallelisation True --real_projection True --weighted_projection True --write_trajectories false --out ${EXDIROUT} || { echo "oxide tracer projection failed" ; exit 1 ; }

#derive detector isolines to visualise local distortions
#python3 -m casra_3d.tools.reconstruction_mapping --mapping_data ${EXDIROUT}/projection_data/projection_data.tsv --det_radius 0.038 --dirname ${EXDIROUT}/reconstruction || { echo "Failed to generate detector mapping isolines" ; exit 1 ; }

python3 -m casra_3d.point_projection_reconstruction.method --input_data ${EXDIROUT}/projection_data/projection_data.tsv --output ${EXDIROUT}/reconstruction --parameters ${EXDIRIN}/pprecon_parameters.tsv 

finish=$(date +'%s')
duration=$((finish-start))
hrs=$(( duration/3600 )); mins=$(( (duration-hrs*3600)/60)); secs=$(( duration-hrs*3600-mins*60 ))
printf 'Time spent: %02d:%02d:%02d\n' $hrs $mins $secs

