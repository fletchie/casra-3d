#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#add current working directory to system path
import os,sys,inspect
sys.path.insert(0,'')

from casra_3d.geometries import generate_model_geometry as generate_model_geometry
import numpy as np

#model parameters
#scale = 3.5e-8
###example commmands###

filename = "c3d_input/examples/void/void.mstate"
directory = "c3d_input/examples/void"

generate_model_geometry.generate_tip(3.0, 1.0, 6.0, filename, directory, name = "bulk", offset = 0.2)

#place void
generate_model_geometry.generate_precipitate(np.array([0.2, 0.2, 6.1]), 0.22,  filename, "phase1", directory)
