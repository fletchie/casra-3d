#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#add current working directory to system path
import os,sys,inspect
sys.path.insert(0,'')

from casra_3d.geometries import generate_model_geometry as generate_model_geometry
import numpy as np

###example commmands###
filename = "c3d_input/examples/finfet/finfet.mstate"
directory = "c3d_input/examples/finfet"

#simulation parameters - for conversion
#scale = 35nm

generate_model_geometry.generate_tip(4.0, 0.7, 12, filename, directory, name = "bulk", offset = 0.2)

#si base layer
p0 = np.array([-10.0, -0.55- 0.31- 0.31, 12.52 - 0.829 - 3.72 * 2.0])
p1 = np.array([10.0, -0.55- 0.31- 0.31, 12.52 - 0.829 - 3.72 * 2.0])
p2 = np.array([-10.0, 0.45+ 0.39+ 0.39, 12.52 - 0.829 - 3.72 * 2.0])
p3 = np.array([10.0, 0.45+ 0.39+ 0.39, 12.52 - 0.829 - 3.72 * 2.0])

p4 = np.array([-10.0, -0.55 - 0.31, 12.52 - 0.829 - 3.72])
p5 = np.array([10.0, -0.55  - 0.31, 12.52 - 0.829 - 3.72])
p6 = np.array([-10.0, 0.45 + 0.39, 12.52 - 0.829 - 3.72])
p7 = np.array([10.0, 0.45 + 0.39, 12.52 - 0.829 - 3.72])

generate_model_geometry.generate_trapezoid(p0, p1, p2, p3, p4, p5, p6, p7, -3.5, 0.0, 0.0, filename, directory, "phase1")

#siGe fin layer
p0 = np.array([-10.0, -0.55- 0.31, 12.52 - 0.829 - 3.72])
p1 = np.array([10.0, -0.55- 0.31, 12.52 - 0.829 - 3.72])
p2 = np.array([-10.0, 0.45+ 0.39, 12.52 - 0.829 - 3.72])
p3 = np.array([10.0, 0.45+ 0.39, 12.52 - 0.829 - 3.72])

p4 = np.array([-10.0, -0.55, 12.52 - 0.829])
p5 = np.array([10.0, -0.55, 12.52 - 0.829])
p6 = np.array([-10.0, 0.45, 12.52 - 0.829])
p7 = np.array([10.0, 0.45, 12.52 - 0.829])

generate_model_geometry.generate_trapezoid(p0, p1, p2, p3, p4, p5, p6, p7, -3.5, 0.0, 0.0, filename, directory, "phase2")

#Ge cap
generate_model_geometry.generate_cuboid(-10.0, 10.0, -10.0, 10.0, 12.52 - 0.829, 12.52 - 0.829 + 0.57, 0.0, 0.0, 0.0, filename, directory, "phase3")
#Si cap
generate_model_geometry.generate_cuboid(-10.0, 10.0, -10.0, 10.0, 12.52 - 0.829 + 0.57, 12.52 - 0.829 + 0.57 + 0.257, 0.0, 0.0, 0.0, filename, directory, "phase4")
