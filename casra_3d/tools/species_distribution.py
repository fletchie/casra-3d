
"""

Module for creating bin species across the detector space
Also outputs voltage curve for experimental data

Author: Charles Fletcher
Atom Probe Tomography Group, Department of Materials, University of Oxford
May 2020

"""

import imp
import os
import math
import sys

try:
    import numpy as np
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python package numpy. " +
                              "Please make sure numpy is installed.")

plotly_installed = True

try:
    import pandas as pd
except ModuleNotFoundError:
    print("Cannot find Python package pandas. Please make sure pandas " +
          "and plotly are both installed to use interactive plotly plots. " +
          "Falling back to matplotlib.")
    plotly_installed = False

try:
    import plotly.express as px
except ModuleNotFoundError:
    print("Cannot find Python package plotly. Please install for " +
          "interactive histogram functionality. \n Falling back to " +
          "matplotlib functions.")
    plotly_installed = False
    try:
        import matplotlib.pyplot as plt
    except ModuleNotFoundError:
        raise ModuleNotFoundError("Cannot find Python package matplotlib. " +
                                  "Please make sure either plotly or " +
                                  "matplotlib is installed.")

from casra_3d.point_projection_reconstruction import point_projection_mapping as ppm
from casra_3d.point_projection_reconstruction import loader as loader
from casra_3d.io import model_writers as model_writers
from casra_3d.io import model_loaders as model_loaders

def ion_histogram(filename, out_dir, rangefile, bins,
                  det_rad=math.inf, autoopen=False):

    """

    Function for generating the ion histogram from the APT data
    If plotly is installed, an interactive plot is outputted as a .html
    Also outputs interactive plotly voltage curve for experimental data

    :param1 filename (str): The input APT data
    :param2 out_dir (str): The output directory
    :param3 rangefile (str): The range file for the APT data
    :param4 bins (int): The number of histogram bins
    :param5 det_rad (float): The detector radius (default is
                             infinity i.e. all ions included)

    :returns:
        None

    :raises:
        :ValueError: No input APT data file found

    """

    #create output directory if not found
    if not os.path.exists(out_dir):
        print("creating output directory: " + out_dir)
        os.mkdir(out_dir)

    #load APT data - inc range data
    print("Loading dataset...")
    (dx, dy, sx, sy, sz, chem, ions, ic, 
     VP, da, da_p, experimental) = model_loaders.load_apt_data(filename, rangefile)
    print("dataset loaded")

    chem = chem.astype(int)
    chem = np.array(chem)
    VP = np.array(VP)

    #filter out ions outside of the detector radius
    rad = dx**2.0 + dy**2.0
    dx = dx[rad < det_rad**2.0]
    dy = dy[rad < det_rad**2.0]
    chem = chem[rad < det_rad**2.0]

    if len(VP) == len(rad):
        VP = VP[rad < det_rad**2.0]

    real_data = False
    #remove unranged ions
    if len(ions) != 0:
        fchem = chem[chem != 0]
        VP = VP[chem != 0]
        real_data = True
    else:
        fchem = chem
        real_data = False

    ion_seq = np.arange(0, len(fchem), 1)
    if len(ions) == 0:
        ions = np.unique(chem)

    #create histogram arrays
    hist_val = np.zeros((len(ions), bins))
    
    #construct histogram
    if real_data is True:
        for j in range(0, len(ions)):
            hist, bin_edges = np.histogram(ion_seq[fchem == j + 1],
                                           bins=bins, range=(0, len(fchem)))
            hist_val[j, :] += hist
            c = (bin_edges[:-1] + bin_edges[1:])/2.0
    else:
        for j in range(0, len(ions)):
            hist, bin_edges = np.histogram(ion_seq[fchem == ions[j]],
                                           bins=bins, range=(0, len(fchem)))
            hist_val[j, :] += hist
            c = (bin_edges[:-1] + bin_edges[1:])/2.0
            
    #output interative plotly visualisations if plotly installed
    if plotly_installed is True:
        #create dataframe and output plotly figure
        df = pd.DataFrame(data=np.vstack((c, hist_val)).T,
                          columns=np.append(["detected ion number"],
                          ions))
        pdf = pd.melt(df,
                      id_vars="detected ion number",
                      var_name="species",
                      value_name="total counts")
        fig = px.line(pdf,
                      x="detected ion number",
                      y="total counts",
                      color='species')
        fig.write_html(out_dir + '/species_histogram.html',
                      auto_open=autoopen)
        print("Successfully outputed plotly interactive species " +
              "histogram to: " + out_dir + '/species_histogram.html')

        pdv = pd.melt(df, id_vars="detected ion number",
                     var_name="species",
                     value_name="total counts")

        if len(VP) == len(ion_seq):
            #output voltage curve
            vdf = pd.DataFrame(data=np.vstack((ion_seq[::10], VP[::10])).T,
                               columns=["detected ion number", "Voltage Curve (kV)"])
            fig = px.line(vdf,
                          x="detected ion number",
                          y="Voltage Curve (kV)")
            fig.write_html(out_dir + '/voltage_curve.html',
                           auto_open=False)
            print("Successfully outputed plotly interactive " +
                  "voltage curve to: " + out_dir + '/voltage_curve.html')

    else:
        #output matplotlib visualisations (plotly installation not detected)
        for a in range(0, len(hist_val)):
            plt.plot(c, np.log(hist_val[a]), label=ions[a])
        plt.legend()
        plt.xlabel("detected event index")
        plt.ylabel("species count")
        plt.savefig(out_dir + "/species_histogram.png")
        print("Successfully outputed matplotlib species histogram " +
              "to: " + out_dir + "/species_histogram.png")
        plt.close()

        plt.plot(VP)
        plt.ylim(0, max(VP))
        plt.xlabel("detected event count")
        plt.ylabel("evaporation voltage")
        plt.savefig(out_dir + "/voltage_curve.png")
        print("Successfully outputed matplotlib voltage curve " +
              "to: " + out_dir + '/voltage_curve.png')
        plt.close()

#argument parser for if called from command line
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('--input_data', metavar='input data',
                        type=str, default="",
                        help='The input APT data (either .tsv, .epos or .ato).')
    parser.add_argument('--output', metavar='', type=str, default="output/species_hist",
                        help='The output directory where the species histogram ' +
                        'will be saved.')
    parser.add_argument('--rangefile', metavar='range file', type=str,
                        default = "",
                        help='The range file required for APT experimental ' +
                        'data for labelling ions')
    parser.add_argument('--bins', metavar='bin number', type=int,
                        default=120, help='Number of histogram bins')
    parser.add_argument('--det_rad', metavar='detector radius',
                        type=float, default=math.inf,
                        help='The detector radius. Default is infinity ' +
                        '(all ions included).')

    args = parser.parse_args()

    #simulation parameter definition
    filename = args.input_data
    if not filename:
        raise ValueError("No input APT data filename found. " +
                         "Please pass in filename.")

    out_dir = args.output
    rangefile = args.rangefile
    bins = args.bins
    det_rad = args.det_rad

    ion_histogram(filename, out_dir, rangefile, bins, det_rad)
