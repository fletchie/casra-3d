
"""

Warning: Errors arise visualising integer point and cell properties for vtks
in early versions of paraview (e.g. 5.4.1). Make sure to use the latest version.

Module for Constructing the phase maps for a particular atom probe dataset
Also outputs the detector space for the APT data as a .vtk file

Author: Charles Fletcher, Daniel Haley
Atom Probe Tomography Group, Department of Materials, University of Oxford
May 2020

"""

import os
import math
import sys

try:
    import numpy as np
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python package numpy. " +
                              "Please make sure numpy has been installed.")

try:
    import sklearn.cluster
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python package scikit-learn. "  +
                              "Please make sure sklearn has been installed.")

try:
    import scipy.ndimage
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python package scipy. " +
                              "Please make sure scipy has been installed.")

from casra_3d.point_projection_reconstruction import point_projection_mapping as ppm
from casra_3d.point_projection_reconstruction import loader as loader
from casra_3d.io import model_writers as model_writers
from casra_3d.io import model_loaders as model_loaders

def phase_map(ind, width, dx, dy, chem, ions, pnumber=2, bins=80):

    """

    Function for generating specific phasemap at experiment time
    defined by ion index position ind.
    Plan is to update method in the future to perform phase map
    calculation using KDE.

    :param1 ind (int): the experiment time index
    :param2 width (int): The number of ions to include in the phase map calculation
    :param3 dx (list): The detector x coordinates
    :param4 dy (list): The detector y coordinates
    :param5 chem (list): The ion chemistry (integer).
                         A value of 0 in this array indicates
                         that the ion was unranged.
    :param6 ions (str): The corresponding ion label
    :param7 bins (int): The number of grid cells to include
                        in the phase maps
    :return:
        :cout (np.array, (D1, D2), int): The filtered array of clustered phases

    :raises:
        None

    """

    #set width such that
    fdx = dx[max((ind - int(width/2)), 0):min((ind + int(width/2)), len(dx))]
    fdy = dy[max((ind - int(width/2)), 0):min((ind + int(width/2)), len(dx))]
    fchem = chem[max((ind - int(width/2)), 0):min((ind + int(width/2)), len(dx))]

    types = np.array([np.zeros((bins, bins))] * len(ions))

    #build species specific histogram
    for j in range(0, len(ions)):
        loc = fchem == j + 1
        ag, xx, xy = np.histogram2d(fdx[loc], fdy[loc],
                                    bins=bins,
                                    range=[[np.min(dx) * 1.05, np.max(dx) * 1.05],
                                           [np.min(dy) * 1.05, np.max(dy) * 1.05]])
        types[j] += ag

    #calculate compositions
    types = np.array(types)
    dat = (np.sum(types, axis=0) > 0).flatten()
    mtypes = np.max(types, axis=0) + 1e-12
    ntypes = types/mtypes
    grid_x, grid_y = np.mgrid[0:bins, 0:bins]
    norm_compositions = np.zeros((0, bins * bins))

    #construct composition stack based off number of ranged elements
    for ab in range(0, len(ntypes)):
        norm_compositions = np.vstack((norm_compositions,
                                       ntypes[ab].flatten()))

    ###number of clusters can be inferred from model
    if method == "kmeans":
        kmeans = sklearn.cluster.KMeans(n_clusters=3).fit(norm_compositions.T)
    else:
        kmeans = sklearn.cluster.DBSCAN(eps=0.13, min_samples=10,
                                        metric='euclidean').fit(norm_compositions.T)

    #output clustering algorithm
    cout = np.zeros(grid_x.shape, dtype=int)
    for a in range(0, len(kmeans.labels_)):
        if dat[a] == True:
            cout[int(a/bins), a % bins] = kmeans.labels_[a] + 1

    ##add filter step (reduce noise)
    coutf = scipy.ndimage.median_filter(cout, size=(3, 3))

    return coutf

def generate_phasemap(filename, out_dir, rangefile, bins, boffset,
                      toffset, spacing, width, pnumber, method):

    """

    Method for generating phase maps and writing the generated vtks to out_dir

    :parma1 filename: The input APT data file (must be either .tsv, .epos, or .ato)
    :parma2 out_dir: The output directory for .vtk phase maps
    :parma3 rangefile: The range file (if experimental APT data)
    :parma4 bins: The number of grid cells (width) making up the phase map
    :parma5 boffset: The number of ions ignored at the beginning of the APT data
    :parma6 toffset: The number of ions ignored at the end of the APT data
    :parma7 spacing: The spacing (number of ions) between phase maps
    :parma8 width: The number of ions included in calculating each phase map
    :parma9 method: A string defining the clustering method to use
                    (options either "KMeans or DBSCAN")

    :return: 0

    """

    #load APT data - inc range data
    print("Loading dataset...")
    dx, dy, sx, sy, sz, chem, ions, ic = model_loaders.load_apt_data(filename,
                                                                     rangefile)

    #create meshgrid for binning
    ddx = np.linspace(min(dx), max(dx), bins + 1)
    ddy = np.linspace(min(dy), max(dy), bins + 1)
    XX, XY = np.meshgrid(ddx, ddy)

    #create Delaunay tessellation
    points = np.vstack((XX.flatten(), XY.flatten())).T
    dhull = scipy.spatial.Delaunay(points)
    trcp = np.mean(points[dhull.simplices], axis=1)

    #scale to match grid coordinates
    gc_trcp = (bins) * (trcp - min(dx))/(max(dx) - min(dx))

    pts = []
    simplices = []
    out_phase_map = []

    for v in np.arange(boffset, len(dx) - toffset, spacing):
        #generate phase map at detector ion index v
        out = phase_map(v, width, dx, dy, chem, ions, bins)
        out_phase_map.append(out[gc_trcp[:, 0].astype(int),
                             gc_trcp[:, 1].astype(int)])
        simplices.append(dhull.simplices)
        pts.append(points)

    ###output phasemaps to .vtk
    #times corresponds to percentage through APT dataset
    times = np.arange(boffset, len(dx) - toffset)/len(dx) * 100.0
    model_writers.output_phasemap_vtk(out_phase_map, pts, simplices,
                                      times, out_dir, True)

#argument parser for if called from command line
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('--input_data',
                        metavar='input data',
                        type=str,
                        default="",
                        help='The APT data file (options ' +
                             'either .tsv, .epos or .ato)')
    parser.add_argument('--output', metavar='output directory', type=str, default="",
                        help='The output directory for phase maps. \n ' +
                             'Will save: original coordinates to original.vtk, ' +
                             'reconstructed coordinates to recon.vtk')
    parser.add_argument('--rangefile',
                        metavar='The range file for labelling ions within the ' +
                                'reconstruction', type=str,
                                default="", help='The range file')
    parser.add_argument('--bins',
                        metavar='The detector bin resolution to use',
                        type=int,
                        default=80,
                        help='The grid width of the phase map')
    parser.add_argument('--boffset',
                        metavar='bottom offset',
                        type=int,
                        default=40000,
                        help='Number of ions to ignore at beginning of ' +
                             'experiment')
    parser.add_argument('--toffset', metavar='top offset',
                        type=int, default=40000,
                        help='Number of ions to ignore at end of experiment')
    parser.add_argument('--spacing',
                        metavar='ion spacing', type=int,
                        default=300000,
                        help='The spacing between phase maps (number of ions)')
    parser.add_argument('--thickness', metavar='ion thickness',
                        type =int, default=100000,
                        help='The number of ions considered per phase map')
    parser.add_argument('--method',
                        metavar='cluster method', type=str,
                        default="kmeans",
                        help='The clustering algorithm to use: ' +
                             'supports "kmeans" or "DBSCAN"')
    parser.add_argument('--thickness', metavar='ion thickness',
                        type=int, default=100000,
                        help='The number of ions considered per phase map')
    parser.add_argument('--phase_number', metavar='number of phases',
                        type=int, default=2,
                        help='The estimated nmber of phases ' +
                        '(required for kmeans). Better to be optimistic.')

    args = parser.parse_args()

    #simulation parameter definition
    filename = args.input_data
    out_dir = args.output
    rangefile = args.rangefile
    bins = args.bins
    boffset = args.boffset
    toffset = args.toffset
    spacing = args.spacing
    width = args.thickness
    method = args.method
    phase_number = args.phase_number

    if not filename:
        raise ValueError("input mapping .tsv file must be parsed to " +
                         "--input_data argument")
    if not out_dir:
        raise ValueError("output directory must be parsed to --output argument")

    generate_phasemap(filename, out_dir, rangefile, bins, boffset,
                      toffset, spacing, width, phase_number,method)
