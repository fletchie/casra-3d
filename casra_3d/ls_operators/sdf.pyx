
"""

Cython module for calculating signed distance fields

Module must be compiled prior to using casra_3d using the following command
when in the casra_3d directory:
`python3 setup.py build_ext --inplace`

Author: Charles Fletcher, Daniel Haley
Atom Probe Tomography Group, Department of Materials, University of Oxford
May 2020

"""

import cython

import numpy as np
cimport numpy as np
from cython.parallel import prange
from libcpp.vector cimport vector
from cpython.exc cimport PyErr_CheckSignals

#external function
cdef extern from "math.h" nogil:
    double sqrt(double m)

cdef extern from "math.h" nogil:
    double signbit(double m)

cdef extern from "math.h" nogil:
    double fabs(double m)

@cython.cdivision(True)
cdef inline double clamp(double v, double lo, double hi) nogil:
  if v < lo:
    return lo
  if hi < v:
    return hi
  return v

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def sdf(double[:, ::1] vertices, double[:, ::1] normal, long[:, ::1] faces, int x, int y, int z, int x0, int y0, int z0):

  """

  Function for calculating the signed distance field for a particular
  closed triangle mesh

  uses algorithm as described
  https://www.geometrictools.com/Documentation/DistancePoint3Triangle3.pdf
  Distance Between Point and Triangle in 3D, David Eberly

  This is based off the algorithm implemented in
  the Geometric Tools library https://www.geometrictools.com

  :param1 vertices (np.array, (M, 3), dtype=float): 2D array of mesh vertices
  :param2 normal (np.array, (N, 3), dtype=float): 2D array of mesh
                                                  triangle element normals
  :param3 faces (np.array, (N, 3), dtype=int): 2D array of the triangle
                                               mesh connectivity matrix
  :param4 x (int): x dimension for signed distance field grid
  :param5 y (int): y dimension for signed distance field grid
  :param6 z (int): z dimension for signed distance field grid
  :param7 x0 (int): x offset between grid and mesh positions
  :param8 y0 (int): y offset between grid and mesh positions
  :param9 z0 (int): z offset between grid and mesh positions

  :returns:
    :sdf (np.array,(L1,L2,L3), dtype=float): The constructed signed distance field

  :raises:
    None

  """

  cdef Py_ssize_t bmax = faces.shape[0]
  assert bmax

  sdf = np.zeros((x, y, z), dtype = np.double) + 1e12
  cdef double[:, :, ::1] sdf_view = sdf

  cdef double dist1 = x*x*x*x
  cdef double dist2 =  x*x*x*x
  cdef double tdist = 0.0

  cdef double c1x = 0.0
  cdef double c1y = 0.0

  cdef double c2x = 0.0
  cdef double c2y = 0.0

  cdef long mv = 0

  cdef Py_ssize_t i, j, k, va
  cdef Py_ssize_t xmax = x
  cdef Py_ssize_t ymax = y
  cdef Py_ssize_t zmax = z

  cdef double sb = 0.0
  cdef double sa = 0.0
  cdef double l = 0.0
  cdef double AB = 0.0

  cdef double sign = 0.0

  cdef double point[3]
  cdef double vec[3]
  cdef double edge1[3]
  cdef double edge2[3]

  cdef double det
  cdef double invdet
  cdef double v
  cdef double w
  cdef double u

  cdef double d00
  cdef double d01
  cdef double d11
  cdef double d20
  cdef double d21

  cdef double t
  cdef double s
  cdef double clp[3]

  cdef double numer
  cdef double denom

  cdef double tmp0
  cdef double tmp1

  cdef double narrowband = 2.5
  cdef int a, b, c, rv
  cdef bint br

  for i in range(xmax):
    PyErr_CheckSignals()
    for j in range(ymax):
        for k in range(zmax):

            dist1 = x*x*x*x
            mv = 0

            #calculate shortest distance between point and each triangle panel
            for va in range(bmax):

                #calculate barycentric coordinates to triangle
                edge1[0] = vertices[faces[va, 1], 0] - vertices[faces[va, 0], 0]
                edge1[1] = vertices[faces[va, 1], 1] - vertices[faces[va, 0], 1]
                edge1[2] = vertices[faces[va, 1], 2] - vertices[faces[va, 0], 2]

                edge2[0] = vertices[faces[va, 2], 0] - vertices[faces[va, 0], 0]
                edge2[1] = vertices[faces[va, 2], 1] - vertices[faces[va, 0], 1]
                edge2[2] = vertices[faces[va, 2], 2] - vertices[faces[va, 0], 2]

                vec[0] = vertices[faces[va, 0], 0] - i + x0
                vec[1] = vertices[faces[va, 0], 1] - j + y0
                vec[2] = vertices[faces[va, 0], 2] - k + z0

                d00 = edge1[0]*edge1[0] + edge1[1]*edge1[1] + edge1[2]*edge1[2]
                d01 = edge1[0]*edge2[0] + edge1[1]*edge2[1] + edge1[2]*edge2[2]
                d11 = edge2[0]*edge2[0] + edge2[1]*edge2[1] + edge2[2]*edge2[2]
                d20 = vec[0]*edge1[0] + vec[1]*edge1[1] + vec[2]*edge1[2]
                d21 = vec[0]*edge2[0] + vec[1]*edge2[1] + vec[2]*edge2[2]

                det = (d00 * d11 - d01 * d01)

                s = d01*d21 - d11*d20
                t = d01*d20 - d00*d21

                #consider cases (closest to plane, edge or corner)
                if ((s + t) < det):
                    if (s < 0.0):
                        if (t < 0.0):
                            if (d20 < 0.0):
                                s = clamp(-d20/d00, 0.0, 1.0)
                                t = 0.0
                            else:
                                s = 0.0
                                t = clamp(-d21/d11, 0.0, 1.0)
                        else:
                            s = 0.0
                            t = clamp(-d21/d11, 0.0, 1.0)
                    elif (t < 0.0):
                        s = clamp(-d20/d00, 0.0, 1.0)
                        t = 0.0
                    else:
                        invdet = 1.0/det
                        s = s*invdet
                        t = t*invdet
                else:
                    if (s < 0.0):
                        tmp0 = d01+d20
                        tmp1 = d11+d21
                        if (tmp1 > tmp0):
                            numer = tmp1 - tmp0
                            denom = d00 - 2.0*d01 + d21
                            s = clamp(numer/denom, 0.0, 1.0)
                            t = 1.0-s
                        else:
                            t = clamp(-d21/d11, 0.0, 1.0)
                            s = 0.0

                    elif (t < 0.0):
                        if ((d00 + d20) > (d01 + d21)):
                            numer = d11+d21 - d01 - d20
                            denom = d00 - 2.0*d01 + d11
                            s = clamp(numer/denom, 0.0, 1.0)
                            t = 1.0 - s
                        else:
                            s = clamp(-d21/d11, 0.0, 0.0)
                            t = 0.0
                    else:
                        numer = d11+d21 - d01 - d20
                        denom = d00 - 2.0*d01 + d11
                        s = clamp(numer/denom, 0.0, 1.0)
                        t = 1.0 - s

                #calculate closest point
                clp[0] = vertices[faces[va, 0], 0] + s * edge1[0] + t * edge2[0]
                clp[1] = vertices[faces[va, 0], 1] + s * edge1[1] + t * edge2[1]
                clp[2] = vertices[faces[va, 0], 2] + s * edge1[2] + t * edge2[2]

                tdist = (i - x0 - clp[0])*(i - x0 - clp[0]) + (j - y0 - clp[1])*(j - y0 - clp[1]) + (k - z0 - clp[2])*(k - z0 - clp[2])

                #closest distance to points
                if tdist < dist1:
                    dist1 = tdist
                    mv = va

            sdf_view[i, j, k] = sqrt(dist1)

            if sdf_view[i, j, k] > narrowband:
              br = False
              for a in range(-1, 2):
                if br == True:
                  break
                for b in range(-1, 2):
                  if br == True:
                    break
                  for c in range(-1, 2):
                    if br == True:
                      break
                    if (i + a >= 0) * (i + a < xmax) * (j + b >= 0) * (j + b < ymax) * (k + c >= 0) * (k + c < zmax):
                      if (a == 0) * (b == 0) * (c == 0) == False:
                        if sdf_view[i + a, j + b, k + c] < 1e11:
                          sign = 1.0 * (sdf_view[i + a, j + b, k + c] < 0.0)
                          sdf_view[i, j, k] = (1.0 - 2.0 * sign) * sdf_view[i, j, k]
                          br = True

              if br == False:
                sign = inside(i - x0, j - y0, k - z0, vertices, faces, bmax)
                sdf_view[i, j, k] = (1.0 - 2.0 * sign) * sdf_view[i, j, k]

            else:
              sign = inside(i - x0, j - y0, k - z0, vertices, faces, bmax)
              sdf_view[i, j, k] = (1.0 - 2.0 * sign) * sdf_view[i, j, k]

  return sdf


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cdef double inside(double px, double py, double pz,
                   double[:, ::1] verts, long[:, ::1] faces,
                   int f_c):

    """

    Function for determining whether point falls inside or outside mesh via raytracing

    :param1 px (double): x coordinate of candidate point
    :param2 py (double): y coordinate of candidate point
    :param3 pz (double): z coordinate of candidate point
    :param4 vertices (np.array, (M, 3), dtype=float): 2D array of mesh vertices
    :param5 faces (np.array, (N, 3), dtype=int): 2D array of the triangle
                                                 mesh connectivity matrix
    :param6 f_c (into): number of mesh elements

    :returns:
      :vote (int): 1 -> outside, 0 -> inside

    :raises:
      None

    """

    cdef int intersec1 = 0
    cdef int intersec2 = 0
    cdef int intersec3 = 0

    cdef int f = 0

    cdef int inside_f = 0
    cdef int outside_f = 0

    cdef double return_val = 0.0

    #perform 3 ray tests originating from the same point but at different launch angles
    #resolves numerical issues close to boundary/intersection point falling on mesh corner
    for f in range(0, f_c):

        intersec1 += ray_intersection(px, py, pz, 1.1, 0.0, 0.0, verts[faces[f, 0], 0],
                                      verts[faces[f, 0], 1], verts[faces[f, 0], 2],
                                      verts[faces[f, 1], 0], verts[faces[f, 1], 1],
                                      verts[faces[f, 1], 2], verts[faces[f, 2], 0],
                                      verts[faces[f, 2], 1], verts[faces[f, 2], 2])
        intersec2 += ray_intersection(px, py, pz, 0.1, 1.0, 0.35, verts[faces[f, 0], 0],
                                      verts[faces[f, 0], 1], verts[faces[f, 0], 2],
                                      verts[faces[f, 1], 0], verts[faces[f, 1], 1],
                                      verts[faces[f, 1], 2], verts[faces[f, 2], 0],
                                      verts[faces[f, 2], 1], verts[faces[f, 2], 2])
        intersec3 += ray_intersection(px, py, pz, -0.1, -0.35, 1.0, verts[faces[f, 0], 0],
                                      verts[faces[f, 0], 1], verts[faces[f, 0], 2],
                                      verts[faces[f, 1], 0], verts[faces[f, 1], 1],
                                      verts[faces[f, 1], 2], verts[faces[f, 2], 0],
                                      verts[faces[f, 2], 1], verts[faces[f, 2], 2])

    #vote on ray intersection results
    if intersec1 % 2 == 0:
        outside_f += 1
    if intersec1 % 2 == 1:
        inside_f += 1
    if intersec2 % 2 == 0:
        outside_f += 1
    if intersec2 % 2 == 1:
        inside_f += 1
    if intersec3 % 2 == 0:
        outside_f += 1
    if intersec3 % 2 == 1:
        inside_f += 1

    #return intersection result
    if inside_f >= outside_f:
        return 0.0
    else:
        return 1.0


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def pointsinside(double[::1] px, double[::1] py, double[::1] pz,
                 double[:, ::1] verts, long[:, ::1] faces, int f_c):

    """

    Python callable function for determining whether array of grid points
    is inside or outside a triangle mesh

    :param1 px (np.array,(L), dtype=double): 1D array of x coordinates making up grid
    :param2 py (np.array,(L), dtype=double): 1D array of y coordinates making up grid
    :param3 pz (np.array,(L), dtype=double): 1D array of z coordinates making up grid
    :param4 vertices (np.array, (M, 3), dtype=float): 2D array of mesh vertices
    :param5 faces (np.array, (N, 3), dtype=int): 2D array of the triangle
                                                 mesh connectivity matrix
    :param6 f_c (int): number of mesh elements

    :returns:
      :vals (np.array,(L, L, L), dtype=int): 3D array of results
                                             (1 -> inside, 0 -> outside)

    :raises:
      None

    """

    cdef int a = 0
    cdef int b = 0
    cdef int c = 0

    cdef int plx = len(px)
    cdef int ply = len(py)
    cdef int plz = len(pz)

    vals = np.zeros((plx, ply, plz), dtype=int)
    cdef long[:, :, ::1] vals_view = vals

    #loop over candidate grid
    for a in range(0, plx):
        for b in range(0, ply):
            for c in range(0, plz):
                vals_view[a, b, c] = 1 - int(inside(px[a], py[b], pz[c],
                                             verts, faces, f_c))

    return vals

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def phase_grid(long[:, :, ::1] pgrid, double[:, :, ::1] XX, double[:, :, ::1] XY,
               double[:, :, ::1] XZ, double[:, ::1] verts, long[:, ::1] faces,
               int phase_id):

    """

    Python callable function for calculating grid cells inside,
    outside, and intersecting phases.

    :param1 pgrid (np.array,(L1, L2, L3), dtype=double): 3D array of local phases
    :param2 XX (np.array,(L1, L2, L3), dtype=double): 3D array of local phase grid
                                                      x edge coordinates
    :param3 XY (np.array,(L1, L2, L3), dtype=double): 3D array of local phase grid
                                                      y edge coordinates
    :param4 XZ (np.array,(L1, L2, L3), dtype=double): 3D array of local phase grid
                                                      z edge coordinates
    :param5 verts (np.array,(M, 3), dtype=double): 2D array of phase mesh coordinates
    :param6 faces (np.array,(N, 3), dtype=int): 2D array representing the phase
                                                mesh connectivity matrix
    :param7 phase_id (int): The integer value to fill phase grid (pgrid)
                            if the grid point is inside the phase mesh
    :returns:
      :pgrid (np.array,(L1, L2, L3), dtype=double): the filled in phase grid

      :raises:
        None

    """

    cdef int f_c = len(faces)

    cdef int xdim = XX.shape[0]
    cdef int ydim = XY.shape[1]
    cdef int zdim = XZ.shape[2]

    ph_out = np.zeros((xdim, ydim, zdim), dtype="long")
    cdef long[:, :, ::1] ph_out_view = ph_out

    cdef int a, b, c

    cdef int top_left, top_right, bottom_left, bottom_right

    #perform corner checks
    for a in range(0, xdim):
        for b in range(0, ydim):
            for c in range(0, zdim):
                ph_out_view[a, b, c] = <long>inside(XX[a, b, c], XY[a, b, c],
                                                    XZ[a, b, c], verts,
                                                    faces, f_c)

    #set phase_grid based on result from corner checks
    for a in range(0, xdim - 1):
        for b in range(0, ydim - 1):
            for c in range(0, zdim - 1):
                #check if all corner points are inside phase
                if (ph_out_view[a, b, c] == 0 and ph_out_view[a + 1, b, c] == 0 and
                    ph_out_view[a, b + 1, c] == 0 and ph_out_view[a + 1, b + 1, c] == 0 and
                    ph_out_view[a, b, c + 1] == 0 and ph_out_view[a + 1, b, c + 1] == 0 and
                    ph_out_view[a, b + 1, c + 1] == 0 and
                    ph_out_view[a + 1, b + 1, c + 1] == 0):
                    pgrid[a, b, c] = phase_id
                #else check if all corner points are outside phase
                elif (ph_out_view[a, b, c] == 1 and ph_out_view[a + 1, b, c] == 1 and
                      ph_out_view[a, b + 1, c] == 1 and ph_out_view[a + 1, b + 1, c] == 1 and
                      ph_out_view[a, b, c + 1] == 1 and ph_out_view[a + 1, b, c + 1] == 1 and
                      ph_out_view[a ,b + 1, c + 1] == 1 and
                      ph_out_view[a + 1, b + 1, c + 1] == 1):
                    pass
                #else check if intersecting boundary
                else:
                    pgrid[a, b, c] = -phase_id

    return np.asarray(pgrid)


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cpdef int pyinside(double px, double py, double pz, double[:, ::1] verts,
                   long[:, ::1] faces, int f_c):

    """

    Python callable function for determining whether point falls
    inside or outside mesh via raytracing

    :param1 px (double): x coordinate of candidate point
    :param2 py (double): y coordinate of candidate point
    :param3 pz (double): z coordinate of candidate point
    :param4 vertices (np.array,(M, 3), dtype=double): 2D array of mesh vertices
    :param5 faces (np.array,(N, 3), dtype=int): 2D array of the triangle mesh
                                                connectivity matrix
    :param6 f_c (int): number of mesh elements

    :returns:
      :vote (bool): 1 -> outside, 0 -> inside

    :raises:
      None

    """

    cdef int intersec1 = 0
    cdef int intersec2 = 0
    cdef int intersec3 = 0

    cdef int f = 0

    cdef int inside_f = 0
    cdef int outside_f = 0

    cdef double return_val = 0.0

    for f in range(0, f_c):

        intersec1 += ray_intersection(px, py, pz, 1.1, -0.1, 0.15,
                                      verts[faces[f, 0], 0], verts[faces[f, 0], 1],
                                      verts[faces[f, 0], 2], verts[faces[f, 1], 0],
                                      verts[faces[f, 1], 1], verts[faces[f, 1], 2],
                                      verts[faces[f, 2], 0], verts[faces[f, 2], 1],
                                      verts[faces[f, 2], 2])
        intersec2 += ray_intersection(px, py, pz, 0.1, 1.0, 0.35, verts[faces[f, 0], 0],
                                      verts[faces[f, 0], 1], verts[faces[f, 0], 2],
                                      verts[faces[f, 1], 0], verts[faces[f, 1], 1],
                                      verts[faces[f, 1], 2], verts[faces[f, 2], 0],
                                      verts[faces[f, 2], 1], verts[faces[f, 2], 2])
        intersec3 += ray_intersection(px, py, pz, -0.1, -0.35, 1.0, verts[faces[f, 0], 0],
                                      verts[faces[f, 0], 1], verts[faces[f, 0], 2],
                                      verts[faces[f, 1], 0], verts[faces[f, 1], 1],
                                      verts[faces[f, 1], 2], verts[faces[f, 2], 0],
                                      verts[faces[f, 2], 1], verts[faces[f, 2], 2])

    if intersec1 % 2 == 0:
        outside_f += 1
    if intersec1 % 2 == 1:
        inside_f += 1
    if intersec2 % 2 == 0:
        outside_f += 1
    if intersec2 % 2 == 1:
        inside_f += 1
    if intersec3 % 2 == 0:
        outside_f += 1
    if intersec3 % 2 == 1:
        inside_f += 1

    if inside_f >= outside_f:
        return 0
    else:
        return 1


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cpdef intersection(double px, double py, double pz, double rx, double ry,
                   double rz, double[:, ::1] verts, long[:, ::1] faces, int f_c):

    """

    Python callable function for determining the intersection point of
    a raytracer o a mesh

    :param1 px (double): x coordinate of candidate point
    :param2 py (double): y coordinate of candidate point
    :param3 pz (double): z coordinate of candidate point
    :param4 rx (double): x component of ray launch vector
    :param5 ry (double): y component of ray launch vector
    :param6 rz (double): z component of ray launch vector
    :param7 vertices (np.array,(M, 3), dtype=double): 2D array of mesh vertices
    :param8 faces (np.array,(N, 3), dtype=int): 2D array of the triangle mesh connectivity matrix
    :param9 f_c (int): number of mesh elements

    :returns:
      :intersections (list): list of intersection points on mesh

    :raises:
      None

    """

    cdef int intersec1 = 0
    cdef int intersec2 = 0
    cdef int intersec3 = 0

    cdef int f = 0

    cdef int inside_f = 0
    cdef int outside_f = 0

    intersections = []

    for f in range(0, f_c):
        intersec, intx, inty, intz = single_ray_intersection(px, py, pz, rx, ry,
                                                             rz, verts[faces[f, 0], 0],
                                                             verts[faces[f, 0], 1],
                                                             verts[faces[f, 0], 2],
                                                             verts[faces[f, 1], 0],
                                                             verts[faces[f, 1], 1],
                                                             verts[faces[f, 1], 2],
                                                             verts[faces[f, 2], 0],
                                                             verts[faces[f, 2], 1],
                                                             verts[faces[f, 2], 2])

        if intersec == 1:
            intersections.append(f)

    return intersections

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cdef inline int ray_intersection(double rox, double roy, double roz, double rv0,
                                 double rv1, double rv2, double v0x, double v0y,
                                 double v0z, double v1x, double v1y, double v1z,
                                 double v2x, double v2y, double v2z) nogil:

    """

    Function for calculating whether a ray intersects a particular triangle element

    Implemented algorithm is the Möller–Trumbore ray-triangle intersection algorithm
    Möller, Tomas; Trumbore, Ben (1997).
    "Fast, Minimum Storage Ray-Triangle Intersection".
    Journal of Graphics Tools. 2: 21–28.
    doi:10.1080/10867651.1997.10487468

    :param1 rox (double): x coordinate of candidate point
    :param2 roy (double): y coordinate of candidate point
    :param3 roz (double): z coordinate of candidate point
    :param4 rv0 (double): x component of ray launch vector
    :param5 rv1 (double): y component of ray launch vector
    :param6 rv2 (double): z component of ray launch vector
    :param7 v0x (double): x component of triangle element corner 1
    :param8 v0y (double): y component of triangle element corner 1
    :param9 v0z (double): z component of triangle element corner 1
    :param10 v1x (double): x component of triangle element corner 2
    :param11 v1y (double): y component of triangle element corner 2
    :param12 v1z (double): z component of triangle element corner 2
    :param13 v2x (double): x component of triangle element corner 3
    :param14 v2y (double): y component of triangle element corner 3
    :param15 v2z (double): z component of triangle element corner 3

    :returns:
      :result (int): 1 -> intersection, 0 -> miss

    :raises:
      None

    """

    #error tolerance for determining whether ray is parallel to triangle
    cdef double EPSILON = 1.0e-9

    cdef double[3] edge1
    edge1[0] = v1x - v0x
    edge1[1] = v1y - v0y
    edge1[2] = v1z - v0z

    cdef double[3] edge2
    edge2[0] = v2x - v0x
    edge2[1] = v2y - v0y
    edge2[2] = v2z - v0z

    cdef double[3] h
    h[0] =  rv1 * edge2[2] - rv2 * edge2[1]
    h[1] =  rv2 * edge2[0] - rv0 * edge2[2]
    h[2] =  rv0 * edge2[1] - rv1 * edge2[0]

    cdef double a = edge1[0] * h[0] + edge1[1] * h[1] + edge1[2] * h[2]

    if (a > -EPSILON and a < EPSILON):
        return 0    # This ray is parallel to this triangle.

    cdef double f = 1.0/a
    cdef double[3] s
    s[0] = rox - v0x
    s[1] = roy - v0y
    s[2] = roz - v0z

    cdef double u = f * (s[0] * h[0] + s[1] * h[1] + s[2] * h[2])

    if (u < 0.0 or u > 1.0):
        return 0

    cdef double[3] q
    q[0] =  s[1] * edge1[2] - s[2] * edge1[1]
    q[1] =  s[2] * edge1[0] - s[0] * edge1[2]
    q[2] =  s[0] * edge1[1] - s[1] * edge1[0]

    cdef double v = f * (rv0 * q[0] + rv1 * q[1] + rv2 * q[2])

    if ((v < 0.0) or (u + v > 1.0)):
        return 0

    # At this stage we can compute t to find out where
    # the intersection point is on the line.
    cdef double t = f * (edge2[0] * q[0] + edge2[1] * q[1] + edge2[2] * q[2])

    if (t > EPSILON): # ray intersection
        #outIntersectionPoint = rayOrigin + rayVector * t

        return 1
    else:
        return 0

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cpdef single_ray_intersection(double rox, double roy, double roz, double rv0,
                              double rv1, double rv2, double v0x, double v0y,
                              double v0z, double v1x, double v1y, double v1z,
                              double v2x, double v2y, double v2z):

    """

    Python callable Function for calculating whether a ray intersects a
    particular triangle element and its intersection point

    Algorithm is the Möller–Trumbore ray-triangle intersection algorithm
    Möller, Tomas; Trumbore, Ben (1997).
    "Fast, Minimum Storage Ray-Triangle Intersection".
    Journal of Graphics Tools. 2: 21–28.
    doi:10.1080/10867651.1997.10487468

    :param1 rox (double): x coordinate of candidate point
    :param2 roy (double): y coordinate of candidate point
    :param3 roz (double): z coordinate of candidate point
    :param4 rv0 (double): x component of ray launch vector
    :param5 rv1 (double): y component of ray launch vector
    :param6 rv2 (double): z component of ray launch vector
    :param7 v0x (double): x component of triangle element corner 1
    :param8 v0y (double): y component of triangle element corner 1
    :param9 v0z (double): z component of triangle element corner 1
    :param10 v1x (double): x component of triangle element corner 2
    :param11 v1y (double): y component of triangle element corner 2
    :param12 v1z (double): z component of triangle element corner 2
    :param13 v2x (double): x component of triangle element corner 3
    :param14 v2y (double): y component of triangle element corner 3
    :param15 v2z (double): z component of triangle element corner 3

    :returns:
      :result (int): 1 -> intersection, 0 -> miss
      :intx (double): x coordinate of intersection point (returns 0 if miss)
      :inty (double): y coordinate of intersection point (returns 0 if miss)
      :intz (double): z coordinate of intersection point (returns 0 if miss)

    :raises:
      None

    """

    #error tolerance for determining whether ray is parallel to triangle
    cdef double EPSILON = 1.0e-9

    cdef double[3] edge1
    edge1[0] = v1x - v0x
    edge1[1] = v1y - v0y
    edge1[2] = v1z - v0z

    cdef double[3] edge2
    edge2[0] = v2x - v0x
    edge2[1] = v2y - v0y
    edge2[2] = v2z - v0z

    cdef double[3] h
    h[0] =  rv1 * edge2[2] - rv2 * edge2[1]
    h[1] =  rv2 * edge2[0] - rv0 * edge2[2]
    h[2] =  rv1 * edge2[1] - rv1 * edge2[0]

    cdef double intx = 0.0
    cdef double inty = 0.0
    cdef double intz = 0.0

    cdef double a = edge1[0] * h[0] + edge1[1] * h[1] + edge1[2] * h[2]

    if (a > -EPSILON and a < EPSILON):
        return 0, intx, inty, intz # This ray is parallel to this triangle.

    cdef double f = 1.0/a
    cdef double[3] s
    s[0] = rox - v0x
    s[1] = roy - v0y
    s[2] = roz - v0z

    cdef double u = f * (s[0] * h[0] + s[1] * h[1] + s[2] * h[2])

    if (u < 0.0 or u > 1.0):
        return 0, intx, inty, intz

    cdef double[3] q
    q[0] =  s[1] * edge1[2] - s[2] * edge1[1]
    q[1] =  s[2] * edge1[0] - s[0] * edge1[2]
    q[2] =  s[0] * edge1[1] - s[1] * edge1[0]

    cdef double v = f * (rv0 * q[0] + rv1 * q[1] + rv2 * q[2])

    if ((v < 0.0) or (u + v > 1.0)):
        return 0, intx, inty, intz

    # At this stage we can compute t to find out where
    # the intersection point is on the line.
    cdef double t = f * (edge2[0] * q[0] + edge2[1] * q[1] + edge2[2] * q[2])

    if (t > EPSILON): # ray intersection
        # outIntersectionPoint = rayOrigin + rayVector * t
        intx = rox + rv0 * t
        inty = roy + rv1 * t
        intz = roz + rv2 * t

        return 1, intx, inty, intz
    else:
        return 0, intx, inty, intz
