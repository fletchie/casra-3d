#!/bin/bash

if [ ! -d casra_3d ] ; then
	echo " please run script from top level folder of casra repository: could not find casra_3d in current dir."
	exit 1
fi

start=$(date +'%s')
echo "Run finfet model pipeline"

#Example directories
EXDIRIN=c3d_input/examples/finfet
EXDIROUT=c3d_output/examples/finfet


#Generate finfet structure/model
python3 ${EXDIRIN}/generate_finfet.py  || { echo "Failed to generate finfet structure" ; exit 1 ; }

#simulate finfet evaporation
python3 -m casra_3d.tip_simulation --mat_db c3d_input/materials --tip_geom ${EXDIRIN}/finfet.mstate --mref ${EXDIRIN}/finfet.mref --parameters ${EXDIRIN}/parameters --it 1000 --out ${EXDIROUT} --termination_height 2.9e-7 || { echo "Finfet evaporation failed" ; exit 1 ; }

#project ions to derive reconstruction mapping
python3 -m casra_3d.perform_projection --tip_geom ${EXDIRIN}/finfet.mstate --dirname ${EXDIROUT} --mref ${EXDIRIN}/finfet.mref --it 1000 --parameters ${EXDIRIN}/parameters --parallelisation True --out ${EXDIROUT} || { echo "Finfet projection failed" ; exit 1 ; }

#derive detector isolines to visualise local distortions
python3 -m casra_3d.tools.reconstruction_mapping --mapping_data ${EXDIROUT}/projection_data/projection_data.tsv --det_radius 0.038 --dirname ${EXDIROUT}/reconstruction --image_transfer "0.12,0,0,0,1" || { echo "Failed to generate detector mapping isolines" ; exit 1 ; }


finish=$(date +'%s')
duration=$((finish-start))
hrs=$(( duration/3600 )); mins=$(( (duration-hrs*3600)/60)); secs=$(( duration-hrs*3600-mins*60 ))
printf 'Time spent: %02d:%02d:%02d\n' $hrs $mins $secs
