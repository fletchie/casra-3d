"""

Module containing the functions for writing simulation outputs

Author: Charles Fletcher, Daniel Haley
Atom Probe Tomography Group, Department of Materials, University of Oxford
May 2020

"""

import re
import struct
import os
import math
from shutil import copyfile
import sqlite3

from casra_3d.io import model_loaders as model_loaders

try:
    import numpy as np
except ModuleNotFoundError:
    raise ModuleNotFoundError("Could not find Python module numpy. " +
                              "Please make sure it is installed.")

def write_mesh_to_mstate(name, verts, faces, filename, is_surface=0):

    """

    Method for writing a particular mesh defining a phase to a. mstate file

    :param1 name (str): The phasename
    :param2 verts (np.array, (M, 3), dtype=float): The mesh vertices
    :param3 faces (np.array, (N, 3), dtype=int): The mesh faces
    :param4 filename (str): The .mstate filename
    :param5 is_surface (np.array, (N), dtype=int): Whether the passed phase
                                                   defining mesh also defines
                                                   the sample surface
    :returns:
        None

    :raises:
        None

    """

    #If surface, wipe file and start again.
    if is_surface == 1:
        f = open(filename, "w")
    else:
        f = open(filename, "a")

    f.write("OBJT:\n")
    f.write(name + "\n")
    f.write(str(len(verts)) + "\n")
    f.write(str(len(faces)) + "\n")
    f.write(str(is_surface) + "\n")
    for a in range(0, len(verts)):
        f.write("\t".join(verts[a].astype(str).tolist()) + str("\n"))
    f.write("\n")
    for a in range(0, len(faces)):
        f.write("\t".join(faces[a].astype(str).tolist()) + str("\n"))
    f.close()

def write_field_to_mstate(field, filename):

    """

    Method for writing a constructed level set field to a .mstate file.
    Allows for model execution without having to rebuild the mesh.

    :param1 field (np.array, (L1,L2,L3), dtype=float): The level set field
    :param2 filename (str): The .mstate filename
    :param3 is_surface (np.array, (N), dtype=int): Whether the passed phase
                                                   defining mesh also defines
                                                   the sample surface
    :returns:
        None

    :raises:
        None

    """

    f = open(filename, "a+")
    f.seek(0)
    #remove old level set field if detected
    text = f.read()
    find_field = [m.start() for m in re.finditer('FIELD:', text)]
    find_field.sort()
    if len(find_field) > 0:
        f.seek(find_field[0])
        f.truncate()
    f.close()

    f = open(filename, "a")
    f.write("FIELD:\n")
    f.write(str(field.shape[0]) + "\n")
    f.write(str(field.shape[1]) + "\n")
    f.write(str(field.shape[2]) + "\n")
    #save array in 2D (x,y) slices
    for a in range(0, field.shape[2]):
        np.savetxt(f, field[:, :, a])
    f.close()

def write_pgrid_to_mstate(phase_grid, pgxmin, pgxmax, pgymin, pgymax, pgzmin, pgzmax, filename):

    """

    Method for writing a constructed level set field to a .mstate file.
    Allows for model execution without having to rebuild the mesh.

    :param1 field (np.array, (L1,L2,L3), dtype=float): The level set field
    :param2 filename (str): The .mstate filename
    :param3 is_surface (np.array, (N), dtype=int): Whether the passed phase
                                                   defining mesh also defines
                                                   the sample surface
    :returns:
        None

    :raises:
        NOne

    """

    f = open(filename, "a+")
    f.seek(0)
    #remove old phase grid if detected
    text = f.read()
    find_pgrid = [m.start() for m in re.finditer('PGRID:', text)]
    find_pgrid.sort()
    print(find_pgrid)
    if len(find_pgrid) > 0:
        f.seek(find_pgrid[0])
        f.truncate()
    f.close()

    f = open(filename, "a")
    f.write("PGRID:\n")
    f.write(str(phase_grid.shape[0]) + "\n")
    f.write(str(phase_grid.shape[1]) + "\n")
    f.write(str(phase_grid.shape[2]) + "\n")

    f.write(str(pgxmin) + "\n")
    f.write(str(pgxmax) + "\n")
    f.write(str(pgymin) + "\n")
    f.write(str(pgymax) + "\n")
    f.write(str(pgzmin) + "\n")
    f.write(str(pgzmax) + "\n")

    #save array in 2D (x,y) slices
    for a in range(0, phase_grid.shape[2]):
        np.savetxt(f, phase_grid[:, :, a].astype(int))
    f.close()

def output_projection_tsv(projection, filename):
    """

    Method for outputing the ion projection data to a tsv file

    file format:

    frame    det_x   det_y   det_z    vel_x    vel_y    vel_z    sample_x    sample_y    sample_z   chemistry   ion_phase   panel_index

    where:
    frame corresponds to the model iteration
    sample_x corresponds to the initial sample surface x launch coordinate
    chemistry corresponds to the originating phase id in the materials database
    ion_phase corresponds to the phase name

    :param1 projection (dict): The projection data dictionary
    :param2 filename (str): The output directory in which to save the .tsv file

    :returns:
        None

    :raises:
        None

    """
    
    print("writing projection data")
    
    if not os.path.exists(filename):
        os.makedirs(filename)

    if "ion_phase" in projection:
        try:
            f = open(filename + "/projection_data.tsv", "w+")
            f.write("frame \t det_x \t det_y \t det_z \t vel_x \t vel_y \t vel_z " + 
                    "\t sample_x \t sample_y \t sample_z \t chemistry \t ion_phase \t panel_index \n")
            for it in range(0, len(projection["num"])):
                for a in range(0, len(projection["sample_space"][it])):
                    temp_str = [str(b) for b in projection["panel_index"][it][a]]
                    f.write(str(projection["num"][it]) + "\t" + 
                            str(projection["detector_space"][it][a][0]) + "\t" +
                            str(projection["detector_space"][it][a][1]) + "\t" + 
                            str(projection["detector_space"][it][a][2]) + "\t" +
                            str(projection["vel"][it][a][0]) + "\t" + 
                            str(projection["vel"][it][a][1]) + "\t" +
                            str(projection["vel"][it][a][2]) + "\t" + 
                            str(projection["sample_space"][it][a][0]) + "\t" +
                            str(projection["sample_space"][it][a][1]) + "\t" + 
                            str(projection["sample_space"][it][a][2]) + "\t" +
                            str(projection["chemistry"][it][a]) + "\t" + 
                            str(projection["ion_phase"][it][a]) + "\t" +
                            ",".join(temp_str) + "\n")
            f.close()
        except:
            f = open(filename + "/projection_data.tsv", "w+")
            f.write("frame \t det_x \t det_y \t det_z \t vel_x \t vel_y " + 
                    "\t vel_z \t sample_x \t sample_y \t sample_z \t chemistry " + 
                    "\t ion_phase \t panel_index \n")
            for it in range(0, len(projection["num"])):
                temp_str = [str(b) for b in projection["panel_index"][it]]
                f.write(str(projection["num"][it]) + "\t" + 
                        str(projection["detector_space"][it][0]) + "\t" +
                        str(projection["detector_space"][it][1]) + "\t" + 
                        str(projection["detector_space"][it][2]) + "\t" +
                        str(projection["vel"][it][0]) + "\t" + 
                        str(projection["vel"][it][1]) + "\t" +
                        str(projection["vel"][it][2]) + "\t" + 
                        str(projection["sample_space"][it][0]) + "\t" +
                        str(projection["sample_space"][it][1]) + "\t" + 
                        str(projection["sample_space"][it][2]) + "\t" +
                        str(projection["chemistry"][it]) + "\t" + 
                        str(projection["ion_phase"][it]) + "\t" +
                        ",".join(temp_str) + "\n")
            f.close()
    else:
        f = open(filename + "/projection_data.tsv", "w+")
        f.write("frame \t det_x \t det_y \t det_z \t vel_x \t vel_y \t vel_z " + 
                "\t sample_x \t sample_y \t sample_z \t chemistry " + 
                "\t panel_index \n")
        for it in range(0, len(projection["num"])):
            for a in range(0, len(projection["sample_space"][it])):
                temp_str = [str(b) for b in projection["panel_index"][it][a]]
                f.write(str(projection["num"][it]) + "\t" + 
                        str(projection["detector_space"][it][a][0]) + "\t" + 
                        str(projection["detector_space"][it][a][1]) + "\t" + 
                        str(projection["sample_space"][it][a][0]) + "\t" + 
                        str(projection["sample_space"][it][a][1]) + "\t" + 
                        str(projection["sample_space"][it][a][2]) + "\t" + 
                        str(projection["chemistry"][it][a]) + "\t" + ",".join(temp_str) + "\n")
        f.close()
    
    print("write complete")


def output_stability_data_tsv(stability, filename):

    """

    Method for outputing the ion projection stability data to a tsv file

    file format:

    frame   sv0x    sv0y    sv0z    sv1x    sv1y    sv1z    sv2x    sv2y    sv2z    dv0x    dv0y    dv0z    dv1x    dv1y    dv1z    dv2x    dv2y    dv2z    vel0x    vel0y    vel0z    vel1x    vel1y    vel1z    vel2x    vel2y    vel2z    jacobian    conformality    J00   J10   J01 J11 evap_rate panel_index


    where:
    frame corresponds to the model iteration
    sv0x corresponds to the vertex 0 surface panel x coordinate
    sv0y corresponds to the vertex 0 surface panel y coordinate
    sv0z corresponds to the vertex 0 surface panel z coordinate
    sv1x corresponds to the vertex 1 surface panel x coordinate
    sv1y corresponds to the vertex 1 surface panel y coordinate
    sv1z corresponds to the vertex 1 surface panel z coordinate
    sv2x corresponds to the vertex 2 surface panel x coordinate
    sv2y corresponds to the vertex 2 surface panel y coordinate
    sv2z corresponds to the vertex 2 surface panel z coordinate
    dv0x corresponds to the projected vertex 0 surface panel x coordinate
    dv0y corresponds to the projected vertex 0 surface panel y coordinate
    dv0z corresponds to the projected vertex 0 surface panel z coordinate
    dv1x corresponds to the projected vertex 1 surface panel x coordinate
    dv1y corresponds to the projected vertex 1 surface panel y coordinate
    dv1z corresponds to the projected vertex 1 surface panel z coordinate
    dv2x corresponds to the projected vertex 2 surface panel x coordinate
    dv2y corresponds to the projected vertex 2 surface panel y coordinate
    dv2z corresponds to the projected vertex 2 surface panel z coordinate
    vel0x corresponds to the projected vertex 0 surface panel x coordinate
    vel0y corresponds to the projected vertex 0 surface panel y coordinate
    vel0z corresponds to the projected vertex 0 surface panel z coordinate
    vel1x corresponds to the projected vertex 1 surface panel x coordinate
    vel1y corresponds to the projected vertex 1 surface panel y coordinate
    vel1z corresponds to the projected vertex 1 surface panel z coordinate
    vel2x corresponds to the projected vertex 2 surface panel x coordinate
    vel2y corresponds to the projected vertex 2 surface panel y coordinate
    vel2z corresponds to the projected vertex 2 surface panel z coordinate
    jacobian corresponds to the panel jacobian determinant value (relative to panel centre point)
    conformality corresponds to the panel conformality value (relative to panel centre point)
    J00 corresponds to the xx component of the Jacobian matrix
    J10 corresponds to the yx component of the Jacobian matrix
    J01 corresponds to the xy component of the Jacobian matrix
    J11 corresponds to the yy component of the Jacobian matrix
    evap_rate corresponds to the local evaporation rate
    panel_index corresponds to the panel index in the .vtk mesh

    :param stability (dict): The stability data dictionary
    :param filename (str): The output directory in which to save the .tsv file
    :returns: 0

    """

    if not os.path.exists(filename):
        os.makedirs(filename)

    print("writing stability data")
    f = open(filename + "/stability_data.tsv", "w+")
    f.write("frame \t sv0x \t sv0y \t sv0z \t sv1x \t sv1y \t sv1z \t sv2x \t sv2y \t sv2z"
    + "\t dv0x \t dv0y \t dv0z \t dv1x \t dv1y \t dv1z \t dv2x \t dv2y \t dv2z"
    + "\t vel0x \t vel0y \t vel0z \t vel1x \t vel1y \t vel1z \t vel2x \t vel2y \t vel2z"
    + "\t jacobian \t conformality \t J00 \t J10 \t J01 \t J11 \t evap_rate \t panel_index \n")
    for it in range(0, len(stability["num"])):
        try:
            for a in range(0, len(stability["sv0"][it])):
                f.write(str(stability["num"][it]) + "\t" +
                        str(stability["sv0"][it][a, 0]) + "\t" + str(stability["sv0"][it][a, 1]) + "\t" + str(stability["sv0"][it][a, 2]) + "\t" +
                        str(stability["sv1"][it][a, 0]) + "\t" + str(stability["sv1"][it][a, 1]) + "\t" + str(stability["sv1"][it][a, 2]) + "\t" +
                        str(stability["sv2"][it][a, 0]) + "\t" + str(stability["sv2"][it][a, 1]) + "\t" + str(stability["sv2"][it][a, 2]) + "\t" +
                        str(stability["dv0"][it][a, 0]) + "\t" + str(stability["dv0"][it][a, 1]) + "\t" + str(stability["dv0"][it][a, 2]) + "\t" +
                        str(stability["dv1"][it][a, 0]) + "\t" + str(stability["dv1"][it][a, 1]) + "\t" + str(stability["dv1"][it][a, 2]) + "\t" +
                        str(stability["dv2"][it][a, 0]) + "\t" + str(stability["dv2"][it][a, 1]) + "\t" + str(stability["dv2"][it][a, 2]) + "\t" +
                        str(stability["vel0"][it][a, 0]) + "\t" + str(stability["vel0"][it][a, 1]) + "\t" + str(stability["vel0"][it][a, 2]) + "\t" +
                        str(stability["vel1"][it][a, 0]) + "\t" + str(stability["vel1"][it][a, 1]) + "\t" + str(stability["vel1"][it][a, 2]) + "\t" +
                        str(stability["vel2"][it][a, 0]) + "\t" + str(stability["vel2"][it][a, 1]) + "\t" + str(stability["vel2"][it][a, 2]) + "\t" +
                        str(stability["jacobian"][it][a]) + "\t" + str(stability["conformality"][it][a]) + "\t" + str(stability["J00"][it][a]) + "\t" +
                        str(stability["J10"][it][a]) + "\t" + str(stability["J01"][it][a]) + "\t" + str(stability["J11"][it][a]) + "\t" +
                        str(stability["evap_rate"][it][a]) + "\n")
        except:
            f.write(str(stability["num"][a]) + "\t" +
                    str(stability["sv0"][a, 0]) + "\t" + str(stability["sv0"][a, 1]) + "\t" + str(stability["sv0"][a, 2]) + "\t" +
                    str(stability["sv1"][a, 0]) + "\t" + str(stability["sv1"][a, 1]) + "\t" + str(stability["sv1"][a, 2]) + "\t" +
                    str(stability["sv2"][a, 0]) + "\t" + str(stability["sv2"][a, 1]) + "\t" + str(stability["sv2"][a, 2]) + "\t" +
                    str(stability["dv0"][a, 0]) + "\t" + str(stability["dv0"][a, 1]) + "\t" + str(stability["dv0"][a, 2]) + "\t" +
                    str(stability["dv1"][a, 0]) + "\t" + str(stability["dv1"][a, 1]) + "\t" + str(stability["dv1"][a, 2]) + "\t" +
                    str(stability["dv2"][a, 0]) + "\t" + str(stability["dv2"][a, 1]) + "\t" + str(stability["dv2"][a, 2]) + "\t" +
                    str(stability["vel0"][a, 0]) + "\t" + str(stability["vel0"][a, 1]) + "\t" + str(stability["vel0"][a, 2]) + "\t" +
                    str(stability["vel1"][a, 0]) + "\t" + str(stability["vel1"][a, 1]) + "\t" + str(stability["vel1"][a, 2]) + "\t" +
                    str(stability["vel2"][a, 0]) + "\t" + str(stability["vel2"][a, 1]) + "\t" + str(stability["vel2"][a, 2]) + "\t" +
                    str(stability["jacobian"][a]) + "\t" + str(stability["conformality"][a]) + "\t" + str(stability["J00"][a]) + "\t" +
                    str(stability["J10"][a]) + "\t" + str(stability["J01"][a]) + "\t" + str(stability["J11"][a]) + "\t" +
                    str(stability["evap_rate"][a]) + "\n")

    f.close()
    print("write complete")


def output_evolution_vtk(filename, surf_state, geom, mstate_filename,
                         field, start=0, end=math.inf):
    """

    Outputs simulated surface evolution from surface state to .vtk

    also saves a .series file of the following format:

    {
      "file-series-version" : "1.0",
      "files" : [
        { "name" : "foo1.vtk", "time" : 0 },
        { "name" : "foo2.vtk", "time" : 5.5 },
        { "name" : "foo3.vtk", "time" : 11.2 },
        ...
      ]
      }

      Also copies the .mstate file into the same output directory

    :param1 filename (str): The output directory
    :param2 surf_state (dict): The simulation data dictionary
    :param3 geom (dict): The simulation geometry dictionary
    :param4 mstate_filename (str): The output .mstate file
    :param5 field (np.array, (L1,L2,L3), dtype=float): The output level set field
    :param6 start (int): The initial frame to output to .vtk
    :param7 end (int): The final frame to output to .vtk

    :returns:
        None

    :raises:
        None


    """

    print("saving to directory: " + filename)

    if end > len(surf_state["verts"]):
        end = len(surf_state["verts"])

    if not os.path.exists(filename):
        os.mkdir(filename)

    if not os.path.exists(filename + "/msim"):
        os.mkdir(filename + "/msim")

    name = filename.split(os.sep)[-1]
    f = open(filename + "/" + name + ".vtk.series", "w")

    f.write("{ \n")
    f.write('\t "file-series-version" : "1.0", \n ')
    f.write('\t "files" : [ \n')
    for aga in range(start, end):
        faces = surf_state["faces"][aga]
        faces[:, [1, 2]] = faces[:, [2, 1]]
        cells = faces
        verts = surf_state["verts"][aga]
        cell_data = {"phase": np.array(surf_state["phase"][aga]).reshape(-1, 1),
                     "potential": np.array(surf_state["pot"][aga]).reshape(-1, 1),
                     "flux": np.array(surf_state["flux"][aga]).reshape(-1, 1),
                     "smflux": np.array(surf_state["smflux"][aga]).reshape(-1, 1),
                     "areas": np.array(surf_state["areas"][aga]).reshape(-1, 1),
                     "normals": np.array(surf_state["normals"][aga])[:, :3],
                     "sample_surface": np.array(surf_state["sample_surface"][aga]).reshape(-1, 1),
                     "smoothed_velocity": np.array(surf_state["smoothed_velocity"][aga]
                                                  ).reshape(-1, 1),
                     "volume": np.array([surf_state["volume"][aga]] * len(faces)).reshape(-1, 1),
                     "temperature": np.array(surf_state["temperature"][aga]).reshape(-1, 1),
                     "voltage": np.array([surf_state["voltage"][aga]] * len(faces)
                                        ).reshape(-1, 1),
                     "iteration_stability": np.array([surf_state["iteration_stability"][aga]] *
                                                     len(faces)).reshape(-1, 1)}
        write_vtk(filename + "/msim/" + name + "_" + str(aga)+".vtk",
                  verts, cells, cell_data, point_data={})

        f.write('\t \t { "name" : "msim/' + name + "_" + str(aga)+
                '.vtk", "time": ' + str(aga) + ' }')
        if aga != end - 1:
            f.write(',')
        f.write('\n')

    f.write('\t ] \n')
    f.write('}')
    f.close()

    #copy the mstate file into this new output directory
    name = filename.split(os.sep)[-1]
    copyfile(mstate_filename, filename + "/" + name + "_init.mstate")

    #Generate mstate file of final simulation state
    for obj in geom["objts"]:
        write_mesh_to_mstate(obj["name"], obj["verts"], obj["faces"],
                             filename + "/" + name + "_final.mstate",
                             obj["surface"])

    write_field_to_mstate(field, filename + "/" + name + "_final.mstate")

    print("completed")

def output_trajectories_vtk(projection, filename):

    """

    output ion trajectories to .vtk (for viewing in paraview)

    :param1 projection (dict): The ion projection data dictionary
    :param2 filename (str): The directory in which to store the projection data

    :returns:
        None

    :raises:
        None

    """

    if not os.path.exists(filename):
        os.mkdir(filename)

    if not os.path.exists(filename + "/mtraj"):
        os.mkdir(filename + "/mtraj")

    name = filename.split(os.sep)[-1]
    f = open(filename + "/traj" + name + ".vtk.series", "w")

    f.write("{ \n")
    f.write('\t "file-series-version" : "1.0", \n ')
    f.write('\t "files" : [ \n')

    for step in range(0, len(projection["num"])):
        accum_int = 0
        trajectories = np.zeros((0, 3))
        line_step = np.zeros((0, 2))
        associated_traj = []
        for i in range(0, len(projection["trajectories"][step])):
            traj = projection["trajectories"][step][i][:, :3]
            vec = np.linspace(0, len(traj) - 1, len(traj) - 1,
                              endpoint=False).astype(int) + accum_int
            line_step_t = np.vstack((vec, vec + 1)).T
            line_step = np.vstack((line_step, line_step_t))
            trajectories = np.vstack((trajectories, traj))
            accum_int += len(traj)
            associated_traj.extend([projection["chemistry"][step][i]]*len(traj))

        #if no phase assigned to trajectory, give trajectory a default unassigned value of -1
        associated_traj = np.array(associated_traj)
        associated_traj[associated_traj == None] = -1
        if len(trajectories > 0):
            trajectories = trajectories.astype(np.float64)
            line_step = line_step.astype(int)
            cells = line_step
            point_data = {"phi": np.array(associated_traj)}
            write_vtk(filename + "/mtraj/traj_" + "_" + str(step) + ".vtk",
                      trajectories, cells, cell_data={}, point_data=point_data)

            f.write('\t \t { "name" : ' + '"mtraj/traj_' + "_" + str(step) +
                    '.vtk", "time": ' + str(projection["num"][step]) + ' }')

            if step != len(projection["num"]) - 1:
                f.write(',')
        f.write('\n')

    f.write('\t ] \n')
    f.write('}')
    f.close()


def output_hitmap_vtk(projection, filename):

    """

    output detector hit coordinates from ion projection

    :param1 projection (dict): The ion projection data dictionary
    :param2 filename (str): The directory in which to store the projection data

    :returns:
        None

    :raises:
        None

    """

    if not os.path.exists(filename):
        os.mkdir(filename)

    if not os.path.exists(filename + "/mhit"):
        os.mkdir(filename + "/mhit")

    name = filename.split(os.sep)[-1]
    f = open(filename + "/hitmap" + name + ".vtk.series", "w")

    f.write("{ \n")
    f.write('\t "file-series-version" : "1.0", \n ')
    f.write('\t "files" : [ \n')

    for step in range(0, len(projection["num"])):
        points = np.array(projection["detector_space"][step])
        if len(points) > 0:
            point_ind = np.linspace(0, len(projection["detector_space"][step]),
                                    len(projection["detector_space"][step]),
                                    endpoint=False).astype(int)
            cells = point_ind.reshape(-1, 1)
            point_data = {"chemistry": projection["chemistry"][step]}
            write_vtk(filename + "/mhit/hitmap_" + "_" + str(step) + ".vtk",
                      points, cells, point_data=point_data)

            f.write('\t \t { "name" : ' + '"mhit/hitmap_' + "_" + str(step) +
                    '.vtk", "time": ' + str(projection["num"][step]) + ' }')
            if step != len(projection["num"]) - 1:
                f.write(',')
            f.write('\n')

    f.write('\t ] \n')
    f.write('}')
    f.close()

def output_phasemap_vtk(chem_vals, points, simplices, times, out_dir, remesh=True):

    """

    Method for writing detector phase maps (determined by composition
    clustering) to .vtk

    :param1 chem_vals (list): The clustered local chemistry
    :param2 points (np.array, (M, 3), dtype=float): array of points defining
                                                    the tessellated detector
    :param3 simplices (np.array, (N, 3), dtype=int): array of simplices
                                                     (triangle elements) defining
                                                     the tessellated detector
    :param4 times (list): The experiment "time" associated with each hitmap
    :param5 out_dir (str): The output directory for the phase map .vtks.
    :param6 remesh (bool): Whether detector hitmap was remeshed (1) or not (0)

    :returns:
        None

    :raises:
        None

    """

    #print("create directory if doesn't exist")
    if not os.path.exists(out_dir):
        print("Creating phase map output directory: " + out_dir)
        os.mkdir(out_dir)

    if not os.path.exists(out_dir + "/phases"):
        os.mkdir(out_dir + "/phases")

    name = out_dir.split(os.sep)[-1]
    f = open(out_dir + "/phase_plot" + name + ".vtk.series", "w")

    f.write("{ \n")
    f.write('\t "file-series-version" : "1.0", \n ')
    f.write('\t "files" : [ \n')

    for step in range(0, len(chem_vals)):
        if remesh is True:
            s = simplices[step]
            p = points[step]
        else:
            s = simplices
            p = points
        cells = s

        cell_data = {"cluster": chem_vals[step].reshape(-1, 1)}
        write_vtk(out_dir + "/phases/phase_plot" + str(step) + ".vtk", p, cells,
                  cell_data, point_data={})
        f.write('\t \t { "name" : ' + '"phases/phase_plot' + str(step) +
                '.vtk", "time": ' + str(times[step]) + ' }')
        if step != len(chem_vals) - 1:
            f.write(',')
        f.write('\n')

    f.write('\t ] \n')
    f.write('}')
    f.close()

def output_mapping_to_vtk(isolines, filename):

    """

    output detector isolines to .vtk (for viewing in paraview)

    :param1 isolines (list): a list of 3D coordinates for the detector isolines
                             (calculated via the reconstruction_mapping.py module)
    :param2 filename (str): The directory in which to store the projection data

    :returns:
        None

    :raises:
        None

    """
    accum_int = 0
    line_step = np.array([[], []], dtype=int).T
    flow = np.array([[], [], []]).T

    for i in range(0, len(isolines)):
        vec = np.linspace(0, len(isolines[i]) - 1, len(isolines[i]) - 1,
                          endpoint=False).astype(int) + accum_int
        line_step_t = np.vstack((vec, vec + 1)).T
        line_step = np.vstack((line_step, line_step_t))
        flow = np.vstack((flow, isolines[i]))
        accum_int += len(isolines[i])

    cells = line_step
    write_vtk(filename + "/isolines.vtk", flow, cells)

def export_pos(filename, vals):

    """

    Method for exporting a 1D array as a pos file (assuming elements in val are
    placed within the correct order)

    :param1 filename (str): The output .epos filename
    :param2 vals (np.array, (A), dtype = float): The 1D array of values that
                                                 will occupy the output .epos file

                                                Columns:
                                                x: Reconstructed x position
                                                y: Reconstructed y position
                                                z: Reconstructed z position
                                                Da: mass/charge ratio of ion

    :returns:
        None

    :raises:
        None

    """

    n = len(vals)

    o = struct.pack('>'+'f'*n, *vals)
    f = open(filename, 'wb')
    f.write(o)
    f.close()

def write_vtk(filename, vertices, cells, cell_data={}, point_data={}):

    """

    Function for saving a triangle surface mesh to a vtk ASCII format file

    :param1 filename (str): The output vtk filename
    :param2 vertices (np.array, (M, 3), dtype = float): The mesh vertices
    :param3 cells (np.array, (N, 3), dtype = int): The mesh cells
                                                   (connectivity matrix)
    :param3 cell_data (dict): Dictionary of mesh cell data (each of length N)
    :param4 point_data (dict): Dictionary of mesh point data (each of length M)

    :returns:
        :returncode (int): 0 if successful

    :raises:
        AssertionError: array of vertices of length 0
        AssertionError: array of cells of length 0
        AssertionError: individual cell of dimension 0
        AssertionError: cell_data names contain spaces
        AssertionError: point_data names contain spaces
        FileNotFoundError: Cannot write to file

    """

    #creat directory if it doesn't exist
    out_dir = "/".join(filename.split("/")[:-1])
    if len(out_dir) != 0:
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)

    #Must have vertices
    assert(len(vertices))

    #Must have cells
    assert(len(cells))

    #cells must have contents
    assert(len(cells[0]))

    #append zero z-column if vertices only 2D
    if len(vertices[0]) == 2:
        vertices = np.hstack((vertices, np.array([0.0] * len(vertices)).reshape(-1, 1)))

    #no spaces in names
    for i in cell_data:
        assert(not " " in i)

    for i in point_data:
        assert(not " " in i)

    try:
        f = open(filename, 'w')

    except FileNotFoundError:
        return False

    f.write("# vtk DataFile Version 4.2\n")
    f.write("#written by casra\n")
    f.write("ASCII\n")
    f.write("DATASET UNSTRUCTURED_GRID\n")

    # write points and cells
    f.write("POINTS {} {}\n".format(len(vertices), "double"))
    for i in vertices:
        v = [str(j) for j in i]
        tmpstr = " ".join(v)
        f.write(tmpstr)
        f.write("\n")

    f.write("CELLS {} {}\n".format(len(cells), len(cells)*(len(cells[0])+1)))
    #Map number of cells to paraview ID
    for i in cells:
        for j in i:
            c = [str(j) for j in i]
        c.insert(0, str(len(i)))
        cell_row = " ".join(c)
        f.write(cell_row)
        f.write("\n")

    f.write("CELL_TYPES {}\n".format(len(cells)))
    #FIXME: This is a very limited subset of supported types.
    # see https://lorensen.github.io/VTKExamples/site/VTKFileFormats/
    cell_len_to_type = {1:1, 2:3, 3:5, 4:9}
    for i in cells:
        f.write(str(cell_len_to_type[len(i)]))
        f.write("\n")

    f.write("POINT_DATA " + str(len(vertices)) + "\n")

    #currently only scalar quantities can be written
    for key, values in point_data.items():
        f.write("SCALARS " + key + " double 1" + "\n")
        f.write("LOOKUP_TABLE default" + "\n")
        for i in range(0, len(values)):
            f.write(str(values[i]))
            f.write(" ")
        f.write("\n")

    f.write("CELL_DATA " + str(len(cells)) + "\n")
    f.write("FIELD FieldData " + str(len(cell_data)) + "\n")

    #cell data supports scalars and tuples
    for key, values in cell_data.items():
    #define tuple dimension
        attribute_dim = len(values[0])
        f.write("{} {} {} double\n".format(key, attribute_dim, len(values)))
        #write cell attribute
        for i in range(0, len(values)):
        #write tuple values
            for j in range(0, len(values[i])):
                f.write(str(values[i][j]))
                f.write(" ")
            f.write("\n")
    f.close()

    return True
