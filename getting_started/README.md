# Getting Started

This directory contains ipython notebooks for taking the user through various examples on how to use CASRA 3D. 

The current examples included are:

* Example 1: The FinFET (amorphous simulation, deriving reconstruction mappings, visualising reconstruction mappings)
* Example 2: The vertical grain boundary (crystallographic simulation, synthetic APT data generation, performing conventional point-projection reconstructions)
* Example 3: The precipitate (amorphous simulation, synthetic APT data generation, model-driven reconstruction)
