#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""

Module for performing hitmap density calculations from a uniform projection stability data
Functions in module correctly account for trajectory crossover (correctly summing density
contributions from flipped projected surface panels)

Module must be compiled prior to using casra_3d using the following command when in the casra_3d directory:
`python3 setup.py build_ext --inplace`

Author: Charles Fletcher
Atom Probe Tomography Group, Department of Materials, University of Oxford
November 2020

"""

import cython
import numpy as np
cimport numpy as np
from cython.parallel import prange
from libc.math cimport round
from libcpp.vector cimport vector
from libcpp.algorithm cimport sort

cdef extern from "math.h" nogil:
    double atan2(double a, double b)

cdef extern from "math.h" nogil:
    double fabs(double v)

cdef int compute_code(double x, double y, double sxmin, double sxmax, double symin, double symax) nogil:

  """

  Identify region around square which the point (x, y) belongs

  :param1 x (double): test point x coordinate
  :param2 y (double): test point y coordinate
  :param3 sxmin (double): rectangle left x coord
  :param4 sxmax (double): rectangle right x coord
  :param5 symin (double): rectangle lower y coord
  :param6 symax (double): rectangle upper y coord

  :returns:
    :code (int): corresponds to region

  """

  cdef int code = 0
  if x < sxmin:
    code |= 1
  elif x > sxmax:
    code |= 2
  if y < symin:
    code |= 4
  elif y > symax:
    code |= 8
  return code

#removes error checking code from compiled cython for matrix index behaviour
@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cdef (bint, double, double, double, double) cohen_sutherland(double sxmin, double sxmax, double symin, double symax, double x1, double y1, double x2, double y2) nogil:


  """

  Implementation of the cohen_sutherland algorithm for calculating intersecting line segment
  of a line segment and rectangle

  :param3 sxmin (double): rectangle left x coord
  :param4 sxmax (double): rectangle right x coord
  :param5 symin (double): rectangle lower y coord
  :param6 symax (double): rectangle upper y coord
  :param5 x1 (double): line segment point 1 x coord
  :param5 y1 (double): line segment point 1 y coord
  :param5 x2 (double): line segment point 2 x coord
  :param5 y2 (double): line segment point 2 y coord

  :returns:
    :accept (bool): whether intersects or not
    :x1 (double): if intersects, intersection point 1 x coord
    :y1 (double): if intersects, intersection point 1 y coord
    :x2 (double): if intersects, intersection point 2 x coord
    :y2 (double): if intersects, intersection point 2 y coord

  """

  cdef double x, y
  cdef int code1 = compute_code(x1, y1, sxmin, sxmax, symin, symax)
  cdef int code2 = compute_code(x2, y2, sxmin, sxmax, symin, symax)
  cdef int code_out = 0
  cdef bint accept = False

  if y2 == y1 and x2 == x1:
    return False, x1, y1, x2, y2

  while True:
    if (code1 == 0) * (code2 == 0):
      accept = True
      break

    elif (code1  & code2) != 0:
      break

    else:
      x = 1.0
      y = 1.0
      if code1 != 0:
        code_out = code1
      else:
        code_out = code2

      if code_out & 8:
        x = x1 + (x2 - x1) * (symax - y1)/(y2 - y1)
        y = symax
      elif code_out & 4:
        x = x1 + (x2 - x1) * (symin - y1)/(y2 - y1)
        y = symin
      elif code_out & 2:
        y = y1 + (y2 - y1) * (sxmax - x1)/(x2 - x1)
        x = sxmax
      elif code_out & 1:
        y = y1 + (y2 - y1) * (sxmin - x1)/(x2 - x1)
        x = sxmin

      if code_out == code1:
        x1 = x
        y1 = y
        code1 = compute_code(x1, y1, sxmin, sxmax, symin, symax)

      else:
        x2 = x
        y2 = y
        code2 = compute_code(x2, y2, sxmin, sxmax, symin, symax)

  return accept, x1, y1, x2, y2

#removes error checking code from compiled cython for matrix index behaviour
@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cpdef (bint, double, double, double, double) cohen_sutherland_py(double sxmin, double sxmax, double symin, double symax, double x1, double y1, double x2, double y2):


  """

  Implementation of the cohen_sutherland algorithm for calculating intersecting line segment
  of a line segment and rectangle

  Callable directly from Python

  :param3 sxmin (double): rectangle left x coord
  :param4 sxmax (double): rectangle right x coord
  :param5 symin (double): rectangle lower y coord
  :param6 symax (double): rectangle upper y coord
  :param5 x1 (double): line segment point 1 x coord
  :param5 y1 (double): line segment point 1 y coord
  :param5 x2 (double): line segment point 2 x coord
  :param5 y2 (double): line segment point 2 y coord

  :returns:
    :accept (bool): whether intersects or not
    :x1 (double): if intersects, intersection point 1 x coord
    :y1 (double): if intersects, intersection point 1 y coord
    :x2 (double): if intersects, intersection point 2 x coord
    :y2 (double): if intersects, intersection point 2 y coord

  """


  cdef double x, y
  cdef int code1 = compute_code(x1, y1, sxmin, sxmax, symin, symax)
  cdef int code2 = compute_code(x2, y2, sxmin, sxmax, symin, symax)
  cdef int code_out = 0
  cdef bint accept = False

  if y2 == y1 and x2 == x1:
    return False, x1, y1, x2, y2

  while True:
    if (code1 == 0) * (code2 == 0):
      accept = True
      break

    elif (code1  & code2) != 0:
      break

    else:
      x = 1.0
      y = 1.0
      if code1 != 0:
        code_out = code1
      else:
        code_out = code2

      if code_out & 8:
        x = x1 + (x2 - x1) * (symax - y1)/(y2 - y1)
        y = symax
      elif code_out & 4:
        x = x1 + (x2 - x1) * (symin - y1)/(y2 - y1)
        y = symin
      elif code_out & 2:
        y = y1 + (y2 - y1) * (sxmax - x1)/(x2 - x1)
        x = sxmax
      elif code_out & 1:
        y = y1 + (y2 - y1) * (sxmin - x1)/(x2 - x1)
        x = sxmin

      if code_out == code1:
        x1 = x
        y1 = y
        code1 = compute_code(x1, y1, sxmin, sxmax, symin, symax)

      else:
        x2 = x
        y2 = y
        code2 = compute_code(x2, y2, sxmin, sxmax, symin, symax)

  return accept, x1, y1, x2, y2

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cdef double triangle_rectangle_intersection(double sxmin, double symin,
                                            double sxmax, double symax,
                                            double txmin, double tymin,
                                            double txmax, double tymax,
                                            double[::1] t0, double[::1] t1,
                                            double[::1] t2, double tarea) nogil:

  """

  Calculate area of intersection for a triangle and rectangle

  :param1 sxmin (double): rectangle left x coord
  :param2 sxmax (double): rectangle right x coord
  :param3 symin (double): rectangle lower y coord
  :param4 symax (double): rectangle upper y coord
  :param5 txmin (double): triangle bounding box left x coord
  :param6 txmax (double): triangle bounding box right x coord
  :param7 tymin (double): triangle bounding box lower y coord
  :param8 tymax (double): triangle bounding box upper y coord
  :param9 t0 (array, 2, dtype=double): triangle coordinate 1
  :param10 t1 (array, 2, dtype=double): triangle coordinate 2
  :param11 t2 (array, 2, dtype=double): triangle coordinate 3
  :param12 tarea (double): triangle area

  :returns:
    :poly_area (double): area of intersecting polygon
  """

  cdef bint ABacc, BCacc, CAacc
  cdef double AB0x, AB0y, AB1x, AB1y, BC0x, BC0y, BC1x, BC1y, CA0x, CA0y, CA1x, CA1y

  ###trivial cases
  #check if triangle bounding box is completely outside square
  if tymax < symin or txmax < sxmin or tymin > symax or txmin > sxmax:
      return 0.0

  #check if triangle bounding box is completely inside square
  if txmin >= sxmin and txmax <= sxmax and tymin >= symin and tymax <= symax:
      return tarea

  #check if square point inside triangle
  cdef bint p0 = PointInTriangle(sxmin, symin, t0, t1, t2)
  cdef bint p1 = PointInTriangle(sxmax, symin, t0, t1, t2)
  cdef bint p2 = PointInTriangle(sxmin, symax, t0, t1, t2)
  cdef bint p3 = PointInTriangle(sxmax, symax, t0, t1, t2)

  #box lies inside triangle
  if p0 * p1 * p2 * p3:
      return (sxmax - sxmin) * (symax - symin)

  ###non trivial cases
  cdef vector[double] polx
  cdef vector[double] poly

  #perform cohen-sutherland segment intersection algorithm
  ABacc, AB0x, AB0y, AB1x, AB1y = cohen_sutherland(sxmin, sxmax,
                                                   symin, symax,
                                                   t0[0], t0[1],
                                                   t1[0], t1[1])
  BCacc, BC0x, BC0y, BC1x, BC1y = cohen_sutherland(sxmin, sxmax,
                                                   symin, symax,
                                                   t1[0], t1[1],
                                                   t2[0], t2[1])
  CAacc, CA0x, CA0y, CA1x, CA1y = cohen_sutherland(sxmin, sxmax,
                                                   symin, symax,
                                                   t2[0], t2[1],
                                                   t0[0], t0[1])

  if ABacc == False and BCacc == False and CAacc == False:

      #catch case where box point lies exactly on triangle segment
      if p0 or p1 or p2 or p3:
          return (sxmax - sxmin) * (symax - symin)
      else:
          return 0.0

  if ABacc == True:
      polx.push_back(AB0x)
      poly.push_back(AB0y)
      polx.push_back(AB1x)
      poly.push_back(AB1y)

  if BCacc == True:
      if ABacc == True:
          if fabs(AB1x - BC0x) > 1e-8 or fabs(AB1y - BC0y) > 1e-8:
             polx.push_back(BC0x)
             poly.push_back(BC0y)
      else:
          polx.push_back(BC0x)
          poly.push_back(BC0y)

      polx.push_back(BC1x)
      poly.push_back(BC1y)

  if CAacc == True:
      if BCacc == True:
          if fabs(BC1x - CA0x) > 1e-8 or fabs(BC1y - CA0y) > 1e-8:
              polx.push_back(CA0x)
              poly.push_back(CA0y)
      else:
          polx.push_back(CA0x)
          poly.push_back(CA0y)

      polx.push_back(CA1x)
      poly.push_back(CA1y)

  ##check four corners - see if they make up inside polygon
  if p0 == True:
      polx.push_back(sxmin)
      poly.push_back(symin)

  if p1 == True:
      polx.push_back(sxmax)
      poly.push_back(symin)

  if p2 == True:
      polx.push_back(sxmin)
      poly.push_back(symax)

  if p3 == True:
      polx.push_back(sxmax)
      poly.push_back(symax)

  cdef int a
  cdef int b

  cdef int pc = polx.size()
  cdef double[2] centroid
  centroid[0] = 0.0
  centroid[1] = 0.0

  for a in range(pc):
    centroid[0] += polx[a]
    centroid[1] += poly[a]

  centroid[0] /= pc
  centroid[1] /= pc

  cdef vector[double] ang
  cdef vector[double] angs
  cdef vector[double] polxs
  cdef vector[double] polys

  for a in range(pc):
    ang.push_back(atan2(poly[a] - centroid[1], polx[a] - centroid[0]))

  #perform argsort to arrange polygon anticlockwise
  angs.push_back(ang[0])
  polxs.push_back(polx[0])
  polys.push_back(poly[0])

  cdef int al = 1
  cdef bint added = False

  for a in range(1, pc):
    added = False
    for b in range(al):
      if ang[a] < angs[b]:
        angs.insert(angs.begin() + b, ang[a])
        polxs.insert(polxs.begin() + b, polx[a])
        polys.insert(polys.begin() + b, poly[a])
        al += 1
        added = True
        break

    if added == False:
      angs.push_back(ang[a])
      polxs.push_back(polx[a])
      polys.push_back(poly[a])
      al += 1

  #shoelace formula for polygon area
  cdef double poly_area = 0.0

  for a in range(0, pc - 1):
    poly_area += polxs[a] * polys[a + 1] - polxs[a + 1] * polys[a]

  poly_area += polxs[pc - 1] * polys[0] - polxs[0] * polys[pc - 1]
  poly_area *= 0.5

  return fabs(poly_area)

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cdef bint PointInTriangle(double ptx, double pty, double[:] t0, double[:] t1, double[:] t2) nogil:

  """

  Function for calculating whether a point is inside or outside a triangle
  defined by points (t0, t1, t2)

  :param1 ptx (double): test point x coordinate
  :param2 pty (double): test point y coordinate
  :param3 t0 (array, 2, dtype=double): triangle point 1
  :param4 t1 (array, 2, dtype=double): triangle point 2
  :param5 t2 (array, 2, dtype=double): triangle point 3

  :return:
    :inside (bool): Whether test point is inside or outside triangle

  """

  cdef double d1 = (ptx - t1[0]) * (t0[1] - t1[1]) - (t0[0] - t1[0]) * (pty - t1[1])
  cdef double d2 = (ptx - t2[0]) * (t1[1] - t2[1]) - (t1[0] - t2[0]) * (pty - t2[1])
  cdef double d3 = (ptx - t0[0]) * (t2[1] - t0[1]) - (t2[0] - t0[0]) * (pty - t0[1])

  cdef bint has_neg = (d1 < 0) or (d2 < 0) or (d3 < 0)
  cdef bint has_pos = (d1 > 0) or (d2 > 0) or (d3 > 0)

  return has_neg*has_pos == 0

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cpdef density_hitmap(double[:, ::1] eXX, double[:, ::1] eXY, double[:, ::1] cXX, double[:, ::1] cXY, double[:, ::1] bbt, double[::1] tareas, double[:, ::1] dv0, double[:, ::1] dv1, double[:, ::1] dv2, double[:] demag, double[:] evap_rate, double det_rad):

  """

  calculates hitmap following initialisation/linker function calculate_hitmap (see below)

  :param1 eXX (np.array, (G+1, G+1), double): 2D array of x edge hitmap grid coordinates
  :param2 eXY (np.array, (G+1, G+1), double): 2D array of y edge hitmap grid coordinates
  :param3 cXX (np.array, (G, G), double): 2D array of x centre hitmap grid coordinates
  :param4 cXY (np.array, (G, G), double): 2D array of y centre hitmap grid coordinates
  :param5 bbt (np.array, (P, 4), double): 2D array containing projected panel bounding box coordinates
  :param6 tareas (np.array, (P), double): projected triangle panel areas
  :param7 dv0 (np.array, (P, 3), double): projected triangle point 1
  :param8 dv1 (np.array, (P, 3), double): projected triangle point 2
  :param9 dv2 (np.array, (P, 3), double): projected triangle point 3
  :param10 demag (np.array, (P), double): projected triangle demagnification
  :param11 evap_rate (np.array, (P), double): panel evaporation rate
  :param12 det_rad double: detector radius

  :return:
    :hitmap (np.array, (G, G), double): density hitmap

  """

  cdef int csize = len(dv0)

  cdef int a, x, y
  cdef int xc = cXX.shape[0]
  cdef int yc = cXY.shape[1]

  cdef double sqarea, sxmin, symin, sxmax, symax, area

  hitmap = np.zeros((xc, yc))
  overlap = np.zeros((xc, yc))

  cdef double[:, ::1] hitmap_view = hitmap
  cdef double[:, ::1] overlap_view = overlap

  for a in range(csize):
      for x in range(xc):
          for y in range(yc):
              sxmin = eXX[x, y]
              symin = eXY[x, y]
              sxmax = eXX[x + 1, y + 1]
              symax = eXY[x + 1, y + 1]
              sqarea = (sxmax - sxmin) * (symax - symin)
              area = triangle_rectangle_intersection(sxmin, symin, sxmax, symax, bbt[a, 0], bbt[a, 1], bbt[a, 2], bbt[a, 3], dv0[a, :2], dv1[a, :2], dv2[a, :2], tareas[a])
              hitmap_view[x, y] += area/sqarea * fabs(demag[a]) * evap_rate[a]

              #check if trajectory overlap occurring in detector region
              if demag[a] < -1e-4 and fabs(area/sqarea) > 0.01:
                  overlap_view[x, y] += 1.0

  return hitmap, overlap

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cpdef extend_hitmap(double[:, ::1] hitmap, long[:, ::1] inside):

    cdef int a, b, c, d
    cdef int hx = hitmap.shape[0]
    cdef int hy = hitmap.shape[1]

    nhitmap = np.zeros((hx, hy))
    cdef double[:, ::1] nhitmap_view = nhitmap

    cdef double closest = 1e20
    cdef double dist
    cdef double cval

    for a in range(hx):
        for b in range(hy):
            nhitmap_view[a, b] = hitmap[a, b]
            if inside[a, b] == 0:
                closest = 1e12
                for c in range(hx):
                    for d in range(hy):
                        if inside[c, d] == 1:
                            dist = (c - a)**2.0 + (d - b)**2.0
                            if dist < closest:
                                closest = dist
                                cval = hitmap[c, d]
                nhitmap_view[a, b] = cval

    return nhitmap

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cpdef calculated_matrix_system(double[:, ::1] eXX, double[:, ::1] eXY, double[:, ::1] cXX, double[:, ::1] cXY, double[:, ::1] bbt, double[::1] tareas, double[:, ::1] dv0, double[:, ::1] dv1, double[:, ::1] dv2, double[:] demag, double det_rad):

  """

  calculates hitmap following initialisation/linker function calculate_hitmap (see below)

  :param1 eXX (np.array, (G+1, G+1), double): 2D array of x edge hitmap grid coordinates
  :param2 eXY (np.array, (G+1, G+1), double): 2D array of y edge hitmap grid coordinates
  :param3 cXX (np.array, (G, G), double): 2D array of x centre hitmap grid coordinates
  :param4 cXY (np.array, (G, G), double): 2D array of y centre hitmap grid coordinates
  :param5 bbt (np.array, (P, 4), double): 2D array containing projected panel bounding box coordinates
  :param6 tareas (np.array, (P), double): projected triangle panel areas
  :param7 dv0 (np.array, (P, 3), double): projected triangle point 1
  :param8 dv1 (np.array, (P, 3), double): projected triangle point 2
  :param9 dv2 (np.array, (P, 3), double): projected triangle point 3
  :param10 demag (np.array, (P), double): projected triangle demagnification
  :param11 evap_rate (np.array, (P), double): panel evaporation rate
  :param12 det_rad double: detector radius

  :return:
    :hitmap (np.array, (G, G), double): density hitmap

  """

  cdef int csize = len(dv0)

  cdef int a, x, y
  cdef int xc = cXX.shape[0]
  cdef int yc = cXY.shape[1]

  cdef double sqarea, sxmin, symin, sxmax, symax

  matrix_system = np.zeros((xc*yc, csize))

  cdef double[:, ::1] matrix_system_view = matrix_system

  for a in range(csize):
      for x in range(xc):
          for y in range(yc):
              sxmin = eXX[x, y]
              symin = eXY[x, y]
              sxmax = eXX[x + 1, y + 1]
              symax = eXY[x + 1, y + 1]
              sqarea = (sxmax - sxmin) * (symax - symin)
              area = triangle_rectangle_intersection(sxmin, symin, sxmax, symax, bbt[a, 0], bbt[a, 1], bbt[a, 2], bbt[a, 3], dv0[a, :2], dv1[a, :2], dv2[a, :2], tareas[a])
              matrix_system_view[x + y * xc, a] += area/sqarea * fabs(demag[a])

  return matrix_system


def calculate_least_squares_system(sv0, sv1, sv2, dv0, dv1, dv2, demag, det_rad, grid_size = 61):

  """

  Python initialisation/linker function for calculating density hitmap
  from uniform projection stability data


  :param1 sv0 (np.array, (P, 3), double): specimen surface triangle point 1
  :param2 sv1 (np.array, (P, 3), double): specimen surface triangle point 2
  :param3 sv2 (np.array, (P, 3), double): specimen surface triangle point 3
  :param4 dv0 (np.array, (P, 3), double): projected triangle point 1
  :param5 dv1 (np.array, (P, 3), double): projected triangle point 2
  :param6 dv2 (np.array, (P, 3), double): projected triangle point 3
  :param7 demag (np.array, (P), double): projected triangle demagnification
  :param8 evap_rate (np.array, (P), double): panel evaporation rate
  :param9 det_rad double: detector radius
  :param10 grid_size int: The grid size for the calculated density hitmap

  :return:
    :hitmap (np.array, (G, G), double): density hitmap

  """

  dx = np.linspace(-det_rad, det_rad, grid_size, endpoint = True)
  eXX, eXY = np.meshgrid(dx, dx)
  cXX = (eXX[1:, 1:] + eXX[:-1, :-1])/2.0
  cXY = (eXY[1:, 1:] + eXY[:-1, :-1])/2.0
  bbt = []
  tareas = []

  for i in range(len(sv0)):
      t0 = dv0[i, :]
      t1 = dv1[i, :]
      t2 = dv2[i, :]

      txmin = min(t0[0], t1[0], t2[0])
      txmax = max(t0[0], t1[0], t2[0])
      tymin = min(t0[1], t1[1], t2[1])
      tymax = max(t0[1], t1[1], t2[1])
      bbt.append([txmin, tymin, txmax, tymax])

      #calculate triangle area - herons formula
      a = np.linalg.norm(t0 - t1)
      b = np.linalg.norm(t1 - t2)
      c = np.linalg.norm(t2 - t0)
      s = (a + b + c)/2.0
      tarea = (s * (s - a) * (s - b) * (s - c))**0.5
      tareas.append(tarea)

  bbt = np.array(bbt)
  tareas = np.array(tareas)

  mat_sys = calculated_matrix_system(eXX, eXY, cXX, cXY, bbt, tareas, dv0, dv1, dv2, demag, det_rad)

  return mat_sys

def calculate_hitmap(sv0, sv1, sv2, dv0, dv1, dv2, demag, evap_rate, det_rad, grid_size = 61):

  """

  Python initialisation/linker function for calculating density hitmap
  from uniform projection stability data


  :param1 sv0 (np.array, (P, 3), double): specimen surface triangle point 1
  :param2 sv1 (np.array, (P, 3), double): specimen surface triangle point 2
  :param3 sv2 (np.array, (P, 3), double): specimen surface triangle point 3
  :param4 dv0 (np.array, (P, 3), double): projected triangle point 1
  :param5 dv1 (np.array, (P, 3), double): projected triangle point 2
  :param6 dv2 (np.array, (P, 3), double): projected triangle point 3
  :param7 demag (np.array, (P), double): projected triangle demagnification
  :param8 evap_rate (np.array, (P), double): panel evaporation rate
  :param9 det_rad double: detector radius
  :param10 grid_size int: The grid size for the calculated density hitmap

  :return:
    :hitmap (np.array, (G, G), double): density hitmap

  """

  dx = np.linspace(-det_rad, det_rad, grid_size, endpoint = True)
  eXX, eXY = np.meshgrid(dx, dx)
  cXX = (eXX[1:, 1:] + eXX[:-1, :-1])/2.0
  cXY = (eXY[1:, 1:] + eXY[:-1, :-1])/2.0
  hitmap = np.zeros(cXX.shape)
  bbt = []
  tareas = []

  for i in range(len(sv0)):
      t0 = dv0[i, :]
      t1 = dv1[i, :]
      t2 = dv2[i, :]

      txmin = min(t0[0], t1[0], t2[0])
      txmax = max(t0[0], t1[0], t2[0])
      tymin = min(t0[1], t1[1], t2[1])
      tymax = max(t0[1], t1[1], t2[1])
      bbt.append([txmin, tymin, txmax, tymax])

      #calculate triangle area - herons formula
      a = np.linalg.norm(t0 - t1)
      b = np.linalg.norm(t1 - t2)
      c = np.linalg.norm(t2 - t0)
      s = (a + b + c)/2.0
      tarea = (s * (s - a) * (s - b) * (s - c))**0.5
      tareas.append(tarea)

  bbt = np.array(bbt)
  tareas = np.array(tareas)

  hitmap, overlap = density_hitmap(eXX, eXY, cXX, cXY, bbt, tareas, dv0, dv1, dv2, demag, evap_rate, det_rad)
  hitmap[cXX**2.0 + cXY**2.0 > det_rad**2.0] = 0.0
  overlap[cXX**2.0 + cXY**2.0 > det_rad**2.0] = 0.0
  #overlap[overlap > 0.0] = 1.0

  return hitmap, overlap
