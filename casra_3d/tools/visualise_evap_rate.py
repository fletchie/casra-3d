"""

Module for outputting the projected evaporation rate onto a regular grid
Currently does not account for crossover (could be corrected for via KDE)

Author: Charles Fletcher
Atom Probe Tomography Group, Department of Materials, University of Oxford
September 2020

"""

import os
import csv
import sys

try:
    import numpy as np
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python package numpy. " +
                              "Please make sure package is installed.")

try:
    import scipy as sp
    import scipy.interpolate as spinterpolate
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python package Scipy. " +
                              "Please make sure package is installed.")

from casra_3d.io import model_loaders as model_loaders
from casra_3d.io import model_writers as model_writers

def projected_evaporation_rate(mapping_data, det_rad, dirname, points=50):

    """

    Function for interpolating local evaporation rates onto a regular grid
    and outputting to .vtk

    :param1 mapping_data (str): The model mapping data .tsv filename
    :param2 det_rad (float): The detector radius
    :param3 dirname (str): The output directory name
    :param4 points (int): Number of grid points for interpolation onto

    :returns:
        None

    :raises:
        :ValueError: No mapping_data filename passed (or mapping_data str blank)
        :ValueError: No dirname output directory passed (or dirname str blank)

    """

    if not os.path.exists(dirname):
        os.makedirs(dirname)

    projection = {"num": [], "sample_space": [], "detector_space": [],
                  "chemistry": [], "ion_phase": [], "trajectories": [],
                  "panel_index": [], "evap_rate": []}

    model_loaders.load_tsv_ion_trajectories(projection, mapping_data)
    sample_points = np.linspace(-det_rad, det_rad, points)

    xx, xy = np.meshgrid(sample_points, sample_points)
    un_mapping = np.unique(projection["num"])

    for m_ind in range(0, len(un_mapping)):
        det_space = projection["detector_space"][projection["num"] == un_mapping[m_ind]]
        evap_rate = projection["evap_rate"][projection["num"] == un_mapping[m_ind]]

        int_evap_rate = spinterpolate.griddata(det_space, evap_rate,
                                               (xx, xy), method='linear')

        cx = xx.flatten()
        cy = xy.flatten()
        int_evap_rate = int_evap_rate.flatten()
        coords = np.vstack((cx, cy)).T

        #sety evaporation rate to zero for points outside of FOV
        rad2 = cx**2.0 + cy**2.0
        int_evap_rate[rad2 > det_rad**2.0] = 0.0
        int_evap_rate = int_evap_rate/np.sum(int_evap_rate)

        cells = np.linspace(0, len(coords), len(coords),
                            endpoint=False, dtype=int).reshape(-1, 1)
        out_name = dirname + "/proj_evap_rate" + str(m_ind) + ".vtk"
        model_writers.write_vtk(out_name, coords, cells,
                                point_data={"evap_rate": int_evap_rate})

    print("Evaporation rate vtk output complete.")

#run if script executed via the command line
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--mapping_data', metavar='filename',
                        type=str, default="",
                        help='.tsv containing mapping data')
    parser.add_argument('--det_radius', metavar='detector radius',
                        type=float, default=np.inf,
                        help='The detector radius')
    parser.add_argument('--dirname', metavar='directory name',
                        type=str, default="",
                        help='The directory to save the projected ' +
                        'evaporation rate')
    parser.add_argument('--points', metavar='points',
                        type=int, default=50,
                        help='The number of grid points')
    args = parser.parse_args()

    #simulation parameter definition
    mapping_data = args.mapping_data
    det_rad = args.det_radius
    dirname = args.dirname
    points = args.points

    if len(mapping_data) == 0:
        raise ValueError("mapping data .tsv file must be parsed to " +
                         "--mapping_data argument")
    if len(dirname) == 0:
        raise ValueError("simulation directory must be parsed to " +
                         "--dirname argument")

    projected_evaporation_rate(mapping_data, det_rad, dirname, points=points)
