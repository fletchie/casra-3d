
"""

Module for running the model evolution iterations

Author: Charles Fletcher
Atom Probe Tomography Group, Department of Materials, University of Oxford
May 2020

"""

import copy
import math

from casra_3d.electrostatic_solver.cbem import BEM_matrix_construction as BEM_matrix_construction
from casra_3d.ls_operators.extension_velocity import extension_velocity as extension_velocity
from casra_3d.ls_operators import sdf as sdf
from casra_3d.mesh.cgal import cgal_linker as cgal_linker
from casra_3d.mesh.non_cgal import mesh_library as mesh_library
from casra_3d.ls_operators import reinitialization as reinitialization

try:
    import scipy
    from scipy.ndimage.filters import gaussian_filter
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python scipy module. Please make sure it is installed.")

try:
    import numpy as np
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python numpy module. Please make sure it is installed.")

try:
    from skimage import measure
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python module skimage. Please make sure " +
                              "it is installed.")

#calculate mesh volume
#calculates enclosed area via the 3d Shoelace formula generalisation
def calculate_volume(verts, faces):

    """

    Calculates mesh volume via 3D Shoelace formula

    :param1 verts (np.array, (M, 3), dtype=float): The input mesh vertices
    :param2 faces (np.array, (N, 3) dtype=int): the input mesh faces

    returns:
        :mesh volume (float): The closed triangle mesh volume

    :raises: None

    """

    vol = 0.0
    for face in faces:
        vol = vol + 1.0/6.0 * np.linalg.det([verts[face[0]], verts[face[1]], verts[face[2]]])
    return vol

def field_iteration_cgal(field, it, geom, qc=np.array([0.0]), adaptive=True, voltage_ramping=True):

    """

    Performs level set field iteration for the sample evaporation

    :param1 field (np.array, (L1,L2,L3), dtype=float): The level set field from the prior iteration
    :param2 it (int): The current iteration count
    :param3 geom (dict): The simulation dictionary
    :param4 adaptive (bool): Whether to perform adaptive remeshing along the shank
    :param5 voltage_ramping (bool): Whether to perform the voltage ramping

    :returns:
        :updated_field (np.array,(L1,L2,L3), dtype=float):
        :dt (float)
        :u (np.array, (N, 3), dtype=float):
        :qc (np.array, (N), dtype=float):
        :cp (np.array, (N, 3), dtype=float):
        :areasc (np.array, (N), dtype=float):
        :normsc (np.array, (N, 3), dtype=float):
        :verts (np.array, (M, 3), dtype=float):
        :faces (np.array, (N, 3), dtype=int):
        :F0a (np.array, (N), dtype=float):
        :qcamed (np.array, (N), dtype=float):
        :sample_surface (np.array, (N), dtype=int):
        :smoothed_velocity (np.array, (N), dtype=float):
        :volume (float):
        :voltage (float):
        :max_z (float): The maximum tip height
        :temperature (np.array, (N), dtype=float):
        :iteration_stability (int):

    :raises:
        None

    """

    iteration_stability = 1

    #obtain simulation parameters
    amp = geom["parameters"]["amplitude"]
    beta = geom["parameters"]["beta"]
    cfl = geom["parameters"]["CFL"]
    min_shank_length = geom["parameters"]["min_shank_length"]
    shank_angle = geom["parameters"]["shank_angle"]
    reinit = geom["parameters"]["reinit"]

    shank = geom["simulation_grid"]["inactive"]
    offx = geom["simulation_grid"]["xmin"]
    offy = geom["simulation_grid"]["ymin"]
    offz = geom["simulation_grid"]["zmin"]

    ###initialise level set field arrays
    forward_x = np.zeros(field.shape)
    forward_y = np.zeros(field.shape)
    forward_z = np.zeros(field.shape)
    backward_x = np.zeros(field.shape)
    backward_y = np.zeros(field.shape)
    backward_z = np.zeros(field.shape)
    dphi_t = np.zeros(field.shape)
    #perform model reinitialisation
    if reinit > 0:
        if it % reinit == 0 and it != 0:
            print("perform reinitialisation")
            field[:, :, :] = reinitialization.sussman_reinit(field[:, :, :], forward_x[:, :, :],
                                                             forward_y[:, :, :], forward_z[:, :, :],
                                                             backward_x[:, :, :], backward_y[:, :, :],
                                                             backward_z[:, :, :])
            print("reinitialisation completed")


    #extract mesh from level set scalar field via marching cubes
    verts, faces, normals, values = measure.marching_cubes_lewiner(field,
                                                                   0.0, allow_degenerate=False)
    verts = verts.astype("double")
    faces = faces.astype("long")
    volume = abs(mesh_library.calculate_volume(verts, faces))

    ###construct face-face adjacency list
    face_face_graph = np.array(mesh_library.adjacency_list_face_faces(verts, faces))

    #perform depth-first search for identifying connected regions
    mesh_list = []
    mesh_listc = []
    inc_faces = np.zeros((len(faces)), dtype=int) + 1
    tfaceind = np.linspace(0, len(inc_faces), len(inc_faces), endpoint=False, dtype=int)

    while np.sum(inc_faces) != 0:
        tr = tfaceind[inc_faces == 1][0]
        connected = mesh_library.DFS_iterative(face_face_graph, tr)
        mesh_listc.append(connected)
        #create new mesh dictionary
        mesh = {}

        #obtain separate meshes
        vind, rfaces = np.unique(faces[connected == 1], return_inverse=True)
        mesh["faces"] = rfaces.reshape(-1, 3)
        mesh["vertices"] = verts[vind]

        #append meshes
        mesh_list.append(mesh)
        inc_faces = inc_faces - connected

    if len(mesh_list) > 1:
        lv = -1
        lvlen = 0

        #find largest contour - corresponds to surface
        for a in range(0, len(mesh_list)):
            mesh_list[a]["volume"] = abs(mesh_library.calculate_volume(mesh_list[a]["vertices"],
                                                                       mesh_list[a]["faces"]))
            if len(mesh_list[a]["faces"]) > lvlen:
                lvlen = len(mesh_list[a]["faces"])
                lv = a
                verts = mesh_list[a]["vertices"]
                faces = mesh_list[a]["faces"]
                volume = mesh_list[a]["volume"]

    #calculate mesh volume prior to mesh coarsening
    #if multiple contours detected i.e. voids, then remove volume from mesh
        for a in range(0, len(mesh_list)):
            if a != lv:
                if mesh_list[a]["volume"] > 2:
                    volume -= mesh_list[a]["volume"]

    else:
        volume = abs(mesh_library.calculate_volume(verts, faces))

    #perform shank extension - fill in missing shank information
    if min_shank_length > 0:
        if (np.max(verts[:, 2]) - np.min(verts[:, 2]) <
                (min_shank_length/geom["simulation_grid"]["cell_width"])):
            base_shift = ((min_shank_length/geom["simulation_grid"]["cell_width"]) -
                          (np.max(verts[:, 2]) - np.min(verts[:, 2])))
            filt = verts[:, 2] < (np.min(verts[:, 2]) + 1.5)
            verts[filt, 2] -= base_shift
            base_centre = np.mean(verts[filt, :2], axis=0)

            #also need to expand x,y for shank
            angles = np.arctan2(verts[:, 1] - base_centre[1], verts[:, 0] - base_centre[0])
            verts[filt, 0] += np.cos(angles[filt]) * base_shift * math.tan(shank_angle)
            verts[filt, 1] += np.sin(angles[filt]) *base_shift * math.tan(shank_angle)


    #simplify mesh to provide better (less skinny) triangles
    verts = verts.astype("double")
    faces = faces.astype("long")

    #only calculate average edge length from non-shank extended model panels
    cp = np.mean(verts[faces], axis=1)
    ffaces = faces[cp[:, 2] >= 0.0]

    dist = np.linalg.norm(verts[ffaces[:, 2]] - verts[ffaces[:, 1]], axis=1)
    dist = np.mean(dist) * 1.3

    #figure out remeshing
    mvalues = np.arange(np.max(verts[:, 2]) + 1e-3, np.min(verts[:, 2]), -10.0)
    fv = (dist*1.8)**np.arange(0, len(mvalues), 1)

    #verts, faces = cgal_linker.remeshing_geometry(verts, faces, dist, False)
    if adaptive == True:
        mvalues = mvalues[fv < 8 * dist]
        fv = fv[fv < 8 * dist]
        verts, faces = cgal_linker.adaptive_remeshing(verts, faces, mvalues, fv, False)
    else:
        mvalues = np.array([mvalues[0]])
        fv = np.array([fv[1]])
        verts, faces = cgal_linker.adaptive_remeshing(verts, faces, mvalues, fv, False)

    #transform mesh to correct simulation scale - for electrostatic calculations
    verts[:, 0] += offx
    verts[:, 1] += offy
    verts[:, 2] += offz

    verts = verts * geom["simulation_grid"]["cell_width"]

    #only solve electric field over surface
    verts = verts.astype("double")
    faces = faces.astype("long")

    #compute longest side squared
    ls2x = np.sum(((verts[faces[:, 1]] -  verts[faces[:, 0]]) *
                   (verts[faces[:, 1]] -  verts[faces[:, 0]])),
                  axis=1)
    ls2y = np.sum(((verts[faces[:, 2]] -  verts[faces[:, 1]]) *
                   (verts[faces[:, 2]] -  verts[faces[:, 1]])),
                  axis=1)
    ls2z = np.sum(((verts[faces[:, 0]] -  verts[faces[:, 2]]) *
                   (verts[faces[:, 0]] -  verts[faces[:, 2]])),
                  axis=1)
    ls2 = np.maximum(np.maximum(ls2x, ls2y), ls2z)

    u = np.array([1.0] * len(faces))
    Fc, Gc, areasc, normsc = BEM_matrix_construction.matrix_construction(verts, faces, ls2, 5e-3)
    qc = np.array([0.0] * len(u))

    #define simple block preconditioner
    M = np.diag(1.0/np.diag(Gc))

    #solve field via GMRES
    qc[:], info = scipy.sparse.linalg.gmres(Gc, np.matmul(Fc, u), M=M)
    #run direct LU solver if GMRES failed to converge
    if info != 0:
        qc[:] = np.linalg.solve(Gc, np.matmul(Fc, u))

    #median filter over surface flux values (to remove collocation spikes)
    qcamed = mesh_library.average_field(faces, areasc, qc[:])
    qcamed[qcamed > 0.0] = 0.0

    #transform coordinates back for extension velocity construction
    verts = verts / geom["simulation_grid"]["cell_width"]
    cp = np.mean(verts[faces], axis=1)

    use_phase_grid = True

    #calculate phase fields from phase grid, and only fall back to point-in-polygon
    #if phase directly intersects grid cell
    if use_phase_grid == True:
        gcx = ((geom["phase_grid"].shape[0] * (cp[:, 0] - geom["pgxmin"])/
                (1e-6 + geom["pgxmax"] - geom["pgxmin"]))).astype(int)
        gcy = ((geom["phase_grid"].shape[1] * (cp[:, 1] - geom["pgymin"])/
                (1e-6 + geom["pgymax"] - geom["pgymin"]))).astype(int)
        gcz = ((geom["phase_grid"].shape[2] * (cp[:, 2] - geom["pgzmin"])/
                (1e-6 + geom["pgzmax"] - geom["pgzmin"]))).astype(int)

        F0a_ind = []
        for a in range(0, len(gcx)):
            if (gcz[a] >= 0 and gcx[a] < geom["phase_grid"].shape[0] and
                    gcx[a] >= 0 and gcy[a] < geom["phase_grid"].shape[1] and
                    gcy[a] >= 0 and gcz[a] < geom["phase_grid"].shape[2]):
                F0a_ind.append(geom["phase_grid"][gcx[a], gcy[a], gcz[a]])
            else:
                F0a_ind.append(-100000)

        F0a_ind = np.array(F0a_ind)
        F0a = np.array([0.0] * len(F0a_ind))

        for o in range(0, len(geom["objts"])):
            if geom["objts"][o]["surface"] == 1:
                for a in range(0, len(cp)):
                    if geom["objts"][o]["amorphous"] == True:
                        F0a[a] = geom["objts"][o]["field"]
                        bfield = geom["objts"][o]["field"]
                    else:
                        F0a[a] = anisotropy(normsc[a], geom["objts"][o]["field"],
                                            geom["objts"][o]["mx"], geom["objts"][o]["my"],
                                            geom["objts"][o]["mz"], geom["objts"][o]["mag"])
                        bfield = geom["objts"][o]["field"]

        for o in range(0, len(geom["objts"])):
            pverts = geom["objts"][o]["sverts"]
            pfaces = geom["objts"][o]["faces"]
            for a in range(0, len(cp)):
                if F0a_ind[a] == -100000:
                    F0a[a] = bfield
                elif F0a_ind[a] == geom["objts"][o]["phase_number"]:
                    if geom["objts"][o]["amorphous"] == True:
                        F0a[a] = geom["objts"][o]["field"]
                    else:
                        F0a[a] = anisotropy(normsc[a], geom["objts"][o]["field"],
                                            geom["objts"][o]["mx"], geom["objts"][o]["my"],
                                            geom["objts"][o]["mz"], geom["objts"][o]["mag"])

        for a in range(0, len(cp)):
            if F0a_ind[a] < 0 and F0a_ind[a] != -100000:
                #if panel inside phase mesh
                for o in range(0, len(geom["objts"])):
                    pverts = geom["objts"][o]["sverts"]
                    pfaces = geom["objts"][o]["faces"]
                    if (sdf.pyinside(cp[a, 0], cp[a, 1], cp[a, 2], pverts, pfaces,
                                     len(pfaces)) == False):
                        if geom["objts"][o]["amorphous"] == True:
                            F0a[a] = geom["objts"][o]["field"]
                        else:
                            F0a[a] = anisotropy(normsc[a], geom["objts"][o]["field"],
                                                geom["objts"][o]["mx"], geom["objts"][o]["my"],
                                                geom["objts"][o]["mz"], geom["objts"][o]["mag"])

    #calculate phase fields directly from phase meshes
    if use_phase_grid == False:
        ####Set the evaporation field here - point in polygon test####
        #set to default field value
        F0a = []
        for a in range(0, len(cp)):
            for o in range(0, len(geom["objts"])):
                if geom["objts"][o]["surface"] == 1:
                    if geom["objts"][o]["amorphous"] == True:
                        F0a.append(geom["objts"][o]["field"])
                    else:
                        F0a.append(anisotropy(normsc[a], geom["objts"][o]["field"],
                                              geom["objts"][o]["mx"], geom["objts"][o]["my"],
                                              geom["objts"][o]["mz"], geom["objts"][o]["mag"]))

            #perform point in polygon test for all phases - break if true
            #overlapping phases listed higher up in geom object will take priority
            for o in range(0, len(geom["objts"])):
                if geom["objts"][o]["surface"] != 1 and geom["objts"][o]["mat_id"] != 0:
                    #if check failed, perform signed distance check
                    pverts = (geom["objts"][o]["verts"] * geom["simulation_grid"]["scale"]/
                              geom["simulation_grid"]["cell_width"])
                    pfaces = geom["objts"][o]["faces"]
                    if sdf.pyinside(cp[a, 0], cp[a, 1], cp[a, 2], pverts,
                                    pfaces, len(pfaces)) == False:
                        if geom["objts"][o]["amorphous"] == True:
                            F0a[-1] = geom["objts"][o]["field"]
                        else:
                            F0a[-1] = anisotropy(normsc[a], geom["objts"][o]["field"],
                                                 geom["objts"][o]["mx"], geom["objts"][o]["my"],
                                                 geom["objts"][o]["mz"], geom["objts"][o]["mag"])
                        break

        F0a = np.array(F0a)

    dbeta = np.array([beta] * len(cp))

    #find maximum voltage - filtering apex cells
    #af = cp[:, 2] > (np.min(cp[:, 2]) + (np.max(cp[:, 2]) - np.min(cp[:, 2])) * 0.9)
    af = cp[:, 2] > (np.max(cp[:, 2]) - 8.0)
    scv = np.max(abs(qcamed[af])/F0a[af])

    ####
    #log of the surface velocity
    if voltage_ramping == True:
        #surface_velocity = np.log(amp) - beta * (1.0 - 1.0/scv * abs(qcamed)/F0a)
        field_frac = dbeta * (1.0 - 1.0/scv * abs(qcamed)/F0a)
        surface_velocity = np.log(amp) - field_frac
        surface_velocity = - np.exp(surface_velocity)
    else:
        #print(amp, beta, u[0], np.unique(F0a))
        surface_velocity = np.log(amp) - dbeta * (1.0 - 1.0 * abs(qcamed)/F0a)
        surface_velocity = - np.exp(surface_velocity)

    sample_surface = np.array([1]*len(faces))

    ###if voids detected
    if len(mesh_list) > 1:
        #build up zero
        for a in range(0, len(mesh_list)):
            if a != lv:
                #ensure not detached blob - remove if volume smaller than two level cet unit
                if mesh_list[a]["volume"] > 2:
                    tf = mesh_list[a]["faces"] + len(verts)

                    tv = copy.deepcopy(mesh_list[a]["vertices"])
                    tv[:, 0] += offx
                    tv[:, 1] += offy
                    tv[:, 2] += offz

                    verts = np.vstack((verts, tv))
                    faces = np.vstack((faces, tf))

                    surface_velocity = np.concatenate((surface_velocity,
                                                       (np.array([0.0] *
                                                                 len(mesh_list[a]["faces"])))))

                    sample_surface = np.append(sample_surface,
                                               np.array([0] * len(mesh_list[a]["faces"])))

                    lcp = np.mean((tv)[mesh_list[a]["faces"]], axis=1)
                    mesh_list[a]["cp"] = lcp
                    cp = np.vstack((cp, lcp))

                    dbeta = np.append(dbeta, np.array([beta] * len(mesh_list[a]["faces"])))
                    qcamed = np.append(qcamed, np.array([0.0] * len(mesh_list[a]["faces"])))
                    qc = np.append(qc, np.array([0.0] * len(mesh_list[a]["faces"])))
                    F0a = np.append(F0a, np.array([0.0] * len(mesh_list[a]["faces"])))
                    u = np.append(u, np.array([u[-1]] * len(mesh_list[a]["faces"])))
                    areasc = np.append(areasc, np.array([0.0] * len(mesh_list[a]["faces"])))
                    normsc = np.vstack((normsc, np.zeros((len(mesh_list[a]["faces"]), 4))))

    ffaces = faces
    fcp = cp
    fsurface_velocity = surface_velocity
    fsample_surface = sample_surface

    ###threshold - remove all shank region from field velocity region that has zero evaporation
    try:
        if shank == -1:
            #try shank approximation. If invalid due to numerical instability, continue with unapproximated iteration
            rm = np.min(cp[:, 2]) + 0.3 * (np.max(cp[:, 2]) - np.min(cp[:, 2]))
            fl = cp[:, 2] > rm
            nsurface_velocity = surface_velocity[fl]/np.min(surface_velocity[fl])
            if len(nsurface_velocity > 1e-4) > 0:
                min_t = np.min(cp[:, 2][fl][nsurface_velocity > 1e-4])
                shank = int(min_t) - 1
                #filter out panels below the z value fmin_t from extension velocity construction
                #By default this optimization is commented out as it might introduce errors under certain geometries
                if len(faces[cp[:, 2] > min_t]) < 4:
                    iteration_stability = 0
                fmin_t = np.min(cp[:, 2][fl][nsurface_velocity > 1e-5])
                ffilt = tuple([cp[:, 2] > fmin_t])
                fcp = cp[ffilt]
                fsurface_velocity = surface_velocity[ffilt]
                ffaces = faces[ffilt]
                dsample_surface = sample_surface[ffilt]
    except:
        shank = 4
        fcp = cp
        fsurface_velocity = surface_velocity
        ffaces = faces
        dsample_surface = sample_surface

    #set minimum possible value for shank to avoid
    if shank < 4:
        shank = 4

    #extension velocity calculation
    ###set full eval to False for approximated faster extension velocity calculation
    ###approximation valid for typical cases, will make biggest difference at higher resolutions

    full_eval = False
    if full_eval == True:
        velocity, closest_surface = extension_velocity.ext_vel(fsurface_velocity, ffaces,
                                                               fcp, fsample_surface,
                                                               field.shape[0],
                                                               field.shape[1],
                                                               field.shape[2],
                                                               -offx, -offy, -offz, shank)
    else:
        velocity, closest_surface = extension_velocity.ext_vel_adaptive(fsurface_velocity,
                                                                        field, ffaces,
                                                                        fcp, fsample_surface,
                                                                        field.shape[0],
                                                                        field.shape[1],
                                                                        field.shape[2],
                                                                        -offx, -offy, -offz, shank)

    #check if void (non sample surface object) is present
    vtrue = np.sum(closest_surface == 0)

    #use unfiltered velocity field around any voids present
    if  vtrue > 0:
        closest_surface = mesh_library.exp_void_region(closest_surface,
                                                       closest_surface.shape[0],
                                                       closest_surface.shape[1],
                                                       closest_surface.shape[2])
        velocity_nf = copy.deepcopy(velocity[closest_surface == 0])

    velocity = gaussian_filter(velocity, 1.0)

    if vtrue > 0:
        velocity[closest_surface == 0] = velocity_nf

    #select spatial integrator
    order = "2"
    if order == "1":
        dphi_t, dt = first_order_upwind_scheme(field, velocity, dphi_t, forward_x, forward_y,
                                               forward_z, backward_x, backward_y, backward_z,
                                               shank, cfl)

    if order == "2":
        dphi_t, dt = second_order_upwind_scheme(field, velocity, dphi_t, forward_x, forward_y,
                                                forward_z, backward_x, backward_y, backward_z,
                                                shank, cfl)

    ###this needs fixing
    ff = cp[:, 2] > 0.0

    smoothed_velocity = np.array([0.0] * len(cp))
    surf_ind_x = (cp[fsample_surface == 1 * ff, 0] - offx).astype(int)
    surf_ind_y = (cp[sample_surface == 1 * ff, 1] - offy).astype(int)
    surf_ind_z = (cp[sample_surface == 1 * ff, 2] - offz).astype(int)

    smoothed_velocity[fsample_surface == 1 * ff] = velocity[surf_ind_x,
                                                            surf_ind_y,
                                                            surf_ind_z]

    non_surf_velocity = np.array([0.0] * (len(qcamed) - len(smoothed_velocity)))
    smoothed_velocity = np.append(smoothed_velocity, non_surf_velocity)
    smoothed_velocity[smoothed_velocity > 0.0] = 0.0

    max_z = np.max(cp[:, 2])

    #transform mesh back for final output
    verts = verts * geom["simulation_grid"]["cell_width"]
    cp = cp * geom["simulation_grid"]["cell_width"]

    print("iteration " + str(it) + " completed")

    #invert to give temperature
    temperature = 1.0/dbeta

    #rescale volume to true size
    volume = volume * geom["simulation_grid"]["cell_width"]**3.0

    return (field + dt * dphi_t, dt, u, qc, cp, areasc,
            normsc, verts, faces, F0a, qcamed, sample_surface,
            smoothed_velocity, volume, 1.0/scv, max_z,
            temperature, iteration_stability)


def first_order_upwind_scheme(field, velocity, dphi_t, forward_x, forward_y,
                              forward_z, backward_x, backward_y, backward_z,
                              shank, cfl):

    """

    first order level set Upwind Scheme for normal oriented numerical integration

    from J. A. Sethian, Level Set Methods and Fast Marching Methods:
    Evolving Interfaces in Computational Geometry, Computer Vision and
    Materials Science, 2nd ed. Cambridge University Press, 1999.

    :param1 field (np.array, (L1,L2,L3), dtype=float): The level set field from the prior iteration
    :param2 velocity (np.array, (L1,L2,L3), dtype=float): The level set field velocity field
    :param3 dphi_t (np.array, (L1,L2,L3), dtype=float): The empty field perturbation update
    :param4 forward_x (np.array, (L1,L2,L3), dtype=float): The forward difference x operator
    :param5 forward_y (np.array, (L1,L2,L3), dtype=float): The forward difference y operator
    :param6 forward_z (np.array, (L1,L2,L3), dtype=float): The forward difference z operator
    :param7 backward_x (np.array, (L1,L2,L3), dtype=float): The backward difference x operator
    :param8 backward_y (np.array, (L1,L2,L3), dtype=float): The backward difference y operator
    :param9 backward_z (np.array, (L1,L2,L3), dtype=float): The backward difference z operator
    :param10 backward_x (np.array, (L1,L2,L3), dtype=float): The backward difference x operator
    :param11 backward_y (np.array, (L1,L2,L3), dtype=float): The backward difference y operator
    :param12 shank (int): The inactive shank field region
    :param12 cfl (double): The cfl fraction

    :returns:
    :dphi_t (np.array, (L1,L2,L3), dtype=float): The calculated field perturbation update
    :dt (double): The update timestep determined by CFL condition

    :raises:
        :ValueError: Zero velocity field detected for update - likely a bug

    """

    forward_x[:-1, :, :] = field[1:, :, :] - field[:-1, :, :]
    forward_y[:, :-1, :] = field[:, 1:, :] - field[:, :-1, :]
    forward_z[:, :, :-1] = field[:, :, 1:] - field[:, :, :-1]

    backward_x[1:, :, :] = field[1:, :, :] - field[:-1, :, :]
    backward_y[:, 1:, :] = field[:, 1:, :] - field[:, :-1, :]
    backward_z[:, :, 1:] = field[:, :, 1:] - field[:, :, :-1]

    #set neumannn boundary conditions
    forward_x[-1, :, :] = backward_x[-1, :, :]
    forward_y[:, -1, :] = backward_y[:, -1, :]
    forward_z[:, :, -1] = backward_y[:, :, -1]
    backward_x[0, :, :] = forward_x[0, :, :]
    backward_y[:, 0, :] = forward_y[:, 0, :]
    backward_z[:, :, 0] = forward_z[:, :, 0]

    bac_grad = ((np.maximum(forward_x, 0.0)**2.0  + np.minimum(backward_x, 0.0)**2.0) +
                (np.maximum(forward_y, 0.0)**2.0  + np.minimum(backward_y, 0.0)**2.0) +
                (np.maximum(forward_z, 0.0)**2.0  + np.minimum(backward_z, 0.0)**2.0))**0.5
    for_grad = ((np.maximum(backward_x, 0.0)**2.0 +  np.minimum(forward_x, 0.0)**2.0) +
                (np.maximum(backward_y, 0.0)**2.0 +  np.minimum(forward_y, 0.0)**2.0) +
                (np.maximum(backward_z, 0.0)**2.0 +  np.minimum(forward_z, 0.0)**2.0))**0.5

    mvel = np.max(abs(velocity))

    if mvel == 0.0:
        raise ValueError("Zero velocity field detected. Possibly caused by " +
                         "too great a specimen geometry rotation. Cause of this bug " +
                         "is currently unknown. If such rotation " +
                         "desired then please rotate input geometry prior.")

    dt = 1.0/np.max(abs(velocity)) * cfl

    dphi = (np.multiply(np.maximum(velocity[:, :, shank:], 0.0), bac_grad[:, :, shank:]) +
            np.multiply(np.minimum(velocity[:, :, shank:], 0.0), for_grad[:, :, shank:]))

    dphi_t[:, :, shank:] = dphi

    return dphi_t, dt


def second_order_upwind_scheme(field, velocity, dphi_t, forward_x, forward_y,
                               forward_z, backward_x, backward_y, backward_z,
                               shank, cfl):

    """

    second order level set Upwind Scheme for normal oriented numerical integration.
    Uses a switch that reverts to first order in presence of shocks.

    from J. A. Sethian, Level Set Methods and Fast Marching Methods:
    Evolving Interfaces in Computational Geometry, Computer Vision and
    Materials Science, 2nd ed. Cambridge University Press, 1999.

    :param1 field (np.array, (L1,L2,L3), dtype=float): The level set field from the prior iteration
    :param2 velocity (np.array, (L1,L2,L3), dtype=float): The level set field velocity field
    :param3 dphi_t (np.array, (L1,L2,L3), dtype=float): The empty field perturbation update
    :param4 forward_x (np.array, (L1,L2,L3), dtype=float): The forward difference x operator
    :param5 forward_y (np.array, (L1,L2,L3), dtype=float): The forward difference y operator
    :param6 forward_z (np.array, (L1,L2,L3), dtype=float): The forward difference z operator
    :param7 backward_x (np.array, (L1,L2,L3), dtype=float): The backward difference x operator
    :param8 backward_y (np.array, (L1,L2,L3), dtype=float): The backward difference y operator
    :param9 backward_z (np.array, (L1,L2,L3), dtype=float): The backward difference z operator
    :param10 backward_x (np.array, (L1,L2,L3), dtype=float): The backward difference x operator
    :param11 backward_y (np.array, (L1,L2,L3), dtype=float): The backward difference y operator
    :param12 shank (int): The inactive shank field region
    :param12 cfl (double): The cfl fraction

    :returns:
    :dphi_t (np.array, (L1,L2,L3), dtype=float): The calculated field perturbation update
    :dt (double): The update timestep determined by CFL condition

    :raises:
        :ValueError: Zero velocity field detected for update - likely a bug

    """

    forward_x_forward_x = np.zeros(field.shape)
    forward_x_backward_x = np.zeros(field.shape)
    backward_x_forward_x = np.zeros(field.shape)
    backward_x_backward_x = np.zeros(field.shape)

    forward_y_forward_y = np.zeros(field.shape)
    forward_y_backward_y = np.zeros(field.shape)
    backward_y_forward_y = np.zeros(field.shape)
    backward_y_backward_y = np.zeros(field.shape)

    forward_z_forward_z = np.zeros(field.shape)
    forward_z_backward_z = np.zeros(field.shape)
    backward_z_forward_z = np.zeros(field.shape)
    backward_z_backward_z = np.zeros(field.shape)

    forward_x[:-1, :, :] = field[1:, :, :] - field[:-1, :, :]
    forward_y[:, :-1, :] = field[:, 1:, :] - field[:, :-1, :]
    forward_z[:, :, :-1] = field[:, :, 1:] - field[:, :, :-1]

    backward_x[1:, :, :] = field[1:, :, :] - field[:-1, :, :]
    backward_y[:, 1:, :] = field[:, 1:, :] - field[:, :-1, :]
    backward_z[:, :, 1:] = field[:, :, 1:] - field[:, :, :-1]

    #set neumannn boundary conditions
    forward_x[-1, :, :] = backward_x[-1, :, :]
    forward_y[:, -1, :] = backward_y[:, -1, :]
    forward_z[:, :, -1] = backward_y[:, :, -1]
    backward_x[0, :, :] = forward_x[0, :, :]
    backward_y[:, 0, :] = forward_y[:, 0, :]
    backward_z[:, :, 0] = forward_z[:, :, 0]

    forward_x_forward_x[:-1, :, :] = forward_x[1:, :, :] - forward_x[:-1, :, :]
    forward_x_backward_x[:-1, :, :] = backward_x[1:, :, :] - backward_x[:-1, :, :]
    backward_x_forward_x[1:, :, :] = forward_x[1:, :, :] - forward_x[:-1, :, :]
    backward_x_backward_x[1:, :, :] = backward_x[1:, :, :] - backward_x[:-1, :, :]

    #set neumannn boundary conditions
    forward_x_forward_x[-1, :, :] = backward_x_forward_x[-1, :, :]
    forward_x_backward_x[-1, :, :] = backward_x_forward_x[-1, :, :]
    backward_x_forward_x[0, :, :] = forward_x_forward_x[0, :, :]
    backward_x_backward_x[0, :, :] = forward_x_backward_x[0, :, :]

    forward_y_forward_y[:, :-1, :] = forward_y[:, 1:, :] - forward_y[:, :-1, :]
    forward_y_backward_y[:, :-1, :] = backward_y[:, 1:, :] - backward_y[:, :-1, :]
    backward_y_forward_y[:, 1:, :] = forward_y[:, 1:, :] - forward_y[:, :-1, :]
    backward_y_backward_y[:, 1:, :] = backward_y[:, 1:, :] - backward_y[:, :-1, :]

    #set neumannn boundary conditions
    forward_y_forward_y[:, -1, :] = backward_y_forward_y[:, -1, :]
    forward_y_backward_y[:, -1, :] = backward_y_forward_y[:, -1, :]
    backward_y_forward_y[:, 0, :] = forward_y_forward_y[:, 0, :]
    backward_y_backward_y[:, 0, :] = forward_y_backward_y[:, 0, :]

    forward_z_forward_z[:, :, :-1] = forward_z[:, :, 1:] - forward_z[:, :, :-1]
    forward_z_backward_z[:, :, :-1] = backward_z[:, :, 1:] - backward_z[:, :, :-1]
    backward_z_forward_z[:, :, 1:] = forward_z[:, :, 1:] - forward_z[:, :, :-1]
    backward_z_backward_z[:, :, 1:] = backward_z[:, :, 1:] - backward_z[:, :, :-1]

    #set neumannn boundary conditions
    forward_z_forward_z[:, :, -1] = backward_z_forward_z[:, :, -1]
    forward_z_backward_z[:, :, -1] = backward_z_forward_z[:, :, -1]
    backward_z_forward_z[:, :, 0] = forward_z_forward_z[:, :, 0]
    backward_z_backward_z[:, :, 0] = forward_z_backward_z[:, :, 0]

    A = backward_x + 0.5 * eno_switch(backward_x_backward_x, forward_x_backward_x)
    B = forward_x - 0.5 * eno_switch(forward_x_forward_x, backward_x_forward_x)
    C = backward_y + 0.5 * eno_switch(backward_y_backward_y, forward_y_backward_y)
    D = forward_y - 0.5 * eno_switch(forward_y_forward_y, backward_y_forward_y)
    E = backward_z + 0.5 * eno_switch(backward_z_backward_z, forward_z_backward_z)
    F = forward_z - 0.5 * eno_switch(forward_z_forward_z, backward_z_forward_z)

    for_grad = ((np.maximum(A, 0.0)**2.0 +  np.minimum(B, 0.0)**2.0) +
                (np.maximum(C, 0.0)**2.0 +  np.minimum(D, 0.0)**2.0) +
                (np.maximum(E, 0.0)**2.0 +  np.minimum(F, 0.0)**2.0))**0.5
    bac_grad = ((np.maximum(B, 0.0)**2.0  + np.minimum(A, 0.0)**2.0) +
                (np.maximum(D, 0.0)**2.0  + np.minimum(C, 0.0)**2.0) +
                (np.maximum(F, 0.0)**2.0  + np.minimum(E, 0.0)**2.0))**0.5

    mvel = np.max(abs(velocity))

    if mvel == 0.0:
        raise ValueError("Zero velocity field detected. Possibly caused by " +
                         "too great a specimen geometry rotation. Cause of this bug " +
                         "is currently unknown. If such rotation " +
                         "desired then please rotate input geometry prior.")

    dt = 1.0/np.max(abs(velocity)) * cfl

    dphi = (np.multiply(np.maximum(velocity[:, :, shank:], 0.0), bac_grad[:, :, shank:]) +
            np.multiply(np.minimum(velocity[:, :, shank:], 0.0), for_grad[:, :, shank:]))

    dphi_t[:, :, shank:] = dphi

    return dphi_t, dt

def eno_switch(x, y):

    """

    Function for second order Godonov scheme acting as a switch for handling
    shocks

    """

    xa = abs(x)
    ya = abs(y)
    return 1.0 * (x * y >= 0) * ((xa <= ya) * x +  (xa > ya) * y)

###anisotropy function modifying evaporation field
def anisotropy(norm, F0, mx, my, mz, mag):

    """

    Function for modifying F0 to handle anisotropic material

    :param1 norm (np.array, dtype = float): Panel normals
    :param2 F0 (np.array, (N), dtype = float): Isotropic field evaporation value for panels
    :param3 mx (np.array, (F), dtype = float): anisotropic field evaporation x normal component
    :param4 my (np.array, (F), dtype = float): anisotropic field evaporation y normal component
    :param5 mz (np.array, (F), dtype = float): anisotropic field evaporation z normal component
    :param6 mag (np.array, (F), dtype = float): anisotropic field evaporation component magnitude

    :returns:
        :F0m (np.array, (N), dtype = float): anisotropic field evaporation values for panels

    :raises:
        None

    """
    #must identify the closest fundamental lattice plane
    norm = norm
    nv = norm[0] * mx + norm[1] * my + norm[2] * mz
    ang = abs(np.arccos(nv/(np.linalg.norm(norm) * (mx * mx + my * my + mz * mz)**0.5)))
    F0m = abs(np.tan(np.min(ang)) * F0 + mag[np.argmin(ang)])

    return F0m
