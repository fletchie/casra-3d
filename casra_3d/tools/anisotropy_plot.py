
"""

Module for outputting the anisotropy surface for a particular material within the materials database

Author: Charles Fletcher
Atom Probe Tomography Group, Department of Materials, University of Oxford
May 2020

"""

import sys
import sqlite3

from casra_3d.io import model_writers as model_writers

try:
    import numpy as np
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python module numpy. " +
                              "Please make sure the module is installed.")

try:
    import scipy.spatial
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python module scipy. " +
                              "Please make sure the module is installed.")

def plot_anisotropy(materials_db, mat_id, out):

    """

    Method for outputting a .vtk of the anisotropy surface for a given
    material id (within the materials database).

    :param1 materials_db (str): The materials database
    :param2 mat_id (int): The specific materials id
    :param3 out (str): The output filename (.vtk format)

    :returns:
        None

    :raises:
        FileNotFoundError: Could not connect to passed SQLite materials database

    """

    amorphous = False

    #connect to materials database
    try:
        conn = sqlite3.connect(materials_db)
        c = conn.cursor()
    except FileNotFoundError:
        raise FileNotFoundError("Cannot find or connect to materials db. " +
                                "Please esure that the database can be found at " +
                                "the input directory (relative): " + materials_db +
                                " and is not corrupted.")

    field = c.execute('SELECT evaporation_field FROM main where mat_id = '  +
                      str(mat_id)).fetchall()
    a = c.execute('SELECT mx,my,mz,mag FROM crystallographic_structure where mat_id = ' +
                  str(mat_id)).fetchall()
    if not a:
        print("No crystallographic information found for material with mat_id: " +
              str(mat_id) + ". Assuming amorphous.")
        amorphous = True
    F0 = np.array(field)[0][0]
    
    if amorphous is False:
        crystallographic_parameters = np.array(a)

        mx = crystallographic_parameters[:, 0]
        my = crystallographic_parameters[:, 1]
        mz = crystallographic_parameters[:, 2]
        mag = crystallographic_parameters[:, 3]
    else:
        mx = np.array([0.0])
        my = np.array([0.0])
        mz = np.array([1.0])
        mag = np.array([0.0])

    points = np.zeros((0, 3))

    phi = np.linspace(0.0, 2.0 * np.pi, 200)
    theta = np.linspace(0.0, np.pi, 200)
    PHI, THETA = np.meshgrid(phi, theta)
    PHI = PHI.flatten()
    THETA = THETA.flatten()

    coords = np.vstack((PHI, THETA)).T

    tri = scipy.spatial.Delaunay(coords)
    simplices = tri.simplices
    F_val = []

    progress = 0
    #construct anisotropy surface via point sampling
    #in spherical polar coordinates
    for a in range(0, len(PHI)):
        nx = np.cos(PHI[a]) * np.sin(THETA[a])
        ny = np.sin(PHI[a]) * np.sin(THETA[a])
        nz = np.cos(THETA[a])
        #update progress bar
        if a % int(len(PHI)/10) == 0:
            progress += 1
            progressbar(progress, prefix="", size=10, file=sys.stdout)

        nv = abs(nx) * mx + abs(ny) * my + abs(nz) * mz
        ang = np.arccos(nv/(np.linalg.norm(nx * nx + ny * ny + nz * nz) *
                            (mx * mx + my * my + mz * mz)**0.5))

        if amorphous is False:
            Fp = abs(np.tan(np.min(ang)) * F0 + mag[np.argmin(ang)])
        else:
            Fp = F0

        cp = np.array([nx * Fp, ny * Fp, nz * Fp])
        F_val.append(Fp)
        points = np.vstack((points, cp))

    print("outputting vtk to: " + out)
    cells = np.array(simplices)
    model_writers.write_vtk(out, points, cells,
                            point_data={"magnitude": np.array(F_val)})

#progress bar for constructing anisotropy surface
def progressbar(it, prefix="", size=10, file=sys.stdout):

    """

    Function for updating progress bar

    :param1 it (int): The progress counter
    :param2 size (int): The progress bar size
    :param3 file (str): The system output file

    :returns:
        None

    :raises:
        None

    """

    def show(j):
        """

        Update progressbar

        """

        x = int(j)
        file.write("%s[%s%s] %i/%i\r" % (prefix, "#"*x, "."*(size-x), j, size) + "\r")
        file.flush()

    show(it)
    file.flush()

#run if script executed via the command line
if __name__ == "__main__":

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--materials_db', metavar='The materials database',
                        type=str, default="c3d_input/materials",
                        help='The materials database to connect to')
    parser.add_argument('--mat_id', metavar='The materials id',
                        type=int, default=0, help='The materials id for which ' +
                        'to plot the anisotropy surface.')
    parser.add_argument('--out', metavar='The output .vtk filename', type=str,
                        default="c3d_output/anisotropy_func.vtk",
                        help='The output directory for the .vtk anisotropy surface')

    args = parser.parse_args()
    materials_db_cl = args.materials_db
    mat_id_cl = args.mat_id
    out_cl = args.out

    plot_anisotropy(materials_db_cl, mat_id_cl, out_cl)
