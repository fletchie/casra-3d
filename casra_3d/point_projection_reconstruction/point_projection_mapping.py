
"""

Module containing core methods for performing the pseudo-stereographic point-projection reconstruction
outlined within the paper Microsc. Microanal. 23, 238–246, 2017 doi:10.1017/S1431927616012721

Author: Charles Fletcher
Atom Probe Tomography Group, Department of Materials, University of Oxford
May 2020

"""
import math
import copy as cp
import re
import sys

try:
    import numpy as np
except ModuleNotFoundError:
    raise ModuleNotFoundError("The Python package numpy cannot be found. Please make sure it has been installed (e.g. via apt or pip)")

#calculate cap radius for shank angle alpha
def cap_radius(z, alpha, R0):

    """

    Function for calculating the apex radius in constant shank mode

    This is calculated from the initial radius and depth of evaporation z

    :param1 z: The evaporation depth i.e. how much the cap has been shifted downwards
    :param2 alpha: The shank angle
    :param3 R0: The initial apex radius
    :returns: The current apex radius

    """

    return R0 - z * math.sin(alpha)/(1.0 - math.sin(alpha))

#calculate pseudo-stereographic projection from detector onto tip cap
def project(dx, dy, L, R, I):

    """

    Function for back-projecting from the detector back onto the apex (De Geuser protocol)

    :param1 dx: The detector x coordinate
    :param2 dy: The detector y coordinate
    :param3 L: The instrument flightpath
    :param4 R: The apex radius
    :param5 I: A parameter calculated from the image compression factor I = (ICF - 1.0)

    :returns: Spherical polar coordinates of the projected ion on the tip apex
        R - The apex radius
        psi - The angle from the projected ion apex position to the analysis axis (z)
        phi - The angle around the analysis (z) axis of the projected ion apex position
    """

    theta = np.arctan2(np.sqrt(dx**2.0 + dy**2.0), L)
    psi = theta + np.arcsin(I * np.sin(theta)) # accounts for the general stereographic projection

    phi = np.arctan2(dx, dy)

    return R, psi, phi

#convert spherical ion coordinates into detector space
def convert(R, psi, phi, shift):

    """

    Function converts spherical polar coordinates into Cartesian coordinates
    Calculates the ion position in detector space

    :param1 R: The current apex radius
    :param2 psi: The angle from the projected ion apex position to the analysis axis (z)
    :param3 phi: The angle around the analysis (z) axis of the projected ion apex position

    :returns:
        x - The ion x coordinate
        y - The ion y coordinate
        z - The ion z coordinate

    """

    x = R * np.cos(phi) * np.sin(psi)
    y = R * np.sin(phi) * np.sin(psi)
    z = R * np.cos(psi) + shift

    return x, y, z

#shift down cap by lattice value for detected ion
def cap_shift(z, dv, e, R, theta_max, vol):

    """

    Function for calculating the z shift in the apex according to De Geuser/Gault protocol

    :param1 z: The current apex z position
    :param2 dv: The ionic volume
    :param3 e: The detector efficiency
    :param4 R: The current apex radius
    :param5 recon_FOV: The experiment Field Of View

    :returns: The new shifted apex z position

    """

    area = math.pi*(R * 1e9 * math.sin(theta_max))**2.0
    shift =  dv/(e * area)
    shift = shift * 1e-9
    vol += dv * 1e-27

    return z - shift, vol

#shift down cap by lattice value for detected ion
def bas_shift(z, dv, eps, e, R, det_area, flightpath):

    """

    Function for calculating the z shift in the apex according to the original Bas protocol

    :param1 z: The current apex z position
    :param2 dv: The ionic volume
    :param3 eps: The image compression factor
    :param4 e: The detector efficiency
    :param5 R: The current apex radius
    :param6 det_area: The detector area
    :param7 flightpath: The instrument flightpath
    :param8 kf: The specimen specific k-factor

    :returns: The new shifted apex z position

    """

    dz = (dv * flightpath**2.0)/(e * det_area * eps**2.0 * R**2.0)
    return z + dz

#given range file and mass to charge, classify ion
def range(da, range_dict):

    """

    Function for classifying ions based off their Da (mass-to-charge) given a range file

    :param1 da: The ionic mass-to-charge ratio
    :param2 range_dict: Python dictionary containing ranges

    :returns:
        chem - 1*len(da) array of ion numbers
        ions - list of corresponding string names for ion numbers in chem (by index)
        ic - The number of atoms for the particular ion (e.g. number of atoms in compound ion)

    """

    chem = np.zeros((len(da)), dtype = int)

    ions = []

    for num in range_dict:
        ions.append(range_dict[num]["comp"])

    ions = list(set(ions))

    da = np.array(da)

    for num in range_dict:
        lower = range_dict[num]["lower"]
        upper = range_dict[num]["upper"]

        #format N:2 -> N2 ion, separate id from N1
        ion = range_dict[num]["comp"]

        ###non-ranged data left as zero
        chem[(lower < da) * (upper > da)] = ions.index(ion) + 1

    #calculate the number of species in a detection event (e.g. number of atoms within a compound ion)
    ic = []
    for a in ions:
        ic.append(0)
        for v in re.split(":| ", a):
            if v.isdigit():
                ic[-1] += int(v[-1])

    #unidentified ions in chem labelled as 0
    return chem, ions, ic

#method to complete reconstruction
def reconstruction(dx, dy, chem, da, dv, R0, domain, sampling, flight_path, det_rad, alpha, eps, detector_efficiency, mode = "shank"):

    """

    Function for performing the point projection reconstruction on APT data.

    Currently only constant shank mode is supported. Voltage reconstructions are planned to be added at a later date.
    Unlike in IVAS ionic volumes are currently assumed to be constant for all elements (dv).

    :param1 dx: Array of ion detector x coordinates
    :param2 dy: Array of ion detector y coordinates
    :param3 chem: Array of ion numbers (corresponding to keys within ions)
    :param4 dv: The ionic volume
    :param5 R0: The initial apex radius
    :param6 domain: tuple of two elements giving the lower and upper ion counts for the reconstruction (i.e. clipping)
    :param7 sampling: The artificial sampling rate (ions to reconstruct)
    :param8 flight_path: The instrument flight path
    :param9 det_rad: The detector radius
    :param10 alpha: The specimen shank angle
    :param11 eps: The experiment Image Compression Factor (ICF)
    :param12 detector_efficiency: The detector efficiency - required to calculate physical shift for apex
    :param13 mode: The method governing the radius evolution (!!!currently only supports shank mode!!!)

    :returns:
        res - 1D list consisting of the cycling values:
            x - the reconstructed ion x coordinate
            y - the reconstructed ion y coordinate
            z - the reconstructed ion z coordinate
            chem - The ion number (corresponding to an index giving the compound name in the `ions` list returned by the range method)

    """

    if mode != "shank":
        raise ValueError("currently only shank mode reconstructions are supported. Set mode = 'shank' and make sure a realistic shank angle has been inserted.")

    res = []
    R = cp.deepcopy(R0)

    cz = 0.0
    percent = 0.1

    I = (eps - 1.0)

    dv = dv * (1e9)**3.0

    #rho - the distance between the projection centre within the detector plane, and the detected hit position
    #calculate FOV (section) - assuming linear projection
    #where has this equation even come from??
    theta_max = math.atan(det_rad/flight_path)

    #invert stereographic projection from edge of detector - this projection
    #should serve to increase the actual FOV of the sample
    theta_max = theta_max + math.asin(I*math.sin(theta_max))

    #if uppper bound for ion number exceeds dataset size, set upper range to maximum within the APT data
    if len(dx) < domain[1]:
        domain[1] = len(dx)

    if domain[0] < 0:
        domain[0] = 0

    #output the experiment FOV
    print("instrument field of view: " + str(theta_max * 180.0/3.14))

    tv = int((domain[1] - domain[0])/int(1/sampling))
    progress = 0
    vol = 0.0

    for i in np.arange(domain[0], domain[1], int(1/sampling)):
        #update progress bar
        if (i + 1 - domain[0]) % int(tv/10) == 0:
            progress += 1
            progressbar(progress, prefix="", size=10, file=sys.stdout)

        ##back project ion onto tip apex
        R, theta, phi = project(dx[i], dy[i], flight_path, R, I)

        ##perform z shift for hemispherical cap representing the sample apex
        ##uncomment for bas shift
        #cz = bas_shift(cz, dv/sampling, eps, detector_efficiency, R, det_area, flight_path)
        cz, vol = cap_shift(cz, dv/sampling, detector_efficiency, R, theta_max, vol)

        ##convert ion coordinates to Cartesian
        x, y, z = convert(R, theta, phi, cz)

        #update hemispherical cap radius
        if mode == "shank": # constant shank angle mode
            Rp = R
            R = cap_radius(cz, alpha, R0)
            #additional z shift accounting for radius enlargement - this shift is currently not volume conserving!
            #cz = cz + (Rp - R) * math.cos(theta_max)
            cz = cz + (Rp - R)
        #append ion coordinates
        res.extend((x, y, z, chem[i], da[i]))

    print("reconstruction complete!")
    print("model reported volume: " + str(vol))
    print("model reported volume 2: " + str(5e-28 * 53712))

    return res

#progress bar for reconstruction
def progressbar(it, prefix="", size=10, file=sys.stdout):
    """

    Function for implementing command line progress bar

    """
    def show(j):
        x = int(j)
        file.write("%s[%s%s] %i/%i\r" % (prefix, "#"*x, "."*(size-x), j, size) + "\r")
        file.flush()
    show(it)
    file.flush()
