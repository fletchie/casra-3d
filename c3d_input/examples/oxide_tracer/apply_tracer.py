#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 18 15:16:20 2021

@author: charlie
"""
import numpy as np

from casra_3d.io import model_loaders as model_loaders
from casra_3d.io import model_writers as model_writers

z0 = 4.59e-7
z1 = 4.53e-7

ironoxide = 22
iron = 23

recon = model_loaders.read_vtk_light("c3d_output/examples/oxide_tracer/reconstruction/recon.vtk")
opos = model_loaders.read_vtk_light("c3d_output/examples/oxide_tracer/reconstruction/original_pos.vtk")

ionp = np.array(opos.points)

tl = (ionp[:, 2] > z1) * (ionp[:, 2] < z0)

recon.point_data["tracer"] = tl * 1.0
recon.point_data["chemistry"] = np.array(recon.point_data["chemistry"])

rind = np.linspace(0, len(tl), len(tl), endpoint = False, dtype = int)
rind = rind[tl]
recon.point_data["chemistry"][tl] = -1

recon.points = np.array(recon.points)
opos.points = np.array(opos.points)

model_writers.write_vtk("c3d_output/examples/oxide_tracer/reconstruction/trrecon2.vtk", recon.points, recon.cells, recon.cell_data, recon.point_data)

vals = np.vstack((recon.points[:, 0]*1e9, recon.points[:, 1]*1e9, recon.points[:, 2]*1e9, recon.point_data["chemistry"])).T.flatten()

model_writers.export_pos("c3d_output/examples/oxide_tracer/reconstruction/trrecon2.pos", vals)

vals = np.vstack((opos.points[:, 0]*1e9, opos.points[:, 1]*1e9, opos.points[:, 2]*1e9, recon.point_data["chemistry"])).T.flatten()

model_writers.export_pos("c3d_output/examples/oxide_tracer/reconstruction/troriginal2.pos", vals)