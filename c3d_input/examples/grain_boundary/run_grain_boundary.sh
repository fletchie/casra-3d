#!/bin/bash

start=$(date +'%s')
echo "Run grain boundary model pipeline"

if [ ! -d casra_3d ] ; then
	echo " please run script from top level folder of casra repository: could not find casra_3d in current dir."
	exit 1
fi

#generate model geometry
python3 c3d_input/examples/grain_boundary/generate_grain_boundary.py 
#run grain boundary evaporation simulation
python3 -m casra_3d.tip_simulation --mat_db c3d_input/materials --tip_geom c3d_input/examples/grain_boundary/grain_boundary.mstate --mref c3d_input/examples/grain_boundary/grain_boundary.mref --parameters c3d_input/examples/grain_boundary/parameters --it 120 --out c3d_output/examples/grain_boundary
#perform ion projection - generate synthetic apt dataset
python3 -m casra_3d.perform_projection --tip_geom c3d_input/examples/grain_boundary/grain_boundary.mstate --dirname c3d_output/examples/grain_boundary --mref c3d_input/examples/grain_boundary/grain_boundary.mref --it 120 --parameters c3d_input/examples/grain_boundary/parameters --parallelisation True --real_projection True --weighted_projection True --out c3d_output/examples/grain_boundary
#perform point projection reconstruction from generated synthetic data
python3 -m casra_3d.point_projection_reconstruction.method --input_data c3d_output/examples/grain_boundary/projection_data/projection_data.tsv --output c3d_output/examples/grain_boundary/reconstruction --parameters c3d_input/examples/grain_boundary/pprecon_parameters.tsv 
finish=$(date +'%s')
duration=$((finish-start))
hrs=$(( duration/3600 )); mins=$(( (duration-hrs*3600)/60)); secs=$(( duration-hrs*3600-mins*60 ))
printf 'Time spent: %02d:%02d:%02d\n' $hrs $mins $secs

