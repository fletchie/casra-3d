/*

Header file for CGAL wrapper

Author: Charles Fletcher
Atom Probe Tomography Group, Department of Materials, University of Oxford
May 2020

*/

#include <vector>

struct mesh_container{
  //define the mesh container (for returning surface meshes to Python)
  std::vector<std::vector<double>> npoints;
  std::vector<std::vector<long>> nfaces;
  std::vector<long> findices;
  std::vector<long> vindices;
};

struct tetra_container{
  //define the tetra mesh container (for returning tetrahedral volume meshes to Python)
  std::vector<long> findices;
  std::vector<std::vector<double>> npoints;
};

int test_return(int d);
tetra_container alpha_shape(double* points, unsigned int pts, unsigned int dim, double alpha);
mesh_container iso_remesh(double* points, long* faces, int el, int fsize, int pts, int dim, double edge_coll);
mesh_container sample_remeshing(double* points, long* faces, int el, int fsize, unsigned int pts, unsigned int dim, std::vector<double> z_regions, std::vector<double> edge_coll);
