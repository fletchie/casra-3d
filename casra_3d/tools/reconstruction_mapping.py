
"""

Module for generating and outputting to .vtk the detector isolines (lines of constant detector position)

Author: Charles Fletcher
Atom Probe Tomography Group, Department of Materials, University of Oxford
May 2020
"""

import os
import csv
import sys
import copy

try:
    import numpy as np
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python package numpy. Please make sure package is installed.")

try:
    import scipy as sp
    import scipy.interpolate as spinterpolate
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python package Scipy. Please make sure package is installed.")

from casra_3d.io import model_loaders as model_loaders
from casra_3d.io import model_writers as model_writers
from casra_3d.core.ion_projection import transfer_function

def plot_detector_flowlines(mapping, xx, xy, FOV = 200):

    """
    Generates detector lines via interpolation. Called from generate_reconstruction_mapping method.

    :param1 mapping: The projection data dictionary
    :param2 xx: The 2D array generated via np.meshgrid for isoline x-coords
    :param3 xy: The 2D array generated via np.meshgrid for isoline y-coords
    :param4 FOV: The detector radius

    :returns:
        :flowline_list: A list of the detector isoline arrays

    """

    un_mapping = np.unique(mapping["num"])

    #check if enough ion projections are present to generate mapping
    if len(un_mapping) < 2:
        raise ValueError("Too few ion projections detected to generate reconstruction " +
                         " mapping (more than two ion projections required). \n " +
                         "Reduce 'mapping_frac' parameter or increase the number of " +
                         " model iterations.")

    flowline_list = [[] for _ in range(len(un_mapping))]

    sample_space = []
    det_space = []
    progress = 0
    for m_ind in range(0, len(un_mapping)):

        #update progress bar
        if (m_ind + 1) % np.max([int(len(np.unique(mapping["num"]))/10),1]) == 0:
            progress += 1
            progressbar(progress, prefix="", size=10, file=sys.stdout)

        sample_space = mapping["sample_space"][mapping["num"] == np.unique(mapping["num"])[m_ind]]
        det_space = mapping["detector_space"][mapping["num"] == np.unique(mapping["num"])[m_ind]]

        sample_space = sample_space[(abs(det_space[:, 0]) < 1e10)*(abs(det_space[:, 1]) < 1e10)]
        det_space = det_space[(abs(det_space[:, 0]) < 1e10) * (abs(det_space[:, 1]) < 1e10)]

        spx = spinterpolate.griddata(det_space, sample_space[:, 0], (xx, xy), method='linear')
        spy = spinterpolate.griddata(det_space, sample_space[:, 1], (xx, xy), method='linear')
        spz = spinterpolate.griddata(det_space, sample_space[:, 2], (xx, xy), method='linear')

        for i in range(0, len(spx)):
            for j in range(0, len(spx)):
                if (xx[i, j]**2.0 + xy[i, j]**2.0) < FOV**2.0:
                    pos = np.array([spx[i, j], spy[i, j], spz[i, j]])
                    flowline_list[m_ind].append(pos)

    flowline_list = np.transpose(np.array(flowline_list), (1, 0, 2))

    return flowline_list

def generate_reconstruction_mapping(mapping_data, det_radius, dirname, lines = 10):

    """

    Function for generating the reconstruction mapping

    :param1 projection: The projection data dictionary
    :param2 det_radius: The detector radius
    :param3 dirname: The output directory for the isolines.vtk
    :param4 lines: The number of isolines spanning the detector diameter (default is 10)

    :returns: 0

    """

    #projection = {"num": [], "sample_space": [], "detector_space": [],
    #              "vel": [], "chemistry": [], "ion_phase": [],
    #              "trajectories": [], "panel_index": []}
    #model_loaders.load_tsv_ion_trajectories(projection, mapping_data)

    if not os.path.exists(dirname):
        os.makedirs(dirname)

    projection["detector_space"] = projection["detector_space"][:, :2]

    sample = lines
    FOV = det_radius
    sample_points = np.linspace(-FOV, FOV, sample)
    xx, xy = np.meshgrid(sample_points, sample_points)
    isolines = plot_detector_flowlines(projection, xx, xy, FOV = FOV)
    #print(isolines)
    print(isolines.shape)

    model_writers.output_mapping_to_vtk(isolines, dirname)
    print("outputed vtk isolines to directory: " + dirname)

def progressbar(it, prefix="", size=10, file=sys.stdout):
    """
    progress bar function
    """
    def show(j):
        x = int(j)
        file.write("%s[%s%s] %i/%i\r" % (prefix, "#"*x, "."*(size-x), j, size) + "\r")
        file.flush()
    show(it)
    file.flush()

#run if script executed via the command line
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--mapping_data', metavar='filename', type = str, default = "",
                        help='.tsv containing mapping data')
    parser.add_argument('--det_radius', metavar='detector radius', type = float, default = 0.038,
                        help='The detector radius')
    parser.add_argument('--detector_lines', metavar='detector lines', type = int, default = 10,
                        help='The number of isolines across the detector diameter')
    parser.add_argument('--dirname', metavar='directory name', type = str, default = "",
                        help='The directory to save isoline .vtk')
    parser.add_argument('--image_transfer', metavar='detector transfer function', type = str,
                        default = "0.102, 0.0, 0.0, 0.0, 0.8",
                        help='List of image transfer parameters. This variable should have the ' +
                             'following structure (list) [flight path, dx-transform, dy-transform, theta, ICF]')
    args = parser.parse_args()

    #simulation parameter definition
    mapping_data = args.mapping_data
    det_radius = args.det_radius
    dirname = args.dirname
    lines = args.detector_lines
    image_transfer = args.image_transfer.split(",")
    image_transfer = [float(a) for a in image_transfer]

    if len(mapping_data) == 0:
        raise ValueError("mapping data .tsv file must be parsed to --mapping_data argument")
    if len(dirname) == 0:
        raise ValueError("simulation directory must be parsed to --dirname argument")

    projection = {"num": [], "sample_space": [], "detector_space": [],
                  "vel": [], "chemistry": [], "ion_phase": [],
                  "trajectories": [], "panel_index": []}
    model_loaders.load_tsv_ion_trajectories(projection, mapping_data)

    flight_path = image_transfer[0]
    ts = np.array([image_transfer[1], image_transfer[2]])
    theta = image_transfer[3]
    kappa = image_transfer[4]
    
    projection["detector_space"] = transfer_function(projection["detector_space"],
                                                     projection["vel"],
                                                     flight_path, kappa)

    coords = projection["detector_space"]
    #rotate trajectory mapping - inverse rigid body transformation
    fl = copy.deepcopy(coords[:, 0])
    coords[:, 0] = coords[:, 1]
    coords[:, 1] = fl

    R = np.array([[np.cos(theta), -np.sin(theta)], [np.sin(theta), np.cos(theta)]]).T
    coords[:, :2] = np.matmul(R, (coords[:, :2]).T).T
    inv_ts = np.matmul(R, ts)

    coords[:, :2] = coords[:, :2] - inv_ts

    fl = copy.deepcopy(coords[:, 0])
    coords[:, 0] = coords[:, 1]
    coords[:, 1] = fl

    projection["detector_space"][:, :2] = coords[:, :2]
    ###########
    projection["detector_space"] = projection["detector_space"][:, :2]

    #create ion projection dictionary
    generate_reconstruction_mapping(projection, det_radius, dirname, lines)
